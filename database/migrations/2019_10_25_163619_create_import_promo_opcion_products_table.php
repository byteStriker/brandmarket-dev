<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportPromoOpcionProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $this->down();
        Schema::create('import_promo_opcion_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('page');
            $table->string('code');
            $table->text('description');
            $table->string('price');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_promo_opcion_products');
    }
}
