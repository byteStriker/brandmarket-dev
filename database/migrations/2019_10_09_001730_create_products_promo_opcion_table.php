<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsPromoOpcionTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    $this->down();
    Schema::create('products_promo_opcion', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->string('item')->nullable();
      $table->string('father')->nullable();
      $table->string('family')->nullable();
      $table->string('name')->nullable();
      $table->text('description')->nullable();
      $table->string('color')->nullable();
      $table->string('colors')->nullable();
      $table->string('size')->nullable();
      $table->string('material')->nullable();
      $table->string('capacity')->nullable();
      $table->string('batteries')->nullable();
      $table->string('printing')->nullable();
      $table->string('area_p')->nullable();
      $table->string('nw')->nullable();
      $table->string('gw')->nullable();
      $table->string('height')->nullable();
      $table->string('width')->nullable();
      $table->string('length')->nullable();
      $table->string('quantity_packaged')->nullable();
      $table->string('img')->nullable();
      $table->string('price')->nullable();
      $table->string('existence')->nullable();
      // $table->string('cantidadalerta')->nullable();
      // $table->string('mensajealerta')->nullable();
      // $table->string('destacado')->nullable();
      // $table->string('stock')->nullable();

      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('products_promo_opcion');
  }
}
