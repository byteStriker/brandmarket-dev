<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('slide_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('slide_id')->nullable();
            $table->string('name')->default('fotografia');
            $table->string('link_text')->nullable();
            $table->string('text_color')->nullable()->default('is-primary');
            $table->string('link_url')->nullable();
            $table->string('link_type')->nullable();
            $table->string('cover_image');
            $table->enum('cover_type', ['mobile', 'desktop'])->default('desktop');
            $table->date('published_at')->useCurrent = true;
            $table->enum('status', ['Published', 'Future', 'Draft'])->default('Published');
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slide_details');
    }
}
