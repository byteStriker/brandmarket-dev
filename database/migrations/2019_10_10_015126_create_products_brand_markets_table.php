<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsBrandMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('products_brand_market', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->bigIncrements('parent_id')->nullable()->default(NULL);
            $table->unsignedInteger('category_id')->nullable()->comment('Field related to: category.id ');
            $table->unsignedInteger('subcategory_id')->nullable()->comment('Field related to: subcategory.id');
            $table->string('provider_name')->nullable()->default(null)->comment('Field used for store the identifier of it\'s provider ');
            $table->string('model')->nullable()->comment('Field for model');
            $table->string('color')->nullable()->comment('Field for color');
            $table->string('colors')->nullable()->comment('Field for array of COLORS using JSON data structure');
            $table->string('name')->nullable()->comment('Name of product');
            $table->string('slug')->nullable()->comment('Slug piece with the Name of product');
            $table->string('description')->nullable()->comment('Description of product');
            $table->string('size')->nullable()->comment('Size of the product');
            $table->string('material')->nullable()->comment('What material is this product made it');
            $table->string('printing')->nullable()->comment('Description of printing technique');
            $table->string('printing_area')->nullable()->comment('Printing available area');
            $table->float('package_weight', 6, 2)->nullable()->default(0)->comment('The weight of the package');
            $table->float('package_height', 6, 2)->nullable()->default(0)->comment('The height of the package');
            $table->float('package_width', 6, 2)->nullable()->default(0)->comment('The width of the package');
            $table->float('package_length', 6, 2)->nullable()->default(0)->comment('The length of the package');
            $table->float('package_quantity', 6, 2)->nullable()->default(0)->comment('The quantity of the package');
            $table->text('image')->nullable()->comment('Image of the product');
            $table->float('original_price', 6, 2)->nullable()->comment('Distribution Price of the product');
            $table->float('price', 6, 2)->nullable()->default(0)->comment('Price of the product');
            $table->enum('existence', ['true', 'false'])->default('false')->comment('The product has existence');
            $table->integer('pieces')->nullable()->default(0);
            $table->bigInteger('min_stock_alert')->default(15)->comment('Number for indicate that the products needs replenish'); // cantidadalerta
            $table->string('min_stock_message')->default(10)->comment('Deault message to show to the user to resupply the product');
            $table->enum('is_featured', ['true', 'false'])->default('false')->comment('Place the product in special location at the quotation cart');
            $table->bigInteger('stock')->default(0)->comment('Quantity of packages of products in stock');
            $table->bigInteger('orig_id')->nullable()->comment("Identification number for original row ID");
            $table->string('article_id')->nullable()->comment("Identification number for article ID from EXCEL (see import_doble_vela_products and import_4_promotional_products)");
            $table->string('category_doble_vela')->nullable()->comment("This field is necessary in order to relate information from Brand Market Products to WS data");
            $table->string('subcategory_doble_vela')->nullable()->comment("This field is necessary in order to relate information from Brand Market Products to WS data");

            $table->string('category_4_promotional')->nullable()->comment("This field is necessary in order to relate information from Brand Market Products to WS data");
            $table->string('subcategory_4_promotional')->nullable()->comment("This field is necessary in order to relate information from Brand Market Products to WS data");
            $table->string('family_promo_opcion')->nullable()->comment("This field is necessary in order to relate information from Brand Market Products to WS data");

            $table->softdeletes();
            $table->timestamps();

            $table->index('model');
            $table->index('name');
            $table->index('slug');
            $table->index('provider_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_brand_market');
    }
}
