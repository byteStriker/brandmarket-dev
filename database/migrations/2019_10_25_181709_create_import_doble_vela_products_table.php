<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportDobleVelaProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_doble_vela_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('element_id'); //elemento
            $table->text('short_name'); //short_name
            $table->string('model'); //model
            $table->string('color'); //color
            $table->string('article_id'); //article_id
            $table->string('line'); //line
            $table->string('units_per_package'); //units_per_package
            $table->string('product_release_date'); //product_release_date
            $table->string('category'); //category
            $table->string('subcategory'); //subcategory
            $table->string('material'); //material
            $table->string('product_size'); //product_size
            $table->string('catalog_page'); //catalog_page
            $table->string('catalog_name'); //catalog_name
            $table->text('product_description'); //product_description
            $table->string('printing_type'); //printing_type
            $table->string('printing_technique'); //printing_technique,
            $table->string('printing_technique_1'); //Técnica Impresión 1
            $table->string('printing_technique_2'); //Técnica Impresión 2
            $table->string('printing_technique_3'); //Técnica Impresión 3
            $table->string('printing_technique_4'); //Técnica Impresión 4
            $table->string('price_distribution'); //Precio Distribuidor
            $table->string('price_public'); //Precio Publico
            $table->string('depth'); //Largo
            $table->string('width'); //Ancho
            $table->string('height'); //Altura
            $table->string('size_master_size'); //Medida de Caja Master
            $table->string('volume'); //Volumen
            $table->string('weight'); //Peso
            $table->text('description'); //Descripción del artículo
            $table->string('status'); //Activo / Desactivo
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_doble_vela_products');
    }
}
