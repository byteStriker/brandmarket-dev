<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimpleCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('simple_carts', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('uuid')->nullable();
            $table->string('wishlist_name')->nullable();
            $table->float('total')->default(0);
            $table->float('total_mxn')->default(0);
            $table->float('freight_amount')->default(50);
            $table->float('subtotal')->default(0);
            $table->float('subtotal_mxn')->default(0);
            $table->float('exchange_rate')->default(0);
            $table->enum('status', ['Open', 'Wishlist', 'Quoted', 'Completed'])->default('Open');
            $table->timestamps();
            $table->softdeletes();
        });
        //
        // Schema::table('simple_carts', function($table) {
        //   $table->foreign('user_id')->references('id')->on('users');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_carts');
    }
}
