<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWSConfigurationsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    $this->down();
    Schema::create('w_s_configurations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('server_url');
      $table->string('server_url_alternating')->nullable();
      $table->string('server_alias')->nullable();
      $table->string('username')->nullable();
      $table->string('password')->nullable();
      $table->text('documentation')->nullable();
      $table->softdeletes();
      $table->timestamps();

    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('w_s_configurations');
  }
}
