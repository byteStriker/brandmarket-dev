<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImport4PromotionalProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $this->down();
        Schema::create('import4_promotional_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('model_color'); // MODELO Y COLOR
            $table->string('model'); // MODELO
            $table->string('name'); // NOMBRE
            $table->string('description'); // DESCRIPCION
            $table->string('color'); // COLOR
            $table->string('is_new'); // NUEV
            $table->string('product_size'); // MEDIDA DEL PRODUCTO
            $table->string('material'); // MATERIAL
            $table->string('category'); // CATEGORIA
            $table->string('sub_category'); // SUB CATEGORIA
            $table->string('blue_tint'); // TINTA AZUL
            $table->string('capacity'); // CAPACIDA
            $table->string('printing_methods'); // METODOS DE IMPRESION
            $table->string('printing_area'); // AREA DE IMPRESION
            $table->string('box_net_weight'); // PESO
            $table->string('height'); // ALTO
            $table->string('width'); // ANCHO
            $table->string('depth'); // PROFUNDIDAD
            $table->string('pieces'); // CANTIDAD DE PZA
            $table->string('slide_image'); // IMAGEN PORTADA
            $table->string('image_by_color'); // IMAGEN POR COLO
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import4_promotional_products');
    }
}
