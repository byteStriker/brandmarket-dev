<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    $this->down();
    Schema::create('slides', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name');
      $table->string('link_text')->nullable();
      $table->string('link_url')->nullable();
      $table->string('link_type')->nullable();
      $table->string('cover_image')->nullable();
      $table->enum('cover_type',['mobile', 'desktop'])->default('desktop');
      $table->date('published_at')->nullable()->useCurrent=True;
      $table->enum('status', ['Published', 'Future', 'Draft'])->default('Published');
      $table->softdeletes();
      $table->timestamps();

    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('slides');
  }
}
