<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceSettingsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    $this->down();
    Schema::create('finance_settings', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('user_id')->nullable();
      $table->float('general_gain_percentaje')->default(0);
      $table->float('individual_gain_percentaje')->default(0);
      $table->softdeletes();
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('finance_settings');
  }
}
