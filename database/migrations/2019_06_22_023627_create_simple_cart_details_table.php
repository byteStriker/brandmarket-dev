<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimpleCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('simple_cart_details', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('simple_cart_id')->unsigned()->nullable();
            $table->integer('products_brand_market_id')->unsigned()->nullable();
            $table->string('model');
            $table->string('name');
            $table->float('original_price');
            $table->float('price_mxn');
            $table->float('subtotal_mxn');
            $table->float('total_mxn');
            $table->float('price');
            $table->string('color')->nullable()->default(null);
            $table->integer('quantity');
            $table->float('subtotal');
            $table->float('discount');
            $table->float('provider_name');
            $table->string('product_parent_category')->nullable()->default(null);
            $table->string('product_parent_subcategory')->nullable()->default(null);
            $table->timestamps();
            $table->softdeletes();
        });

        // Schema::table('simple_cart_details', function($table) {
        //   $table->foreign('simple_cart_id')->references('id')->on('simple_carts');
        // });
        //
        // Schema::table('simple_cart_details', function($table) {
        //   $table->foreign('course_id')->references('id')->on('courses');
        // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_cart_details');
    }
}
