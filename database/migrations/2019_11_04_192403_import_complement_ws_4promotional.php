<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImportComplementWs4promotional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('import_complement_ws_4promotional', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->nullable(); // descripcion
            $table->string('images')->nullable(); // imagenes
            $table->string('color')->nullable(); // color
            $table->string('color_image')->nullable(); // color
            $table->string('article_name')->nullable(); // nombre artículo
            $table->string('is_promotion')->nullable(); // producto_promocion
            $table->string('category')->nullable();
            $table->string('print_methods')->nullable();
            $table->string('width')->nullable();
            $table->decimal('price')->nullable();
            $table->string('web_color')->nullable();
            $table->string('web')->nullable();
            $table->string('print_area')->nullable();
            $table->string('is_new')->nullable();
            $table->string('subcategory')->nullable();
            $table->string('id_article')->nullable();
            $table->string('height')->nullable();
            $table->string('is_available')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('subcategory_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_complement_ws_4promotional');
    }
}
