<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportComplementWsDobleVelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('import_complement_ws_doble_velas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mongo_id')->nullable();
            $table->string('element_key')->nullable();
            $table->string('short_name')->nullable();
            $table->string('model')->nullable();
            $table->string('color')->nullable();
            $table->string('article_id')->nullable();
            $table->string('line', 6, 2)->nullable();
            $table->string('units_per_package', 6, 2)->nullable();
            $table->string('creation_date')->nullable();
            $table->string('family')->nullable();
            $table->string('subfamily')->nullable();
            $table->string('material')->nullable();
            $table->string('product_size')->nullable();
            $table->string('catalog_page')->nullable();
            $table->string('catalog')->nullable();
            $table->string('description')->nullable();
            $table->string('printing_type')->nullable();
            $table->string('printing_tech_1')->nullable();
            $table->string('printing_tech_2')->nullable();
            $table->string('printing_tech_3')->nullable();
            $table->string('printing_tech_4')->nullable();
            $table->string('price_distribution')->nullable();
            $table->string('price_list')->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('size_box_master')->nullable();
            $table->string('volume')->nullable();
            $table->string('weight')->nullable();
            $table->string('description_caps')->nullable();
            /*
            Nombre Corto    Modelo    Color    Número de artículo    Linea    Unidad por Empaque    FECHA DE CREACION    Familia    Subfamilia    Material    Medida Producto    Pagina Catalogo    Catálogo    Descrpcion del producto    Tipo de Impresión    Técnica Impresión 1    Técnica Impresión 2    Técnica Impresión 3    Técnica Impresión 4    Precio Distribuidor    Precio Publico    Largo    Ancho    Altura    Medida de Caja Master    Volumen    Peso    Descripción del artículo    Activo / Desactivo
             */
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_complement_ws_doble_velas');
    }
}
