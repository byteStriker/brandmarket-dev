<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    $this->down();
    Schema::create('articles', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title')->comment('Field for the title of content/article');
      $table->string('slug')->unique()->comment('Slug for post');
      $table->text('content')->comment('Content for this post/article');
      $table->date('published_at')->comment('Publication date (could be in future)')->useCurrent = true;
      $table->string('hero_image')->nullable()->comment('Available if content/article needs an image');
      $table->string('seo_keywords')->nullable()->comment('Available for SEO Key Words');
      $table->string('seo_description')->nullable()->comment('Available for SEO Description');
      $table->enum('status', ['Published', 'Future', 'Draft'])->comment('Status of the content/article')->default('Published');
      $table->enum('type', ['Post', 'Content'])->comment("When type is POST the row is used for display it in the website. When type is Content,\
       the row is used for displaying content inside any place of the webpage or even store information for the PDFs")->default('Post');
      $table->integer('priority')->nullable()->comment("Disposition for displaying products, while the priority \
      number is closer to zero, the priority will be higher");
      $table->softdeletes();
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('articles');
  }
}
