<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\SimpleCart;
use Faker\Generator as Faker;

$factory->define(SimpleCart::class, function (Faker $faker) {
    return [
      // 'user_id' => $faker->randomNumber(3),
      'total' => $faker->randomNumber(3),
      'subtotal' => $faker->randomNumber(3),
      'status'   => 'Completed'
    ];
});
