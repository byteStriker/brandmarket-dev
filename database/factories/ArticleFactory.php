<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
      'title' => $faker->words(5),
      'content' => $faker->paragraphs(4),
      'published_at' => $faker->date('Y-m-d'),
      'hero_image' => $faker->file(),
      'thumb_image' => $faker->file(),
      'seo_keywords' => $faker->words(3),
      'seo_description' => $faker->words(30),
      'priority' => $faker->unique()->randomDigit
    ];
});
