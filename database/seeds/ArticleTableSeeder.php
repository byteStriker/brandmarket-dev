<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Article;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      foreach (range(1,10) as $index) {
        // DB::table('article')->insert([
        //   'title' => $faker->words(5),
        //   'content' => $faker->paragraphs(4),
        //   'published_at' => $faker->date('Y-m-d'),
        //   'hero_image' => $faker->word(),
        //   'thumb_image' => $faker->word(),
        //   'seo_keywords' => $faker->words(3),
        //   'seo_description' => $faker->words(30),
        //   'priority' => $faker->unique()->randomDigit
        // ]);
        $article = new Article();
        $article->title = $faker->words(3, true);
        $article->slug = Str::slug($article->title);
        $article->content = implode('.', $faker->paragraphs(5));
        $article->published_at = date('Y-m-d');
        // $article->hero_image = $faker->image('storage/app/public',400,300, null, false);;
        $article->hero_image = $faker->imageUrl($width = 800, $height = 600);
        $article->seo_keywords = $faker->word();
        $article->seo_description = $faker->word();
        $article->status = 'Published';
        $article->priority = 1;
        $article->save();

      }
    }
}
