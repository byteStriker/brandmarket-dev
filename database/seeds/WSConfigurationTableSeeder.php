<?php

use Illuminate\Database\Seeder;
use App\WSConfiguration;

class WSConfigurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $WS1 = new WSConfiguration();
      $WS1->server_url = 'http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl';
      // $WS1->username = 'none';
      $WS1->server_alias = 'PromoOpcion';
      $WS1->server_url_alternating = 'https://www.promoopcion.com/excel.php?name=dbFicha';
      $WS1->password = '';
      $WS1->documentation = "WEB SERVICE 01
      PromoOpción Web Service
      Clave: 5KW1SA
       
      1.       PASO 1:
      ·         Este es el link para obtener nuestra Base de Datos (http://www.promoopcion.com/exportadb.php?ubk=MX)
      ·         Los datos se visualizarán en un navegador web (utilizar Firefox de preferencia).
      ·         Los datos se encontrarán divididos por columnas por ejemplo, Item(código hijo), Father (código padre), Family (Familia), Nombre, Descripción, Color, Colors,etc.
      ·         Para descargar la DB es necesario dirigirse hasta la parte inferior izquierda donde se encontrará un botón para exportar todo el contenido en un archivo Excel (Utilizar Firefox de preferencia).
       
       
       
      2.       PASO 2:
      ·         Obtener el Web Service “FullFilmentMX” de PromoOpcion® a través del siguiente link:
      ·         http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl
       
      3.       PASO 3:
      ·         Configurar el servicio web “FullFilmentMX” en tu ambiente:
 
       
      A continuación realizamos un ejemplo para configurar el servicio web consumiendo el método “exitencias”.
      Método de ejemplo: (existencias) 
      ·        Método: existencias
      ·         Parámetros: codigo (Código hijo) y distribuidor (*******) 
      ·        Ejemplo para su invocación: (lenguaje PHP y su Librería es nusoap )
      ·        Descarga de las librerías nusoap: http://sourceforge.net/projects/nusoap/
       
      Código del consumo del WS en PHP 
      include('lib/nusoap.php');
      \$client = new nusoap_client('http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl','wsdl');
      \$err = \$client->getError();
      if (\$err) {     echo 'Error en Constructor' . \$err ; }
      \$CardCode = ' DFE7471';
      \$param = array('codigo' => 'ANF 009 A', 'distribuidor' = > \$CardCode);
      \$result = \$client->call('existencias', \$param);
      print_r(\$result);
      if (\$client->fault) {
              echo 'Fallo';
              print_r(\$result);
      } else {        // Chequea errores
              \$err = \$client->getError();
              if (\$err) {             // Muestra el error
                      echo 'Error' . \$err ;
              } else {                // Muestra el resultado
                      echo 'Resultado';
                      print_r (\$result);
              }
      }
      El servicio web cuenta con 4 métodos que a continuación describo:
      ·         Método 1 : colorItem
      Regresa todos los colores que existe del articulo ingresado, por ejemplo ; ANF 006 A (Amarillo Translucido, Azul Translucido, Morado, Negro, Rosa, etc.)
      Este método se invoca con el parámetro “codigo” el cual representa al código hijo de un artículo, por ejemplo ANF 006 A:
      \$param = array('codigo' => 'ANF 009 A');
      \$result = \$client->call('colorItem', \$param);
       
      ·         Método 2 : colors
      Regresa todos los colores que maneja Promoopción. No tiene parámetros para enviar.
      \$result = \$client->call('colors');
       
      ·         Método 3 : existencias
      Regresa las existencias (stock) de todos los artículos, puede validar estos datos contra el stock que muestra nuestro sitio web una vez ingresando con su clave de distribuidor.
      Este método se invoca con dos parámetros “codigo” el cual representa al código hijo de un artículo, “distribuidor” donde se introduce su ID de distribuidor, ejemplo:
      \$CardCode = 'DFE4416';
      \$param = array('codigo' => 'ANF 009 A', 'distribuidor' = > \$CardCode);
      \$result = \$client->call('existencias', \$param);
       
      ·         Método 4 : fichaTecnica
      Regresa la descripción general del artículo, algunos de los datos que regresa son; nombre del producto, familia, color, material, tamaño, altura, anchura, peso, etc.
      Este método se invoca con un parámetro llamado “codigo” el cual representa al código hijo de un artículo, ejemplo;
      \$param = array('codigo' => 'ANF 006 A');
      \$result = \$client->call('colorItem', \$param);";
      $WS1->save();

      $WS2 = new WSConfiguration();
      $WS2->server_url = 'http://srv-datos.dyndns.info/doblevela/service.asmx';
      // $WS2->username = 'none';
      $WS2->server_alias = 'DobleVela';
      $WS2->password = '5Dp/qvDrxtWqg8powRrOzA==';
      $WS2->documentation = "WEB SERVICE 02
Doble Vela Web Service
Credenciales: 5Dp/qvDrxtWqg8powRrOzA==

JSON http://srv-datos.dyndns.info/doblevela/service.asmx ";
      $WS2->save();

      $WS3 = new WSConfiguration();
      $WS3->server_url = 'http://forpromotional.homelinux.com:9090/WsEstrategia/inventario';
      // $WS3->username = 'none';
      $WS3->server_alias = '4promotional';
      $WS3->password = '';
      $WS3->documentation = "WEB SERVICE 03
4Promotional Web Service
Credenciales: Ninguna

Cambio de Catalogo – 1 vez al año en ExpoPublicitas
Pedir por correo


Formato JSON
http://forpromotional.homelinux.com:9090/WsEstrategia/inventario
 
Formato TXT
http://forpromotional.homelinux.com:9090/WsEstrategia/inventarioTxt
";
      $WS3->save();
    }
}
