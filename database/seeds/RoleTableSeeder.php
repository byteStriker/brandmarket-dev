<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $roleAdmin = new Role();
      $roleAdmin->id = 1;
      $roleAdmin->name = 'admin';
      $roleAdmin->description = 'Administrator';
      $roleAdmin->save();
      $roleUser = new Role();
      $roleUser->id = 2;
      $roleUser->name = 'user';
      $roleUser->description = 'User';
      $roleUser->save();
    }
}
