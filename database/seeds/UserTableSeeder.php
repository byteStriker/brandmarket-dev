<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $userAdmin = new User();
      $userAdmin->name = 'admin';
      $userAdmin->email = 'admin@example.com';
      $userAdmin->email_verified_at = date('Y-m-d h:i:s');
      $userAdmin->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
      $userAdmin->remember_token = '3903838292';
      $userAdmin->save();
      $userAdmin->roles()->attach(Role::where('name', 'admin')->first());


      $userMortal = new User();
      $userMortal->name = 'user';
      $userMortal->email = 'user@example.com';
      $userMortal->email_verified_at = date('Y-m-d h:i:s');
      $userMortal->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
      $userMortal->remember_token = '3903838292';
      $userMortal->save();
      $userMortal->roles()->attach(Role::where('name', 'user')->first());


      $userMortal = new User();
      $userMortal->name = 'Pedro Navarro';
      $userMortal->email = 'devel@brandmarket.com';
      $userMortal->email_verified_at = date('Y-m-d h:i:s');
      $userMortal->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
      $userMortal->remember_token = '3903838292';
      $userMortal->save();
      $userMortal->roles()->attach(Role::where('name', 'user')->first());

      $userMortal = new User();
      $userMortal->name = 'Gonzalo García';
      $userMortal->email = 'tester@brandmarket.com';
      $userMortal->email_verified_at = date('Y-m-d h:i:s');
      $userMortal->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
      $userMortal->remember_token = '3903838292';
      $userMortal->save();
      $userMortal->roles()->attach(Role::where('name', 'user')->first());

    }
}
