<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
  * Seed the application's database.
  *
  * @return void
  */
  public function run()
  {
    $this->call(RoleTableSeeder::class);
    $this->call(UserTableSeeder::class);
    $this->call(ArticleTableSeeder::class);
    $this->call(CategoryTableSeeder::class);
    $this->call(SubcategoryTableSeeder::class);
    $this->call(WSConfigurationTableSeeder::class);
    $this->call(ProductsBrandMarketSeeder::class);
    $this->call(FinaceSettingsTableSeeder::class);

    $this->call(SlideTableSeeder::class);
    $this->call(SlideDetailsTableSeeder::class);
    $this->call(SimpleCartDetailsTableSeeder::class);


  }
}
