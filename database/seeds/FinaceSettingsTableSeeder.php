<?php

use Illuminate\Database\Seeder;
use App\FinanceSettings;
class FinaceSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $financeSettings = new FinanceSettings();
      $financeSettings->user_id = 1;
      $financeSettings->general_gain_percentaje = 0.56;
      $financeSettings->individual_gain_percentaje = 0.97;
      $financeSettings->save();
    }
}
