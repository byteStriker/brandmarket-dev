<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Subcategory;
use Faker\Factory as Faker;
use Illuminate\Support\Str;

class SubcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      // por cada uno (delos de aquí llamarlos así $index  )
      // foreach (range(1,10) as $index) {
      //   $category = new Category();
      //   $category->name = $faker->word();
      //   $category->cover_image = $faker->image('storage/app/public',400,300, null, false);
      //   $category->save();
      //   foreach (range(1,5) as $sb){
      //     $subcategory = new Subcategory();
      //     $subcategory->category_id = $category->id;
      //     $subcategory->name = $faker->word();
      //     $subcategory->cover_image = $faker->image('storage/app/public',400,300, null, false);
      //
      //     $subcategory->save();
      //   }
      // }
      $BrandMarketCategories = array();

      $BrandMarketCategories['Category']['Bar & Gourmet'] = array();

      $BrandMarketCategories['Category']['Bebidas'] = array();
      array_push ($BrandMarketCategories['Category']['Bebidas'],"Cilindros Plásticos");
      array_push ($BrandMarketCategories['Category']['Bebidas'],"Cilindros Metálicos");
      array_push ($BrandMarketCategories['Category']['Bebidas'],"Tazas");
      array_push ($BrandMarketCategories['Category']['Bebidas'],"Vasos");
      array_push ($BrandMarketCategories['Category']['Bebidas'],"Termos");
      //
      $BrandMarketCategories['Category']['Bolígrafos'] = array();
      array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Plásticos");
      array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Metálicos");
      array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Ecologicos");
      array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Multifuncionales");
      array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Lápices y Repuestos");
      //
      $BrandMarketCategories['Category']['Cuidado Personal'] = array();
      array_push ($BrandMarketCategories['Category']['Cuidado Personal'], "Salud");
      array_push ($BrandMarketCategories['Category']['Cuidado Personal'], "Belleza");
      //
      $BrandMarketCategories['Category']['Deportes y Entretenimiento'] = array();
      $BrandMarketCategories['Category']['Herramientas'] = array();
      $BrandMarketCategories['Category']['Hieleras y Loncheras'] = array();
      $BrandMarketCategories['Category']['Mascotas'] = array();
      $BrandMarketCategories['Category']['Niños'] = array();
      //
      $BrandMarketCategories['Category']['Oficina'] = array();
      //
      array_push ($BrandMarketCategories['Category']['Oficina'], "Agendas y carpetas");
      array_push ($BrandMarketCategories['Category']['Oficina'], "Libretas");
      array_push ($BrandMarketCategories['Category']['Oficina'], "Llaveros");
      array_push ($BrandMarketCategories['Category']['Oficina'], "Generales de Oficina");
      array_push ($BrandMarketCategories['Category']['Oficina'], "Portarretratos");
      array_push ($BrandMarketCategories['Category']['Oficina'], "Relojes");
      array_push ($BrandMarketCategories['Category']['Oficina'], "Anti estrés");
      $BrandMarketCategories['Category']['Paraguas e impermeables'] = array();
      $BrandMarketCategories['Category']['Textiles'] = [];
      array_push ($BrandMarketCategories['Category']['Textiles'],"Bolsas y morrales");
      array_push ($BrandMarketCategories['Category']['Textiles'],"Chamarras, chalecos y sudaderas");
      array_push ($BrandMarketCategories['Category']['Textiles'],"Gorras");
      array_push ($BrandMarketCategories['Category']['Textiles'],"Playeras y tipo Polo");
      array_push ($BrandMarketCategories['Category']['Textiles'],"Maletas");
      array_push ($BrandMarketCategories['Category']['Textiles'],"Mochilas");
      array_push ($BrandMarketCategories['Category']['Textiles'],"Portafolios");
      $BrandMarketCategories['Category']['Tecnología'] = array();
      array_push($BrandMarketCategories['Category']['Tecnología'], "Accesorios de computo");
      array_push($BrandMarketCategories['Category']['Tecnología'], "Accesorios Smartphone y tablets");
      array_push($BrandMarketCategories['Category']['Tecnología'], "Audio");
      array_push($BrandMarketCategories['Category']['Tecnología'], "Power Bank");
      array_push($BrandMarketCategories['Category']['Tecnología'], "USB's");
      $BrandMarketCategories['Category']['Viaje'] = array();

      // print_r($BrandMarketCategories);
      // por cada uno de los elementos de Category: as ()
      foreach($BrandMarketCategories['Category'] as $idx => $brandmarketCategory){
        print("{$idx}  (".count($brandmarketCategory).") elementos => \n");
        $category = new Category();
        $category->name = $idx;
        $category->slug = Str::slug($category->name);
        // $category->cover_image = $faker->image('storage/app/public',400,300, null, false);
        $category->cover_image = "{$category->slug}.png";
        $category->save();
        if (count($brandmarketCategory)>=1) {
          foreach ($brandmarketCategory as $brandmarketSubCategory) {
            $subcategory = new Subcategory();
            $subcategory->category_id = $category->id;
            $subcategory->name = $brandmarketSubCategory;
            $subcategory->slug = Str::slug($subcategory->name);
            // $subcategory->cover_image = $faker->image('storage/app/public',400,300, null, false);
            // $subcategory->cover_image = $faker->imageUrl($width = 640, $height = 480);
            $subcategory->cover_image = "{$subcategory->slug}.png";
            $subcategory->save();
          }
        }
      }


    }
}
