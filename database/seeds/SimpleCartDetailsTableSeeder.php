<?php

use Illuminate\Database\Seeder;
use App\SimpleCart;
use App\SimpleCartDetails;
use Faker\Factory as Faker;
use Illuminate\Support\Str;

class SimpleCartDetailsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker::create();
    // por cada uno (delos de aquí llamarlos así $index  )
    foreach (range(1,4) as $index) {
      $simpleCart = new SimpleCart();
      $simpleCart->user_id = 2;
      $simpleCart->total = 10;
      $simpleCart->subtotal = 10;
      $simpleCart->status = 'Wishlist';
      $simpleCart->wishlist_name = $faker->words(5, true);
      $simpleCart->uuid = Str::uuid();
      $simpleCart->save();
      $i = 1;
      foreach (range(1,10) as $sb){
        $simpleCartDetails = new SimpleCartDetails();
        $simpleCartDetails->simple_cart_id = $simpleCart->id;
        $simpleCartDetails->products_brand_market_id = $i;
        $simpleCartDetails->name = $faker->words(4, true);
        $simpleCartDetails->price = 1;
        $simpleCartDetails->quantity = 1;
        $simpleCartDetails->subtotal = 1;
        $simpleCartDetails->discount = 0;
        $simpleCartDetails->save();
        $i++;
      }
    }
    $simpleCart = new SimpleCart();
    $simpleCart->user_id = 2;
    $simpleCart->total = 100;
    $simpleCart->subtotal = 100;
    $simpleCart->status = 'Open';
    $simpleCart->save();
    $i = 0;
    foreach (range(1,10) as $sb){
      $simpleCartDetails = new SimpleCartDetails();
      $simpleCartDetails->simple_cart_id = $simpleCart->id;
      $simpleCartDetails->products_brand_market_id = $i;
      $simpleCartDetails->name = $faker->words(4, true);
      $simpleCartDetails->price = 100;
      $simpleCartDetails->quantity = 1;
      $simpleCartDetails->subtotal = 100;
      $simpleCartDetails->discount = 0;
      $simpleCartDetails->save();
      $i++;
    }
  }
}
