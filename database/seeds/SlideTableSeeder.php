<?php

use Illuminate\Database\Seeder;
use App\Slide;

use Faker\Factory as Faker;

class SlideTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $faker = Faker::create();
    foreach (range(1,3) as $index) {

      $slide = new Slide();
      $slide->name = $faker->word();
      $slide->link_text = $faker->word();
      $slide->link_url = $faker->word();
      $slide->link_type = $faker->word();
      // $slide->cover_image = $faker->image('storage/app/public',400,300, null, false);;
      $slide->cover_image = $faker->imageUrl($width = 640, $height = 480);
      $slide->cover_type = 'desktop';
      $slide->published_at = $faker->date();
      $slide->status = 'Published';
      $slide->save();
    }
  }

}
