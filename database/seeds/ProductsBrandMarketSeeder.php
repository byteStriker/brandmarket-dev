<?php

use App\ProductsBrandMarket;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductsBrandMarketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;
        foreach (range(1, 42) as $index) {

            $product = new ProductsBrandMarket();

            if ($i <= 10) {
                $product->category_id = 2; // $table->unsignedInteger('')->nullable()->comment('Field related to: category.id ');
                $product->subcategory_id = 1; // $table->unsignedInteger('')->nullable()->comment('Field related to: subcategory.id');
            }
            if ($i <= 20) {
                $product->category_id = 2; // $table->unsignedInteger('')->nullable()->comment('Field related to: category.id ');
                $product->subcategory_id = 2; // $table->unsignedInteger('')->nullable()->comment('Field related to: subcategory.id');
            }
            if ($i <= 30) {
                $product->category_id = 2; // $table->unsignedInteger('')->nullable()->comment('Field related to: category.id ');
                $product->subcategory_id = 3; // $table->unsignedInteger('')->nullable()->comment('Field related to: subcategory.id');
            }

            if ($i <= 40) {
                $product->category_id = 2; // $table->unsignedInteger('')->nullable()->comment('Field related to: category.id ');
                $product->subcategory_id = 4; // $table->unsignedInteger('')->nullable()->comment('Field related to: subcategory.id');
            }

            $product->model = $faker->word(5); // $table->string('')->nullable()->comment('Field for model');
            $product->color = $faker->word(5); // $table->string('')->nullable()->comment('Field for color');
            $product->colors = $faker->word(5); // $table->string('')->nullable()->comment('Field for array of COLORS using JSON data structure');
            $product->name = $faker->word(5); // $table->string('')->nullable()->comment('Name of product');
            $product->slug = Str::slug("{$product->model} {$product->name}");
            $product->description = $faker->word(3); // $table->string('')->nullable()->comment('Description of product');
            $randomWidth = rand(0, 100) / 10;
            $randomHeight = rand(0, 100) / 10;

            $product->size = "{$randomWidth} cm x {$randomHeight} cm"; // $table->string('')->nullable()->comment('Size of the product');
            $product->material = $faker->word(5); // $table->string('')->nullable()->comment('What material is this product made it');
            $product->printing = $faker->word(5); // $table->string('')->nullable()->comment('Description of printing technique');
            $randomPrintWidth = rand(0, 10) / 10;
            $randomPrintHeight = rand(0, 10) / 10;
            $randomPackageWeight = rand(0, 109300) / 10;
            $product->printing_area = "{$randomPrintWidth} cm x {$randomPrintHeight} cm"; // $table->string('')->nullable()->comment('Printing available area');
            // print(" \$randomPackageWeight: {$randomPackageWeight} \n");
            $product->package_weight = 0; // $table->float('')->nullable()->comment('The weight of the package');
            $product->package_height = 0; // $faker->randomFloat();// $table->float('')->nullable()->comment('The height of the package');
            $product->package_width = 0; // $faker->randomFloat();// $table->float('')->nullable()->comment('The width of the package');
            $product->package_length = 0; // $faker->randomFloat();// $table->float('')->nullable()->comment('The length of the package');
            $product->package_quantity = 0; // $faker->randomFloat();// $table->float('')->nullable()->comment('The quantity of the package');
            // $product->image = $faker->image('storage/app/public',400,300, null, false);
            $product->image = $faker->imageUrl($width = 800, $height = 600);

            $product->original_price = 0; // $faker->randomFloat(2);// $table->float('')->nullable()->comment('Price of the product');
            $product->price = 0; // $faker->randomFloat(2);// $table->float('')->nullable()->comment('Price of the product');
            $product->existence = 'true'; // $table->enum('',['true', 'false'])->default('false')->comment('The product has existence');
            $product->min_stock_alert = $faker->numberBetween(13, 42); // $table->int('')->nullable()->default('Number for indicate that the products needs replenish');// cantidadalerta
            $product->min_stock_message = $faker->sentence(5); // $table->string('')->nullable()->default(10)->comment('Deault message to show to the user to resupply the product');
            $product->is_featured = 'false'; // $table->enum('',['true', 'false'])->nullable()->default('false')->comment('Place the product in special location at the quotation cart');
            $product->stock = $faker->randomNumber(3); // $table->int('')->nullable()->default(0)->comment('Quantity of packages of products in stock');
            $product->save();
            $i++;
        }

    }
}
