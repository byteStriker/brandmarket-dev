<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  /**
   * users
   *
   * Show users that this user belongs to.
   *
   * @return return object
   */
  public function users()
  {
    return $this->belongsToMany(User::class)->withTimestamps();
  }
}
