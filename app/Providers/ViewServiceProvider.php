<?php
namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      /*

      */
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer('frontend.*', 'App\Http\View\Composers\CartComposer');
        View::composer('frontend.*', 'App\Http\View\Composers\FeaturedProductsComposer');
        View::composer('frontend.*', 'App\Http\View\Composers\AllProductsOptionsComposer');
        // Using Closure based composers...
        // View::composer('dashboard', function ($view) {
        //
        // });
    }
}
?>
