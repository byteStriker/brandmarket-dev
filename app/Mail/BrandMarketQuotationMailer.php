<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BrandMarketQuotationMailer extends Mailable
{
    use SerializesModels;
    public $quotation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
     // public function __construct($data, $quotation)
    public function __construct($quotation)
    {
      // dd($quotation->output());
      // $this->data = $data;
      $this->quotation = $quotation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      // return $this->view('frontend.mailing.email-response')
      return $this->view('frontend.mailing.internal-quotation')
      ->with('page_title' ,  'Brand Market')
      ->with('simpleCart' ,  $this->quotation['simpleCart'])
      ->with('internalCode' ,  $this->quotation['internalCode'])
      ->with('year' ,  $this->quotation['year'])
      ->with('month' ,  $this->quotation['month'])
      ->with('day' ,  $this->quotation['day'])
      ->with('time' ,  $this->quotation['time'])
      ->with('timezone' ,  $this->quotation['timezone'])
      ->with('client_name' ,  $this->quotation['client_name'])
      ->with('client_number' ,  $this->quotation['client_number'])
      ->subject('Brand Market - Pedido');

      // ->attachData($this->pdf->output(), "pedido-".auth()->user()->name."-".auth()->user()->id.".pdf");
      ;
    }
}
