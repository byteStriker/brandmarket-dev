<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

// class ClientQuotationMailer extends Mailable implements ShouldQueue
class ClientQuotationMailer extends Mailable
{
    use SerializesModels;
    public $clientQuotationInfo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    // public function __construct($data, $pdf)
    public function __construct($clientQuotationInfo)
    {
        // \Log::info("Constructing mailer!");
        $this->clientQuotationInfo = $clientQuotationInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // \Log::info("Building Mailer!");
        // \Log::info("\$this->clientQuotationInfo->simpleCart " . print_R($this->clientQuotationInfo['simpleCart'], 1));
        $this->subject('Brand Market - Confirmación de Pedido');
        return $this
            ->view('frontend.mailing.client-quotation')
            ->with('page_title', 'Brand Market')
            ->with('simpleCart', $this->clientQuotationInfo['simpleCart'])
            ->with('internalCode', $this->clientQuotationInfo['internalCode'])
            ->with('year', $this->clientQuotationInfo['year'])
            ->with('month', $this->clientQuotationInfo['month'])
            ->with('day', $this->clientQuotationInfo['day'])
            ->with('time', $this->clientQuotationInfo['time'])
            ->with('timezone', $this->clientQuotationInfo['timezone'])
            // ->with('provider_name', $this->clientQuotationInfo['provider_name'])
            ->with('client_name', auth()->user()->name)
            ->with('client_number', auth()->user()->id);
            // ->attach($this->pdf, [
            //     'as' => 'confirmación-pedido.pdf',
            //     'mime' => 'application/pdf',
            // ]);
        ;
            // ->attachData($this->pdf, "confirmacion-pedido.pdf");
        ;
    }
}
