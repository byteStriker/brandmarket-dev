<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProviderQuotationMailer extends Mailable
{
    use SerializesModels;
    public $clientQuotationInfo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    // public function __construct($data, $pdf)
    public function __construct($clientQuotationInfo)
    {
        // \Log::info("Constructing mailer!");
        $this->clientQuotationInfo = $clientQuotationInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (strtolower($this->clientQuotationInfo['provider_name']) == 'doble-vela') {
            // \Log::info("Building Mailer!");
            // \Log::info("\$this->clientQuotationInfo->simpleCart " . print_R($this->clientQuotationInfo['simpleCart'], 1));
            $this->subject('Brand Market - Levantamiento de pedido de Pedido');
            return $this
                ->view('frontend.mailing.doble-vela-quotation')
                ->with('page_title', 'Levantamiento de Pedido')
                ->with('simpleCart', $this->clientQuotationInfo['simpleCart'])
                ->with('internalCode', $this->clientQuotationInfo['internalCode'])
                ->with('year', $this->clientQuotationInfo['year'])
                ->with('month', $this->clientQuotationInfo['month'])
                ->with('day', $this->clientQuotationInfo['day'])
                ->with('time', $this->clientQuotationInfo['time'])
                ->with('timezone', $this->clientQuotationInfo['timezone'])
                ->with('provider_name', $this->clientQuotationInfo['provider_name'])
                ->with('client_name', auth()->user()->name)
                ->with('client_number', auth()->user()->id);
        } else if (strtolower($this->clientQuotationInfo['provider_name']) == '4-promotional') {
            // \Log::info("Building Mailer!");
            // \Log::info("\$this->clientQuotationInfo->simpleCart " . print_R($this->clientQuotationInfo['simpleCart'], 1));
            $this->subject('Brand Market - Levantamiento de pedido de Pedido');
            return $this
                ->view('frontend.mailing.four-promotional-quotation')
                ->with('page_title', 'Levantamiento de Pedido')
                ->with('simpleCart', $this->clientQuotationInfo['simpleCart'])
                ->with('internalCode', $this->clientQuotationInfo['internalCode'])
                ->with('year', $this->clientQuotationInfo['year'])
                ->with('month', $this->clientQuotationInfo['month'])
                ->with('day', $this->clientQuotationInfo['day'])
                ->with('time', $this->clientQuotationInfo['time'])
                ->with('timezone', $this->clientQuotationInfo['timezone'])
                ->with('provider_name', $this->clientQuotationInfo['provider_name'])
                ->with('client_name', auth()->user()->name)
                ->with('client_number', auth()->user()->id);
        }
    }
}
