<?php

namespace App;

use App\Category;
use App\Subcategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsBrandMarket extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    protected $table = 'products_brand_market';
    protected $fillable = ['category_id', 'subcategory_id', 'model', 'color', 'colors',
        'name', 'slug', 'description', 'size', 'material', 'printing', 'printing_area', 'package_weight',
        'package_height', 'package_width', 'package_length', 'package_quantity', 'image',
        'original_price', 'price', 'existence', 'pieces', 'min_stock_alert', 'min_stock_message', 'is_featured', 'stock',
    ];

    /**
     * Subcategory
     *
     * Show a subcategory the products belongs to
     *
     * @param type var Description
     * @return return type
     */
    public function Subcategory()
    {
        // return $this->belongsTo('App\Subcategory', 'subcategory_id', 'id');
        return $this->belongsTo(Subcategory::class);
    }

    /**
     * Category
     *
     * Show a subcategory the products belongs to
     *
     * @param type var Description
     * @return return type
     */
    public function Category()
    {
        // return $this->belongsTo('App\Category', 'category_id', 'id');
        // return $this->belongsTo('App\Category', 'category_id', 'id');
        return $this->belongsTo(Category::class);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param type var Description
     * @return return type
     */
    public function CartDetails()
    {
        return $this->hasMany(SimpleCartDetails::class, 'products_brand_market_id', 'id');
    }

}
