<?php

namespace App\Imports;

use App\ImportPromoOpcionProducts;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class PromoOpcionExcelImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProductsPromoOpcion([
          'page' => $row[0],
          'code' => $row[1],
          'cilinders' => $row[2],
          'price' => $row[3],
        ]);
    }
}
