<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportDobleVelaProducts extends Model
{
    //
    protected $fillable = ["element_id", "short_name", "model", "color", "article_number", "line",
    "units_per_package", "creation_date", "family", "subfamily", "material", "size",
    "product", "page_catalog", "catalog", "description", "printing_type", "print_tech_1",
    "print_tech_2", "print_tech_3", "print_tech_4", "dist_price", "pub_price", "length",
    "width", "height", "box_size", "master", "volume", "weight", "description_article", "status"
  ];
}
