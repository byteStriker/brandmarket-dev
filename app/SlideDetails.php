<?php

namespace App;
use App\Slide;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

use Eloquent;


class SlideDetails extends Model
{
  //
  use SoftDeletes;
  protected $table = 'slide_details';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['id', 'slide_id', 'name', 'link_text', 'link_url', 'link_type', 'cover_image', 'cover_type', 'published_at', 'status' ];

  protected $primaryKey = 'id';

  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [];


  /**
  * slide
  *
  * Show a list of records that are parent of the row
  *
  * @param type var Description
  * @return return type
  */
  public function slide()
  {
    return $this->belongsTo(Slide::class);

  }

}
