<?php

namespace App;

use App\SimpleCartDetails;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SimpleCart extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    protected $table = 'simple_carts';
    protected $fillable = ['user_id', 'total', 'wishlist_name', 'freight_amount', 'subtotal', 'status', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * User
     *
     * relation for user
     *
     * @param type var Description
     * @return return type
     */
    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Details
     *
     * relation for detail of products
     *
     * @param type var Description
     * @return return type
     */
    public function Details()
    {
        return $this->hasMany(SimpleCartDetails::class, 'simple_cart_id', 'id');
    }

    /**
     * Products
     *
     * relation of products for the cart
     *
     * @param type var Description
     * @return return type
     */
    public function Products()
    {
        // return $this->hasManyThrough(ProductsBrandMarket::class, SimpleCartDetails::class);
        // return $this->hasMany(ProductsBrandMarket::class,'products_brand_market_id', 'id');
        // return $this->hasManyThrough(SimpleCartDetails::class);
        // return $this->hasManyThrough(ProductsBrandMarket::class, SimpleCartDetails::class);

    }

    // public function cartCourses()
    // {
    //     return $this->hasOneThrough('App\SimpleCartDetails', 'App\Courses');
    // }
}
