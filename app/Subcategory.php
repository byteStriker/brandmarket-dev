<?php

namespace App;
use App\Category;
use App\ProductsBrandMarket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id','name', 'slug', 'cover_image' ];

    protected $primaryKey = 'id';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * Category
     *
     * Show a list of records that are children of the category
     *
     * @param type var Description
     * @return return type
     */
    public function Category()
    {
      return $this->belongsTo(Category::class);
    }


  /**
   * products
   *
   * Show the products related to this subcategory
   *
   * @param type var Description
   * @return return type
   */
  public function Products()
  {
    return $this->hasMany(ProductsBrandMarket::class);
  }


}
