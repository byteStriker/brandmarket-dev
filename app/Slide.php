<?php

namespace App;
use App\SlideDetails;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
  //
  use SoftDeletes;
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['id', 'name', 'link_text', 'link_url', 'link_type', 'cover_image', 'cover_type', 'published_at', 'status' ];
  protected $primaryKey = 'id';

  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [];

  /**
  * SlideDetails
  *
  * Show a list of records that are children of the Slides
  *
  * @param type var Description
  * @return return type
  */
  public function SlideDetails()
  {
    return $this->hasMany(SlideDetails::class);
  }

}
