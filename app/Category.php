<?php

namespace App;
use App\Subcategory;
use App\ProductsBrandMarket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

  use SoftDeletes;
  protected $primaryKey = 'id';

  protected $table_name = 'categories';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'slug', 'cover_image' ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [];

  /**
   * subcategories
   *
   * Show a list of records that are children of the category
   *
   * @param type var Description
   * @return return type
   */
  public function Subcategories()
  {
    // return $this->hasMany('App\Subcategory', 'id', 'category_id')->withDefaults();
    return $this->hasMany(Subcategory::class);
  }
  /**
   * products
   *
   * Show the products related to this subcategory
   *
   * @param type var Description
   * @return return type
   */
  public function Products()
  {
    return $this->hasMany(ProductsBrandMarket::class);
  }


}
