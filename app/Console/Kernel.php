<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // $schedule->command('inspire')
        //          ->hourly();
        if (!\App::environment(['local', 'staging', 'dev'])) { // IS PRODUCTION
            \Log::info("Executing scheduled tasks");
            $schedule->command('doble-vela:update-data')->everyFifteenMinutes();
            $schedule->command('four-promo:get-full-db')->everyFifteenMinutes();
        } else { // IS DEVEL
            // \Log::info("Message from scheduled update for doblevela!");
            $schedule->command('doble-vela:update-data')->everyFiveMinutes();
            $schedule->command('four-promo:get-full-db')->everyFiveMinutes();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
