<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Traits\FourPromotionalTrait;

class GetFullDb4Promo extends Command
{
    use FourPromotionalTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'four-promo:get-full-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command suppose to download the full db for 4promotional products; this task is scheduled';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getFullDB();
    }
}
