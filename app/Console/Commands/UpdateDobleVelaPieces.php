<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Traits\DobleVelaTrait;


class UpdateDobleVelaPieces extends Command
{
    use DobleVelaTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doble-vela:update-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command suppose to update the pieces and price for doblevela products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->updateStockAndPrice();
    }
}
