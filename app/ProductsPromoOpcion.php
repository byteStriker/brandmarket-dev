<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsPromoOpcion extends Model
{
  protected $table = "products_promo_opcion";
  // protected $table = "import_promo_opcion_products";
  protected $fillable = ['id','item','father','family','name','description','color','colors','size','material',
  'capacity','batteries','printing','area_p','nw','gw','height','width','length','quantity_packaged',
  'img','price','existence','created_at','updated_at'];
}
