<?php
/**
 * File for class WsFullFilmentMXStructArrayOfColores
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructArrayOfColores originally named ArrayOfColores
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructArrayOfColores extends WsFullFilmentMXWsdlClass
{
    /**
     * The Colores
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var WsFullFilmentMXStructColores
     */
    public $Colores;
    /**
     * Constructor method for ArrayOfColores
     * @see parent::__construct()
     * @param WsFullFilmentMXStructColores $_colores
     * @return WsFullFilmentMXStructArrayOfColores
     */
    public function __construct($_colores = NULL)
    {
        parent::__construct(array('Colores'=>$_colores),false);
    }
    /**
     * Get Colores value
     * @return WsFullFilmentMXStructColores|null
     */
    public function getColores()
    {
        return $this->Colores;
    }
    /**
     * Set Colores value
     * @param WsFullFilmentMXStructColores $_colores the Colores
     * @return WsFullFilmentMXStructColores
     */
    public function setColores($_colores)
    {
        return ($this->Colores = $_colores);
    }
    /**
     * Returns the current element
     * @see WsFullFilmentMXWsdlClass::current()
     * @return WsFullFilmentMXStructColores
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see WsFullFilmentMXWsdlClass::item()
     * @param int $_index
     * @return WsFullFilmentMXStructColores
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see WsFullFilmentMXWsdlClass::first()
     * @return WsFullFilmentMXStructColores
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see WsFullFilmentMXWsdlClass::last()
     * @return WsFullFilmentMXStructColores
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see WsFullFilmentMXWsdlClass::last()
     * @param int $_offset
     * @return WsFullFilmentMXStructColores
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see WsFullFilmentMXWsdlClass::getAttributeName()
     * @return string Colores
     */
    public function getAttributeName()
    {
        return 'Colores';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructArrayOfColores
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
