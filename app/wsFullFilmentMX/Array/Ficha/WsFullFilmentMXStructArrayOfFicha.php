<?php
/**
 * File for class WsFullFilmentMXStructArrayOfFicha
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructArrayOfFicha originally named ArrayOfFicha
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructArrayOfFicha extends WsFullFilmentMXWsdlClass
{
    /**
     * The Ficha
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var WsFullFilmentMXStructFicha
     */
    public $Ficha;
    /**
     * Constructor method for ArrayOfFicha
     * @see parent::__construct()
     * @param WsFullFilmentMXStructFicha $_ficha
     * @return WsFullFilmentMXStructArrayOfFicha
     */
    public function __construct($_ficha = NULL)
    {
        parent::__construct(array('Ficha'=>$_ficha),false);
    }
    /**
     * Get Ficha value
     * @return WsFullFilmentMXStructFicha|null
     */
    public function getFicha()
    {
        return $this->Ficha;
    }
    /**
     * Set Ficha value
     * @param WsFullFilmentMXStructFicha $_ficha the Ficha
     * @return WsFullFilmentMXStructFicha
     */
    public function setFicha($_ficha)
    {
        return ($this->Ficha = $_ficha);
    }
    /**
     * Returns the current element
     * @see WsFullFilmentMXWsdlClass::current()
     * @return WsFullFilmentMXStructFicha
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see WsFullFilmentMXWsdlClass::item()
     * @param int $_index
     * @return WsFullFilmentMXStructFicha
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see WsFullFilmentMXWsdlClass::first()
     * @return WsFullFilmentMXStructFicha
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see WsFullFilmentMXWsdlClass::last()
     * @return WsFullFilmentMXStructFicha
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see WsFullFilmentMXWsdlClass::last()
     * @param int $_offset
     * @return WsFullFilmentMXStructFicha
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see WsFullFilmentMXWsdlClass::getAttributeName()
     * @return string Ficha
     */
    public function getAttributeName()
    {
        return 'Ficha';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructArrayOfFicha
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
