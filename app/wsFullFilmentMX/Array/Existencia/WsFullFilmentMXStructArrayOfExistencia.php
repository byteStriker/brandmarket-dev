<?php
/**
 * File for class WsFullFilmentMXStructArrayOfExistencia
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructArrayOfExistencia originally named ArrayOfExistencia
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructArrayOfExistencia extends WsFullFilmentMXWsdlClass
{
    /**
     * The Existencia
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var WsFullFilmentMXStructExistencia
     */
    public $Existencia;
    /**
     * Constructor method for ArrayOfExistencia
     * @see parent::__construct()
     * @param WsFullFilmentMXStructExistencia $_existencia
     * @return WsFullFilmentMXStructArrayOfExistencia
     */
    public function __construct($_existencia = NULL)
    {
        parent::__construct(array('Existencia'=>$_existencia),false);
    }
    /**
     * Get Existencia value
     * @return WsFullFilmentMXStructExistencia|null
     */
    public function getExistencia()
    {
        return $this->Existencia;
    }
    /**
     * Set Existencia value
     * @param WsFullFilmentMXStructExistencia $_existencia the Existencia
     * @return WsFullFilmentMXStructExistencia
     */
    public function setExistencia($_existencia)
    {
        return ($this->Existencia = $_existencia);
    }
    /**
     * Returns the current element
     * @see WsFullFilmentMXWsdlClass::current()
     * @return WsFullFilmentMXStructExistencia
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see WsFullFilmentMXWsdlClass::item()
     * @param int $_index
     * @return WsFullFilmentMXStructExistencia
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see WsFullFilmentMXWsdlClass::first()
     * @return WsFullFilmentMXStructExistencia
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see WsFullFilmentMXWsdlClass::last()
     * @return WsFullFilmentMXStructExistencia
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see WsFullFilmentMXWsdlClass::last()
     * @param int $_offset
     * @return WsFullFilmentMXStructExistencia
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see WsFullFilmentMXWsdlClass::getAttributeName()
     * @return string Existencia
     */
    public function getAttributeName()
    {
        return 'Existencia';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructArrayOfExistencia
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
