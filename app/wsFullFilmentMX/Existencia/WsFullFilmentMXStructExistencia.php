<?php
/**
 * File for class WsFullFilmentMXStructExistencia
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructExistencia originally named Existencia
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructExistencia extends WsFullFilmentMXWsdlClass
{
    /**
     * The Almacen
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Almacen;
    /**
     * The Stok
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Stok;
    /**
     * Constructor method for Existencia
     * @see parent::__construct()
     * @param string $_almacen
     * @param string $_stok
     * @return WsFullFilmentMXStructExistencia
     */
    public function __construct($_almacen = NULL,$_stok = NULL)
    {
        parent::__construct(array('Almacen'=>$_almacen,'Stok'=>$_stok),false);
    }
    /**
     * Get Almacen value
     * @return string|null
     */
    public function getAlmacen()
    {
        return $this->Almacen;
    }
    /**
     * Set Almacen value
     * @param string $_almacen the Almacen
     * @return string
     */
    public function setAlmacen($_almacen)
    {
        return ($this->Almacen = $_almacen);
    }
    /**
     * Get Stok value
     * @return string|null
     */
    public function getStok()
    {
        return $this->Stok;
    }
    /**
     * Set Stok value
     * @param string $_stok the Stok
     * @return string
     */
    public function setStok($_stok)
    {
        return ($this->Stok = $_stok);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructExistencia
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
