<?php
/**
 * File for class WsFullFilmentMXStructColorsResponse
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructColorsResponse originally named colorsResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructColorsResponse extends WsFullFilmentMXWsdlClass
{
    /**
     * The colorsResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var WsFullFilmentMXStructArrayOfColores
     */
    public $colorsResult;
    /**
     * Constructor method for colorsResponse
     * @see parent::__construct()
     * @param WsFullFilmentMXStructArrayOfColores $_colorsResult
     * @return WsFullFilmentMXStructColorsResponse
     */
    public function __construct($_colorsResult = NULL)
    {
        parent::__construct(array('colorsResult'=>($_colorsResult instanceof WsFullFilmentMXStructArrayOfColores)?$_colorsResult:new WsFullFilmentMXStructArrayOfColores($_colorsResult)),false);
    }
    /**
     * Get colorsResult value
     * @return WsFullFilmentMXStructArrayOfColores|null
     */
    public function getColorsResult()
    {
        return $this->colorsResult;
    }
    /**
     * Set colorsResult value
     * @param WsFullFilmentMXStructArrayOfColores $_colorsResult the colorsResult
     * @return WsFullFilmentMXStructArrayOfColores
     */
    public function setColorsResult($_colorsResult)
    {
        return ($this->colorsResult = $_colorsResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructColorsResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
