<?php
/**
 * File for class WsFullFilmentMXServiceColors
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXServiceColors originally named Colors
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXServiceColors extends WsFullFilmentMXWsdlClass
{
    /**
     * Method to call the operation originally named colors
     * @uses WsFullFilmentMXWsdlClass::getSoapClient()
     * @uses WsFullFilmentMXWsdlClass::setResult()
     * @uses WsFullFilmentMXWsdlClass::saveLastError()
     * @param WsFullFilmentMXStructColors $_wsFullFilmentMXStructColors
     * @return WsFullFilmentMXStructColorsResponse
     */
    public function colors()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->colors());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see WsFullFilmentMXWsdlClass::getResult()
     * @return WsFullFilmentMXStructColorsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
