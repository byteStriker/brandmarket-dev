<?php
/**
 * File for the class which returns the class map definition
 * @package WsFullFilmentMX
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * Class which returns the class map definition by the static method WsFullFilmentMXClassMap::classMap()
 * @package WsFullFilmentMX
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS
     * @return array
     */
    final public static function classMap()
    {
        return array (
  'ArrayOfColores' => 'WsFullFilmentMXStructArrayOfColores',
  'ArrayOfExistencia' => 'WsFullFilmentMXStructArrayOfExistencia',
  'ArrayOfFicha' => 'WsFullFilmentMXStructArrayOfFicha',
  'Colores' => 'WsFullFilmentMXStructColores',
  'Existencia' => 'WsFullFilmentMXStructExistencia',
  'Ficha' => 'WsFullFilmentMXStructFicha',
  'colorItem' => 'WsFullFilmentMXStructColorItem',
  'colorItemResponse' => 'WsFullFilmentMXStructColorItemResponse',
  'colors' => 'WsFullFilmentMXStructColors',
  'colorsResponse' => 'WsFullFilmentMXStructColorsResponse',
  'existencias' => 'WsFullFilmentMXStructExistencias',
  'existenciasResponse' => 'WsFullFilmentMXStructExistenciasResponse',
  'fichaTecnica' => 'WsFullFilmentMXStructFichaTecnica',
  'fichaTecnicaResponse' => 'WsFullFilmentMXStructFichaTecnicaResponse',
);
    }
}
