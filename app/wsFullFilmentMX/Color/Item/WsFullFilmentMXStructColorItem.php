<?php
/**
 * File for class WsFullFilmentMXStructColorItem
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructColorItem originally named colorItem
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructColorItem extends WsFullFilmentMXWsdlClass
{
    /**
     * The codigo
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $codigo;
    /**
     * Constructor method for colorItem
     * @see parent::__construct()
     * @param string $_codigo
     * @return WsFullFilmentMXStructColorItem
     */
    public function __construct($_codigo = NULL)
    {
        parent::__construct(array('codigo'=>$_codigo),false);
    }
    /**
     * Get codigo value
     * @return string|null
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    /**
     * Set codigo value
     * @param string $_codigo the codigo
     * @return string
     */
    public function setCodigo($_codigo)
    {
        return ($this->codigo = $_codigo);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructColorItem
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
