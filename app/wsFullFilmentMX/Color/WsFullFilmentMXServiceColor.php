<?php
/**
 * File for class WsFullFilmentMXServiceColor
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXServiceColor originally named Color
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXServiceColor extends WsFullFilmentMXWsdlClass
{
    /**
     * Method to call the operation originally named colorItem
     * @uses WsFullFilmentMXWsdlClass::getSoapClient()
     * @uses WsFullFilmentMXWsdlClass::setResult()
     * @uses WsFullFilmentMXWsdlClass::saveLastError()
     * @param WsFullFilmentMXStructColorItem $_wsFullFilmentMXStructColorItem
     * @return WsFullFilmentMXStructColorItemResponse
     */
    public function colorItem(WsFullFilmentMXStructColorItem $_wsFullFilmentMXStructColorItem)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->colorItem($_wsFullFilmentMXStructColorItem));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see WsFullFilmentMXWsdlClass::getResult()
     * @return WsFullFilmentMXStructColorItemResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
