<?php
/**
 * File for class WsFullFilmentMXStructColores
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructColores originally named Colores
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructColores extends WsFullFilmentMXWsdlClass
{
    /**
     * The Color
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Color;
    /**
     * Constructor method for Colores
     * @see parent::__construct()
     * @param string $_color
     * @return WsFullFilmentMXStructColores
     */
    public function __construct($_color = NULL)
    {
        parent::__construct(array('Color'=>$_color),false);
    }
    /**
     * Get Color value
     * @return string|null
     */
    public function getColor()
    {
        return $this->Color;
    }
    /**
     * Set Color value
     * @param string $_color the Color
     * @return string
     */
    public function setColor($_color)
    {
        return ($this->Color = $_color);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructColores
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
