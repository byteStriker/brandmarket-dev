<?php
/**
 * File to load generated classes once at once time
 * @package WsFullFilmentMX
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * Includes for all generated classes files
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
require_once dirname(__FILE__) . '/WsFullFilmentMXWsdlClass.php';
require_once dirname(__FILE__) . '/Colors/Response/WsFullFilmentMXStructColorsResponse.php';
require_once dirname(__FILE__) . '/Colors/WsFullFilmentMXStructColors.php';
require_once dirname(__FILE__) . '/Ficha/Tecnica/WsFullFilmentMXStructFichaTecnica.php';
require_once dirname(__FILE__) . '/Ficha/Response/WsFullFilmentMXStructFichaTecnicaResponse.php';
require_once dirname(__FILE__) . '/Ficha/WsFullFilmentMXStructFicha.php';
require_once dirname(__FILE__) . '/Array/Ficha/WsFullFilmentMXStructArrayOfFicha.php';
require_once dirname(__FILE__) . '/Colores/WsFullFilmentMXStructColores.php';
require_once dirname(__FILE__) . '/Array/Colores/WsFullFilmentMXStructArrayOfColores.php';
require_once dirname(__FILE__) . '/Array/Existencia/WsFullFilmentMXStructArrayOfExistencia.php';
require_once dirname(__FILE__) . '/Existencias/Response/WsFullFilmentMXStructExistenciasResponse.php';
require_once dirname(__FILE__) . '/Existencia/WsFullFilmentMXStructExistencia.php';
require_once dirname(__FILE__) . '/Color/Item/WsFullFilmentMXStructColorItem.php';
require_once dirname(__FILE__) . '/Color/Response/WsFullFilmentMXStructColorItemResponse.php';
require_once dirname(__FILE__) . '/Existencias/WsFullFilmentMXStructExistencias.php';
require_once dirname(__FILE__) . '/Existencias/WsFullFilmentMXServiceExistencias.php';
require_once dirname(__FILE__) . '/Color/WsFullFilmentMXServiceColor.php';
require_once dirname(__FILE__) . '/Colors/WsFullFilmentMXServiceColors.php';
require_once dirname(__FILE__) . '/Ficha/WsFullFilmentMXServiceFicha.php';
require_once dirname(__FILE__) . '/WsFullFilmentMXClassMap.php';
