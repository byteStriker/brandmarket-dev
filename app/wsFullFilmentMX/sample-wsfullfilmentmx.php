<?php
/**
 * Test with WsFullFilmentMX for 'http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl'
 * @package WsFullFilmentMX
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
ini_set('memory_limit','512M');
ini_set('display_errors',true);
error_reporting(-1);
/**
 * Load autoload
 */
require_once dirname(__FILE__) . '/WsFullFilmentMXAutoload.php';
/**
 * Wsdl instanciation infos. By default, nothing has to be set.
 * If you wish to override the SoapClient's options, please refer to the sample below.
 * 
 * This is an associative array as:
 * - the key must be a WsFullFilmentMXWsdlClass constant beginning with WSDL_
 * - the value must be the corresponding key value
 * Each option matches the {@link http://www.php.net/manual/en/soapclient.soapclient.php} options
 * 
 * Here is below an example of how you can set the array:
 * $wsdl = array();
 * $wsdl[WsFullFilmentMXWsdlClass::WSDL_URL] = 'http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl';
 * $wsdl[WsFullFilmentMXWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
 * $wsdl[WsFullFilmentMXWsdlClass::WSDL_TRACE] = true;
 * $wsdl[WsFullFilmentMXWsdlClass::WSDL_LOGIN] = 'myLogin';
 * $wsdl[WsFullFilmentMXWsdlClass::WSDL_PASSWD] = '**********';
 * etc....
 * Then instantiate the Service class as: 
 * - $wsdlObject = new WsFullFilmentMXWsdlClass($wsdl);
 */
/**
 * Examples
 */


/***********************************************
 * Example for WsFullFilmentMXServiceExistencias
 */
$wsFullFilmentMXServiceExistencias = new WsFullFilmentMXServiceExistencias();
// sample call for WsFullFilmentMXServiceExistencias::existencias()
if($wsFullFilmentMXServiceExistencias->existencias(new WsFullFilmentMXStructExistencias(/*** update parameters list ***/)))
    print_r($wsFullFilmentMXServiceExistencias->getResult());
else
    print_r($wsFullFilmentMXServiceExistencias->getLastError());

/*****************************************
 * Example for WsFullFilmentMXServiceColor
 */
$wsFullFilmentMXServiceColor = new WsFullFilmentMXServiceColor();
// sample call for WsFullFilmentMXServiceColor::colorItem()
if($wsFullFilmentMXServiceColor->colorItem(new WsFullFilmentMXStructColorItem(/*** update parameters list ***/)))
    print_r($wsFullFilmentMXServiceColor->getResult());
else
    print_r($wsFullFilmentMXServiceColor->getLastError());

/******************************************
 * Example for WsFullFilmentMXServiceColors
 */
$wsFullFilmentMXServiceColors = new WsFullFilmentMXServiceColors();
// sample call for WsFullFilmentMXServiceColors::colors()
if($wsFullFilmentMXServiceColors->colors())
    print_r($wsFullFilmentMXServiceColors->getResult());
else
    print_r($wsFullFilmentMXServiceColors->getLastError());

/*****************************************
 * Example for WsFullFilmentMXServiceFicha
 */
$wsFullFilmentMXServiceFicha = new WsFullFilmentMXServiceFicha();
// sample call for WsFullFilmentMXServiceFicha::fichaTecnica()
if($wsFullFilmentMXServiceFicha->fichaTecnica(new WsFullFilmentMXStructFichaTecnica(/*** update parameters list ***/)))
    print_r($wsFullFilmentMXServiceFicha->getResult());
else
    print_r($wsFullFilmentMXServiceFicha->getLastError());
