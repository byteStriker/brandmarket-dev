<?php
/**
 * File for class WsFullFilmentMXStructExistencias
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructExistencias originally named existencias
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructExistencias extends WsFullFilmentMXWsdlClass
{
    /**
     * The codigo
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $codigo;
    /**
     * The distribuidor
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $distribuidor;
    /**
     * Constructor method for existencias
     * @see parent::__construct()
     * @param string $_codigo
     * @param string $_distribuidor
     * @return WsFullFilmentMXStructExistencias
     */
    public function __construct($_codigo = NULL,$_distribuidor = NULL)
    {
        parent::__construct(array('codigo'=>$_codigo,'distribuidor'=>$_distribuidor),false);
    }
    /**
     * Get codigo value
     * @return string|null
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    /**
     * Set codigo value
     * @param string $_codigo the codigo
     * @return string
     */
    public function setCodigo($_codigo)
    {
        return ($this->codigo = $_codigo);
    }
    /**
     * Get distribuidor value
     * @return string|null
     */
    public function getDistribuidor()
    {
        return $this->distribuidor;
    }
    /**
     * Set distribuidor value
     * @param string $_distribuidor the distribuidor
     * @return string
     */
    public function setDistribuidor($_distribuidor)
    {
        return ($this->distribuidor = $_distribuidor);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructExistencias
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
