<?php
/**
 * File for class WsFullFilmentMXStructExistenciasResponse
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructExistenciasResponse originally named existenciasResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructExistenciasResponse extends WsFullFilmentMXWsdlClass
{
    /**
     * The existenciasResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var WsFullFilmentMXStructArrayOfExistencia
     */
    public $existenciasResult;
    /**
     * Constructor method for existenciasResponse
     * @see parent::__construct()
     * @param WsFullFilmentMXStructArrayOfExistencia $_existenciasResult
     * @return WsFullFilmentMXStructExistenciasResponse
     */
    public function __construct($_existenciasResult = NULL)
    {
        parent::__construct(array('existenciasResult'=>($_existenciasResult instanceof WsFullFilmentMXStructArrayOfExistencia)?$_existenciasResult:new WsFullFilmentMXStructArrayOfExistencia($_existenciasResult)),false);
    }
    /**
     * Get existenciasResult value
     * @return WsFullFilmentMXStructArrayOfExistencia|null
     */
    public function getExistenciasResult()
    {
        return $this->existenciasResult;
    }
    /**
     * Set existenciasResult value
     * @param WsFullFilmentMXStructArrayOfExistencia $_existenciasResult the existenciasResult
     * @return WsFullFilmentMXStructArrayOfExistencia
     */
    public function setExistenciasResult($_existenciasResult)
    {
        return ($this->existenciasResult = $_existenciasResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructExistenciasResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
