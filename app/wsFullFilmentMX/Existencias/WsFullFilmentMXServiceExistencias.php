<?php
/**
 * File for class WsFullFilmentMXServiceExistencias
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXServiceExistencias originally named Existencias
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXServiceExistencias extends WsFullFilmentMXWsdlClass
{
    /**
     * Method to call the operation originally named existencias
     * @uses WsFullFilmentMXWsdlClass::getSoapClient()
     * @uses WsFullFilmentMXWsdlClass::setResult()
     * @uses WsFullFilmentMXWsdlClass::saveLastError()
     * @param WsFullFilmentMXStructExistencias $_wsFullFilmentMXStructExistencias
     * @return WsFullFilmentMXStructExistenciasResponse
     */
    public function existencias(WsFullFilmentMXStructExistencias $_wsFullFilmentMXStructExistencias)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->existencias($_wsFullFilmentMXStructExistencias));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see WsFullFilmentMXWsdlClass::getResult()
     * @return WsFullFilmentMXStructExistenciasResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
