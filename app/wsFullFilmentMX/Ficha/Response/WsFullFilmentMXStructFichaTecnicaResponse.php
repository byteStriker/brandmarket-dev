<?php
/**
 * File for class WsFullFilmentMXStructFichaTecnicaResponse
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructFichaTecnicaResponse originally named fichaTecnicaResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructFichaTecnicaResponse extends WsFullFilmentMXWsdlClass
{
    /**
     * The fichaTecnicaResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var WsFullFilmentMXStructArrayOfFicha
     */
    public $fichaTecnicaResult;
    /**
     * Constructor method for fichaTecnicaResponse
     * @see parent::__construct()
     * @param WsFullFilmentMXStructArrayOfFicha $_fichaTecnicaResult
     * @return WsFullFilmentMXStructFichaTecnicaResponse
     */
    public function __construct($_fichaTecnicaResult = NULL)
    {
        parent::__construct(array('fichaTecnicaResult'=>($_fichaTecnicaResult instanceof WsFullFilmentMXStructArrayOfFicha)?$_fichaTecnicaResult:new WsFullFilmentMXStructArrayOfFicha($_fichaTecnicaResult)),false);
    }
    /**
     * Get fichaTecnicaResult value
     * @return WsFullFilmentMXStructArrayOfFicha|null
     */
    public function getFichaTecnicaResult()
    {
        return $this->fichaTecnicaResult;
    }
    /**
     * Set fichaTecnicaResult value
     * @param WsFullFilmentMXStructArrayOfFicha $_fichaTecnicaResult the fichaTecnicaResult
     * @return WsFullFilmentMXStructArrayOfFicha
     */
    public function setFichaTecnicaResult($_fichaTecnicaResult)
    {
        return ($this->fichaTecnicaResult = $_fichaTecnicaResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructFichaTecnicaResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
