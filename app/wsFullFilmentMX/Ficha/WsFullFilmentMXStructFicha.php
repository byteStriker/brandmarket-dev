<?php
/**
 * File for class WsFullFilmentMXStructFicha
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXStructFicha originally named Ficha
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl}
 * @package WsFullFilmentMX
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXStructFicha extends WsFullFilmentMXWsdlClass
{
    /**
     * The Producto
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Producto;
    /**
     * The Des1
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Des1;
    /**
     * The Des2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Des2;
    /**
     * The Familia
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Familia;
    /**
     * The Color
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Color;
    /**
     * The Capacidad
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Capacidad;
    /**
     * The Material
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Material;
    /**
     * The Size
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Size;
    /**
     * The Empaque
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Empaque;
    /**
     * The Weight
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Weight;
    /**
     * The Height
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Height;
    /**
     * The Width
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Width;
    /**
     * The Length
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Length;
    /**
     * The Volumen
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Volumen;
    /**
     * The TecnicaImp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $TecnicaImp;
    /**
     * The AreaImp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $AreaImp;
    /**
     * Constructor method for Ficha
     * @see parent::__construct()
     * @param string $_producto
     * @param string $_des1
     * @param string $_des2
     * @param string $_familia
     * @param string $_color
     * @param string $_capacidad
     * @param string $_material
     * @param string $_size
     * @param string $_empaque
     * @param string $_weight
     * @param string $_height
     * @param string $_width
     * @param string $_length
     * @param string $_volumen
     * @param string $_tecnicaImp
     * @param string $_areaImp
     * @return WsFullFilmentMXStructFicha
     */
    public function __construct($_producto = NULL,$_des1 = NULL,$_des2 = NULL,$_familia = NULL,$_color = NULL,$_capacidad = NULL,$_material = NULL,$_size = NULL,$_empaque = NULL,$_weight = NULL,$_height = NULL,$_width = NULL,$_length = NULL,$_volumen = NULL,$_tecnicaImp = NULL,$_areaImp = NULL)
    {
        parent::__construct(array('Producto'=>$_producto,'Des1'=>$_des1,'Des2'=>$_des2,'Familia'=>$_familia,'Color'=>$_color,'Capacidad'=>$_capacidad,'Material'=>$_material,'Size'=>$_size,'Empaque'=>$_empaque,'Weight'=>$_weight,'Height'=>$_height,'Width'=>$_width,'Length'=>$_length,'Volumen'=>$_volumen,'TecnicaImp'=>$_tecnicaImp,'AreaImp'=>$_areaImp),false);
    }
    /**
     * Get Producto value
     * @return string|null
     */
    public function getProducto()
    {
        return $this->Producto;
    }
    /**
     * Set Producto value
     * @param string $_producto the Producto
     * @return string
     */
    public function setProducto($_producto)
    {
        return ($this->Producto = $_producto);
    }
    /**
     * Get Des1 value
     * @return string|null
     */
    public function getDes1()
    {
        return $this->Des1;
    }
    /**
     * Set Des1 value
     * @param string $_des1 the Des1
     * @return string
     */
    public function setDes1($_des1)
    {
        return ($this->Des1 = $_des1);
    }
    /**
     * Get Des2 value
     * @return string|null
     */
    public function getDes2()
    {
        return $this->Des2;
    }
    /**
     * Set Des2 value
     * @param string $_des2 the Des2
     * @return string
     */
    public function setDes2($_des2)
    {
        return ($this->Des2 = $_des2);
    }
    /**
     * Get Familia value
     * @return string|null
     */
    public function getFamilia()
    {
        return $this->Familia;
    }
    /**
     * Set Familia value
     * @param string $_familia the Familia
     * @return string
     */
    public function setFamilia($_familia)
    {
        return ($this->Familia = $_familia);
    }
    /**
     * Get Color value
     * @return string|null
     */
    public function getColor()
    {
        return $this->Color;
    }
    /**
     * Set Color value
     * @param string $_color the Color
     * @return string
     */
    public function setColor($_color)
    {
        return ($this->Color = $_color);
    }
    /**
     * Get Capacidad value
     * @return string|null
     */
    public function getCapacidad()
    {
        return $this->Capacidad;
    }
    /**
     * Set Capacidad value
     * @param string $_capacidad the Capacidad
     * @return string
     */
    public function setCapacidad($_capacidad)
    {
        return ($this->Capacidad = $_capacidad);
    }
    /**
     * Get Material value
     * @return string|null
     */
    public function getMaterial()
    {
        return $this->Material;
    }
    /**
     * Set Material value
     * @param string $_material the Material
     * @return string
     */
    public function setMaterial($_material)
    {
        return ($this->Material = $_material);
    }
    /**
     * Get Size value
     * @return string|null
     */
    public function getSize()
    {
        return $this->Size;
    }
    /**
     * Set Size value
     * @param string $_size the Size
     * @return string
     */
    public function setSize($_size)
    {
        return ($this->Size = $_size);
    }
    /**
     * Get Empaque value
     * @return string|null
     */
    public function getEmpaque()
    {
        return $this->Empaque;
    }
    /**
     * Set Empaque value
     * @param string $_empaque the Empaque
     * @return string
     */
    public function setEmpaque($_empaque)
    {
        return ($this->Empaque = $_empaque);
    }
    /**
     * Get Weight value
     * @return string|null
     */
    public function getWeight()
    {
        return $this->Weight;
    }
    /**
     * Set Weight value
     * @param string $_weight the Weight
     * @return string
     */
    public function setWeight($_weight)
    {
        return ($this->Weight = $_weight);
    }
    /**
     * Get Height value
     * @return string|null
     */
    public function getHeight()
    {
        return $this->Height;
    }
    /**
     * Set Height value
     * @param string $_height the Height
     * @return string
     */
    public function setHeight($_height)
    {
        return ($this->Height = $_height);
    }
    /**
     * Get Width value
     * @return string|null
     */
    public function getWidth()
    {
        return $this->Width;
    }
    /**
     * Set Width value
     * @param string $_width the Width
     * @return string
     */
    public function setWidth($_width)
    {
        return ($this->Width = $_width);
    }
    /**
     * Get Length value
     * @return string|null
     */
    public function getLength()
    {
        return $this->Length;
    }
    /**
     * Set Length value
     * @param string $_length the Length
     * @return string
     */
    public function setLength($_length)
    {
        return ($this->Length = $_length);
    }
    /**
     * Get Volumen value
     * @return string|null
     */
    public function getVolumen()
    {
        return $this->Volumen;
    }
    /**
     * Set Volumen value
     * @param string $_volumen the Volumen
     * @return string
     */
    public function setVolumen($_volumen)
    {
        return ($this->Volumen = $_volumen);
    }
    /**
     * Get TecnicaImp value
     * @return string|null
     */
    public function getTecnicaImp()
    {
        return $this->TecnicaImp;
    }
    /**
     * Set TecnicaImp value
     * @param string $_tecnicaImp the TecnicaImp
     * @return string
     */
    public function setTecnicaImp($_tecnicaImp)
    {
        return ($this->TecnicaImp = $_tecnicaImp);
    }
    /**
     * Get AreaImp value
     * @return string|null
     */
    public function getAreaImp()
    {
        return $this->AreaImp;
    }
    /**
     * Set AreaImp value
     * @param string $_areaImp the AreaImp
     * @return string
     */
    public function setAreaImp($_areaImp)
    {
        return ($this->AreaImp = $_areaImp);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see WsFullFilmentMXWsdlClass::__set_state()
     * @uses WsFullFilmentMXWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return WsFullFilmentMXStructFicha
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
