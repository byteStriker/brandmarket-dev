<?php
/**
 * File for class WsFullFilmentMXServiceFicha
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
/**
 * This class stands for WsFullFilmentMXServiceFicha originally named Ficha
 * @package WsFullFilmentMX
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2019-10-08
 */
class WsFullFilmentMXServiceFicha extends WsFullFilmentMXWsdlClass
{
    /**
     * Method to call the operation originally named fichaTecnica
     * @uses WsFullFilmentMXWsdlClass::getSoapClient()
     * @uses WsFullFilmentMXWsdlClass::setResult()
     * @uses WsFullFilmentMXWsdlClass::saveLastError()
     * @param WsFullFilmentMXStructFichaTecnica $_wsFullFilmentMXStructFichaTecnica
     * @return WsFullFilmentMXStructFichaTecnicaResponse
     */
    public function fichaTecnica(WsFullFilmentMXStructFichaTecnica $_wsFullFilmentMXStructFichaTecnica)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->fichaTecnica($_wsFullFilmentMXStructFichaTecnica));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see WsFullFilmentMXWsdlClass::getResult()
     * @return WsFullFilmentMXStructFichaTecnicaResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
