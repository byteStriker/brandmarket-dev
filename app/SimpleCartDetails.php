<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\SimpleCart;
use App\ProductsBrandMarket;
class SimpleCartDetails extends Model
{
  use SoftDeletes;
  protected $primaryKey = 'id';
  protected $table  = 'simple_cart_details';
  protected $fillable = ['simple_cart_id', 'products_brand_market_id', 'price', 'quantity', 'subtotal', 'discount', 'created_at'];

  public function Cart()
  {
    return $this->belongsTo(SimpleCart::class, 'simple_cart_id', 'id');
  }

    /**
     * Details
     *
     * relation for detail of products
     *
     * @param type var Description
     * @return return type
     */
    public function Product()
    {
      return $this->belongsTo(ProductsBrandMarket::class, 'products_brand_market_id', 'id');
      // return $this->hasOne(ProductsBrandMarket::class, 'products_brand_market_id', 'id');
    }


}
