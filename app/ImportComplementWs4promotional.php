<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportComplementWs4promotional extends Model
{
    protected $table = 'import_complement_ws_4promotional';
    protected $primaryKey = 'id';
    protected $fillable = ['description', 'images', 'color', 'article_name', 'is_promotion', 'category',
    'print_methods', 'width', 'price', 'web_color', 'web', 'print_area', 'is_new', 'subcategory',
    'id_article', 'height', 'is_available'
    ];
}
