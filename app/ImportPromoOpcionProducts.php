<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportPromoOpcionProducts extends Model
{
    //
    protected $table = 'import_promo_opcion_products';
    protected $fillable = ['page', 'code', 'clinders', 'price'];
}
