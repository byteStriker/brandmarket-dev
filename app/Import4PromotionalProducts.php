<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import4PromotionalProducts extends Model
{
    protected $table = 'import4_promotional_products';
    protected $primary_key = 'id';
    protected $fillable = [
        'model_color', 'model', 'name', 'description', 'color', 'is_new', 'product_size', 'material',
        'category', 'sub_category', 'blue_tint', 'capacity', 'printing_methods', 'printing_area', 'box_net_weight',
        'height', 'width', 'depth', 'pieces', 'slide_image', 'image_by_color', 'created_at', 'updated_at',
    ];
}
