<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsFourPromotional extends Model
{
    protected $table = 'import4_promotional_products';
    protected $fillable = ['id','model_color','model','name','description','color','is_new',
    'product_size','material','category','sub_category','blue_tint','capacity',
    'printing_methods','printing_area','box_net_weight','height',
    'width','depth','pieces','slide_image','image_by_color'
    ];
}
