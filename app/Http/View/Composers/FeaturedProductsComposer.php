<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
// use App\Repositories\UserRepository;
use App\ProductsBrandMarket;
use DB;

class FeaturedProductsComposer
{
  /**
   * The product brand master model implementation.
   *
   * @var ProductsBrandMarket
   */
  protected $products;

  /**
   * Create a new cart composer.
   *
   * @param  ProductsBrandMarket  $cart
   * @return void
   */
  public function __construct(ProductsBrandMarket $ProductsBrandMarket)
  {
    // Dependencies automatically resolved by service container...
    // $hasFeaturedProducts = $ProductsBrandMarket::where('is_featured', 'true')->count();//get();
    // \Log::info(__METHOD__ . "->" . __LINE__ . "->\$ProductsBrandMarket::where('is_featured', 'true')->count() " . $ProductsBrandMarket::where('is_featured', 'true')->toSql());
    if ($ProductsBrandMarket::where('is_featured', 'true')->count()) {
      $this->products = DB::table('products_brand_market')->leftJoin('categories', 'products_brand_market.category_id', '=', 'categories.id')
        ->leftJoin('subcategories', 'products_brand_market.subcategory_id', '=', 'subcategories.id')
        ->whereNull('products_brand_market.deleted_at')

        ->select(
          'products_brand_market.*',
          'categories.id as cat_id',
          'categories.name AS cat_name',
          'categories.slug AS cat_slug',
          'subcategories.id AS subcat_id',
          'subcategories.name AS subcat_name',
          'subcategories.slug AS subcat_slug'
        )
        ->where('is_featured', 'true')->orderBy('id')->groupBy('products_brand_market.slug')->limit(5)->inRandomOrder()->get();
    }
  }

  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    // \Log::info(__METHOD__ . "->" . __LINE__ . "->");
    $view->with('featuredProducts', $this->products);
  }
}
