<?php

namespace App\Http\View\Composers;

use App\SimpleCart;
// use App\Repositories\UserRepository;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

use DB;
use Illuminate\View\View;

class CartComposer
{
    /**
     * The cart model implmentation
     *
     * @var Cart
     */
    protected $myCart;
    protected $myCartDetails;
    protected $myWishList;
    protected $myWishListDetails;
    protected $exchangeRate;
    protected $cartCount;
    /**
     * Create a new cart composer.
     *
     * @param  SimpleCart  $cart
     * @return void
     */
    public function __construct(SimpleCart $cart)
    {

        if (auth()->user()) {
            # code...
            // dd($cart->where('status', 'open')->where('user_id', auth()->user()->id)->count());
            if ($cart->where('status', 'open')->where('user_id', auth()->user()->id)->count()) {
                // \Log::info(__METHOD__ . "->" . __LINE__ . "->\$cartCount: {$cartCount}");
                $this->myCart = $cart->with('Details')->where('status', 'open')->where('user_id', auth()->user()->id)->first();
                $this->myCartDetails = DB::table('simple_carts')
                    ->leftJoin('simple_cart_details', 'simple_cart_details.simple_cart_id', '=', 'simple_carts.id')
                    ->leftJoin('products_brand_market', 'simple_cart_details.products_brand_market_id', '=', 'products_brand_market.id')
                    ->where('simple_carts.status', 'open')
                    ->where('simple_carts.user_id', auth()->user()->id)
                    ->whereNull('simple_cart_details.deleted_at')
                    ->whereNull('products_brand_market.deleted_at')
                    ->select(
                        'simple_carts.user_id',
                        'simple_carts.wishlist_name',
                        'simple_carts.total',
                        'simple_carts.freight_amount',
                        'simple_carts.id AS sc_id',
                        'simple_carts.id AS id',
                        'simple_carts.subtotal',
                        'simple_carts.status',
                        'simple_cart_details.name AS scd_product_name',
                        'simple_cart_details.price AS scd_price',
                        'simple_cart_details.quantity AS scd_quantity',
                        'simple_cart_details.subtotal AS scd_subtotal',
                        'simple_cart_details.id AS scd_id',
                        'simple_carts.created_at',
                        'simple_cart_details.discount AS scd_discount',
                        'products_brand_market.name AS pbm_name',
                        'simple_cart_details.original_price',
                        'products_brand_market.description AS pbm_description',
                        'products_brand_market.image AS pbm_image',
                        'products_brand_market.provider_name AS pbm_provider_name',
                        'products_brand_market.color AS pbm_color',
                        'products_brand_market.colors AS pbm_colors',
                        'simple_cart_details.name AS scd_name',
                        'simple_cart_details.color AS scd_color',
                        'products_brand_market.existence AS pbm_existence',
                        'products_brand_market.stock as pbm_stock'
                    )
                    ->get();
                // dd($this->myCartDetails);
                // $this->cartCount = $cart->where('status', 'open')->where('user_id', auth()->user()->id)->count();
                $this->cartCount = DB::table('simple_carts')
                    ->leftJoin('simple_cart_details', 'simple_cart_details.simple_cart_id', '=', 'simple_carts.id')
                    ->leftJoin('products_brand_market', 'simple_cart_details.products_brand_market_id', '=', 'products_brand_market.id')
                    ->where('simple_carts.status', 'open')
                    ->where('simple_carts.user_id', auth()->user()->id)
                    ->whereNull('simple_cart_details.deleted_at')
                    ->whereNull('products_brand_market.deleted_at')
                    ->selectRaw('COUNT(*) AS cartCount')
                    ->first()->cartCount;
                // print($this->cartCount->cartCount);
            }
            if ($cart->where('status', 'wishlist')->where('user_id', auth()->user()->id)->get()) {
                $this->myWishList = $cart->where('status', 'wishlist')->where('user_id', auth()->user()->id)->get();
            }
            // calling api in order to get the exchange rate amount
            $this->getExchangeRate();
        } else {
            // \Log::error(__METHOD__ . "->" . __LINE__ . ": unable to load information, the user hasn't been logged in ");
        }
    }

    /**
     * getExchangeRate
     *
     * Get Currency Conversion from Banxico API
     *
     * @param type var Description
     * @return return array
     */
    public function getExchangeRate()
    {
        // dd(storage_path());
        try {
            $currencyRateFileName = md5(date('y-m-d') . '-conversion-rate.json');
            if (!\Storage::exists($currencyRateFileName)) {

                $httpClient = new GuzzleClient();
                // See token info at: https://www.banxico.org.mx/SieAPIRest/service/v1/token
                $token = config('app.banxico_token');
                // Get catalog series from https://www.banxico.org.mx/SieAPIRest/service/v1/doc/catalogoSeries#
                $catalogs = [
                    'SF43787', // currency exchange rate USD to 48 hours: OPENING
                    'SF43786', // currency exchange rate USD to 48 hours: CLOSING
                ];
                $series = implode($catalogs, ',');
                $query = 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/' . $series . '/datos/oportuno?token=' . $token;
                $request = $httpClient->request('GET', $query);
                if ($request->getStatusCode() == '200' && $request->getReasonPhrase() == 'OK') {
                    // print('\$request->getReasonPhrase(): ' . $request->getReasonPhrase() . "<br/>");
                    $response = $request->getBody();
                    $content = $response->getContents();
                    // print("<pre>responseContent: {$content}</pre>");
                    $exchange = json_decode($content);

                    // $a = var_export($content);
                    $a = json_encode($content);
                    // print("Lo que viene en la letra AAAA " . $a . " <br/>");
                    \Storage::put($currencyRateFileName, (string) $content);
                    list($exchangeRateBuy, $exchangeRateSell) = [$exchange->bmx->series[0]->datos[0]->dato, $exchange->bmx->series[1]->datos[0]->dato];

                    $this->exchangeRate = $exchangeRateSell;
                    // \Log::info("exchangeRate ", $exchangeRateSell);
                } else {
                    $this->getExchangeRate();
                }
            } else {
                $currencyExchangeRate = \Storage::get($currencyRateFileName);
                $exchange = json_decode($currencyExchangeRate);
                list($exchangeRateBuy, $exchangeRateSell) = [$exchange->bmx->series[0]->datos[0]->dato, $exchange->bmx->series[1]->datos[0]->dato];
                $this->exchangeRate = $exchangeRateSell;
                // \Log::info(__METHOD__ . "->" . __LINE__ . "   *** exchangeRate " . $exchangeRateSell);
            }
        } catch (RequestException $e) {
            \Log::info('"This is the exception: " . Psr7\str($e->getRequest()) ' . "This is the exception: " . Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                \Log::info(' "This is the response of has response" . Psr7\str($e->getResponse()) ' . Psr7\str($e->getResponse()));
            }
            // But we need to call it back for recursion
            $this->getExchangeRate();
        }
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // dd($this->myWishList);
        $view->with('myCart', $this->myCart)
            ->with('myCartDetails', $this->myCartDetails)
            ->with('myWishList', $this->myWishList)
            ->with('myWishListDetails', $this->myWishListDetails)
            ->with('exchangeRate', $this->exchangeRate)
            ->with('cartCount', $this->cartCount);
    }
}
