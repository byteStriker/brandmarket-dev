<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
// use App\Repositories\UserRepository;
use App\Category;
use App\Subcategory;
use App\User;

class AllProductsOptionsComposer
{
  /**
   * All the options for send the frontend.* views;
   *
   * @var $options
   */

  protected $options = [];
  protected $nav_category = [];
  protected $nav_subcategories = [];

  /**
   * For all header-views
   *
   * @param  Category $category
   * @return void
   */
  public function __construct(Category $category)
  {
    // dd(request()->route()->getPrefix());
    if (request()->route()->getPrefix() != '/admin') {
      // dd(request()->route()->getPrefix());

      // Dependencies automatically resolved by service container...
      if (Category::all()->count()) {
        $this->options = Category::all();
      }
      // now, lets check if request()->category is setted and not empty
      if (isset(request()->category)) {
        if (!empty(request()->category)) {


          // \Log::info(__CLASS__."->".__METHOD__."->".__LINE__.": request()->category ".request()->category);
          $this->nav_category = Category::where('slug', request()->category)->first();
          $this->nav_subcategories = Subcategory::where('category_id', $this->nav_category->id)->get();
        }
      }
    }
  }

  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    // dd(request()->input());
    $view->with('allProductsOptions', $this->options)
      ->with('nav_subcategories', $this->nav_subcategories)
      ->with('nav_category', $this->nav_category);
  }
}
