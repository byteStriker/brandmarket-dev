<?php

namespace App\Http\Traits;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

trait ExchangeRateTrait {

    /**
     * getExchangeRate
     *
     * Get Currency Conversion from Banxico API
     *
     * @param type var Description
     * @return return array
     */
    public function getExchangeRate()
    {
        // dd(storage_path());
        try {
            $currencyRateFileName = md5(date('y-m-d') . '-conversion-rate.json');
            if (!\Storage::exists($currencyRateFileName)) {

                $httpClient = new GuzzleClient();
                // See token info at: https://www.banxico.org.mx/SieAPIRest/service/v1/token
                $token = config('app.banxico_token');
                // Get catalog series from https://www.banxico.org.mx/SieAPIRest/service/v1/doc/catalogoSeries#
                $catalogs = [
                    'SF43787', // currency exchange rate USD to 48 hours: OPENING
                    'SF43786', // currency exchange rate USD to 48 hours: CLOSING
                ];
                $series = implode($catalogs, ',');
                $query = 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/' . $series . '/datos/oportuno?token=' . $token;
                $request = $httpClient->request('GET', $query);
                if ($request->getStatusCode() == '200' && $request->getReasonPhrase() == 'OK') {
                    // print('\$request->getReasonPhrase(): ' . $request->getReasonPhrase() . "<br/>");
                    $response = $request->getBody();
                    $content = $response->getContents();
                    // print("<pre>responseContent: {$content}</pre>");
                    $exchange = json_decode($content);

                    // $a = var_export($content);
                    $a = json_encode($content);
                    // print("Lo que viene en la letra AAAA " . $a . " <br/>");
                    \Storage::put($currencyRateFileName, (string) $content);
                    list($exchangeRateBuy, $exchangeRateSell) = [$exchange->bmx->series[0]->datos[0]->dato, $exchange->bmx->series[1]->datos[0]->dato];
                    return [$exchangeRateBuy, $exchangeRateSell];
                } else {
                    $this->getExchangeRate();
                }
            } else {
                $currencyExchangeRate = \Storage::get($currencyRateFileName);
                // print($currencyExchangeRate);
                $exchange = json_decode($currencyExchangeRate);
                // print_r($exchange);

                list($exchangeRateBuy, $exchangeRateSell) = [$exchange->bmx->series[0]->datos[0]->dato, $exchange->bmx->series[1]->datos[0]->dato];
                return [$exchangeRateBuy, $exchangeRateSell];
                // ver si se puede dejar este botón como un schedule.

            }

        } catch (RequestException $e) {
            \Log::info('"This is the exception: " . Psr7\str($e->getRequest()) ' . "This is the exception: " . Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                \Log::info(' "This is the response of has response" . Psr7\str($e->getResponse()) ' . Psr7\str($e->getResponse()));
            }
            // But we need to call it back for recursion
            $this->getExchangeRate();
        }

    }

}
