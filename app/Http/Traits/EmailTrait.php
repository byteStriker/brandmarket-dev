<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use App\SimpleCart;

trait EmailTrait
{

    /**
     * mailingTester
     *
     * mailing logic
     *
     * @return return type
     */
    public function sendEmails(Request $request, SimpleCart $SimpleCart)
    {

        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $time = date('h:i:s');
        $timezone = date_default_timezone_get();
        // $provider_name = '';
        $internalCode = $SimpleCart->uuid;

        // $starttime = microtime(true);
        // Client Quotation
        {

            // @block for client quotation

            $getClientQuotation = \DB::table('simple_carts as sc')
                ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name, scd.id ')
                ->selectRaw('scd.original_price as d_original_price, scd.subtotal_mxn as d_subtotal_mx, scd.price as d_price,  scd.subtotal as d_subtotal')
                ->selectRaw('sc.total as total_usd, sc.subtotal as subtotal_usd, scd.provider_name , pbm.image')
                ->selectRaw('sc.total_mxn as total_mxn, sc.subtotal_mxn as subtotal_mxn ')
                ->selectRaw('pbm.category_doble_vela as product_parent_category')
                ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
                ->join('products_brand_market as pbm', 'pbm.id', '=', 'scd.products_brand_market_id')
                ->where('sc.user_id', auth()->user()->id)
                ->where('sc.id', $SimpleCart->id)
                ->where('sc.status', 'open')
                // ->where('scd.provider_name', 'doble-vela')
                ->groupBy('scd.id', 'scd.model', 'scd.name', 'scd.color', 'scd.provider_name')
                ->orderBy('scd.created_at');
            // print("<pre>\$getClientQuotation:  {$getClientQuotation->toSql()}</pre>");
            $simpleCart = $getClientQuotation->get();

            $clientQuotationInfo = [
                'page_title' => 'Brand Market',
                'simpleCart' => $simpleCart,
                'internalCode' => $internalCode,
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'time' => $time,
                'timezone' => $timezone,
                // 'provider_name' => '4-promotiona',
                'client_name' => strtoupper(auth()->user()->name),
                'client_number' => auth()->user()->id,
            ];
            // return (new \App\Mail\ClientQuotationMailer($clientQuotationInfo))->render();
            if (\App::environment(['local', 'staging', 'dev'])) {
                \Mail::to('g33k.gu@gmail.com')
                    ->send(new \App\Mail\ClientQuotationMailer($clientQuotationInfo));
            } else {
                \Mail::
                to(auth()->user()->email)
                // to('gogarcia@brandmarket.com.mx')
                    ->bcc('g33k.gu@gmail.com')
                    ->send(new \App\Mail\ClientQuotationMailer($clientQuotationInfo));
            }
        }
        // // Provider Quotation
        {
            // @Block for provider quotation:
            // grouping products in simple cart details by provider name;
            $getProviderQuotationDobleVela = \DB::table('simple_carts as sc')
                ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name, scd.id ')
                ->selectRaw('scd.original_price as d_original_price, scd.subtotal_mxn as d_subtotal_mx, scd.price as d_price,  scd.subtotal as d_subtotal')
                ->selectRaw('sc.total as total_usd, sc.subtotal as subtotal_usd, scd.provider_name ')
                ->selectRaw('sc.total_mxn as total_mxn, sc.subtotal_mxn as subtotal_mxn ')
                ->selectRaw('pbm.category_doble_vela as product_parent_category')
                ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
                ->join('products_brand_market as pbm', 'pbm.id', '=', 'scd.products_brand_market_id')
                ->where('sc.user_id', auth()->user()->id)
                ->where('sc.id', $SimpleCart->id)
                ->where('sc.status', 'open')
                ->where('scd.provider_name', 'doble-vela')
                ->groupBy('scd.id', 'scd.model', 'scd.name', 'scd.color', 'scd.provider_name')
                ->orderBy('scd.created_at');
            // print("<pre>\$getProviderQuotationDobleVela:  {$getProviderQuotationDobleVela->toSql()}</pre>");
            $simpleCart = $getProviderQuotationDobleVela->get();
            // print("<pre>\$productsProviderDobleVela: " . count($productsProviderDobleVela) . "</pre>");
            $dobleVelaQuotation = [
                'page_title' => 'Brand Market',
                'simpleCart' => $simpleCart,
                'internalCode' => $internalCode,
                // 'internalCode' => $simpleCart[0]->uuid,
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'time' => $time,
                'timezone' => $timezone,
                'provider_name' => 'doble-vela',
                'client_name' => auth()->user()->name,
                'client_number' => auth()->user()->id,
            ];
            if (\App::environment(['local', 'staging', 'dev'])) {
                // return (new \App\Mail\ProviderQuotationMailer($dobleVelaQuotation))->render();
                \Mail::to('g33k.gu@gmail.com')
                // to('gogarcia@brandmarket.com.mx')
                // ->bcc('g33k.gu@gmail.com')
                // ->cc(auth()->user()->email)
                ->send(new \App\Mail\ProviderQuotationMailer($dobleVelaQuotation));
            } else {
                \Mail::to('gogarcia@brandmarket.com.mx')
                ->bcc('g33k.gu@gmail.com')
                // ->cc(auth()->user()->email)
                ->send(new \App\Mail\ProviderQuotationMailer($dobleVelaQuotation));
            }


            // @Block for provider quotation:
            // grouping products in simple cart details by provider name;
            $getProviderQuotation4Promotional = \DB::table('simple_carts as sc')
                ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name, scd.id ')
                ->selectRaw('scd.original_price as d_original_price, scd.subtotal_mxn as d_subtotal_mx, scd.price as d_price,  scd.subtotal as d_subtotal')
                ->selectRaw('sc.total as total_usd, sc.subtotal as subtotal_usd, scd.provider_name, pbm.image ')
                ->selectRaw('sc.total_mxn as total_mxn, sc.subtotal_mxn as subtotal_mxn ')
                ->selectRaw('pbm.category_doble_vela as product_parent_category')
                ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
                ->join('products_brand_market as pbm', 'pbm.id', '=', 'scd.products_brand_market_id')
                ->where('sc.user_id', auth()->user()->id)
                ->where('sc.id', $SimpleCart->id)
                ->where('sc.status', 'open')
                ->where('scd.provider_name', '4-promotional')
                ->groupBy('scd.id', 'scd.model', 'scd.name', 'scd.color', 'scd.provider_name')
                ->orderBy('scd.created_at');
            // print("<pre>\$getProviderQuotation4Promotional:  {$getProviderQuotation4Promotional->toSql()}</pre>");
            $simpleCart = $getProviderQuotation4Promotional->get();
            // print("<pre>\$productsProviderDobleVela: " . count($productsProviderDobleVela) . "</pre>");
            $fourPromotionalQuotation = [
                'page_title' => 'Brand Market',
                'simpleCart' => $simpleCart,
                'internalCode' => $internalCode,
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'time' => $time,
                'timezone' => $timezone,
                'provider_name' => '4-promotional',
                'client_name' => auth()->user()->name,
                'client_number' => auth()->user()->id,
            ];
            // return (new \App\Mail\ProviderQuotationMailer($fourPromotionalQuotation))->render();
            if (\App::environment(['local', 'staging', 'dev'])) {
                \Mail::to('g33k.gu@gmail.com')
                    ->send(new \App\Mail\ProviderQuotationMailer($fourPromotionalQuotation));
            } else {
                \Mail::
                to('gogarcia@brandmarket.com.mx')
                    ->bcc('g33k.gu@gmail.com')
                    ->send(new \App\Mail\ProviderQuotationMailer($fourPromotionalQuotation));
            }
        }
        // internal quotation
        // {
        //     // @BLOCK BRAND MARKET
        //     // grouping products in simple cart details by provider name;
        //     $getBrandMarketQuotation = \DB::table('simple_carts as sc')
        //         ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name, scd.id ')
        //         ->selectRaw('scd.original_price as d_original_price, scd.subtotal_mxn as d_subtotal_mx, scd.price as d_price,  scd.subtotal as d_subtotal')
        //         ->selectRaw('pbm.category_doble_vela as product_parent_category')
        //         ->selectRaw('sc.total as total_usd, sc.subtotal as subtotal_usd, scd.provider_name ')
        //         ->selectRaw('sc.total_mxn as total_mxn, sc.subtotal_mxn as subtotal_mxn ')
        //         ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
        //         ->join('products_brand_market as pbm', 'pbm.id', '=', 'scd.products_brand_market_id')
        //         ->where('sc.user_id', auth()->user()->id)
        //         ->where('sc.id', $SimpleCart->id)
        //         ->where('sc.status', 'open')
        //         ->groupBy('scd.id', 'scd.provider_name')
        //         ->orderBy('scd.provider_name');
        //     $simpleCart = $getBrandMarketQuotation->get();
        //     // print("<pre>\$getBrandMarketQuotation:  {$getBrandMarketQuotation->toSql()}</pre>");
        //     // print("<pre>\$simpleCart: " . count($simpleCart) . "</pre>");
        //     $brandMarketQuotation = [
        //         'page_title' => 'Brand Market',
        //         'simpleCart' => $simpleCart,
        //         'internalCode' => $internalCode,
        //         'year' => $year,
        //         'month' => $month,
        //         'day' => $day,
        //         'time' => $time,
        //         'timezone' => $timezone,
        //         'client_name' => auth()->user()->name,
        //         'client_number' => auth()->user()->id,
        //     ];
        //     // return (new \App\Mail\BrandMarketQuotationMailer($brandMarketQuotation))->render();
        //     \Mail::to('g33k.gu@gmail.com')
        //         // ->cc('gonzgarcia@icloud.com')
        //         ->send(new \App\Mail\BrandMarketQuotationMailer($brandMarketQuotation));
        //     // // return view('frontend.mailing.internal-quotation', $brandMarketQuotation);
        //     // return dd("STUFF");

        // }
        // return @redirect(@route('account.home'));
    }
}
