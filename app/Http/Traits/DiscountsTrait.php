<?php
namespace App\Http\Traits;
use App\Http\Traits\ExchangeRateTrait;

trait DiscountsTrait
{
    use ExchangeRateTrait;
    /**
     * getDiscount
     *
     * get the discount json file for the passed provider
     *
     * @param String $provider_name The provider for this product
     * @return Object
     **/
    public function getDiscount(String $provider_name = null)
    {
        switch (strtolower($provider_name)) {
            case '4-promotional':
                $jsonString = file_get_contents(base_path('resources/provider-sources/discount-4-promotional.json'));
                $data = json_decode($jsonString, true);
                return collect($data);
                break;
            case 'doble-vela':
                $jsonString = file_get_contents(base_path('resources/provider-sources/discount-doble-vela.json'));
                $data = json_decode($jsonString, true);
                return collect($data);

                break;
            case 'promo-opcion':
                $jsonString = file_get_contents(base_path('resources/provider-sources/discount-doble-vela.json'));
                $data = json_decode($jsonString, true);
                return collect($data);
                break;

            default:
                # code...
                break;
        }
    }


    /**
     * getPriceWithDiscount
     *
     * test
     *
     * @return type
     * @throws conditon
     **/
    public function getPriceWithDiscount($provider_name, $quantity, $origPrice)
    {
        $providerDiscount = $this->getDiscount($provider_name);
        $quantity = $quantity;
        $origPrice = $origPrice;
        $shipperTotalPrice = $quantity * $origPrice;
        // \Log::info("\$shipperTotalPrice: {$shipperTotalPrice}");
        $discountFromProvider = 0;
        $rateUtility = floatval(1.20);
        $tax = floatval(1.16);
        $redeem = floatval(0.35);
        // while ($rateExchangeAmount = $this->getExchangeRate() == null) {
        //     Log::info("stuff");
        // }
        // dd($providerDiscount);
        $rateExchangeAmount = $this->getExchangeRate();
        // \Log::info("\$rateExchangeAmount: " . print_r($rateExchangeAmount, 1));
        $sellPrice = $rateExchangeAmount[1];
        // \Log::info("\$sellPrice: " . $sellPrice);

        foreach ($providerDiscount as $discountRow) {
            $a = $discountRow['amount'];
            if ($shipperTotalPrice >= $a['from'] && $shipperTotalPrice <= $a['to']) {
                $discountFromProvider = $discountRow['discount_percentaje'];
                break;
            }
        }

        $shipperDiscount = floatval($shipperTotalPrice) * floatval($discountFromProvider);
        // \Log::info("\$shipperDiscount: {$shipperDiscount}");
        $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
        // \Log::info("\$tempPrice: {$tempPrice}");
        $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
        // \Log::info("\$shipperPricePlusUtility: {$shipperPricePlusUtility}");
        $priceWithTax = floatval($shipperPricePlusUtility) * floatval($tax);
        // \Log::info("\$priceWithTax: {$priceWithTax}");
        $priceTemp = (floatval($priceWithTax) / (floatval($sellPrice) - floatval($redeem)));
        // \Log::info("\$priceTemp: {$priceTemp}");
        $priceWithDiscount = floatval($priceTemp) / (floatval($quantity));
        // \Log::info("\$priceWithDiscount: {$priceWithDiscount}");
        $dataDiscount = [];
        $dataDiscount['priceWithDiscount'] = floatval($priceWithDiscount);
        $dataDiscount['percentaje'] = floatval($discountFromProvider);
        $dataDiscount['rateExchangeAmount'] = floatVal($rateExchangeAmount);
        $dataDiscount['priceMXN'] = floatVal($priceWithTax) / (floatval($quantity));
        // dd($dataDiscount);
        return $dataDiscount;
    }


}
