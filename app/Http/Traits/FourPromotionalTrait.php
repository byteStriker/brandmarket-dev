<?php

namespace App\Http\Traits;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

trait FourPromotionalTrait
{

  /**
   * getFullDB
   *
   * get Full database from ws
   *
   * @param Type $var Description
   * @return type
   * @throws conditon
   **/
  public function getFullDB()
  {

    $dbFileName = 'four-promotional.json';

    $httpClient = new GuzzleClient();
    $hasDB = false;
    $query = 'http://forpromotional.homelinux.com:9090/WsEstrategia/inventario';
    $request = $httpClient->request('GET', $query);
    if ($request->getStatusCode() == '200' && $request->getReasonPhrase() == 'OK') {
      // print('\$request->getReasonPhrase(): ' . $request->getReasonPhrase() . "<br/>");
      $response = $request->getBody();
      $content = $response->getContents();
      // print("<pre>responseContent: {$content}</pre>");
      // $exchange = json_decode($content);
      // $a = var_export($content);
      // $a = json_encode($content);
      // \Log::info("this is the content of request: " . print_r($a, 1));
      // print_r($a);
      // print("Lo que viene en la letra AAAA " . $a . " <br/>");
      \Storage::put("providers-db/{$dbFileName}", (string) $content);
      // list($exchangeRateBuy, $exchangeRateSell) = [$exchange->bmx->series[0]->datos[0]->dato, $exchange->bmx->series[1]->datos[0]->dato];
      // return [$exchangeRateBuy, $exchangeRateSell];
      $hasDB = true;
    }
    if ($hasDB) {
      $this->getPrice();
    }
  }

  /**
   * getPrice
   *
   * match the product from brand market to db WS
   * updates the price of brand market product
   * @return type
   **/
  public function getPrice()
  {
    // $dbFileName = 'providers-db/four-promotional.json';
    $dbFileName = 'four-promotional.json';


    $db4Promotional = \Storage::get("providers-db/{$dbFileName}");

    // print($db4Promotional);
    $json = json_decode($db4Promotional);
    // print_r($json);
    // $rows = collect($json);
    // dd($json[0]);
    $count = 1;
    foreach ($json as $rowDb) {
      if (isset($rowDb->nombre_articulo)) {
        // print("<pre>\$rowDb->nombre_articulo: " . strtoupper($rowDb->nombre_articulo));
        // print("   ->id_article: " . strtoupper($rowDb->id_articulo));
        // print("   ->disponible: " . strtoupper($rowDb->disponible));
        // print("   ->precio: " . strtoupper($rowDb->precio));
        // print("   ->color: " . strtoupper($rowDb->color));
        // print("   ->categoria: " . strtoupper($rowDb->categoria));
        // print("   ->sub_categoria: " . strtoupper($rowDb->sub_categoria));
        // print("</pre>");
        $productName = strtoupper($rowDb->nombre_articulo);
        $productDescription = strtoupper($rowDb->descripcion);
        $articleId = strtoupper($rowDb->id_articulo);
        // print("<pre>\$articleId: ${articleId}</pre>");
        $productColor = strtoupper($rowDb->color);
        $productPrice = $rowDb->precio;
        $productPieces = $rowDb->disponible;
        if (
          $ProductsBrandMarket = \App\ProductsBrandMarket::where('provider_name', '4-promotional')
          // ->where('name', $productName)
          ->where('description', $productDescription)
          // ->orWhere('article_id', \Str::slug($articleId))
          ->orWhere('article_id', $articleId)
          ->where('color', $productColor)
          // ->where('color', $productColor)
          ->first()
        ) {
          // print("<pre>\$productName: {$productName} --- there is a match here {$ProductsBrandMarket->model} ->color: {$ProductsBrandMarket->color}</pre>");
          $ProductsBrandMarket->pieces = $productPieces;
          $ProductsBrandMarket->color = $productColor;
          $ProductsBrandMarket->original_price = $productPrice;
          $ProductsBrandMarket->save();
        }

        // else if (
        // 	$ProductsBrandMarket = ProductsBrandMarket::where('provider_name', '4-promotional')
        // 	->where('model', $articleId)
        // 	->where('color', $productColor)
        // 	// ->where('color', $productColor)
        // 	->first()
        // ) {
        // 	print("<pre>******** by article id::: {$articleId} there is a match here {$ProductsBrandMarket->model} ->color: {$ProductsBrandMarket->color}</pre>");
        // 	$ProductsBrandMarket->pieces = $productPieces;
        // 	$ProductsBrandMarket->color = $productColor;
        // 	$ProductsBrandMarket->original_price = $productPrice;
        // 	$ProductsBrandMarket->save();
        // }
      } else {
        // dd($rowDb);
        // print("<pre>THIS GUY IS COMMITING FELONY: ");
        // print("   ->descripcion: " . strtoupper($rowDb->descripcion));
        // print("   ->piezas: " . strtoupper($rowDb->disponible));
        // print("   ->precio: " . strtoupper($rowDb->precio));
        // print("   ->color: " . strtoupper($rowDb->color));
        // print("   ->categoria: " . strtoupper($rowDb->categoria));
        // print("   ->sub_categoria: " . strtoupper($rowDb->sub_categoria));
        // print("</pre>");
      }
      $count++;
    }
    // print("<pre>\$count: {$count}</pre>");
  }
}
