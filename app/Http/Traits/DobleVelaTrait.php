<?php

namespace App\Http\Traits;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

trait DobleVelaTrait
{


    /**
     * _setCategorySubcategoryBrandMarket
     *
     * Determines which is the best category/subcategory to fit the product in
     *
     * @param String $category category
     * @param String $subcategory subcategory
     * @return Array
     **/
    public function setCategorySubcategoryBrandMarket(String $category = null, String $subcategory = null)
    {
        // \Log::info(__METHOD__ . "    {$category} => {$subcategory}");
        // Bolsas, Maletas Y Textiles    ACCESORIOS AUTO
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('BOLSAS, MALETAS Y TEXTILES')) {
                if (strtoupper(trim(rtrim($subcategory))) == 'ACCESORIOS AUTO') {
                    // 12 Textiles
                    // 21 chamarras-chalecos-y-sudaderas
                    return [12, 21];
                }
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Bolsas')) {
                    // 12 Textiles
                    // 20 bolsas-y-morrales
                    return [12, 20];
                }
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Gorras')) {
                    // 12 Textiles
                    // 22 gorras
                    return [12, 22];
                }

                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Hieleras Y Loncheras')) {
                    // 7 Hieleras y loncheras
                    // NULL
                    return [7, null];
                }

                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Maletas') || strtoupper(trim(rtrim($subcategory))) == strtoupper('Maletines')) {
                    // 12 Textiles
                    // 24 Maletas
                    return [12, 24];
                }

                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Mochilas')) {
                    // 12 Textiles
                    // 25 Mochilas
                    return [12, 25];
                }

                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Paraguas E Impermeables')) {
                    // 11 Paraguas e Impermeables
                    // NULL
                    return [11, null];
                }
            }
            // Salud Y Belleza
            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud Y Belleza')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Bolsas')) {
                    // 12 Textiles
                    // 20 bolsas-y-morrales
                    return [12, 20];
                }
            }
            // SUBLIMACIÓN
            if (strtoupper(trim(rtrim($category))) == strtoupper('SUBLIMACIÓN')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Gorras') || strtoupper(trim(rtrim($subcategory))) == strtoupper('Textil')) {
                    // 12 Textiles
                    // 22 gorras
                    return [12, 22];
                }
            }
        }

        // BOLÍGRAFOS
        {

            if (strtoupper(trim(rtrim($category))) == strtoupper('ESCRITURA Y MÁS')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('BOLÍGRAFOS ECOLÓGICOS')) {
                    // 3 BOLÍGRAFOS
                    // 8 ecológicos
                    return [3, 8];
                }

                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('BOLÍGRAFOS METÁLICOS') || strtoupper(trim(rtrim($subcategory))) == strtoupper('METALICO')) {
                    // 3 BOLÍGRAFOS
                    // 7 METÁLICOS
                    return [3, 7];
                }

                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('LAPICES')) {
                    // 3 BOLÍGRAFOS
                    // 10 lapices-y-repuestos
                    return [3, 10];
                }


                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('BOLÍGRAFOS MULTIFUNCION')) {
                    // 3 BOLÍGRAFOS
                    // 9 MULTIFUNCIONALES
                    return [3, 9];
                }

                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('BOLÍGRAFO Resaltador') || strtoupper(trim('BOLÍGRAFOS PLÁSTICOS'))) {
                    // 3 BOLÍGRAFOS
                    // 6 PLÁSTICOS
                    return [3, 6];
                }

                // GENERALES DE OFICINA
                if (strtoupper(trim(rtrim($category))) == strtoupper('ESCRITURA Y MÁS')) {
                    if (
                        strtoupper(trim(rtrim($subcategory))) == strtoupper('ESTUCHES DE REGALO')
                        || strtoupper(trim(rtrim($subcategory))) == strtoupper('EXHIBIDORES')
                        || strtoupper(trim(rtrim($subcategory))) == strtoupper('SET DE BOLIGRAFOS')
                    ) {
                        // 10 OFICINA
                        // 16 generales-de-OFICINA
                        return [10, 16];
                    }
                }


                // AGENDAS Y CARPETAS
                if (strtoupper(trim(rtrim($category))) == strtoupper('ESCRITURA Y MÁS')) {
                    if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Carpetas')) {
                        // 10 OFICINA
                        // 13 agendas-y-carpetas
                        return [10, 13];
                    }
                }
            }
            // if (strtoupper(trim(rtrim($category))) == strtoupper('ESCRITURA Y MÁS')) {
            //     if (strtoupper(trim(rtrim($subcategory))) == strtoupper('BOLÍGRAFOS METÁLICOS') || strtoupper(trim(rtrim($subcategory))) == 'Metalico') {
            //         // 3 BOLÍGRAFOS
            //         // 7 METÁLICOS
            //         return [3, 7];
            //     }
            // }

            // if (strtoupper(trim(rtrim($category))) == strtoupper('ESCRITURA Y MÁS')) {
            //     if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Lapices')) {
            //         // 3 BOLÍGRAFOS
            //         // 10 lapices-y-repuestos
            //         return [3, 10];
            //     }
            // }


            // if (strtoupper(trim(rtrim($category))) == strtoupper('ESCRITURA Y MÁS')) {
            //     if (strtoupper(trim(rtrim($subcategory))) == strtoupper('BOLÍGRAFO Resaltador') || strtoupper(trim('BOLÍGRAFOS PLÁSTICOS'))) {
            //         // 3 BOLÍGRAFOS
            //         // 6 PLÁSTICOS
            //         return [3, 6];
            //     }
            // }


        }

        // OFICINA
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('OFICINA')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Agendas')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Carpetas')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Libretas')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Portafolios Y Carpetas')

                ) {
                    // 10 OFICINA
                    // 13 agendas-y-carpetas
                    return [10, 13];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('LLAVEROS, LINTERNAS Y HERRAMIE')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Pins')) {
                    // 10 OFICINA
                    // 16 generales-de-OFICINA
                    return [10, 16];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('OFICINA')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De Escritorio')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Calculadoras')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Memos Y Adhesivos')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Porta Documentos')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Portagafetes')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Tarjeteros')

                ) {
                    // 10 OFICINA
                    // 13 agendas-y-carpetas
                    return [10, 13];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('SUBLIMACIÓN')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Azulejos')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Decorativos')
                ) {
                    // 10 OFICINA
                    // 16 generales-de-OFICINA
                    return [10, 16];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('TECNOLOGÍA')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De Escritorio')
                ) {
                    // 10 OFICINA
                    // 16 generales-de-OFICINA
                    return [10, 16];
                }
            }
            // LLAVEROS
            if (strtoupper(trim(rtrim($category))) == strtoupper('Llaveros, Linternas Y Herramie')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Llavero Madera')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Llaveros Luminosos')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Llaveros METÁLICOS')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Llaveros PLÁSTICOS')
                ) {
                    // 10 OFICINA
                    // 15 LLAVEROS
                    return [10, 15];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud y Belleza')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Llaveros METÁLICOS')) {
                    // 10 OFICINA
                    // 15 LLAVEROS
                    return [10, 15];
                }
            }
            //RELOJES
            if (strtoupper(trim(rtrim($category))) == strtoupper('OFICINA')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Relojes')) {
                    // 10 OFICINA
                    // 18 RELOJES
                    return [10, 18];
                }
            }
            //ANTIESTRÉS
            if (strtoupper(trim(rtrim($category))) == strtoupper('SALUD Y BELLEZA')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('ANTIESTRES')) {
                    // 10 OFICINA
                    // 19 ANTIEST´res
                    return [10, 19];
                }
            }
        }

        // TECNOLOGIA
        {
            // accesorios-de-computo
            if (strtoupper(trim(rtrim($category))) == strtoupper('SUBLIMACIÓN')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De CÓMPUTO')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Varios')
                ) {
                    // 13 TECNOLOGÍA
                    // 27 accesorios-de-computo
                    return [10, 27];
                }
            }
            // accesorios-de-computo
            if (strtoupper(trim(rtrim($category))) == strtoupper('TECNOLOGÍA')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De CÓMPUTO')) {
                    // 13 TECNOLOGÍA
                    // 27 accesorios-de-computo
                    return [10, 27];
                }
            }
            // accesorios-de-computo
            if (strtoupper(trim(rtrim($category))) == strtoupper('Hogar Y Estilo De Vida')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('ACCESORIOS AUTO')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De Escritorio')
                ) {
                    // 13 TECNOLOGÍA
                    // 27 accesorios-de-computo
                    return [10, 27];
                }
            }

            // accesorios-smartphone-y-tablets
            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud Y Belleza')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Celular Y Tablet')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De Escritorio')
                ) {
                    // 13 TECNOLOGÍA
                    // 28 aaccesorios-smartphone-y-tablets
                    return [10, 28];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('SUBLIMACIÓN')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Fundas')) {
                    // 13 TECNOLOGÍA
                    // 28 aaccesorios-smartphone-y-tablets
                    return [10, 28];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('TECNOLOGÍA')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Celular Y Tablet')) {
                    // 13 TECNOLOGÍA
                    // 28 aaccesorios-smartphone-y-tablets
                    return [10, 28];
                }
            }
            // audio
            if (strtoupper(trim(rtrim($category))) == strtoupper('TECNOLOGÍA')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Audifonos Y Bocinas')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Audífonos Y Bocinas')
                ) {
                    // 13 TECNOLOGÍA
                    // 29 audio
                    return [10, 29];
                }
            }
            // powerbanks
            if (strtoupper(trim(rtrim($category))) == strtoupper('TECNOLOGÍA')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Cargadores')) {
                    // 13 TECNOLOGÍA
                    // 30 power bank
                    return [10, 30];
                }
            }
            // usbs
            if (strtoupper(trim(rtrim($category))) == strtoupper('TECNOLOGÍA')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Usb')) {
                    // 13 TECNOLOGÍA
                    // 31 usbs
                    return [10, 31];
                }
            }
        }

        // VIAJE
        {
            // viaje
            if (strtoupper(trim(rtrim($category))) == strtoupper('VIAJE Y RECREACIÓN')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Para Viaje')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Brazaletes')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Cuidado Personal')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Recreacion')
                ) {
                    // 14
                    // NULL
                    return [14, null];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud Y Belleza')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Para Viaje')) {
                    // 14
                    // NULL
                    return [14, null];
                }
            }

            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud Y Belleza')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Para Viaje')) {
                    // 14
                    // NULL
                    return [14, null];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('Hogar Y Estilo De Vida')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Para Viaje')) {
                    // 14
                    // NULL
                    return [14, null];
                }
            }
        }

        // Bar & Gourmet
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('Hogar Y Estilo De Vida')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De Cocina')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Para Hogar')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Para Vino')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Bbq')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Distincion Y Gusto')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Gourmet')
                ) {
                    // 1 - bar & gourmet
                    // NULL
                    return [1, null];
                }
            }

            if (strtoupper(trim(rtrim($category))) == strtoupper('Llaveros, Linternas Y Herramie')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios Para Vino')) {
                    // 1 - bar & gourmet
                    // NULL
                    return [1, null];
                }
            }

            if (strtoupper(trim(rtrim($category))) == strtoupper('TAZAS, TERMOS Y CILINDROS')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Accesorios De Cocina')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Gourmet')
                ) {
                    // 1 - bar & gourmet
                    // NULL
                    return [1, null];
                }
            }
        }

        // cuidado-personal
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud Y Belleza')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Salud')) {
                    // 4 - cuidado-personal
                    // 11 - salud
                    return [4, 11];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud Y Belleza')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Belleza')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Cuidado Personal')
                ) {
                    // 4 - cuidado-personal
                    // 12 - belleza
                    return [4, 12];
                }
            }

            if (strtoupper(trim(rtrim($category))) == strtoupper('Llaveros, Linternas Y Herramie')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Cuidado Personal')) {
                    // 4 - cuidado-personal
                    // 12 - belleza
                    return [4, 12];
                }
            }
        }

        // herramientas
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('Hogar Y Estilo De Vida')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Linternas')) {
                    // 6 - herramietnas
                    return [6, null];
                }
            }

            if (strtoupper(trim(rtrim($category))) == strtoupper('Llaveros, Linternas Y Herramie')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('ACCESORIOS AUTO')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Flexometro')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Herramientas')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Linternas')
                ) {
                    // 6 - herramietnas
                    return [6, null];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('Salud Y Belleza')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Linternas')) {
                    // 4 - cuidado-personal
                    // 12 - belleza
                    return [4, 12];
                }
            }
        }

        // mascotas
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('Hogar Y Estilo De Vida')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Mascotas')) {
                    // 8 - mascotas
                    return [8, null];
                }
            }
        }

        // niños
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('Kids')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Alcancias')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Juegos')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Peluche')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Recreo')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Set De Boligrafos')
                ) {
                    // 9 - niños

                    return [9, null];
                }
            }
        }

        // Bebidas
        {
            if (strtoupper(trim(rtrim($category))) == strtoupper('SUBLIMACIÓN')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Cilindros')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Vasos')
                ) {
                    // 2 - bebidas
                    // 4 - vasos
                    return [2, 4];
                }
            }

            if (strtoupper(trim(rtrim($category))) == strtoupper('TAZAS, TERMOS Y CILINDROS')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('Cilindros')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Libre 1')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Repuestos')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Varios')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('Vasos')

                ) {
                    // 2 - bebidas
                    // 4 - vasos
                    return [2, 4];
                }
            }

            // tazas
            if (strtoupper(trim(rtrim($category))) == strtoupper('SUBLIMACIÓN')) {
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('TAZAS Y TARROS')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('TAZAS Y TERMOS')
                ) {
                    // 2 - bebidas
                    // 3 - tazas
                    return [2, 3];
                }
            }
            //
            if (strtoupper(trim(rtrim($category))) == strtoupper('TAZAS, TERMOS Y CILINDROS')) {
                // *** ESTE NO ESTABA!
                if (
                    strtoupper(trim(rtrim($subcategory))) == strtoupper('TAZAS Y TARROS')
                    || strtoupper(trim(rtrim($subcategory))) == strtoupper('TAZAS Y TERMOS')
                ) {
                    // 2 - bebidas
                    // 3 - tazas
                    return [2, 3];
                }
            }
            if (strtoupper(trim(rtrim($category))) == strtoupper('TECNOLOGÍA')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('TAZAS Y TERMOS')) {
                    // 2 - bebidas
                    // 3 - tazas
                    return [2, 3];
                }
            }

            // termos
            if (strtoupper(trim(rtrim($category))) == strtoupper('SUBLIMACIÓN')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Termos')) {
                    // 2 - bebidas
                    // 5 - termos
                    return [2, 5];
                }
            }

            if (strtoupper(trim(rtrim($category))) == strtoupper('TAZAS, TERMOS Y CILINDROS')) {
                if (strtoupper(trim(rtrim($subcategory))) == strtoupper('Termos')) {
                    // 2 - bebidas
                    // 5 - termos
                    return [2, 5];
                }
            }
        }
    }

    /**
     * getImagesDobleVela
     *
     * Sends a post request to 4promotional's ws.
     *
     * @param String $code Product Code
     * @return type
     * @throws conditon
     **/
    public function getImagesDobleVela(String $code = null)
    {
        try {
            // $url = $FourPromotionalConfig->server_url;
            $httpClient = new GuzzleClient();
            $key = '5Dp/qvDrxtWqg8powRrOzA==';
            $url = 'http://srv-datos.dyndns.info/doblevela/service.asmx/GetrProdImagenes';
            $url .= "?Codigo={'CLAVES':['${code}']}";
            $url .= "&Key={$key}";
            $response = $httpClient->request('GET', $url);
            $xml = simplexml_load_string($response->getBody()->getContents());
            $return = json_decode($xml[0]);
            if ($return->intCodigo == 0) {
                if (count($return->Resultado->MODELOS) >= 1) {
                    return $return->Resultado->MODELOS[0]->encodedImage;
                }
            }
            return false;
        } catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }


    /**
     * getExistenciasAll
     *
     * Get raw response with a json object.
     * That response will be parsed and used to update brand market products
     * This method is part of the "initial load" for the entire catalog
     *
     * @return type
     * @throws conditon
     **/
    public function getExistenciasAll()
    {
        $key = '5Dp/qvDrxtWqg8powRrOzA==';

        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> fetching db json formatted from doble vela\n");
        $url = 'http://srv-datos.dyndns.info/doblevela/service.asmx/GetExistenciaAll';
        // $url .= "?Codigo={'CLAVES':[\"" . implode('","', $missingImages) . "\"]}";
        $url .= "?Key={$key}";
        // \Log::info("<code>{$url}</code>");
        // dd($missingImages);
        $httpClient = new GuzzleClient();
        $response = $httpClient->request('GET', $url);
        $xml = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_PARSEHUGE);
        $return = json_decode($xml[0]);
        if ($return->intCodigo == 0) {
            if (count($return->Resultado) >= 1) {
                foreach ($return->Resultado  as $model) {
                    // \Log::info("Looking for: {$model->MODELO}");
                    // if($product = \DB::table('products_brand_market')->where('model', 'like',"{$model->MODELO}%")){
                    if ($product = \App\ProductsBrandMarket::where('model', 'like', "{$model->MODELO}%")) {
                        if ($specificModel = $product->get()) {
                            foreach ($specificModel as $r) {

                                $r->original_price = $model->Price;

                                $r->save();
                                // \Log::info("\$r->id {$r->id} / {$r->model} / {$r->stock} ");
                            }
                        } else {
                            // \Log::error("\$r->id {$r->id} / {$r->model} ** NOT FOUND");
                        }
                    }
                }
            }
        }
    }


    /**
     * updateStockAndPrice
     *
     * Get stock for selected product
     *
     **/
    public function updateStockAndPrice()
    {
        $key = '5Dp/qvDrxtWqg8powRrOzA==';
        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> fetching product price for every single row in db:\n");
        $rows = \App\ProductsBrandMarket::where('provider_name', 'Doble-Vela')->orderBy('model', 'ASC');
        if ($rows->count() > 0) {
            // dd($missingImages);
            $httpClient = new GuzzleClient();
            $updated = 1;
            $outdated = [];
            foreach ($rows->get() as $r) {
                $url = 'http://srv-datos.dyndns.info/doblevela/service.asmx/GetExistencia';
                $url .= "?Codigo=" . strtoupper($r->article_id);
                // $url .= "?Codigo={$row->article_id}";
                $url .= "&Key={$key}";
                // \Log("<code>{$url}</code><br/>");
                $response = $httpClient->request('GET', $url);
                $xml = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_PARSEHUGE);
                $return = json_decode($xml[0]);
                if ($return->intCodigo == 0) {
                    if (count($return->Resultado) >= 1) {
                        // // \Log::info("price for:  [{$r->article_id}] / {$r->model}  " . print_r($return->Resultado, 1));
                        foreach ($return->Resultado as $response) {

                            // // \Log::info("price for:  [{$response->MODELO}] / {$response->Price}  / {$response->Color}");
                            if ($ProductDobleVela = \App\ProductsBrandMarket::where('article_id', '=', $response->MODELO)->where('color', '=', $response->COLOR)->first()) {
                                // \Log::info(__METHOD__ . "->" . __LINE__ . "-> stuff: " . print_r($ProductDobleVela, 1));
                                $ProductDobleVela->stock = 0;
                                $ProductDobleVela->original_price = $response->Price;
                                $ProductDobleVela->stock = $response->EXISTENCIAS;
                                // // \Log::info(__METHOD__ . "->" . __LINE__ . "->ProductDobleVela: " . print_r($ProductDobleVela, 1));
                                $ProductDobleVela->save();
                                $updated++;
                                // // \Log::info("<code>OK Color:[$ProductDobleVela->color] Price[{$ProductDobleVela->original_price}] Model[{$ProductDobleVela->model}]: {$url}</code>");
                                // \Log::info("<code>OK Color:[$ProductDobleVela->color] Price[{$ProductDobleVela->original_price}] Model[{$ProductDobleVela->model}] Stock[{$ProductDobleVela->stock}]</code>");
                            } else {
                                // \Log::error("<code>Model[{$response->MODELO}] {$url}</code>");
                            }
                        }
                    } else {
                        array_push($outdated, $r->article_id);
                    }
                }
            }
            // \Log::info("<pre> \$updated = {$updated}; <br/> \$outdated = " . count($outdated) . "; </pre>");
        }
    }


    /**
     * updateStockAndPrice
     *
     * Get stock for selected product
     *
     **/
    public function updateStockAndPriceByModel($model)
    {
        $key = '5Dp/qvDrxtWqg8powRrOzA==';
        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> fetching product price for a single row in db: {$model}\n");
        $rows = \App\ProductsBrandMarket::where('provider_name', 'Doble-Vela')->where('model', $model)->orderBy('model', 'ASC');
        if ($rows->count() > 0) {
            // dd($missingImages);
            $httpClient = new GuzzleClient();
            $updated = [];
            $outdated = [];
            $url = 'http://srv-datos.dyndns.info/doblevela/service.asmx/GetExistencia';
            $url .= "?Codigo=" . strtoupper($model);
            // $url .= "?Codigo={$row->article_id}";
            $url .= "&Key={$key}";
            // \Log("<code>{$url}</code><br/>");
            $response = $httpClient->request('GET', $url);
            $xml = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_PARSEHUGE);
            $return = json_decode($xml[0]);
            if ($return->intCodigo == 0) {
                if (count($return->Resultado) >= 1) {
                    // // \Log::info("price for:  [{$r->article_id}] / {$r->model}  " . print_r($return->Resultado, 1));
                    foreach ($return->Resultado as $response) {

                        // // \Log::info("price for:  [{$response->MODELO}] / {$response->Price}  / {$response->Color}");
                        if ($ProductDobleVela = \App\ProductsBrandMarket::where('article_id', '=', $response->MODELO)->where('color', '=', $response->COLOR)->first()) {
                            $ProductDobleVela->original_price = $response->Price;
                            $ProductDobleVela->stock = $response->EXISTENCIAS;
                            // // \Log::info(__METHOD__ . "->" . __LINE__ . "->ProductDobleVela: " . print_r($ProductDobleVela, 1));
                            $ProductDobleVela->save();
                            // $updated++;
                            array_push($updated, $ProductDobleVela);
                            // // \Log::info("<code>OK Color:[$ProductDobleVela->color] Price[{$ProductDobleVela->original_price}] Model[{$ProductDobleVela->model}]: {$url}</code>");
                            // \Log::info("<code>OK {$ProductDobleVela->id} Color:[$ProductDobleVela->color] Price[{$ProductDobleVela->original_price}] Model[{$ProductDobleVela->model}]</code>");
                        } else {
                            // \Log::error("<code>Model[{$response->MODELO}] {$url}</code>");
                        }
                    }
                } else {
                    array_push($outdated, $model);
                }
            }
            return [$updated, $outdated];
            // \Log::info("<pre> \$updated = {$updated}; <br/> \$outdated = " . count($outdated) . "; </pre>");
        }
    }
}
