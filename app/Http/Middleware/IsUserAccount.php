<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsUserAccount
{

    /**
     * getUserType
     *
     * User type
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function getUserType()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> auth()->user(): " . auth()->user()->email);
        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> this->getUserType(): {$this->getUserType()}");
        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> isAdmin: " . Auth::user()->isAdmin());
        if ($this->getUserType() == 0) { // 0 for user
            return $next($request);
        } else  if ($this->getUserType() == 1) { // 1 for admin
            return redirect(@route('admin.dashboard'));
        } else {
            return redirect('/login');
        }
    }
}
