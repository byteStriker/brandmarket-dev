<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{

    /**
     * getUserType
     *
     * User type
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function getUserType()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> isAdmin: " . Auth::user()->isAdmin());
        if ($this->getUserType() == 1) { // 1 is for ADMIN
            return $next($request);
        }
        // else  if ($this->getUserType() == 0) { // 0 is for USERs
        //     return redirect('/account/home');
        // }

        else {
            return redirect('/login');
        }
    }
}
