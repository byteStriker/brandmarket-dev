<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Route;
use Str;

class UserController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    // $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $rows = User::where('name', '<>', 'admin')->paginate(10, ['id', 'name']);
    $columns = ['Nombre de la Usuario'];
    return view('backoffice.user.index', [
      'rows' => $rows, 'columns' => $columns,
      'page_title' => $this->title . ' - Usuarios', 'message' => 'Aquí podrás consultar, editar y borrar las categorías de los productos de la plataforma'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('backoffice.user.create', [
      'page_title' => "{$this->title} - Agregar", 'message' => 'Usuario'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // dd($request->input());
    $validatedData = $request->validate([
      'name' => ['required', 'string', 'max:255'],
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);

    if (!$validatedData) {
      return redirect(route('user.create'))->withErrors($validatedData, 'create');
    }
    $row = new User([
      'name' => $request['name'],
      'email' => $request['email'],
      'password' => Hash::make($request['password'])
      // 'api_token' => Str::random(60)
    ]);
    $row->api_token = Str::random(60);
    // \Log::info(__METHOD__ . "->" . __LINE__ . "-> user token is: {$row->api_token} ");
    $row->save();
    $row->roles()->attach(Role::where('name', 'user')->first());

    $redirectTo = [];
    $action = $request->action;
    switch ($action) {
      case 'saveAddOther':
        $redirectTo['route_name'] = 'user.create';
        $redirectTo['params'] = [];
        break;
      default:
        $redirectTo['route_name'] = 'user.show';
        $redirectTo['params'] = $row;
        break;
    }
    return redirect(route($redirectTo['route_name'], $redirectTo['params']))->with('success', 'Registro Agregado');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {

    $row = User::where('id', $user->id)->first();
    return view('backoffice.user.show', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name}", 'message' => 'Usuario'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function edit(User $user)
  {
    $row = User::findOrFail($user->id);
    return view('backoffice.user.edit', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name}", 'message' => 'Usuario'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, User $user)
  {
    // dd([$request->email, $user]);
    if ($user->email != $request->email) {
      $validatedData = \Validator::make($request->all(), [
        'email' => 'required|unique:users|max:190', ['email' => 'El :attribute es requerido'], ['email' => 'correo'],
      ]);
      if ($validatedData->fails()) {
        return redirect(route('user.edit'))->withErrors($validatedData, 'update')->withInput($request->input());
      }
      $user->email = $request->email;
    }

    if ($user->password != $request->password) {
      $new_pass = Hash::make($request->password);
      if ($new_pass != $user->password) {
        $user->password = $new_pass;
      }
    }
    $user->name = $request->name;

    // dd($user);
    $user->save();

    return redirect(route('user.show', $user))->with('success', 'Usuario actualizada');
  }

  /**
   * Display the specified resource to confirm deletion
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function confirmDelete(User $user)
  {
    // dd($user);
    $row = User::findOrFail($user->id);
    return view('backoffice.user.delete', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name} [delete]", 'message' => 'Eliminar Usuario'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    $row = User::findOrFail($user->id);
    $user->roles()->detach(Role::where('name', 'user')->first());

    $row->delete();
    return redirect(route('user.index'))->with('success', 'Row destroyed');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function myProfile(Request $request)
  {
    $row = User::findOrFail(auth()->user()->id);
    return view('backoffice.user.profile', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name}", 'message' => 'Usuario'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function updateMyProfile(Request $request)
  {
    $row = User::findOrFail(auth()->user()->id);
    $row->name = $request->name;
    $row->email = $request->email;

    if ($row->password != $request->password) {
      $new_pass = Hash::make($request->password);
      if ($new_pass != $row->password) {
        $row->password = $new_pass;
      }
    }
    $row->save();
    $row->roles()->attach(Role::where('name', 'user')->first());
    return view('backoffice.user.profile', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name}", 'message' => 'Usuario'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }
}
