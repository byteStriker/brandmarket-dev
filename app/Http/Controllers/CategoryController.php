<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Route;
use Storage;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel Category';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {

  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $rows = Category::with('Subcategories')->paginate(25);
    $columns = ['Nombre de la Categoría'];
    return view('backoffice.category.index', ['rows'=>$rows, 'columns' => $columns,
    'page_title'=>$this->title.' - Category', 'message' => 'Aquí podrás consultar, editar y borrar las categorías de los productos de la plataforma'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('backoffice.category.create', [
      'page_title'=>"{$this->title} - Agregar", 'message' => 'Categoría'])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());

  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $validatedData = $request->validate(['name' => 'required|unique:categories|max:190',['name'=>'El :attribute es requerido']]);
    if (!$validatedData) {
      return redirect(route('category.create'))->withErrors($validatedData, 'create');
    }
    $row = new Category();
    $row->name = $request->name;
    $row->slug = Str::slug($request->name);
    if ($request->hasfile('cover_image')) {
      $destinationPath = "";
      $filename = $request->file('cover_image')->hashName();
      $request->file('cover_image')->store('public');
      $row->cover_image = "{$filename}";
    }

    $row->save();
    $redirectTo = [];
    $action = $request->action;
    switch ($action) {
      case 'saveAddOther':
      $redirectTo['route_name'] = 'category.create';
      $redirectTo['params'] = [];
      break;
      case 'saveAddSubCategory':
      $redirectTo['route_name'] = 'subcategory.create';
      $redirectTo['params'] = ['category'=>$row];
      break;
      default:
      $redirectTo['route_name'] = 'category.show';
      $redirectTo['params'] = ['category'=>$row];
      break;
    }
    return redirect(route($redirectTo['route_name'], $redirectTo['params']))->with('success', 'Registro Agregado');
    // return redirect(route('category.show', ['category'=>$row]))->with('success', 'Categoría actualizada');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Category  $category
  * @return \Illuminate\Http\Response
  */
  public function show(Category $category)
  {
    $row = Category::where('id', $category->id)->first();
    return view('backoffice.category.show', ['row'=>$row,
    'page_title'=>"{$this->title} - {$row->name}", 'message' => 'Categoría'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Category  $category
  * @return \Illuminate\Http\Response
  */
  public function edit(Category $category)
  {
    $row = Category::findOrFail($category->id);
    return view('backoffice.category.edit', ['row'=>$row,
    'page_title'=>"{$this->title} - {$row->name}", 'message' => 'Categoría'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Category  $category
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request)
  {
    $row = Category::findOrFail($request->id);
    if ($row->name != $request->name) {
      $validatedData = $request->validate(['name' => 'required|unique:categories|max:190',['name'=>'El :attribute es requerido'], ['name' => 'nombre']]);
      if (!$validatedData) {
        return redirect(route('category.edit'))->withErrors($validatedData, 'update')->withInput($request->all());
      }
      $row->name = $request->name;
    }
    $row->slug = Str::slug($row->name);
    if ($request->hasfile('cover_image')) {
      $filename = $request->file('cover_image')->hashName();
      if ($filename != $row->cover_image) {
        if(Storage::disk('public')->exists($row->cover_image)){
          Storage::disk('public')->delete($row->cover_image);
        }
      }
      $request->file('cover_image')->store('public');
      $row->cover_image = "{$filename}";
    }
    $row->save();
    return redirect(route('category.show', $row))->with('success', 'Categoría actualizada');
  }

  /**
  * Display the specified resource to confirm deletion
  *
  * @param  \App\Category  $category
  * @return \Illuminate\Http\Response
  */
  public function confirmDelete(Category $category)
  {
    // dd($category);
    $row = Category::findOrFail($category->id);
    return view('backoffice.category.delete', ['row'=>$row,
    'page_title'=>"{$this->title} - {$row->name} [delete]", 'message' => 'Eliminar Categoría'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Category  $category
  * @return \Illuminate\Http\Response
  */
  public function destroy(Category $category)
  {
    $row = Category::findOrFail($category->id);
    if(Storage::disk('public')->exists($row->cover_image)){
      Storage::disk('public')->delete($row->cover_image);
    }
    $row->delete();
    return redirect(route('category.index'))->with('success', 'Row destroyed');
  }


}
