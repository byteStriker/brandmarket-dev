<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Category;
use App\Product;
use App\SimpleCart;
use App\SimpleCartDetails;
use App\Subcategory;

use App\Http\Traits\DiscountsTrait;
use App\Http\Traits\DobleVelaTrait;
// use App\Http\Traits\ExchangeRateTrait;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class PublicController extends Controller
{
    use DobleVelaTrait;
    use DiscountsTrait;
    // use ExchangeRateTrait;


    /**
     * getOptions
     * Get Info from Category fo fill subcategory <select> from ajax request
     *
     * @param type var Description
     * @return return type
     */
    public function getCategoryOptions($categorySlug)
    {
        $Category = Category::where('slug', $categorySlug)->with('Subcategories')->first()->Subcategories->all();
        return response()->json(collect($Category));
    }

    public function searchProducts(Request $request)
    {

        $search = $request->get('term');
        // \Log::info(__CLASS__."->".__METHOD__."->".__LINE__." \$request->term {$request->term}");
        // \Log::info(__CLASS__."->".__METHOD__."->".__LINE__." \$request->input('term') {$request->input('term')} ");
        // \Log::info(__CLASS__."->".__METHOD__."->".__LINE__." \$request->get('term') {$request->get('term')}");
        // \Log::info(__CLASS__."->".__METHOD__."->".__LINE__." \$request->query('term') {$request->query('term')}");
        // $result = ProductsBrandMarket::where('name', 'LIKE', '%'. $search. '%')->get();
        // $result = DB::table('products_brand_market')->whereNull('deleted_at')->where('name', 'LIKE', "%{$search}%")->get();
        $productSearch = Product::selectRaw("model, name, slug, category_id, subcategory_id, 'no' as is_category")
            // ->selectRaw("(select name from categories where categories.id = products_brand_market.category_id ) AS category_name")
            ->selectRaw("(select slug from categories where categories.id = products_brand_market.category_id ) AS category_slug")
            // ->selectRaw("(select name from subcategories where subcategories.id = products_brand_market.subcategory_id ) AS subcategory_name")
            ->selectRaw("(select slug from subcategories where subcategories.id = products_brand_market.subcategory_id ) AS subcategory_slug")
            ->selectRaw("MD5(LCASE(provider_name)) AS crypt_provider_name, provider_name")
            ->where('description', 'like', "%{$search}%")
            ->orWhere('name', 'like', "%{$search}%")
            ->orWhere('model', 'like', "%{$search}%")
            ->whereRaw("name <> '' and model <> '' and color <> '' ")
            ->groupBy('model', 'name', 'slug');
        // \Log::info(__CLASS__ . "->" . __METHOD__ . "->" . __LINE__ . " result->toSql() {$productSearch->toSql()}  [{$search}]");
        $categoriesFound = $productSearch->pluck('category_slug')->unique();
        // \Log::info(__CLASS__ . "->" . __METHOD__ . "->" . __LINE__ . " categoriesFound " . print_r($categoriesFound->toArray(), 1));
        $categorySearch = Category::selectRaw("'yes' as is_category, name, slug")->whereIn('slug', $categoriesFound->toArray());
        // \Log::info(__CLASS__ . "->" . __METHOD__ . "->" . __LINE__ . " categorySearch " . print_r($categorySearch->get()->toArray(), 1));
        $mergedResult = array_merge($productSearch->get()->toArray(), $categorySearch->get()->toarray());
        // return response($productSearch->get()->toJson());
        return response($mergedResult);
    }

    /**
     * Add a new product for Simple Cart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addProductSimpleCart(Request $request)
    {
        // \Log::info("\$request->input() " . print_r($request->input(), 1));
        $user =  auth()->user();

        if ($cart = SimpleCart::where('user_id', $user->id)->where('status', 'Open')->get()) {
            if (isset($cart[0])) {
                $cart = $cart[0];
            } else {
                $cart = new SimpleCart();
                $cart->user_id = $user->id;
                $cart->total = 0;
                $cart->uuid = \Str::uuid();
                $cart->subtotal = 0;
                $cart->status = 'Open';
                $cart->save();
            }
        }

        if (Gate::denies('cart', $cart)) {
            abort(403);
        }

        $quantity = $request->quantity;
        $productId = $request->productId;
        $color = $request->color;
        $unitPrice = $request->unitPrice;
        $providerName = $request->providerName;
        $model = $request->model;
        $originalPrice = $request->originalPrice;
        $product = Product::findOrFail($productId);
        $productParentCategory = $request->productParentCategory;
        $productParentSubCategory = $request->productParentSubCategory;
        $redeem = 0.35;
        if (is_null($product)) {
            return response()->json(['status' => 'ERROR', 'error' => '404 not found'], 404);
        }
        $discountPercentaje = 0;
        $rateExchangeAmount = 0;
        $priceMXN = $product->original_price;
        // ($provider_name, $quantity, $origPrice)
        if ($providerDiscount = $this->getPriceWithDiscount($providerName, $quantity, $originalPrice)) {
            $unitPrice = $providerDiscount['priceWithDiscount'];
            $discountPercentaje = $providerDiscount['percentaje'];
            $rateExchangeAmount = $providerDiscount['rateExchangeAmount'];
            $priceMXN = $providerDiscount['priceMXN'];
            // $dataDiscount['priceWithDiscount'] = floatval($priceWithDiscount);
            // $dataDiscount['percentaje'] = floatval($discountFromProvider);
        }
        // \Log::info("STUFF!!!");
        $newProduct = new SimpleCartDetails();
        $newProduct->simple_cart_id = $cart->id;
        $newProduct->products_brand_market_id = $product->id;
        $newProduct->model = $product->model;
        $newProduct->name = $product->name;
        $newProduct->original_price = $product->original_price;
        $newProduct->price_mxn = $priceMXN;
        // $newProduct->original_price = ($product->original_price / $rateExchangeAmount);
        $newProduct->subtotal_mxn = ($product->original_price * $quantity);
        $newProduct->price = $unitPrice;
        $newProduct->color = $color;
        $newProduct->quantity = $quantity;
        $newProduct->subtotal = ($unitPrice * $newProduct->quantity) / $rateExchangeAmount;
        $newProduct->discount = $discountPercentaje;
        $newProduct->provider_name = $providerName;
        $newProduct->product_parent_category = $productParentCategory;
        $newProduct->product_parent_subcategory = $productParentSubCategory;

        // \Log::info("newProduct: " . print_r($newProduct->toArray(), 1));
        $newProduct->save();
        // UPDATING TOTALS

        // Get all products for this cart

        $cart = SimpleCart::findOrFail($cart->id);
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  \$cart: {$cart->id}");

        $sums = SimpleCartDetails::selectRaw("sum(subtotal_mxn) as subtotal_mxn, sum(subtotal) as subtotal")
            ->where('simple_cart_id', $cart->id);
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  sums SQL {$sums->toSql()} ");
        $updateTotals = $sums->first();
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  updateTotals->subtotal_mxn {$updateTotals->subtotal_mxn} ");
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  updateTotals->subtotal {$updateTotals->subtotal} ");
        // return redirect(route('cart.index'));
        $cart->exchange_rate = $this->getExchangeRate()[1];

        // \Log::info(__METHOD__.": ".__LINE__."-> \Session::get('exchangeRate'): ". \Session::get('exchangeRate'));
        // \Log::info(__METHOD__.": ".__LINE__."-> session()->get('exchangeRate)': ". session()->get('exchangeRate') );
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->exchange_rate: {$cart->exchange_rate} ");
        $cart->subtotal = $updateTotals->subtotal;
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->subtotal {$cart->subtotal} ");
        $cart->subtotal_mxn = $updateTotals->subtotal_mxn;
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->subtotal_mxn {$cart->subtotal_mxn} ");
        $logistics = ($cart->freight_amount / ($cart->exchange_rate - $redeem));

        $cart->total = ($updateTotals->subtotal + $logistics);
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->subtotal_mxn {$cart->subtotal} ");

        $cart->total_mxn = ($updateTotals->subtotal_mxn + $cart->freight_amount);

        $cart->save();

        return response()->json(['cart' => $cart->toArray(), 'cartDetails' => $newProduct->toArray()]);
    }

    /**
     * Add a new product for Wish List
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addProductWishList(Request $request)
    {
        // \Log::info("\$request->input() " . print_r($request->input(), 1));
        $user =  auth()->user();
        if ($cart = SimpleCart::where('user_id', $user->id)->where('status', 'Open')->get()) {
            if (isset($cart[0])) {
                $cart = $cart[0];
            } else {
                $cart = new SimpleCart();
                $cart->user_id = $user->id;
                $cart->total = 0;
                $cart->uuid = \Str::uuid();
                $cart->wishlist_name = $request->wishlist_name;
                $cart->subtotal = 0;
                $cart->status = 'Wishlist';
                $cart->save();
            }
        }

        if (Gate::denies('cart', $cart)) {
            abort(403);
        }

        $quantity = $request->quantity;
        $productId = $request->productId;
        $color = $request->color;
        $unitPrice = $request->unitPrice;
        $providerName = $request->providerName;
        $model = $request->model;
        $originalPrice = $request->originalPrice;
        $product = Product::findOrFail($productId);
        $productParentCategory = $request->productParentCategory;
        $productParentSubCategory = $request->productParentSubCategory;
        $redeem = 0.35;
        if (is_null($product)) {
            return response()->json(['status' => 'ERROR', 'error' => '404 not found'], 404);
        }
        $discountPercentaje = 0;
        $rateExchangeAmount = 0;
        $priceMXN = $product->original_price;
        // ($provider_name, $quantity, $origPrice)
        if ($providerDiscount = $this->getPriceWithDiscount($providerName, $quantity, $originalPrice)) {
            $unitPrice = $providerDiscount['priceWithDiscount'];
            $discountPercentaje = $providerDiscount['percentaje'];
            $rateExchangeAmount = $providerDiscount['rateExchangeAmount'];
            $priceMXN = $providerDiscount['priceMXN'];
            // $dataDiscount['priceWithDiscount'] = floatval($priceWithDiscount);
            // $dataDiscount['percentaje'] = floatval($discountFromProvider);
        }
        // \Log::info("STUFF!!!");
        $newProduct = new SimpleCartDetails();
        $newProduct->simple_cart_id = $cart->id;
        $newProduct->products_brand_market_id = $product->id;
        $newProduct->model = $product->model;
        $newProduct->name = $product->name;
        $newProduct->original_price = $product->original_price;
        $newProduct->price_mxn = $priceMXN;
        // $newProduct->original_price = ($product->original_price / $rateExchangeAmount);
        $newProduct->subtotal_mxn = ($product->original_price * $quantity);
        $newProduct->price = $unitPrice;
        $newProduct->color = $color;
        $newProduct->quantity = $quantity;
        $newProduct->subtotal = ($unitPrice * $newProduct->quantity) / $rateExchangeAmount;
        $newProduct->discount = $discountPercentaje;
        $newProduct->provider_name = $providerName;
        $newProduct->product_parent_category = $productParentCategory;
        $newProduct->product_parent_subcategory = $productParentSubCategory;

        // \Log::info("newProduct: " . print_r($newProduct->toArray(), 1));
        $newProduct->save();
        // UPDATING TOTALS

        // Get all products for this cart

        $cart = SimpleCart::findOrFail($cart->id);
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  \$cart: {$cart->id}");

        $sums = SimpleCartDetails::selectRaw("sum(subtotal_mxn) as subtotal_mxn, sum(subtotal) as subtotal")
            ->where('simple_cart_id', $cart->id);
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  sums SQL {$sums->toSql()} ");
        $updateTotals = $sums->first();
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  updateTotals->subtotal_mxn {$updateTotals->subtotal_mxn} ");
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  updateTotals->subtotal {$updateTotals->subtotal} ");
        // return redirect(route('cart.index'));
        $cart->exchange_rate = $this->getExchangeRate()[1];

        // \Log::info(__METHOD__.": ".__LINE__."-> \Session::get('exchangeRate'): ". \Session::get('exchangeRate'));
        // \Log::info(__METHOD__.": ".__LINE__."-> session()->get('exchangeRate)': ". session()->get('exchangeRate') );
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->exchange_rate: {$cart->exchange_rate} ");
        $cart->subtotal = $updateTotals->subtotal;
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->subtotal {$cart->subtotal} ");
        $cart->subtotal_mxn = $updateTotals->subtotal_mxn;
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->subtotal_mxn {$cart->subtotal_mxn} ");
        $logistics = ($cart->freight_amount / ($cart->exchange_rate - $redeem));

        $cart->total = ($updateTotals->subtotal + $logistics);
        // \Log::info(__METHOD__ . ": " . __LINE__ . "->  cart->subtotal_mxn {$cart->subtotal} ");

        $cart->total_mxn = ($updateTotals->subtotal_mxn + $cart->freight_amount);

        $cart->save();

        return response()->json(['cart' => $cart->toArray(), 'cartDetails' => $newProduct->toArray()]);
    }

    /**
     * getStock
     *
     * gets the stock from doblevela's ws
     *
     * @param Type $request Request object
     * @return json
     **/
    public function getStock(Request $request)
    {
        // \Log::info(__METHOD__ . "->" . __LINE__ . "->request: " . print_r($request->model, 1));
        return response()->json($this->updateStockAndPriceByModel($request->model));
    }
}
