<?php

namespace App\Http\Controllers;

use DB;

use App\Http\Traits\DiscountsTrait;
use App\Http\Traits\DobleVelaTrait;

use Route;

class ProductController extends Controller
{
    use DobleVelaTrait;
    use DiscountsTrait;
    // use ExchangeRateTrait;
    /**
     * Display a listing of the resource.
     *
     * @var $category for receive the uri-segment for slug category
     * @var $subcategory for receive the uri-segment for sub category
     * @return \Illuminate\Http\Response
     */
    public function list($category, $subcategory = null)
    {
        // print("Is this user an admin?? ". auth()->user()->isAdmin(). " <br/>");
        if (!$category) {
            redirect(route('category.all'));
        }
        // group_concat(distinct(concat(lcase(substr(color, 5)),'|', p.id)))
        $filteredProducts = DB::table('products_brand_market as p');
        $filteredProducts->selectRaw("p.name, p.slug, group_concat(p.image) as image, p.stock,
        p.original_price, p.provider_name, p.description, p.model, p.category_doble_vela, p.subcategory_doble_vela");
        $filteredProducts->selectRaw("p.category_4_promotional, p.subcategory_4_promotional, family_promo_opcion");
        $filteredProducts->selectRaw("case when  lcase(p.provider_name) = 'doble-vela' then group_concat(concat( trim(substr(lcase(p.color),5)), '|', p.id, '|', p.stock))
        when lcase(provider_name) = '4-promotional' then group_concat(concat( lcase(p.color), '|', p.id, '|', p.stock)) end AS colors");
        $filteredProducts->whereRaw("p.name <> '' and p.model <> '' and p.color <> '' ");
        $filteredProducts->whereRaw("p.category_id in (select id from categories c where c.slug= " . app('db')->getPdo()->quote($category) . " )"); //
        if ($subcategory) {
            $filteredProducts->whereRaw("p.subcategory_id in (select id from subcategories s where s.slug = " . app('db')->getPdo()->quote($subcategory) . " )"); //
        }
        $filteredProducts->groupBy('p.name');
        $jsonString = file_get_contents(base_path('resources/provider-sources/colors.json'));
        $tmpColor = json_decode($jsonString, true);
        $colors = collect($tmpColor);
        // dd($filteredProducts->toSql());
        return view('frontend.products.index', ['colors' => $colors->toArray()])
            ->with('filteredProducts', $filteredProducts->get())
            ->with('rateExchangeDate', Date('Y-m-d'))
            ->with('rateExchangeAmount', $this->getExchangeRate()[1])
            ->with('cat_slug', $category)
            ->with('subcat_slug', $subcategory)
            ->with('current_route', Route::currentRouteName());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function product($provider, $category, $subcategory, $slug, $model)
    {
        // print("category: {$category}<br> subcategory: {$subcategory}<br/>slug: {$slug} <br/>model: {$model}<br/>");
        // print("<pre>\$provider: {$provider} - " . md5('doble-vela') . "</pre>");
        if ($provider == md5('doble-vela')) {

            if (!$slug || !$model) {
                redirect(route('account.home'));
            }
            $product = DB::table('products_brand_market as p')
                ->selectRaw('c.slug as cat_slug, s.slug as subcat_slug, p.category_id, p.subcategory_id, p.model, p.slug')
                ->selectRaw("case when  lcase(p.provider_name) = 'doble-vela' then group_concat(concat( trim(substr(lcase(p.color),5)), '|', p.id, '|', p.stock))
                when lcase(provider_name) = '4-promotional' then group_concat(concat( lcase(p.color), '|', p.id, '|', p.stock)) end AS colors")
                ->selectRaw('group_concat(p.image) as image, p.name, p.description, p.stock, p.price, p.material, p.size, group_concat(p.id) as ids, p.provider_name')
                ->selectRaw('p.category_doble_vela, p.subcategory_doble_vela, p.original_price, p.pieces, p.article_id ')
                ->selectRaw('p.category_4_promotional, p.subcategory_4_promotional, p.original_price, p.pieces, p.article_id ')
                ->whereRaw('p.name is not null ')
                ->where('c.slug', '=', $category);
            if ($subcategory) {
                if ($subcategory != $category) {
                    // $product->whereRaw("p.subcategory_id in (select id from subcategories s where s.slug = " . app('db')->getPdo()->quote($subcategory) . " )");
                    $product->where('s.slug', '=', $subcategory);
                }
            }
            $product->leftJoin('categories as c', 'p.category_id', '=', 'c.id')
                ->leftJoin('subcategories as s', 'p.subcategory_id', '=', 's.id')
                ->where('p.slug', '=', $slug)
                ->where('p.model', '=', $model)
                ->whereNull('p.deleted_at');


            // related products
            $relatedProducts = DB::table('products_brand_market as p')
                ->selectRaw('c.slug as cat_slug, s.slug as subcat_slug, p.category_id, p.subcategory_id, p.model, p.slug')
                ->selectRaw("case when  lcase(p.provider_name) = 'doble-vela' then group_concat(concat( trim(substr(lcase(p.color),5)), '|', p.id, '|', p.stock))
                when lcase(provider_name) = '4-promotional' then group_concat(concat( lcase(p.color), '|', p.id, '|', p.stock)) end AS colors")
                ->selectRaw('group_concat(p.image) as image, p.name, p.description, p.stock, p.price, p.material, p.size, group_concat(p.id) as ids, p.provider_name')
                ->selectRaw('p.category_doble_vela, p.subcategory_doble_vela, p.original_price, p.pieces, p.article_id ')
                ->selectRaw('p.category_4_promotional, p.subcategory_4_promotional, p.original_price, p.pieces, p.article_id ')
                ->whereRaw('p.name is not null ')
                ->where('c.slug', '=', $category);
            if ($subcategory) {
                if ($subcategory != $category) {
                    // $relatedProducts->whereRaw("p.subcategory_id in (select id from subcategories s where s.slug = " . app('db')->getPdo()->quote($subcategory) . " )");
                    $relatedProducts->where('s.slug', '=', $subcategory);
                }
            }
            $relatedProducts->leftJoin('categories as c', 'p.category_id', '=', 'c.id')
                ->leftJoin('subcategories as s', 'p.subcategory_id', '=', 's.id')
                ->whereNull('p.deleted_at');

            $relatedProducts->groupBy('p.model');
            $relatedProducts->inRandomOrder();
            $relatedProducts->limit(5);
            // print("<pre>\$relatedProducts: {$relatedProducts->toSql()}</pre>");
            // print("<pre>product->toSql: {$relatedProducts->toSql()}</pre>");
            $imageFromWS = [];
            $imageFromWS = $this->getImagesDobleVela($model);
            $productProviderView = '-doble-vela';
            // dd($product->toSql());
            $productProviderView = '';
            if (strtolower($product->first()->provider_name) == 'doble-vela') {
                if (!strstr($product->first()->image, 'doble-vela')) {
                    $imageFromWS = $this->getImagesDobleVela($model);
                }
                $productProviderView = '-doble-vela';
            }

            $jsonString = file_get_contents(base_path('resources/provider-sources/colors.json'));
            $tmpColor = json_decode($jsonString, true);
            $colors = collect($tmpColor);

            // print("<pre>productColors: " . print_r($productColors, 1) . "</pre>");
            // print("<pre>{$product->toSql()}</pre>");
            return view("frontend.products.single{$productProviderView}", ['colors' => $colors])
                ->with('product', $product->first())
                ->with('images', $imageFromWS)
                ->with('cat_slug', $category)
                ->with('subcat_slug', $subcategory)
                ->with('rateExchangeDate', Date('Y-m-d'))
                ->with('rateExchangeAmount', $this->getExchangeRate()[1])
                // ->with('discount4Promotional', $this->getDiscount('4-promotional'))
                ->with('discountDobleVela', $this->getDiscount('doble-vela'))
                ->with('relatedProducts', $relatedProducts->get())
                // ->with('discountPromoOpcion', $this->getDiscount('promo-opcion'))
                ->with('current_route', Route::currentRouteName());

            // ENDS IF HHERE!!
        } else if ($provider == md5('4-promotional')) {

            if (!$slug || !$model) {
                redirect(route('account.home'));
            }
            $product = DB::table('products_brand_market as p')
                ->selectRaw('c.slug as cat_slug, s.slug as subcat_slug, p.category_id, p.subcategory_id, p.model, p.slug')
                ->selectRaw("case when  lcase(p.provider_name) = 'doble-vela' then group_concat(concat( trim(substr(lcase(p.color),5)), '|', p.id, '|', p.stock))
                when lcase(provider_name) = '4-promotional' then group_concat(concat( lcase(p.color), '|', p.id, '|', p.stock)) end AS colors")
                ->selectRaw('group_concat(p.image) as image, p.name, p.description, p.stock, p.price, p.material, p.size, group_concat(p.id) as ids, p.provider_name')
                ->selectRaw('p.category_doble_vela, p.subcategory_doble_vela, p.original_price, p.pieces, p.article_id ')
                ->selectRaw('p.category_4_promotional, p.subcategory_4_promotional, p.original_price, p.pieces, p.article_id ')
                ->whereRaw('p.name is not null ')
                ->where('c.slug', '=', $category);
            if ($subcategory) {
                if ($subcategory != $category) {
                    // $product->whereRaw("p.subcategory_id in (select id from subcategories s where s.slug = " . app('db')->getPdo()->quote($subcategory) . " )");
                    $product->where('s.slug', '=', $subcategory);
                }
            }
            $product->leftJoin('categories as c', 'p.category_id', '=', 'c.id')
                ->leftJoin('subcategories as s', 'p.subcategory_id', '=', 's.id')
                ->where('p.slug', '=', $slug)
                // ->where('p.model', '=', $model)
                ->whereNull('p.deleted_at');
            // related products
            $relatedProducts = DB::table('products_brand_market as p')
                ->selectRaw('c.slug as cat_slug, s.slug as subcat_slug, p.category_id, p.subcategory_id, p.model, p.slug')
                ->selectRaw("case when  lcase(p.provider_name) = 'doble-vela' then group_concat(concat( trim(substr(lcase(p.color),5)), '|', p.id, '|', p.stock))
                when lcase(provider_name) = '4-promotional' then group_concat(concat( lcase(p.color), '|', p.id, '|', p.stock)) end AS colors")
                ->selectRaw('group_concat(p.image) as image, p.name, p.description, p.stock, p.price, p.material, p.size, group_concat(p.id) as ids, p.provider_name')
                ->selectRaw('p.category_doble_vela, p.subcategory_doble_vela, p.original_price, p.pieces, p.article_id ')
                ->selectRaw('p.category_4_promotional, p.subcategory_4_promotional, p.original_price, p.pieces, p.article_id ')
                ->whereRaw('p.name is not null ')
                ->where('c.slug', '=', $category);
            if ($subcategory) {
                if ($subcategory != $category) {
                    // $relatedProducts->whereRaw("p.subcategory_id in (select id from subcategories s where s.slug = " . app('db')->getPdo()->quote($subcategory) . " )");
                    $relatedProducts->where('s.slug', '=', $subcategory);
                }
            }
            $relatedProducts->leftJoin('categories as c', 'p.category_id', '=', 'c.id')
                ->leftJoin('subcategories as s', 'p.subcategory_id', '=', 's.id')
                ->whereNull('p.deleted_at');

            $relatedProducts->groupBy('p.model');
            $relatedProducts->inRandomOrder();
            $relatedProducts->limit(5);

            $imageFromWS = [];
            $productProviderView = '';
            $imageFromWS = explode(',', $this->getImages4Promotional($product->first()->slug));
            // print(print_r($imageFromWS, 1));
            // \Log::info(__METHOD__ . "->" . __LINE__ . "-> \$this->getImages4Promotional(\$product->first()->slug) " . print_r($this->getImages4Promotional($product->first()->slug), 1));
            $productProviderView = '-4-promotional';


            $jsonString = file_get_contents(base_path('resources/provider-sources/colors.json'));
            $tmpColor = json_decode($jsonString, true);
            $colors = collect($tmpColor);

            return view("frontend.products.single{$productProviderView}", ['colors' => $colors])
                ->with('product', $product->first())
                ->with('images', $imageFromWS)
                ->with('cat_slug', $category)
                ->with('subcat_slug', $subcategory)
                ->with('rateExchangeDate', Date('Y-m-d'))
                ->with('rateExchangeAmount', $this->getExchangeRate()[1])
                ->with('discount4Promotional', $this->getDiscount('4-promotional'))
                ->with('discountDobleVela', $this->getDiscount('4-promotional'))
                ->with('discountPromoOpcion', $this->getDiscount('promo-opcion'))
                ->with('relatedProducts', $relatedProducts->get())
                ->with('current_route', Route::currentRouteName());
        }
    }

    /**
     * getImages4Promotional
     *
     * pulling images from 4promotional's json
     *
     * @param String $model model of article
     * @return array
     * @throws conditon
     **/
    public function getImages4Promotional(String $slug)
    {
        $images = \DB::table('products_brand_market as p')
            ->selectRaw('group_concat(p.image) as images')
            ->where('slug', $slug);
        return $images->first()->images;
    }

    /**
     * _getDB4promotional
     *
     * Retrive [Cached]  4 promotional mongoloid JSON response
     *
     *
     * @param Type $codigo Description
     * @return type
     * @throws conditon
     **/
    protected function _getDB4promotional()
    {
        $jsonString = file_get_contents(base_path('resources/provider-sources/four-promotional.inventory.json'));
        $data = json_decode($jsonString, true);
        return $data;
    }
}
/**
 *
 *
 * // in your model file
            public function next(){
                // get next user
                return User::where('id', '>', $this->id)->orderBy('id','asc')->first();

            }
            public  function previous(){
                // get previous  user
                return User::where('id', '<', $this->id)->orderBy('id','desc')->first();

            }
            // in your controller file
            $user = User::find(5);
            // a clean object that can be used anywhere
            $user->next();
            $user->previous();
 */
