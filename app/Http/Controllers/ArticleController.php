<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Storage;
use Route;
use Str;



class ArticleController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {

  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $rows = Article::paginate(5);
    $columns = ['Título del Artículo', 'SEO Keywords', 'SEO Description'];
    return view('backoffice.article.index', ['rows'=>$rows, 'columns' => $columns,
    'page_title'=>$this->title.' - Article', 'message' => 'Aquí podrás consultar, editar y borrar los artículos (publicaciones)'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('backoffice.article.create', [
      'page_title'=>"{$this->title} - Artículos - Agregar", 'message' => 'Artículo'])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
      $validatedData = $request->validate([

        'title' => 'required', ['title' => 'El :attribute es requerido', ['title' => 'title']],
        'content' => 'required', ['content' => 'El :attribute es requerido', ['content' => 'content']],
        'hero_image' => 'required', ['hero_image' => 'El :attribute es requerido', ['hero_image' => 'hero_image']],
        'seo_keywords' => 'required', ['seo_keywords' => 'El :attribute es requerido', ['seo_keywords' => 'seo_keywords']],
        'seo_description' => 'required', ['seo_description' => 'El :attribute es requerido', ['seo_description' => 'seo_description']],
        'status' => 'required', ['status' => 'El :attribute es requerido', ['status' => 'status']],
        'priority' => 'required', ['priority' => 'El :attribute es requerido', ['priority' => 'priority']]

      ]);
      if (!$validatedData) {
        return redirect(route('article.create'))->withErrors($validatedData, 'create');
      }
      $row = new Article([
        'title' => $request->title,
        'slug' => Str::slug($request->title),
        'content' => $request->content,
        'published_at' => $request->published_at,
        'seo_keywords' => $request->seo_keywords,
        'seo_description' => $request->seo_description,
        'status' => $request->status,
        'priority' => $request->priority,

      ]);
      if ($request->hasfile('hero_image')) {
        $destinationPath = "";
        $filename = $request->file('hero_image')->hashName();
        $request->file('hero_image')->store('public');
        $row->hero_image = "{$filename}";
      }
      // dd($row);

      $row->save();
      $redirectTo = [];
      $action = $request->action;
      switch ($action) {
        case 'saveAddOther':
        $redirectTo['route_name'] = 'article.create';
        $redirectTo['params'] = [];
        break;
        default:
        $redirectTo['route_name'] = 'article.show';
        $redirectTo['params'] = ['article'=>$row];
        break;
      }
      return redirect(route($redirectTo['route_name'], $redirectTo['params']))->with('success', 'Registro Agregado');

    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Article  $article
    * @return \Illuminate\Http\Response
    */
    public function show(Article $article)
    {
      $row = Article::where('id', $article->id)->first();
      return view('backoffice.article.show', ['row'=>$row,
      'page_title'=>"{$this->title} - {$row->name}", 'message' => 'Artículo'])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Article  $article
    * @return \Illuminate\Http\Response
    */
    public function edit(Article $article)
    {
      $row = Article::findOrFail($article->id);
      return view('backoffice.article.edit', ['row'=>$row,
      'page_title'=>"{$this->title} - {$row->name}", 'message' => 'Artículo'])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Article  $article
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request)
    {
      $validatedData = $request->validate([

        'title' => 'required', ['title' => 'El :attribute es requerido', ['title' => 'title']],
        'content' => 'required', ['content' => 'El :attribute es requerido', ['content' => 'content']],
        'hero_image' => 'required', ['hero_image' => 'El :attribute es requerido', ['hero_image' => 'hero_image']],
        'seo_keywords' => 'required', ['seo_keywords' => 'El :attribute es requerido', ['seo_keywords' => 'seo_keywords']],
        'seo_description' => 'required', ['seo_description' => 'El :attribute es requerido', ['seo_description' => 'seo_description']],
        'status' => 'required', ['status' => 'El :attribute es requerido', ['status' => 'status']],
        'priority' => 'required', ['priority' => 'El :attribute es requerido', ['priority' => 'priority']]

      ]);
      if (!$validatedData) {
        return redirect(route('article.edit'))->withErrors($validatedData, 'update')->withInput($request->input());
      }

      $row = Article::findOrFail($request->id);
      $row->title = $request->title;
      $row->slug = Str::slug($request->title);
      $row->content = $request->content;
      $row->published_at = $request->published_at;
      if ($request->hasfile('hero_image')) {
        $filename = $request->file('hero_image')->hashName();
        if ($filename != $row->hero_image) {
          if(Storage::disk('public')->exists($row->hero_image)){
            Storage::disk('public')->delete($row->hero_image);
          }
        }
        $request->file('hero_image')->store('public');
        $row->hero_image = "{$filename}";
      }

      $row->seo_keywords = $request->seo_keywords;
      $row->seo_description = $request->seo_description;
      $row->status = $request->status;
      $row->priority = $request->priority;

      $row->save();
      // $article = Article::findOrFail($request->id);
      return redirect(route('article.show',$row))->with('success', 'Artículo Actualizado');
    }

    /**
    * Display the specified resource to confirm deletion
    *
    * @param  \App\Article  $article
    * @return \Illuminate\Http\Response
    */
    public function confirmDelete(Article $article)
    {
      // dd($category);
      $row = Article::findOrFail($article->id);
      return view('backoffice.article.delete', ['row'=>$row,
      'page_title'=>"{$this->title} - {$row->name} [delete]", 'message' => 'Eliminar Artículo'])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Article  $article
    * @return \Illuminate\Http\Response
    */
    public function destroy(Article $article)
    {
      $row = Article::findOrFail($article->id);
      if(Storage::disk('public')->exists($row->hero_image)){
        Storage::disk('public')->delete($row->hero_image);
      }
      $row->delete();
      return redirect(route('article.index'))->with('success', 'Row destroyed');
    }
  }
