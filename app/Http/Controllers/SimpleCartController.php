<?php

namespace App\Http\Controllers;

use App\Http\Traits\DiscountsTrait;
use App\Http\Traits\DobleVelaTrait;
use App\Http\Traits\EmailTrait;
use App\SimpleCart;
use App\SimpleCartDetails;
use Illuminate\Http\Request;
use Illuminate\Routing\Response;
use Illuminate\Support\Facades\Gate;
use Route;
use Str;

class SimpleCartController extends Controller
{

    use DobleVelaTrait;
    use DiscountsTrait;
    use EmailTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // print("STUFF!<br/>");
        return view('frontend.cart.invoice', ['page_title' => 'Mis Compras'])
            ->with('current_route', Route::currentRouteName())
            ->with('error', false);
    }

    /**
     * Update the quantity of the product in the simple cart details table.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProductQuantity(Request $request)
    {
        // print("<pre>request is: ".print_r($request->input(), 1)."</pre>");
        $SimpleCart = SimpleCart::where('id', $request->simple_cart_id)->first();
        if (Gate::denies('cart', $SimpleCart)) {
            abort(403);
        }

        $SimpleCartDetails = SimpleCartDetails::where('id', $request->simple_cart_details_id)->first();
        // Save the new subtotal
        // $product = Products::where('id', $SimpleCartDetails->products_brand_market_id)->first();
        $providerName = strtolower($SimpleCartDetails->provider_name);
        $productOriginalPrice = $request->originalPrice;
        $quantity = $request->simple_cart_details_quantity;
        $rateUtility = floatval(1.20);
        $tax = floatval(1.16);
        $redeem = floatval(0.35);

        $SimpleCartDetails->quantity = $quantity;
        // struct
        {
            $discountPercentaje = 0;
            $unitPrice = 0;
            // UnitPrice calculations
            $shipperPrice = floatval($productOriginalPrice);
            $maxPrice = floatval($shipperPrice) * floatval($rateUtility);
            $unitPriceCalculus = $unitPrice = ((floatval($shipperPrice) * floatval($rateUtility)) * floatval($tax));
            $providerDiscount = $this->getPriceWithDiscount($providerName, $quantity, $productOriginalPrice);
            $rateExchangeAmount = $providerDiscount['rateExchangeAmount'];
            if ((float) $providerDiscount['priceWithDiscount'] > 0) {
                $SimpleCartDetails->price = $providerDiscount['priceWithDiscount'];
                $SimpleCartDetails->price_mxn = $providerDiscount['priceMXN'];
            } else {
                $SimpleCartDetails->price = $unitPrice / ($rateExchangeAmount - $redeem);
                $SimpleCartDetails->price_mxn = $unitPrice;
            }
            if ((float) $providerDiscount['percentaje'] > 0) {
                $discountPercentaje = $providerDiscount['percentaje'];
            }
        }

        // $SimpleCartDetails->original_price = $productOriginalPrice;
        $SimpleCartDetails->subtotal_mxn = (($unitPrice * $quantity));
        $SimpleCartDetails->quantity = $quantity;
        $SimpleCartDetails->discount = $discountPercentaje;
        $SimpleCartDetails->subtotal = ($SimpleCartDetails->price * $SimpleCartDetails->quantity); //  / $rateExchangeAmount;
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$unitPrice: {$unitPrice} </pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$quantity: {$quantity} </pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$rateExchangeAmount: {$rateExchangeAmount} </pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$SimpleCartDetails->original_price: {$SimpleCartDetails->original_price}</pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$SimpleCartDetails->price: {$SimpleCartDetails->price}</pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$SimpleCartDetails->subtotal_mxn: {$SimpleCartDetails->subtotal_mxn}</pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$SimpleCartDetails->quantity: {$SimpleCartDetails->quantity}</pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$SimpleCartDetails->discount: {$SimpleCartDetails->discount}</pre>");
        // print("<pre>".__METHOD__."->".__LINE__."  ->  \$SimpleCartDetails->subtotal: {$SimpleCartDetails->subtotal}</pre>");

        $SimpleCartDetails->save();

        $updateTotals = SimpleCartDetails::selectRaw("sum(subtotal_mxn) as subtotal_mxn, sum(subtotal) as subtotal")
            ->where('simple_cart_id', $request->simple_cart_id)
            ->whereNull('deleted_at')->first();
        // return redirect(route('cart.index'));
        $SimpleCart->subtotal = $updateTotals->subtotal;
        $SimpleCart->subtotal_mxn = $updateTotals->subtotal_mxn;
        $SimpleCart->total = ($updateTotals->subtotal + ($SimpleCart->freight_amount / ($rateExchangeAmount - $redeem)));
        $SimpleCart->total_mxn = ($updateTotals->subtotal_mxn + $SimpleCart->freight_amount);
        // print("<pre>\$SimpleCart->subtotal_mxn: {$SimpleCart->subtotal_mxn}</pre>");
        // print("<pre>\$SimpleCart->subtotal: {$SimpleCart->subtotal}</pre>");
        // print("<pre>\$SimpleCart->total: {$SimpleCart->total}</pre>");
        // print("<pre>\$SimpleCart->total_mxn: {$SimpleCart->total_mxn}</pre>");
        $SimpleCart->save();

        return @redirect(@route('cart.index'));
    }

    /**
     * Check's out Cart
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SimpleCart  $simpleCart
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request)
    {
        $cart = SimpleCart::findOrFail($request->id);
        if (Gate::denies('cart', $cart)) {
            abort(403);
        }

        if ($cart->total <= 0.00) {
            return view('frontend.cart.invoice', ['page_title' => 'Mis Compras'])
                ->with('current_route', Route::currentRouteName())
                ->with('error', "El carrito de compras está vacío, no podemos atender tu solicitud.");
        }
        // dd($cart->details());
        // event(new PurchaseOrder($cart));
        // $data['page_title'] = "Brand Martek Mailing System";
        $this->sendEmails($request, $cart);
        $cart->status = 'Quoted';
        $cart->save();
        return view('frontend.mailing.thanks');
    }

    // /**
    //  * mailingTester
    //  *
    //  * método para probar mailing
    //  *
    //  * @return return type
    //  */
    public function toSendEmailInternal(Request $request, SimpleCart $simpleCart)
    {


        // $year = date('Y');
        // $month = date('m');
        // $day = date('d');
        // $time = date('h:i:s');
        // $timezone = date_default_timezone_get();
        // $provider_name = '';
        // $internalCode = Str::uuid();

        // $starttime = microtime(true); {

        //     // @block for client quotation

        //     $getClientQuotation = \DB::table('simple_carts as sc')
        //         ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name, scd.id ')
        //         ->selectRaw('scd.original_price as d_original_price, scd.subtotal_mxn as d_subtotal_mx, scd.price as d_price,  scd.subtotal as d_subtotal')
        //         ->selectRaw('sc.total as total_usd, sc.subtotal as subtotal_usd, scd.provider_name ')
        //         ->selectRaw('sc.total_mxn as total_mxn, sc.subtotal_mxn as subtotal_mxn ')
        //         ->selectRaw('pbm.category_doble_vela as product_parent_category')
        //         ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
        //         ->join('products_brand_market as pbm', 'pbm.id', '=', 'scd.products_brand_market_id')
        //         ->where('sc.user_id', auth()->user()->id)
        //         ->where('sc.status', 'open')
        //         ->where('scd.provider_name', 'doble-vela')
        //         ->groupBy('scd.id', 'scd.model', 'scd.name', 'scd.color', 'scd.provider_name')
        //         ->orderBy('scd.created_at');
        //     // print("<pre>\$getClientQuotation:  {$getClientQuotation->toSql()}</pre>");
        //     $simpleCart = $getClientQuotation->get();

        //     $clientQuotationInfo = [
        //         'page_title' => 'Brand Market',
        //         'simpleCart' => $simpleCart,
        //         'internalCode' => '',
        //         'year' => $year,
        //         'month' => $month,
        //         'day' => $day,
        //         'time' => $time,
        //         'timezone' => $timezone,
        //         'provider_name' => '',
        //         'client_name' => strtoupper(auth()->user()->name),
        //         'client_number' => auth()->user()->id,
        //     ];
        //     // return (new \App\Mail\ClientQuotationMailer($clientQuotationInfo))->render();
        //     \Mail::to('g33k.gu@gmail.com')->send(new \App\Mail\ClientQuotationMailer($clientQuotationInfo));
        // }

        // {
        //     // @Block for provider quotation:
        //     // grouping products in simple cart details by provider name;
        //     $getProviderQuotationDobleVela = \DB::table('simple_carts as sc')
        //         ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name, scd.id ')
        //         ->selectRaw('scd.original_price as d_original_price, scd.subtotal_mxn as d_subtotal_mx, scd.price as d_price,  scd.subtotal as d_subtotal')
        //         ->selectRaw('sc.total as total_usd, sc.subtotal as subtotal_usd, scd.provider_name ')
        //         ->selectRaw('sc.total_mxn as total_mxn, sc.subtotal_mxn as subtotal_mxn ')
        //         ->selectRaw('pbm.category_doble_vela as product_parent_category')
        //     ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
        //     ->join('products_brand_market as pbm', 'pbm.id', '=', 'scd.products_brand_market_id')
        //         ->where('sc.user_id', auth()->user()->id)
        //         ->where('sc.status', 'open')
        //         ->where('scd.provider_name', 'doble-vela')
        //         ->groupBy('scd.id', 'scd.model', 'scd.name', 'scd.color', 'scd.provider_name')
        //         ->orderBy('scd.created_at');
        //     // print("<pre>\$getProviderQuotationDobleVela:  {$getProviderQuotationDobleVela->toSql()}</pre>");
        //     $simpleCart = $getProviderQuotationDobleVela->get();
        //     // print("<pre>\$productsProviderDobleVela: " . count($productsProviderDobleVela) . "</pre>");
        //     $dobleVelaQuotation = [
        //         'page_title' => 'Brand Market',
        //         'simpleCart' => $simpleCart,
        //         'internalCode' => $simpleCart[0]->uuid,
        //         'year' => $year,
        //         'month' => $month,
        //         'day' => $day,
        //         'time' => $time,
        //         'timezone' => $timezone,
        //         'provider_name' => 'DOBLE VELA',
        //         'client_name' => auth()->user()->name,
        //         'client_number' => auth()->user()->id,
        //     ];
        //     // return (new \App\Mail\ProviderQuotationMailer($dobleVelaQuotation))->render();
        //     \Mail::to('g33k.gu@gmail.com')->cc('gonzgarcia@icloud.com')->send(new \App\Mail\ProviderQuotationMailer($dobleVelaQuotation));

        //     // // @Block for provider quotation:
        //     // // grouping products in simple cart details by provider name;
        //     // $getProviderQuotationfourPromotional = \DB::table('simple_carts as sc')
        //     //     ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name')
        //     //     ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
        //     //     ->where('sc.user_id', auth()->user()->id)
        //     //     ->where('sc.status', 'open')
        //     //     ->where('scd.provider_name', 'four-promotional')
        //     //     ->groupBy('scd.id', 'scd.model', 'scd.name', 'scd.color', 'scd.provider_name')
        //     //     ->orderBy('scd.created_at');
        //     // // print("<pre>\$getProviderQuotationfourPromotional:  {$getProviderQuotationfourPromotional->toSql()}</pre>");
        //     // $simpleCart = $getProviderQuotationfourPromotional->get();
        //     // // print("<pre>\$productsProviderDobleVela: " . count($productsProviderDobleVela) . "</pre>");
        //     // $fourPromotionalQuotation = [
        //     //     'page_title' => 'Brand Market',
        //     //     'simpleCart' => $simpleCart,
        //     //     'internalCode' => $internalCode,
        //     //     'year' => $year,
        //     //     'month' => $month,
        //     //     'day' => $day,
        //     //     'time' => $time,
        //     //     'timezone' => $timezone,
        //     //     'provider_name' => 'FOUR PROMOTIONAL',
        //     //     'client_name' => auth()->user()->name,
        //     //     'client_number' => auth()->user()->id,
        //     // ];

        //     // // @Block for provider quotation:
        //     // // grouping products in simple cart details by provider name;
        //     // $getProviderQuotationfourPromotional = \DB::table('simple_carts as sc')
        //     //     ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name')
        //     //     ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
        //     //     ->where('sc.user_id', auth()->user()->id)
        //     //     ->where('sc.status', 'open')
        //     //     ->where('scd.provider_name', 'promo-opcion')
        //     //     ->groupBy('scd.id', 'scd.model', 'scd.name', 'scd.color', 'scd.provider_name')
        //     //     ->orderBy('scd.created_at');
        //     // // print("<pre>\$getProviderQuotationfourPromotional:  {$getProviderQuotationfourPromotional->toSql()}</pre>");
        //     // $simpleCart = $getProviderQuotationfourPromotional->get();
        //     // // print("<pre>\$productsProviderDobleVela: " . count($productsProviderDobleVela) . "</pre>");
        //     // $fourPromotionalQuotation = [
        //     //     'page_title' => 'Brand Market',
        //     //     'simpleCart' => $simpleCart,
        //     //     'internalCode' => $internalCode,
        //     //     'year' => $year,
        //     //     'month' => $month,
        //     //     'day' => $day,
        //     //     'time' => $time,
        //     //     'timezone' => $timezone,
        //     //     'provider_name' => 'PROMO OPCION',
        //     //     'client_name' => auth()->user()->name,
        //     //     'client_number' => auth()->user()->id,
        //     // ];

        // }

        // {
        //     // @BLOCK BRAND MARKET
        //     // grouping products in simple cart details by provider name;
        //     $getBrandMarketQuotation = \DB::table('simple_carts as sc')
        //         ->selectRaw('scd.id, sc.user_id, sc.uuid, scd.model, scd.name, scd.quantity, scd.color, scd.provider_name, scd.id ')
        //         ->selectRaw('scd.original_price as d_original_price, scd.subtotal_mxn as d_subtotal_mx, scd.price as d_price,  scd.subtotal as d_subtotal')
        //         ->selectRaw('pbm.category_doble_vela as product_parent_category')
        //         ->selectRaw('sc.total as total_usd, sc.subtotal as subtotal_usd, scd.provider_name ')
        //         ->selectRaw('sc.total_mxn as total_mxn, sc.subtotal_mxn as subtotal_mxn ')
        //         ->join('simple_cart_details as scd', 'sc.id', '=', 'scd.simple_cart_id')
        //     ->join('products_brand_market as pbm', 'pbm.id', '=', 'scd.products_brand_market_id')
        //         ->where('sc.user_id', auth()->user()->id)
        //         ->where('sc.status', 'open')
        //         ->groupBy('scd.id', 'scd.provider_name')
        //         ->orderBy('scd.provider_name');
        //     $simpleCart = $getBrandMarketQuotation->get();
        //     // print("<pre>\$getBrandMarketQuotation:  {$getBrandMarketQuotation->toSql()}</pre>");
        //     // print("<pre>\$simpleCart: " . count($simpleCart) . "</pre>");
        //     $brandMarketQuotation = [
        //         'page_title' => 'Brand Market',
        //         'simpleCart' => $simpleCart,
        //         'internalCode' => $internalCode,
        //         'year' => $year,
        //         'month' => $month,
        //         'day' => $day,
        //         'time' => $time,
        //         'timezone' => $timezone,
        //         'client_name' => auth()->user()->name,
        //         'client_number' => auth()->user()->id,
        //     ];
        //     // return (new \App\Mail\BrandMarketQuotationMailer($brandMarketQuotation))->render();
        //     \Mail::to('g33k.gu@gmail.com')->cc('gonzgarcia@icloud.com')->send(new \App\Mail\BrandMarketQuotationMailer($brandMarketQuotation));
        //     // // return view('frontend.mailing.internal-quotation', $brandMarketQuotation);
        //     // return dd("STUFF");

        // }
        // return @redirect(@route('account.home'));
    }

    /**
     * Show All Carts in Validation status
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SimpleCart  $simpleCart
     * @return \Illuminate\Http\Response
     */
    public function pending_validation(Request $request)
    {
        $rows = SimpleCart::where('status', 'Validation')->where('user_id', auth()->user()->id)->paginate(10, ['id', 'created_at', 'total', 'subtotal']);
        $columns = ['Solicitado', 'Subtotal', 'Descuento', 'Total', 'Artículos'];
        return view('all_my_shoppings', [
            'rows' => $rows, 'columns' => $columns,
            'page_title' => 'Compras Pendientes de Aprobar', 'message' => 'Aquí verás los carritos de compra que están pendientes de aprobación del pago correspondiente.'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Show Successful Shoppings
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SimpleCart  $simpleCart
     * @return \Illuminate\Http\Response
     */
    public function show_success_shoppings(Request $request)
    {
        $rows = SimpleCart::where('status', 'Completed')->where('user_id', auth()->user()->id)->paginate(10, ['id', 'created_at', 'total', 'subtotal']);
        $columns = ['Solicitado el', 'Subtotal', 'Descuento', 'Total de la Compra', 'Productos Comprados'];
        return view('all_my_shoppings', [
            'rows' => $rows, 'columns' => $columns,
            'page_title' => 'Compras Exitosas', 'message' => 'Te presentamos todas tus compras exitosas en el listado de abajo.'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Update the specified resource as Wishlist
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SimpleCart  $simpleCart
     * @return \Illuminate\Http\Response
     */
    public function saveAsWishlist(Request $request)
    {
        // dd($request->input());
        $row = SimpleCart::findOrFail($request->id);
        if (Gate::denies('cart', $row)) {
            abort(403);
        }

        $row->wishlist_name = $request->wishlist_name;
        $row->status = 'wishlist';
        $row->save();
        if (isset($request->wishList2Cart_id)) {
            $row = SimpleCart::findOrFail($request->wishList2Cart_id);
            // $row->wishlist_name = $request->wishList2Cart_id;
            $row->status = 'open';
            $row->save();
        }

        return redirect(route('cart.wish-list'))->with('success', 'Categoría actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SimpleCart  $simpleCart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (Gate::denies('cart', SimpleCart::find($request->simple_cart_id))) {
            abort(403);
        }

        $redeem = 0.35;
        $products = SimpleCartDetails::whereNull('deleted_at')->where('simple_cart_id', $request->simple_cart_id)->get();
        $subtotal = 0;
        $total = 0;
        $exchangeRate = $this->getExchangeRate()[1];
        foreach ($products as $product) {
            if ($product->id == $request->simple_cart_details_id) {
                SimpleCartDetails::findOrFail($request->simple_cart_details_id)->delete();
            }
        }

        // $total = $subtotal;
        $updateTotals = SimpleCartDetails::selectRaw("sum(subtotal_mxn) as subtotal_mxn, sum(subtotal) as subtotal")
            ->where('simple_cart_id', $request->simple_cart_id)
            ->whereNull('deleted_at')->first();
        $SimpleCart = SimpleCart::where('id', $request->simple_cart_id)->first();
        // return redirect(route('cart.index'));
        $SimpleCart->subtotal = $updateTotals->subtotal;
        $SimpleCart->subtotal_mxn = $updateTotals->subtotal_mxn;
        if ($SimpleCart->subtotal > 0 && $SimpleCart->subtotal_mxn > 0) {
            $SimpleCart->total = ($updateTotals->subtotal + ($SimpleCart->freight_amount / ($exchangeRate - $redeem)));
            $SimpleCart->total_mxn = ($updateTotals->subtotal_mxn + $SimpleCart->freight_amount);
        } else {
            $SimpleCart->total = 0;
            $SimpleCart->total_mxn = 0;
        }
        $SimpleCart->save();
        return redirect(route('cart.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SimpleCart  $simpleCart
     * @return \Illuminate\Http\Response
     */
    function empty(Request $request)
    {

        if (Gate::denies('cart', SimpleCart::findOrFail($request->id))) {
            abort(403);
        }

        // Get all products for this cart
        if ($products = SimpleCartDetails::where('simple_cart_id', $request->id)->get()) {
            foreach ($products as $p) {
                $tmp = SimpleCartDetails::findOrFail($p->id);
                $tmp->delete();
            }
        }
        SimpleCart::findOrFail($request->id)->delete();
        return redirect(route('cart.index'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wishList(Request $request)
    {
        $request->user()->authorizeRoles(['user']);
        return view('frontend.cart.wish-list', [
            'title' => '', 'current_route' => Route::currentRouteName(),
            'page_title' => 'Plataforma de Administración'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function openWishList(Request $request)
    {
        $request->user()->authorizeRoles(['user']);
        // dd($request->input());
        $WhishList = SimpleCart::where('id', $request->simple_cart_id)->where('status', 'wishlist')->first();

        if (Gate::denies('cart', $WhishList)) {
            abort(403);
        }

        $hasCartOpen = SimpleCart::where('status', 'open')->where('user_id', auth()->user()->id)->first();
        if (!$hasCartOpen) {
            $WhishList->status = 'Open';
            $WhishList->save();
            redirect(route('cart.index'));
        } else {
            return view('frontend.cart.confirm-wishlist-2-cart', [
                'title' => '', 'openCart' => $hasCartOpen, 'wishList2Cart' => $WhishList, 'current_route' => Route::currentRouteName(),
                'page_title' => 'Plataforma de Administración'
            ]);
        }
        // return view('frontend.cart.wish-list', ['title'=>'', 'current_route'=>Route::currentRouteName(),
        // 'page_title'=>'Plataforma de Administración']);
    }
}
