<?php

namespace App\Http\Controllers;

use App\ProductsBrandMarket;

use DB;
use Illuminate\Http\Request;
use Route;

use GuzzleHttp\Client as GuzzleClient;
// use GuzzleHttp\Exception\RequestException;
// use GuzzleHttp\Psr7;
// use SoapClient;
// use DOMDocument;
// use Storage;
// use Illuminate\Http\UploadedFile;
class FourPromotionalController extends Controller
{
	public $current_route = '';
	public $current_user = '';
	public $title = 'Administration Panel';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{ }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		// dd('hola mundo');
		// $rows = ProductsFourPromotional::paginate(1500);
		// $rows = ImportComplementWs4promotional::paginate(1500);
		// $rows = ProductsFourPromotional::simplePaginate(100)->onEachSide(5);
		// $rows = DB::table('import4_promotional_products')->simplePaginate(100);
		// dd($rows);
		// CUSTOM SQL
		$sqlGetWSRows = "select DISTINCT REPLACE(model, ' ', '-') AS id_article_formatted, category, sub_category, ";
		$sqlGetWSRows .= "description, color, name, model_color ";
		$sqlGetWSRows .= " from import4_promotional_products ws";
		// $sqlGetWSRows .= " in (select distinct REPLACE(id_article, ' ', '-') from import4_promotional_products)";
		$rows = DB::select($sqlGetWSRows);
		// dd($rows);
		$columns = ['id_article_formatted', 'category', 'sub_category', 'description', 'color', 'name', 'model_color'];
		return view('backoffice.products.4-promotional.index', [
			'rows' => compact('rows'), 'columns' => $columns,
			'page_title' => $this->title . ' - Four Promotional Products',
			'message' => 'Aquí podrás consultar, editar y borrar las categorías de los productos de la plataforma'
		])
			->with('user', auth()->user())->with('current_route', Route::currentRouteName());
	}

	/**
	 * complementWebServiceData
	 *
	 * Complement Web Service Data with First Excel Data Source
	 *
	 * @param Type $var Description
	 * @return type
	 * @throws conditon
	 **/
	public function complementWebServiceData(Request $request)
	{
		print("<pre>Start Migration Process</pre><br/>\n");
		// $wsRows = DB::table('import_complement_ws_doble_velas')
		$wsRows = DB::table('import4_promotional_products')
			->where('model', '<>', '');
		// ->groupBy('family', 'subfamily', 'color', 'article_id', 'id');
		print("<pre>total: {$wsRows->count()} SQL {$wsRows->toSql()}</pre><br/>\n");

		if ($rows = $wsRows->get()) {
			foreach ($rows as $row) {
				// print("<pre></pre>");
				$parent = $this->_saveProduct($row);
			}
		}
	}


	/**
	 * saveParentProduct
	 *
	 * Save Unique Product AS Parent ID for co-related table
	 *
	 * @param object $row
	 * @return object
	 **/
	private function _saveProduct($row)
	{
		$hasDuplicates = ProductsBrandMarket::where('model', '=', $row->model)
			->where('category_4_promotional', $row->category)
			->where('subcategory_4_promotional', $row->sub_category)
			->where('color', $row->color);
		// dd($hasDuplicates->get()->count());
		if ($hasDuplicates->get()->count()) {
			print("is Product Duplicated: {$hasDuplicates->toSql()}");
			print("<pre style=\"color:red;\">[{$row->id}] / {$row->model} / {$row->category} / {$row->sub_category}</pre>");
			return false;
		}

		$Product4Promotional = new ProductsBrandMarket();
		// dd([$row, $categoryId, $subCategoryId]);
		print("<pre>{$row->category}, {$row->sub_category}</pre>");
		list($categoryId, $subCategoryId) = $this->_setCategorySubcategoryBrandMarket(strtoupper($row->category), strtoupper($row->sub_category));
		print("<pre>It supposed to be a couple of info here: {$categoryId}, {$subCategoryId}</pre>");
		if (!$categoryId && !$subCategoryId) {
			print("<pre>*** ERROR ***<span style=\"background-color:#ff0000\">{$categoryId}, {$subCategoryId}</span></pre>");
		}
		$Product4Promotional->category_id = $categoryId;
		$Product4Promotional->subcategory_id = $subCategoryId;
		$Product4Promotional->provider_name = '4-promotional';
		$Product4Promotional->model = $row->model; // BM (inicialArticulo) / (codigo-color)  / (inicial-material) /  num_proveedor / (inicial Nombre)
		// $Product4Promotional->model = bin2hex($row->model); // BM (inicialArticulo) / (codigo-color)  / (inicial-material) /  num_proveedor / (inicial Nombre)
		$Product4Promotional->color = $row->color;
		$Product4Promotional->colors = '';
		$Product4Promotional->name = $row->name;
		$Product4Promotional->slug = \Str::slug($row->model);
		$Product4Promotional->description = $row->description;
		$Product4Promotional->size = $row->product_size; /////////// find this info at other db $specificModel->product_size
		$Product4Promotional->material = $row->material; /////////// find this info at other db  $specificModel->material
		$Product4Promotional->printing = $row->printing_methods;
		// $Product4Promotional->printing_area = $row->print_area;
		$Product4Promotional->package_weight = (float) $row->box_net_weight; /////////// find this info at other db $specificModel->box_net_weight
		$Product4Promotional->package_height = (float) $row->height;
		$Product4Promotional->package_width = (float) $row->width;
		$Product4Promotional->package_length = (float) $row->depth; /////////// find this info at other db $specificModel->depth
		$Product4Promotional->package_quantity = (float) $row->pieces; /////////// find this info at other db  $specificModel->
		$Product4Promotional->image = $row->image_by_color; /////////// find this info at other db  $specificModel->slide_image

		$Product4Promotional->original_price = 0; // (float) $row->price_distribution;
		$Product4Promotional->price = 0;
		$Product4Promotional->pieces = intval(str_replace(",", "", $row->pieces));
		$Product4Promotional->existence = 'True';
		// $Product4Promotional->min_stock_alert = ''; /////////// find this info at other db
		// $Product4Promotional->min_stock_message = ''; /////////// find this info at other db
		// $Product4Promotional->is_featured = ''; /////////// find this info at other db
		$Product4Promotional->article_id = $row->model;
		$Product4Promotional->created_at = Date('Y-m-d h:i:s');
		$Product4Promotional->stock = 0; /////////// find this info at other db $specificModel->slide_image
		$Product4Promotional->orig_id = $row->id;
		$Product4Promotional->category_4_promotional = $row->category;
		$Product4Promotional->subcategory_4_promotional = $row->sub_category;
		$Product4Promotional->save();
		print("*");
		print("{$row->id} - Saved product: {$Product4Promotional->name} <br/>");
		// $rowFromWebService->delete();
		//   return $Product4Promotional->id;

	}


	/**
	 * getFullDB
	 *
	 * get Full database from ws
	 *
	 * @param Type $var Description
	 * @return type
	 * @throws conditon
	 **/
	public function getFullDB()
	{

		$dbFileName = 'providers-db/four-promotional.json';

		$httpClient = new GuzzleClient();

		$query = 'http://forpromotional.homelinux.com:9090/WsEstrategia/inventario';
		$request = $httpClient->request('GET', $query);
		if ($request->getStatusCode() == '200' && $request->getReasonPhrase() == 'OK') {
			// print('\$request->getReasonPhrase(): ' . $request->getReasonPhrase() . "<br/>");
			$response = $request->getBody();
			$content = $response->getContents();
			// print("<pre>responseContent: {$content}</pre>");
			// $exchange = json_decode($content);
			// $a = var_export($content);
			$a = json_encode($content);
			// print_r($a);
			// print("Lo que viene en la letra AAAA " . $a . " <br/>");
			\Storage::put("providers-db/{$dbFileName}", (string) $content);
			// list($exchangeRateBuy, $exchangeRateSell) = [$exchange->bmx->series[0]->datos[0]->dato, $exchange->bmx->series[1]->datos[0]->dato];
			// return [$exchangeRateBuy, $exchangeRateSell];
		}
	}

	/**
	 * getPrice
	 *
	 * match the product from brand market to db WS
	 * updates the price of brand market product
	 * @return type
	 **/
	public function getPrice()
	{
		// $dbFileName = 'providers-db/four-promotional.json';
		$dbFileName = 'providers-db/four-promotional.json';


		$db4Promotional = \Storage::get("providers-db/{$dbFileName}");

		// print($db4Promotional);
		$json = json_decode($db4Promotional);
		// print_r($json);
		// $rows = collect($json);
		// dd($json[0]);
		$count = 1;
		foreach ($json as $rowDb) {
			if (isset($rowDb->nombre_articulo)) {
				print("<pre>\$rowDb->nombre_articulo: " . strtoupper($rowDb->nombre_articulo));
				print("   ->id_article: " . strtoupper($rowDb->id_articulo));
				print("   ->disponible: " . strtoupper($rowDb->disponible));
				print("   ->precio: " . strtoupper($rowDb->precio));
				print("   ->color: " . strtoupper($rowDb->color));
				print("   ->categoria: " . strtoupper($rowDb->categoria));
				print("   ->sub_categoria: " . strtoupper($rowDb->sub_categoria));
				print("</pre>");
				$productName = strtoupper($rowDb->nombre_articulo);
				$productDescription = strtoupper($rowDb->descripcion);
				$articleId = strtoupper($rowDb->id_articulo);
				// print("<pre>\$articleId: ${articleId}</pre>");
				$productColor = strtoupper($rowDb->color);
				$productPrice = $rowDb->precio;
				$productPieces = $rowDb->disponible;
				if (
					$ProductsBrandMarket = ProductsBrandMarket::where('provider_name', '4-promotional')
					// ->where('name', $productName)
					->where('description', $productDescription)
					// ->orWhere('article_id', \Str::slug($articleId))
					->orWhere('article_id', $articleId)
					->where('color', $productColor)
					// ->where('color', $productColor)
					->first()
				) {
					print("<pre>\$productName: {$productName} --- there is a match here {$ProductsBrandMarket->model} ->color: {$ProductsBrandMarket->color}</pre>");
					$ProductsBrandMarket->pieces = $productPieces;
					$ProductsBrandMarket->color = $productColor;
					$ProductsBrandMarket->original_price = $productPrice;
					$ProductsBrandMarket->save();
				}

				// else if (
				// 	$ProductsBrandMarket = ProductsBrandMarket::where('provider_name', '4-promotional')
				// 	->where('model', $articleId)
				// 	->where('color', $productColor)
				// 	// ->where('color', $productColor)
				// 	->first()
				// ) {
				// 	print("<pre>******** by article id::: {$articleId} there is a match here {$ProductsBrandMarket->model} ->color: {$ProductsBrandMarket->color}</pre>");
				// 	$ProductsBrandMarket->pieces = $productPieces;
				// 	$ProductsBrandMarket->color = $productColor;
				// 	$ProductsBrandMarket->original_price = $productPrice;
				// 	$ProductsBrandMarket->save();
				// }
			} else {
				// dd($rowDb);
				print("<pre>THIS GUY IS COMMITING FELONY: ");
				print("   ->descripcion: " . strtoupper($rowDb->descripcion));
				print("   ->piezas: " . strtoupper($rowDb->disponible));
				print("   ->precio: " . strtoupper($rowDb->precio));
				print("   ->color: " . strtoupper($rowDb->color));
				print("   ->categoria: " . strtoupper($rowDb->categoria));
				print("   ->sub_categoria: " . strtoupper($rowDb->sub_categoria));
				print("</pre>");
			}
			$count++;
		}
		print("<pre>\$count: {$count}</pre>");
	}

	/**
	 * _setCategorySubcategoryBrandMarket
	 *
	 * Determines which is the best category/subcategory to fit the product in
	 *
	 * @param String $category category
	 * @param String $subcategory subcategory
	 * @return Array
	 **/
	public function _setCategorySubcategoryBrandMarket(String $category = null, String $subcategory = null)
	{
		print(__METHOD__ . "    {$category} => {$subcategory}");
		// BAR AND GOURMET
		{
			if (strtoupper(trim($category)) == strtoupper('ASADOR')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('BAR')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('COCINA')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('DESTAPADOR')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('JUEGO DE TAZAS')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('licorera')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('popote')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Sacacorchos')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Set de vino')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [1, NULL];
				}
			}
		}


		// Bebidas
		{

			// Cilindros de plástico
			if (
				strtoupper(trim($category)) == strtoupper('CILINDROS DE PLÁSTICO')
				|| strtoupper(trim($category)) == strtoupper('CILINDROS DE PLASTICO')
				|| strtoupper(trim($category)) == strtoupper('CILINDRO DE PLASTICO')
				|| strtoupper(trim($category)) == strtoupper('CILINDRO DE PLÁSTICO')
			) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
				) {
					// 2 - bebidas
					// 1 -  cilindros plásticos
					return [2, 1];
				}
			}

			// Cilindros metálicos
			if (strtoupper(trim($category)) == strtoupper('CILINDROS METÁLICOS') || strtoupper(trim($category)) == strtoupper('CILINDROS METALICOS')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
				) {
					// 2 - bebidas
					// 2 -  cilindros metálicos
					return [2, 2];
				}
			}
			// Cilindros metálicos
			if (strtoupper(trim($category)) == strtoupper('BOTELLA')) {
				if (strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')) {
					// 2 - bebidas
					// 2 -  cilindros metálicos
					return [2, 2];
				}
			}

			// Tazas
			if (strtoupper(trim($category)) == strtoupper('BEBIDAS')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('TAZAS')
					|| strtoupper(trim($subcategory)) == strtoupper('TAZA')
				) {
					// 2 - bebidas
					// 3 -  tazas
					return [2, 3];
				}
			}

			// Tazas
			if (strtoupper(trim($category)) == strtoupper('TAZA')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')
				) {
					// 2 - bebidas
					// 3 -  tazas
					return [2, 3];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('TARRO')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')
				) {
					// 2 - bebidas
					// 3 -  tazas
					return [2, 3];
				}
			}


			// Tazas
			if (strtoupper(trim($category)) == strtoupper('TAZAS')) {
				if (strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')) {
					// 2 - bebidas
					// 3 -  tazas
					return [2, 3];
				}
			}


			// vasos
			if (strtoupper(trim($category)) == strtoupper('VASOS Y TARROS')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
				) {
					// 2 - bebidas
					// 4 -  vasos
					return [2, 4];
				}
			}
			if (strtoupper(trim($category)) == strtoupper('BEBIDAS')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('VASOS Y TARROS')
				) {
					// 2 - bebidas
					// 4 -  vasos
					return [2, 4];
				}
			}



			// termos
			if (strtoupper(trim($category)) == strtoupper('TERMOS')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
				) {
					// 2 - bebidas
					// 5 -  termos
					return [2, 5];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('TERMO')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')
				) {
					// 2 - bebidas
					// 5 -  termos
					return [2, 5];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('BEBIDAS')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('TERMOS')
				) {
					// 2 - bebidas
					// 5 -  termos
					return [2, 5];
				}
			}
		}

		// Bolígrafos
		{
			// PLÁSTICO
			if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
				if (
					strtoupper(trim($subcategory)) == 'BOLIGRAFOS DE PLASTICO'
					|| strtoupper(trim($subcategory)) == 'BOLIGRAFO DE PLASTICO'
					|| strtoupper(trim($subcategory)) == 'BOLIGRAFO DE PLÁSTICO'
					|| strtoupper(trim($subcategory)) == 'BOLIGRAFOS DE PLÁSTICO'
				) {
					return [3, 6];
				}
			}
			// METALICOS
			if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
				if (
					strtoupper(trim($subcategory)) == 'BOLIGRAFOS METÁLICOS'
					|| strtoupper(trim($subcategory)) == 'BOLIGRAFOS METALICOS'
				) {
					return [3, 7];
				}
			}
			// MULTIFUNCIONALES
			if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
				if (
					strtoupper(trim($subcategory)) == 'BOLIGRAFOS MULTIFUNCIONALES'
					|| strtoupper(trim($subcategory)) == 'BOLIGRAFOS MULTIFUCIONALES'
				) {
					return [3, 8];
				}
			}
			// ECOLGICOS
			if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
				if (
					strtoupper(trim($subcategory)) == 'ECOLÓGICOS'
					|| strtoupper(trim($subcategory)) == 'ECOLOGICOS'
				) {
					return [3, 9];
				}
			}
		}

		// Cuidado Personal
		{
			// Belleza
			if (strtoupper(trim($category)) == strtoupper('BELLEZA')) {
				if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
					return [4, 11];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Cosmetiquera')) {
				if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
					return [4, 11];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Cuidado personal')) {
				if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
					return [4, 11];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Gancho')) {
				if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
					return [4, 11];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('KIT DE BAÑO')) {
				if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
					return [4, 11];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Salud')) {
				if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
					return [4, 11];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Toallitas humedas')) {
				if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
					return [4, 11];
				}
			}

			//
		}

		// Deportes y Entretenimiento
		{
			if (strtoupper(trim($category)) == strtoupper('Entretenimiento')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [5, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Frisbee')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [5, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Pelota')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [5, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Raquetas')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [5, NULL];
				}
			}
		}

		// Herramientas
		{
			if (strtoupper(trim($category)) == strtoupper('Accesorios para auto')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Herramientas')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Lamparas') || strtoupper(trim($category)) == strtoupper('LÁMPARAS')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Linterna')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Navajas multifuncionales')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Organizador')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Set de cables')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Set de herramientas')) {
				if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
					return [6, NULL];
				}
			}
		}

		// HIELERAS Y LONCHERAS
		{
			if (strtoupper(trim($category)) == strtoupper('Loncheras')) {
				if (strtoupper(trim($subcategory)) == 'TEXTIL') {
					return [7, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Portavianda')) {
				if (strtoupper(trim($subcategory)) == 'HOGAR') {
					return [7, NULL];
				}
			}
			//
		}

		// mascotas
		{
			if (strtoupper(trim($category)) == strtoupper('MASCOTAS')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [8, NULL];
				}
			}
		}

		// NIÑOS
		{
			if (strtoupper(trim($category)) == strtoupper('ALCANCIA')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [9, NULL];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('ESCOLARES')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [9, NULL];
				}
			}
		}

		// OFICINA
		{
			// Agendas y carpetas
			if (strtoupper(trim($category)) == strtoupper('Carpeta')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 13];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Cartera')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 13];
				}
			}

			// libretas
			if (strtoupper(trim($category)) == strtoupper('LIBRETA')) {
				if (
					strtoupper(trim($subcategory)) == 'ECOLÓGICOS'
					|| strtoupper(trim($subcategory)) == 'ECOLOGICOS'
				) {
					return [10, 14];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('LIBRETA')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 14];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('LIBRETA')) {
				if (strtoupper(trim($subcategory)) == 'SUBLIMACION') {
					return [10, 14];
				}
			}


			if (strtoupper(trim($category)) == strtoupper('Llaveros metalicos')) {
				if (strtoupper(trim($subcategory)) == 'LLAVEROS') {
					return [10, 15];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Llaveros multifuncionales')) {
				if (strtoupper(trim($subcategory)) == 'LLAVEROS') {
					return [10, 15];
				}
			}

			// Generales de oficina
			if (strtoupper(trim($category)) == strtoupper('Accesorios')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Accesorios de Oficina')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Cinta Porta Gafete')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}
			//

			if (strtoupper(trim($category)) == strtoupper('Lapicera')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('MarcaTextos')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}

			if (
				strtoupper(trim($category)) == strtoupper('Mouse pad')
				|| strtoupper(trim($category)) == strtoupper('MOUSEPAD')
			) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Mouse pad')) {
				if (strtoupper(trim($subcategory)) == 'SUBLIMACION') {
					return [10, 16];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Set de oficina')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Ventilador')) {
				if (strtoupper(trim($subcategory)) == 'OFICINA') {
					return [10, 16];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Antiestres')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [10, 19];
				}
			}
		}

		// PARAGUAS E IMPERMEABLES
		{
			if (strtoupper(trim($category)) == strtoupper('Paraguas')) {
				if (strtoupper(trim($subcategory)) == 'TEXTIL') {
					return [11, NULL];
				}
			}
		}

		// TEXTILES
		{
			// Bolsas y morrales
			if (
				strtoupper(trim($category)) == strtoupper('Bolsas ecologicas')
				|| strtoupper(trim($category)) == strtoupper('BOLSAS ECOLÓGICAS')
			) {
				if (strtoupper(trim($subcategory)) == 'TEXTIL') {
					return [12, 20];
				}
			}


			// GORRAS
			if (strtoupper(trim($category)) == strtoupper('GORRAS')) {
				if (strtoupper(trim($subcategory)) == 'TEXTIL') {
					return [12, 22];
				}
			}

			// MALETAS
			if (strtoupper(trim($category)) == strtoupper('MALETAS')) {
				if (strtoupper(trim($subcategory)) == 'TEXTIL') {
					return [12, 24];
				}
			}

			// MOCHILAS
			if (strtoupper(trim($category)) == strtoupper('MOCHILAS')) {
				if (strtoupper(trim($subcategory)) == 'TEXTIL') {
					return [12, 25];
				}
			}
		}

		//TECNOLOGÍA
		{
			// ACCESORIOS DE COMPUTO
			if (strtoupper(trim($category)) == strtoupper('ENCENDEDOR')) {
				if (
					strtoupper(trim($subcategory)) == strtoupper('TECNOLOGÍA')
					|| strtoupper(trim($subcategory)) == strtoupper('TECNOLOGIA')
				) {
					return [13, 27];
				}
			}

			if (
				strtoupper(trim($category)) == strtoupper('ACCESORIOS Y CARGADORES')
				|| strtoupper(trim($category)) == strtoupper('SOPORTE')
				|| strtoupper(trim($category)) == strtoupper('SUJETADOR')
				|| strtoupper(trim($category)) == strtoupper('TARJETERO')
				|| strtoupper(trim($category)) == strtoupper('CARPETA')
			) {
				if (
					strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
					|| strtoupper(trim($subcategory)) == 'TECNOLOGIA'
				) {
					return [13, 28];
				}
			}

			// audio
			if (
				strtoupper(trim($category)) == strtoupper('AUDIFONOS')
				|| strtoupper(trim($category)) == strtoupper('BOCINAS')
				|| strtoupper(trim($category)) == strtoupper('BOCINA')
			) {
				if (
					strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
					|| strtoupper(trim($subcategory)) == 'TECNOLOGIA'
				) {
					return [13, 29];
				}
			}


			// POWER BANK
			if (
				strtoupper(trim($category)) == strtoupper('POWER BANKS')
				|| strtoupper(trim($category)) == strtoupper('POWER BANK')
			) {
				if (
					strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
					|| strtoupper(trim($subcategory)) == 'TECNOLOGIA'
				) {
					return [13, 30];
				}
			}

			// USBS
			if (
				strtoupper(trim($category)) == strtoupper('USB')
			) {
				if (
					strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
					|| strtoupper(trim($subcategory)) == 'TECNOLOGIA'
				) {
					return [13, 30];
				}
			}
		}

		// viaje
		{
			if (strtoupper(trim($category)) == strtoupper('Porta Pasaporte')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [14, null];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Set de viaje')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [14, null];
				}
			}

			if (strtoupper(trim($category)) == strtoupper('Viaje')) {
				if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
					return [14, null];
				}
			}
		}

		// return to main stuff
	}

	//
}
