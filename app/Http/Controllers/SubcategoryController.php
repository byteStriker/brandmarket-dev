<?php

namespace App\Http\Controllers;

use App\Subcategory;
use App\Category;
use Illuminate\Http\Request;
use Route;
use Storage;
use Validator;

use Illuminate\Support\Str;

class SubcategoryController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel Sub Categories';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    // $this->middleware('auth');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Category $category)
  {
    $rows = Category::find($category->id)->Subcategories()->paginate(5);
    $columns = ['Subcategoría'];
    return view('backoffice.subcategory.index', ['rows'=>$rows, 'columns' => $columns,
    // 'optsCategory' => $optsCategory, 'selectedCategory'=>$selectedCategory,
    'page_title'=>"{$this->title} - Subcategorías", 'message' => 'Listado de Subcategorías'])
    ->with('category_id', $category->id)
    ->with('category', $category)
    ->with('current_route', Route::currentRouteName());
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Category $category)
  {

    return view('backoffice.subcategory.create', [
      'page_title'=>"{$this->title} - Subcategory - Agregar",
      'category'=> $category,
      'message' => "Subcategorías de {$category->name}"])
      ->with('current_route', Route::currentRouteName());
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Category $category, Request $request)
  {

    $validatedData = \Validator::make($request->all(),[
        'name' => 'required|unique:subcategories|max:190', ['name'=>'El :attribute es requerido'], ['name' => 'nombre'],
        // 'cover_image' => 'required', ['cover_image'=>'El :attribute es requerido'], ['cover_image' => 'imagen de portada'],
    ]);
    // dd($validatedData->fails());
    if ($validatedData->fails()) {
      return redirect(route('slide-details.create',['category' => $category]))->withInput()->withErrors($validatedData, 'create');
    }
    // $row = new Subcategory(['category_id'=>$request->category_id,'name'=>$request->name]);
    $row = new Subcategory();
    $row->category_id = $category->id;
    $row->name = $request->input('name');
    $row->slug = Str::slug($request->input('name'));

    if ($request->hasfile('cover_image')) {
      $destinationPath = "";
      $filename = $request->file('cover_image')->hashName();
      $request->file('cover_image')->store('public');
      $row->cover_image = "{$filename}";
    }

    $row->save();
    $redirectTo = [];
    $action = $request->action;
    switch ($action) {
      case 'saveAddOther':
      $redirectTo['route_name'] = 'subcategory.create';
      $redirectTo['params'] = $category;
      break;
      default:
      $redirectTo['route_name'] = 'subcategory.show';
      $redirectTo['params'] = [$category, $row];
      break;
    }
    return redirect(route($redirectTo['route_name'], $redirectTo['params']))->with('success', 'Registro Agregado');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Subcategory  $subcategory
  * @return \Illuminate\Http\Response
  */
  // public function show(Category $category, Subcategory $subCategory, Request $request)
  public function show(Request $request, Category $category, Subcategory $subcategory)
  {
    return view('backoffice.subcategory.show', ['category'=>$category,
    'row'=>$subcategory,
    'page_title'=>"{$this->title} {$subcategory->name}",
    'message' => 'Detalles de las subcategorías'])
    ->with('current_route', Route::currentRouteName());
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Subcategory  $subcategory
  * @return \Illuminate\Http\Response
  */
  public function edit(Category $category, Subcategory $subcategory)
  {
    return view('backoffice.subcategory.edit', ['row'=>$subcategory, 'category'=>$category,
    'page_title'=>"{$this->title} - {$subcategory->name}", 'message' => 'Subcategoría']
    )->with('current_route', Route::currentRouteName());
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Subcategory  $subcategory
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Category $category)
  {
    // dd($request->input());
    $row = Subcategory::findOrFail($request->id);
    if ($row->name != $request->name) {

      $validatedData = \Validator::make($request->all(),[
          'name' => 'required|unique:subcategories|max:190', ['name'=>'El :attribute es requerido'], ['name' => 'nombre'],
          // 'cover_image' => 'required', ['cover_image'=>'El :attribute es requerido'], ['cover_image' => 'imagen de portada'],
          // 'published_at' => 'date_format:Y-m-d', ['name'=>'El :attribute requiere un formato YYYY-mm-dd'],
      ]);

      if ($validatedData->fails()) {
        return redirect(route('subcategory.edit',[$category, $row]))->withInput()->withErrors($validatedData, 'create');
      }
      $row->name = $request->name;
    }

    $row->category_id = $category->id;
    $row->name = $request->name;
    $row->slug = Str::slug($row->name);

    if ($request->hasfile('cover_image')) {
      $filename = $request->file('cover_image')->hashName();
      if ($filename != $row->cover_image) {
        if(Storage::disk('public')->exists($row->cover_image)){
          Storage::disk('public')->delete($row->cover_image);
        }
      }
      $request->file('cover_image')->store('public');
      $row->cover_image = "{$filename}";

    }
    // dd($row);

    $row->save();
    return redirect(route('subcategory.show',[$category, $row]))->with('success', 'Registro Actualizado');
  }

  /**
  * Display the specified resource to confirm deletion
  *
  * @param  \App\Subcategory  $subcategory
  * @return \Illuminate\Http\Response
  */
  public function confirmDelete(Subcategory $subcategory, Request $request)
  {
    // dd([$request->subcategory->Category()->first()]);
    // $row = Subcategory::with('Category')->findOrFail($subcategory->id);
    return view('backoffice.subcategory.delete', ['row'=>$subcategory,
    'category'=> $request->subcategory->Category()->first(),
    'page_title'=>"{$this->title} - {$subcategory->name} [delete]", 'message' => 'Eliminar Subcategoría'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }
  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Subcategory  $subcategory
  * @return \Illuminate\Http\Response
  */
  public function destroy(Subcategory $subcategory)
  {
    $row = Subcategory::findOrFail($subcategory->id);
    if(Storage::disk('public')->exists($row->cover_image)){
      Storage::disk('public')->delete($row->cover_image);
    }

    $row->delete();
    return redirect(route('subcategory.index',['category'=>$subcategory->category_id]))->with('success', 'Row destroyed');
  }
}
