<?php

namespace App\Http\Controllers;

// use App\Category;
use App\ImportPromoOpcionProducts;
// use App\Imports\PromoOpcionExcelImport;
use App\ProductsBrandMarket;
use Artisaninweb\SoapWrapper\Client as SoapWrapperClient;

use GuzzleHttp\Client as GuzzleClient;
// use GuzzleHttp\Exception\RequestException;
// use GuzzleHttp\Psr7;

use DB;
use Illuminate\Http\Request;

use Route;

class PromoOpcionController extends Controller
{
    public $current_route = '';
    public $current_user = '';
    public $title = 'Administration Panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = ImportPromoOpcionProducts::paginate(1500, ['page', 'code', 'description', 'price']);
        // dd($rows);
        $columns = ['Página', 'Código', 'Descripción', 'Precio'];
        return view('backoffice.products.promo-opcion.index', [
            'rows' => $rows, 'columns' => $columns,
            'page_title' => $this->title . ' - ProductsPromoOpcion', 'message' => 'Promo Opción'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $importer1 = new  \App\Importers\ImporterPromoOpcion();
        // dd($importer1);
        return view('backoffice.products.promo-opcion.create', [
            'page_title' => "{$this->title} - Agregar", 'message' => 'Carga de Base de Datos (excel)'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * complementWebServiceData
     *
     * Complement Web Service Data with First Excel Data Source
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function complementWebServiceData(Request $request)
    {
        print("<pre>Start Migration Process</pre><br/>\n");
        // $wsRows = DB::table('import_complement_ws_doble_velas')
        $wsRows = DB::table('import_promo_opcion_products')->limit("500");
            // ->where('model', '<>', '');
        // ->groupBy('family', 'subfamily', 'color', 'article_id', 'id');
        print("<pre>total: {$wsRows->count()} SQL {$wsRows->toSql()}</pre><br/>\n");
        $initFamily = 'CILINDROS';

        if ($rows = $wsRows->get()) {
            foreach ($rows as $row) {
                // print("<pre></pre>");
                $parent = $this->_saveProduct($row, $initFamily);
            }
        }
    }


    /**
     * saveParentProduct
     *
     * Save Unique Product AS Parent ID for co-related table
     *
     * @param object $row
     * @return object
     **/
    private function _saveProduct($row, $initFamily='')
    {
        // if($row->description == ''){
        //     return true;
        // }
        // $hasDuplicates = ProductsBrandMarket::where('provider_name', 'promo-opcion')->get();
        // // dd($hasDuplicates->get()->count());
        // if ($hasDuplicates->count()) {
        //     // print("is Product Duplicated: {$hasDuplicates->toSql()}");
        //     print("<pre style=\"color:red;\">[{$row->id}] / {$row->description} / {$row->price}</pre>");
        //     return false;
        // }

        $ProductPromoOpcion = new ProductsBrandMarket();
        // dd([$row, $categoryId, $subCategoryId]);
        // print("<pre>family_promo_opcion: {$row->family_promo_opcion}</pre>");
        // list($categoryId, $subCategoryId) = $this->_setCategorySubcategoryBrandMarket(strtoupper($row->category), strtoupper($row->sub_category));
        // print("<pre>It supposed to be a couple of info here: {$categoryId}, {$subCategoryId}</pre>");
        // if (!$categoryId && !$subCategoryId) {
        //     print("<pre>*** ERROR ***<span style=\"background-color:#ff0000\">{$categoryId}, {$subCategoryId}</span></pre>");
        // }
        // $ProductPromoOpcion->category_id = $categoryId;
        // $ProductPromoOpcion->subcategory_id = $subCategoryId;
        $ProductPromoOpcion->name = strtoupper($row->description);
        $ProductPromoOpcion->provider_name = 'promo-opcion';
        $ProductPromoOpcion->model = strtoupper(\Str::slug($row->code)); // BM (inicialArticulo) / (codigo-color)  / (inicial-material) /  num_proveedor / (inicial Nombre)
        // $ProductPromoOpcion->model = bin2hex($row->model); // BM (inicialArticulo) / (codigo-color)  / (inicial-material) /  num_proveedor / (inicial Nombre)
        $ProductPromoOpcion->slug = \Str::slug($row->code);
        $ProductPromoOpcion->description = $row->description;
        $ProductPromoOpcion->original_price = number_format((float)$row->price, 2, '.', '.'); // (float) $row->price_distribution;
        $ProductPromoOpcion->article_id = $row->code;
        $ProductPromoOpcion->created_at = Date('Y-m-d h:i:s');
        $ProductPromoOpcion->orig_id = $row->id;
        print("<pre>{$row->description}</pre>");
        if ($row->id == 1) {
            $initFamily = 'cilindros';
            $ProductPromoOpcion->family_promo_opcion = $initFamily;
            // $ProductPromoOpcion->$ProductPromoOpcion->save();
            print("*");
            print("{$row->id} - Saved product: {$ProductPromoOpcion->name} <br/>");
        }

        if (strtolower($row->code) == strtolower('código')) {
            $initFamily = $row->description;
            $ProductPromoOpcion->family_promo_opcion = $initFamily;
            // $ProductPromoOpcion->$ProductPromoOpcion->save();
            print("*");
            print("{$row->id} - Saved product: {$ProductPromoOpcion->name} <br/>");
        }

        $ProductPromoOpcion->save();
        print("<pre> \$initFamily: {$initFamily}</pre>");
    }


    /**
     * getFullDB
     *
     * get Full database from ws
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function getFullDB()
    {

        $ProductsPromoOpcion = ProductsBrandMarket::where('provider_name', 'promo-opcion')->get();
        print("<pre>MEEE LLAMARON??</pre>");
        // processExistencias
        $ws = $this->ws();
        print("<pre>\$ws: here the ws instance??</pre>");
        foreach($ProductsPromoOpcion as $product){
            print("Primera vez aquí!!=!=!= <br/> ");
            // print("<pre>\$product->article_id: {$product->article_id}</pre>");
            // $s = $ws->fichaTecnica($product->article_id);
            // $param = array('codigo' => 'ANF 009 A');
            // dd($product->article_id);
            if($result = $ws->call('fichaTecnica', ['codigo'=>$product->article_id])){
                print_r($result);
                dd();

            }
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function ws()
    {

        try {
            $arrContextOptions = array(
                "ssl" => array("verify_peer" => false, "verify_peer_name" => false, 'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT),
            );
            $options = array(
                // 'debug' => true,
                // 'soap_version'=>SOAP_1_1,
                // 'password' => $wsConfig->password,
                // 'exceptions'=>true,
                'trace' => 1,
                // 'cache_wsdl'=>WSDL_CACHE_NONE,
                // 'stream_context' => stream_context_create($arrContextOptions),
                // 'ssl_method' => SOAP_SSL_METHOD_TLS,
                // 'allow_url_fopen' => 1,
                'connection_timeout' => 7200,
                'user_agent' => 'BrandMarket WS Client Agent',
            );
            return new SoapWrapperClient('http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl', $options);
        } catch (Exception $e) {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
        }
    }

    /**
     * getPrice
     *
     * match the product from brand market to db WS
     * updates the price of brand market product
     * @return type
     **/
    public function getPrice()
    {
        // $dbFileName = 'providers-db/four-promotional.json';
        $dbFileName = 'providers-db/four-promotional.json';


        $db4Promotional = \Storage::get("providers-db/{$dbFileName}");

        // print($db4Promotional);
        $json = json_decode($db4Promotional);
        // print_r($json);
        // $rows = collect($json);
        // dd($json[0]);
        $count = 1;
        foreach ($json as $rowDb) {
            if (isset($rowDb->nombre_articulo)) {
                print("<pre>\$rowDb->nombre_articulo: " . strtoupper($rowDb->nombre_articulo));
                print("   ->id_article: " . strtoupper($rowDb->id_articulo));
                print("   ->disponible: " . strtoupper($rowDb->disponible));
                print("   ->precio: " . strtoupper($rowDb->precio));
                print("   ->color: " . strtoupper($rowDb->color));
                print("   ->categoria: " . strtoupper($rowDb->categoria));
                print("   ->sub_categoria: " . strtoupper($rowDb->sub_categoria));
                print("</pre>");
                $productName = strtoupper($rowDb->nombre_articulo);
                $productDescription = strtoupper($rowDb->descripcion);
                $articleId = strtoupper($rowDb->id_articulo);
                // print("<pre>\$articleId: ${articleId}</pre>");
                $productColor = strtoupper($rowDb->color);
                $productPrice = $rowDb->precio;
                $productPieces = $rowDb->disponible;
                if (
                    $ProductsBrandMarket = ProductsBrandMarket::where('provider_name', '4-promotional')
                    // ->where('name', $productName)
                    ->where('description', $productDescription)
                    // ->orWhere('article_id', \Str::slug($articleId))
                    ->orWhere('article_id', $articleId)
                    ->where('color', $productColor)
                    // ->where('color', $productColor)
                    ->first()
                ) {
                    print("<pre>\$productName: {$productName} --- there is a match here {$ProductsBrandMarket->model} ->color: {$ProductsBrandMarket->color}</pre>");
                    $ProductsBrandMarket->pieces = $productPieces;
                    $ProductsBrandMarket->color = $productColor;
                    $ProductsBrandMarket->original_price = $productPrice;
                    $ProductsBrandMarket->save();
                }

                // else if (
                // 	$ProductsBrandMarket = ProductsBrandMarket::where('provider_name', '4-promotional')
                // 	->where('model', $articleId)
                // 	->where('color', $productColor)
                // 	// ->where('color', $productColor)
                // 	->first()
                // ) {
                // 	print("<pre>******** by article id::: {$articleId} there is a match here {$ProductsBrandMarket->model} ->color: {$ProductsBrandMarket->color}</pre>");
                // 	$ProductsBrandMarket->pieces = $productPieces;
                // 	$ProductsBrandMarket->color = $productColor;
                // 	$ProductsBrandMarket->original_price = $productPrice;
                // 	$ProductsBrandMarket->save();
                // }
            } else {
                // dd($rowDb);
                print("<pre>THIS GUY IS COMMITING FELONY: ");
                print("   ->descripcion: " . strtoupper($rowDb->descripcion));
                print("   ->piezas: " . strtoupper($rowDb->disponible));
                print("   ->precio: " . strtoupper($rowDb->precio));
                print("   ->color: " . strtoupper($rowDb->color));
                print("   ->categoria: " . strtoupper($rowDb->categoria));
                print("   ->sub_categoria: " . strtoupper($rowDb->sub_categoria));
                print("</pre>");
            }
            $count++;
        }
        print("<pre>\$count: {$count}</pre>");
    }

    /**
     * _setCategorySubcategoryBrandMarket
     *
     * Determines which is the best category/subcategory to fit the product in
     *
     * @param String $category category
     * @param String $subcategory subcategory
     * @return Array
     **/
    public function _setCategorySubcategoryBrandMarket(String $category = null, String $subcategory = null)
    {
        print(__METHOD__ . "    {$category} => {$subcategory}");
        // BAR AND GOURMET
        {
            if (strtoupper(trim($category)) == strtoupper('ASADOR')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('BAR')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('COCINA')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('DESTAPADOR')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('JUEGO DE TAZAS')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('licorera')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('popote')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Sacacorchos')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Set de vino')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [1, NULL];
                }
            }
        }


        // Bebidas
        {

            // Cilindros de plástico
            if (
                strtoupper(trim($category)) == strtoupper('CILINDROS DE PLÁSTICO')
                || strtoupper(trim($category)) == strtoupper('CILINDROS DE PLASTICO')
                || strtoupper(trim($category)) == strtoupper('CILINDRO DE PLASTICO')
                || strtoupper(trim($category)) == strtoupper('CILINDRO DE PLÁSTICO')
            ) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
                ) {
                    // 2 - bebidas
                    // 1 -  cilindros plásticos
                    return [2, 1];
                }
            }

            // Cilindros metálicos
            if (strtoupper(trim($category)) == strtoupper('CILINDROS METÁLICOS') || strtoupper(trim($category)) == strtoupper('CILINDROS METALICOS')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
                ) {
                    // 2 - bebidas
                    // 2 -  cilindros metálicos
                    return [2, 2];
                }
            }
            // Cilindros metálicos
            if (strtoupper(trim($category)) == strtoupper('BOTELLA')) {
                if (strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')) {
                    // 2 - bebidas
                    // 2 -  cilindros metálicos
                    return [2, 2];
                }
            }

            // Tazas
            if (strtoupper(trim($category)) == strtoupper('BEBIDAS')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('TAZAS')
                    || strtoupper(trim($subcategory)) == strtoupper('TAZA')
                ) {
                    // 2 - bebidas
                    // 3 -  tazas
                    return [2, 3];
                }
            }

            // Tazas
            if (strtoupper(trim($category)) == strtoupper('TAZA')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')
                ) {
                    // 2 - bebidas
                    // 3 -  tazas
                    return [2, 3];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('TARRO')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')
                ) {
                    // 2 - bebidas
                    // 3 -  tazas
                    return [2, 3];
                }
            }


            // Tazas
            if (strtoupper(trim($category)) == strtoupper('TAZAS')) {
                if (strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')) {
                    // 2 - bebidas
                    // 3 -  tazas
                    return [2, 3];
                }
            }


            // vasos
            if (strtoupper(trim($category)) == strtoupper('VASOS Y TARROS')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
                ) {
                    // 2 - bebidas
                    // 4 -  vasos
                    return [2, 4];
                }
            }
            if (strtoupper(trim($category)) == strtoupper('BEBIDAS')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('VASOS Y TARROS')
                ) {
                    // 2 - bebidas
                    // 4 -  vasos
                    return [2, 4];
                }
            }



            // termos
            if (strtoupper(trim($category)) == strtoupper('TERMOS')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('BEBIDAS')
                ) {
                    // 2 - bebidas
                    // 5 -  termos
                    return [2, 5];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('TERMO')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('SUBLIMACION')
                ) {
                    // 2 - bebidas
                    // 5 -  termos
                    return [2, 5];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('BEBIDAS')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('TERMOS')
                ) {
                    // 2 - bebidas
                    // 5 -  termos
                    return [2, 5];
                }
            }
        }

        // Bolígrafos
        {
            // PLÁSTICO
            if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
                if (
                    strtoupper(trim($subcategory)) == 'BOLIGRAFOS DE PLASTICO'
                    || strtoupper(trim($subcategory)) == 'BOLIGRAFO DE PLASTICO'
                    || strtoupper(trim($subcategory)) == 'BOLIGRAFO DE PLÁSTICO'
                    || strtoupper(trim($subcategory)) == 'BOLIGRAFOS DE PLÁSTICO'
                ) {
                    return [3, 6];
                }
            }
            // METALICOS
            if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
                if (
                    strtoupper(trim($subcategory)) == 'BOLIGRAFOS METÁLICOS'
                    || strtoupper(trim($subcategory)) == 'BOLIGRAFOS METALICOS'
                ) {
                    return [3, 7];
                }
            }
            // MULTIFUNCIONALES
            if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
                if (
                    strtoupper(trim($subcategory)) == 'BOLIGRAFOS MULTIFUNCIONALES'
                    || strtoupper(trim($subcategory)) == 'BOLIGRAFOS MULTIFUCIONALES'
                ) {
                    return [3, 8];
                }
            }
            // ECOLGICOS
            if (strtoupper(trim($category)) == strtoupper('BOLIGRAFO')) {
                if (
                    strtoupper(trim($subcategory)) == 'ECOLÓGICOS'
                    || strtoupper(trim($subcategory)) == 'ECOLOGICOS'
                ) {
                    return [3, 9];
                }
            }
        }

        // Cuidado Personal
        {
            // Belleza
            if (strtoupper(trim($category)) == strtoupper('BELLEZA')) {
                if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
                    return [4, 11];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Cosmetiquera')) {
                if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
                    return [4, 11];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Cuidado personal')) {
                if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
                    return [4, 11];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Gancho')) {
                if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
                    return [4, 11];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('KIT DE BAÑO')) {
                if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
                    return [4, 11];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Salud')) {
                if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
                    return [4, 11];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Toallitas humedas')) {
                if (strtoupper(trim($subcategory)) == 'SALUD Y CUIDADO PERSONAL') {
                    return [4, 11];
                }
            }

            //
        }

        // Deportes y Entretenimiento
        {
            if (strtoupper(trim($category)) == strtoupper('Entretenimiento')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [5, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Frisbee')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [5, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Pelota')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [5, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Raquetas')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [5, NULL];
                }
            }
        }

        // Herramientas
        {
            if (strtoupper(trim($category)) == strtoupper('Accesorios para auto')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Herramientas')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Lamparas') || strtoupper(trim($category)) == strtoupper('LÁMPARAS')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Linterna')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Navajas multifuncionales')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Organizador')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Set de cables')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Set de herramientas')) {
                if (strtoupper(trim($subcategory)) == 'HERRAMIENTAS') {
                    return [6, NULL];
                }
            }
        }

        // HIELERAS Y LONCHERAS
        {
            if (strtoupper(trim($category)) == strtoupper('Loncheras')) {
                if (strtoupper(trim($subcategory)) == 'TEXTIL') {
                    return [7, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Portavianda')) {
                if (strtoupper(trim($subcategory)) == 'HOGAR') {
                    return [7, NULL];
                }
            }
            //
        }

        // mascotas
        {
            if (strtoupper(trim($category)) == strtoupper('MASCOTAS')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [8, NULL];
                }
            }
        }

        // NIÑOS
        {
            if (strtoupper(trim($category)) == strtoupper('ALCANCIA')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [9, NULL];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('ESCOLARES')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [9, NULL];
                }
            }
        }

        // OFICINA
        {
            // Agendas y carpetas
            if (strtoupper(trim($category)) == strtoupper('Carpeta')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 13];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Cartera')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 13];
                }
            }

            // libretas
            if (strtoupper(trim($category)) == strtoupper('LIBRETA')) {
                if (
                    strtoupper(trim($subcategory)) == 'ECOLÓGICOS'
                    || strtoupper(trim($subcategory)) == 'ECOLOGICOS'
                ) {
                    return [10, 14];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('LIBRETA')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 14];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('LIBRETA')) {
                if (strtoupper(trim($subcategory)) == 'SUBLIMACION') {
                    return [10, 14];
                }
            }


            if (strtoupper(trim($category)) == strtoupper('Llaveros metalicos')) {
                if (strtoupper(trim($subcategory)) == 'LLAVEROS') {
                    return [10, 15];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Llaveros multifuncionales')) {
                if (strtoupper(trim($subcategory)) == 'LLAVEROS') {
                    return [10, 15];
                }
            }

            // Generales de oficina
            if (strtoupper(trim($category)) == strtoupper('Accesorios')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Accesorios de Oficina')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Cinta Porta Gafete')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }
            //

            if (strtoupper(trim($category)) == strtoupper('Lapicera')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('MarcaTextos')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }

            if (
                strtoupper(trim($category)) == strtoupper('Mouse pad')
                || strtoupper(trim($category)) == strtoupper('MOUSEPAD')
            ) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Mouse pad')) {
                if (strtoupper(trim($subcategory)) == 'SUBLIMACION') {
                    return [10, 16];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Set de oficina')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Ventilador')) {
                if (strtoupper(trim($subcategory)) == 'OFICINA') {
                    return [10, 16];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Antiestres')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [10, 19];
                }
            }
        }

        // PARAGUAS E IMPERMEABLES
        {
            if (strtoupper(trim($category)) == strtoupper('Paraguas')) {
                if (strtoupper(trim($subcategory)) == 'TEXTIL') {
                    return [11, NULL];
                }
            }
        }

        // TEXTILES
        {
            // Bolsas y morrales
            if (
                strtoupper(trim($category)) == strtoupper('Bolsas ecologicas')
                || strtoupper(trim($category)) == strtoupper('BOLSAS ECOLÓGICAS')
            ) {
                if (strtoupper(trim($subcategory)) == 'TEXTIL') {
                    return [12, 20];
                }
            }


            // GORRAS
            if (strtoupper(trim($category)) == strtoupper('GORRAS')) {
                if (strtoupper(trim($subcategory)) == 'TEXTIL') {
                    return [12, 22];
                }
            }

            // MALETAS
            if (strtoupper(trim($category)) == strtoupper('MALETAS')) {
                if (strtoupper(trim($subcategory)) == 'TEXTIL') {
                    return [12, 24];
                }
            }

            // MOCHILAS
            if (strtoupper(trim($category)) == strtoupper('MOCHILAS')) {
                if (strtoupper(trim($subcategory)) == 'TEXTIL') {
                    return [12, 25];
                }
            }
        }

        //TECNOLOGÍA
        {
            // ACCESORIOS DE COMPUTO
            if (strtoupper(trim($category)) == strtoupper('ENCENDEDOR')) {
                if (
                    strtoupper(trim($subcategory)) == strtoupper('TECNOLOGÍA')
                    || strtoupper(trim($subcategory)) == strtoupper('TECNOLOGIA')
                ) {
                    return [13, 27];
                }
            }

            if (
                strtoupper(trim($category)) == strtoupper('ACCESORIOS Y CARGADORES')
                || strtoupper(trim($category)) == strtoupper('SOPORTE')
                || strtoupper(trim($category)) == strtoupper('SUJETADOR')
                || strtoupper(trim($category)) == strtoupper('TARJETERO')
                || strtoupper(trim($category)) == strtoupper('CARPETA')
            ) {
                if (
                    strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
                    || strtoupper(trim($subcategory)) == 'TECNOLOGIA'
                ) {
                    return [13, 28];
                }
            }

            // audio
            if (
                strtoupper(trim($category)) == strtoupper('AUDIFONOS')
                || strtoupper(trim($category)) == strtoupper('BOCINAS')
                || strtoupper(trim($category)) == strtoupper('BOCINA')
            ) {
                if (
                    strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
                    || strtoupper(trim($subcategory)) == 'TECNOLOGIA'
                ) {
                    return [13, 29];
                }
            }


            // POWER BANK
            if (
                strtoupper(trim($category)) == strtoupper('POWER BANKS')
                || strtoupper(trim($category)) == strtoupper('POWER BANK')
            ) {
                if (
                    strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
                    || strtoupper(trim($subcategory)) == 'TECNOLOGIA'
                ) {
                    return [13, 30];
                }
            }

            // USBS
            if (
                strtoupper(trim($category)) == strtoupper('USB')
            ) {
                if (
                    strtoupper(trim($subcategory)) == 'TECNOLOGÍA'
                    || strtoupper(trim($subcategory)) == 'TECNOLOGIA'
                ) {
                    return [13, 30];
                }
            }
        }

        // viaje
        {
            if (strtoupper(trim($category)) == strtoupper('Porta Pasaporte')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [14, null];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Set de viaje')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [14, null];
                }
            }

            if (strtoupper(trim($category)) == strtoupper('Viaje')) {
                if (strtoupper(trim($subcategory)) == 'TIEMPO LIBRE') {
                    return [14, null];
                }
            }
        }

        // return to main stuff
    }

    //
}
