<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\ProductsBrandMarket;
use Route;

use DB;
class CategoryBrandMarketController extends Controller
{

  /**
  * Display all categories
  *
  * @return \Illuminate\Http\Response
  */
  public function allCategories(Request $request)
  {
    $Category = Category::with('Subcategories')->get();
    return view('frontend.category.all-categories')
    ->with('category', $Category)
    ->with('current_route', Route::currentRouteName());
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function list($category='')
  {
    $rowCategory = Category::where('slug', $category)->firstOrFail();
    $rowsSubcategory = Subcategory::with("Category")->where('category_id', $rowCategory->id)->get();
    return view('frontend.category.index')
    ->with('subcategory', $rowsSubcategory)
    ->with('current_route', Route::currentRouteName());
  }
}
