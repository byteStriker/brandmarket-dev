<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;
use Route;
use Storage;

class SlideController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    // $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $rows = Slide::with('slidedetails')->paginate(5);
    $columns = ['Nombre'];
    // dd($rows);
    return view('backoffice.slide.index', [
      'rows' => $rows, 'columns' => $columns,
      'page_title' => $this->title . ' - Slides', 'message' => 'Aquí podrás consultar, editar y borrar las galerías para el website'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('backoffice.slide.create', [
      'page_title' => "{$this->title} - Agregar", 'message' => 'Slides'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'name' => 'required|max:190', ['name' => 'El :attribute es requerido'], ['name' => 'nombre'],
      'cover_image' => 'required', ['cover_image' => 'El :attribute es requerido'], ['cover_image' => 'nombre']
    ]);
    if (!$validatedData) {
      return redirect(route('slide.create'))->withErrors($validatedData, 'create');
    }
    $row = new Slide([
      'name' => $request->name,
      'link_text' => $request->link_text,
      'link_url' => $request->link_url,
      'link_type' => $request->link_type,
      // 'cover_image' => $request->cover_image,
      'cover_type' => $request->cover_type,
      'published_at' => $request->published_at,
      'status' => $request->status
    ]);
    if ($request->hasfile('cover_image')) {
      $filename = $request->file('cover_image')->hashName();
      $path = $request->file('cover_image');
      $handle = fopen($path, 'r');
      $content = fread($handle, filesize($path));
      fclose($handle);
      \Storage::disk('slides')->put($filename, $content);
      $row->cover_image = "main-slide/{$filename}";
      // dd([$request->hasfile('cover_image'), "estoy por acá {$filename}"]);
    }
    $row->save();
    $redirectTo = [];
    $action = $request->action;
    switch ($action) {
      case 'saveAddOther':
        $redirectTo['route_name'] = 'slide.create';
        $redirectTo['params'] = [];
        break;
      case 'saveAddSlideDetail':
        $redirectTo['route_name'] = 'slide-details.create';
        $redirectTo['params'] = ['slide' => $row];
        break;
      default:
        $redirectTo['route_name'] = 'slide.show';
        $redirectTo['params'] = ['slide' => $row];
        break;
    }
    return redirect(route($redirectTo['route_name'], $redirectTo['params']))->with('success', 'Registro Agregado');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Slide  $slide
   * @return \Illuminate\Http\Response
   */
  public function show(Slide $slide)
  {
    $row = Slide::findOrFail($slide->id);
    return view('backoffice.slide.show', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name}", 'message' => 'Slide'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Slide  $slide
   * @return \Illuminate\Http\Response
   */
  public function edit(Slide $slide)
  {
    $row = Slide::findOrFail($slide->id);
    return view('backoffice.slide.edit', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name}", 'message' => 'Slide'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Slide  $slide
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Slide $slide)
  {
    $validatedData = $request->validate([
      'name' => 'required|max:190', ['name' => 'El :attribute es requerido'], ['name' => 'nombre'],
      'cover_image' => 'required', ['cover_image' => 'El :attribute es requerido'], ['cover_image' => 'nombre']

    ]);
    if (!$validatedData) {
      return redirect(route('slide.edit'))->withErrors($validatedData, 'update')->withInput($request->input());
    }
    // Slide::where('id',$request->id)->update($validatedData);
    $row = Slide::findOrFail($request->id);
    $row->name = $request->name;
    $row->link_text = $request->link_text;
    $row->link_url = $request->link_url;
    $row->link_type = $request->link_type;
    // $row->cover_image = $request->cover_image;
    if ($request->hasfile('cover_image')) {
      $filename = $request->file('cover_image')->hashName();
      if ($filename != $row->cover_image) {
        if (Storage::disk('public')->exists($row->cover_image)) {
          Storage::disk('public')->delete($row->cover_image);
        }
      }
      $request->file('cover_image')->store('storage/main-slide');
      $row->cover_image = "storage/main-slide/{$filename}";
    }
    $row->cover_type = $request->cover_type;
    $row->published_at = $request->published_at;
    $row->status = $request->status;
    $row->save();
    return redirect(route('slide.show', $row))->with('success', 'Slide actualizada');
  }

  /**
   * Display the specified resource to confirm deletion
   *
   * @param  \App\Slide  $slide
   * @return \Illuminate\Http\Response
   */
  public function confirmDelete(Slide $slide)
  {
    // dd($slide);
    $row = Slide::findOrFail($slide->id);
    return view('backoffice.slide.delete', [
      'row' => $row,
      'page_title' => "{$this->title} - {$row->name} [delete]", 'message' => 'Eliminar Slide'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Slide  $slide
   * @return \Illuminate\Http\Response
   */
  public function destroy(Slide $slide)
  {
    if ($slide->id != 1) {
      $row = Slide::findOrFail($slide->id);
      // dd($row);
      $row->delete();
    }
    return redirect(route('slide.index'))->with('success', 'Row destroyed');
  }
}
