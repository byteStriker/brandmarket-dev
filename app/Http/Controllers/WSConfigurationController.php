<?php

namespace App\Http\Controllers;

use App\WSConfiguration;
use Illuminate\Http\Request;
use Route;

class WSConfigurationController extends Controller
{

  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    // $this->middleware('auth');
  }

  /**
  * Set default information for all WS.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function setDefaults()
  {
    $WS1 = new WSConfiguration();
    $WS1->server_url = 'http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl';
    // $WS1->username = 'none';
    $WS1->server_alias = 'PromoOpcion';
    $WS1->server_url_alternating = 'https://www.promoopcion.com/excel.php?name=dbFicha';
    $WS1->password = '';
    $WS1->documentation = "WEB SERVICE 01
    PromoOpción Web Service
    Clave: 5KW1SA
     
    1.       PASO 1:
    ·         Este es el link para obtener nuestra Base de Datos (http://www.promoopcion.com/exportadb.php?ubk=MX)
    ·         Los datos se visualizarán en un navegador web (utilizar Firefox de preferencia).
    ·         Los datos se encontrarán divididos por columnas por ejemplo, Item(código hijo), Father (código padre), Family (Familia), Nombre, Descripción, Color, Colors,etc.
    ·         Para descargar la DB es necesario dirigirse hasta la parte inferior izquierda donde se encontrará un botón para exportar todo el contenido en un archivo Excel (Utilizar Firefox de preferencia).
     
     
     
    2.       PASO 2:
    ·         Obtener el Web Service “FullFilmentMX” de PromoOpcion® a través del siguiente link:
    ·         http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl
     
    3.       PASO 3:
    ·         Configurar el servicio web “FullFilmentMX” en tu ambiente:
 
     
    A continuación realizamos un ejemplo para configurar el servicio web consumiendo el método “exitencias”.
    Método de ejemplo: (existencias) 
    ·        Método: existencias
    ·         Parámetros: codigo (Código hijo) y distribuidor (*******) 
    ·        Ejemplo para su invocación: (lenguaje PHP y su Librería es nusoap )
    ·        Descarga de las librerías nusoap: http://sourceforge.net/projects/nusoap/
     
    Código del consumo del WS en PHP 
    include('lib/nusoap.php');
    \$client = new nusoap_client('http://desktop.promoopcion.com:8095/wsFullFilmentMX/FullFilmentMX.asmx?wsdl','wsdl');
    \$err = \$client->getError();
    if (\$err) {     echo 'Error en Constructor' . \$err ; }
    \$CardCode = ' DFE7471';
    \$param = array('codigo' => 'ANF 009 A', 'distribuidor' = > \$CardCode);
    \$result = \$client->call('existencias', \$param);
    print_r(\$result);
    if (\$client->fault) {
            echo 'Fallo';
            print_r(\$result);
    } else {        // Chequea errores
            \$err = \$client->getError();
            if (\$err) {             // Muestra el error
                    echo 'Error' . \$err ;
            } else {                // Muestra el resultado
                    echo 'Resultado';
                    print_r (\$result);
            }
    }
    El servicio web cuenta con 4 métodos que a continuación describo:
    ·         Método 1 : colorItem
    Regresa todos los colores que existe del articulo ingresado, por ejemplo ; ANF 006 A (Amarillo Translucido, Azul Translucido, Morado, Negro, Rosa, etc.)
    Este método se invoca con el parámetro “codigo” el cual representa al código hijo de un artículo, por ejemplo ANF 006 A:
    \$param = array('codigo' => 'ANF 009 A');
    \$result = \$client->call('colorItem', \$param);
     
    ·         Método 2 : colors
    Regresa todos los colores que maneja Promoopción. No tiene parámetros para enviar.
    \$result = \$client->call('colors');
     
    ·         Método 3 : existencias
    Regresa las existencias (stock) de todos los artículos, puede validar estos datos contra el stock que muestra nuestro sitio web una vez ingresando con su clave de distribuidor.
    Este método se invoca con dos parámetros “codigo” el cual representa al código hijo de un artículo, “distribuidor” donde se introduce su ID de distribuidor, ejemplo:
    \$CardCode = 'DFE4416';
    \$param = array('codigo' => 'ANF 009 A', 'distribuidor' = > \$CardCode);
    \$result = \$client->call('existencias', \$param);
     
    ·         Método 4 : fichaTecnica
    Regresa la descripción general del artículo, algunos de los datos que regresa son; nombre del producto, familia, color, material, tamaño, altura, anchura, peso, etc.
    Este método se invoca con un parámetro llamado “codigo” el cual representa al código hijo de un artículo, ejemplo;
    \$param = array('codigo' => 'ANF 006 A');
    \$result = \$client->call('colorItem', \$param);";
    $WS1->save();

    $WS2 = new WSConfiguration();
    $WS2->server_url = 'http://srv-datos.dyndns.info/doblevela/service.asmx';
    // $WS2->username = 'none';
    $WS2->server_alias = 'DobleVela';
    $WS2->password = '5Dp/qvDrxtWqg8powRrOzA==';

    $WS2->documentation = "WEB SERVICE 02
    Doble Vela Web Service
    Credenciales: 5Dp/qvDrxtWqg8powRrOzA==

    JSON  http://srv-datos.dyndns.info/doblevela/service.asmx ";
    $WS2->save();

    $WS3 = new WSConfiguration();
    $WS3->server_url = 'http://forpromotional.homelinux.com:9090/WsEstrategia/inventario';
    // $WS3->username = 'none';
    $WS3->server_alias = '4promotional';
    $WS3->password = '';
    $WS3->documentation = "WEB SERVICE 03
    4Promotional Web Service
    Credenciales: Ninguna

    Cambio de Catalogo – 1 vez al año en ExpoPublicitas
    Pedir por correo


    Formato JSON
    http://forpromotional.homelinux.com:9090/WsEstrategia/inventario
     
    Formato TXT
    http://forpromotional.homelinux.com:9090/WsEstrategia/inventarioTxt
    ";

    $WS3 = new WSConfiguration();
    $WS3->server_url = 'http://forpromotional.homelinux.com:9090/WsEstrategia/inventario';
    // $WS3->username = 'none';
    $WS3->server_alias = '4promotional';
    $WS3->password = '';
    $WS3->documentation = "WEB SERVICE 03
      4Promotional Web Service
      Credenciales: Ninguna

      Cambio de Catalogo – 1 vez al año en ExpoPublicitas
      Pedir por correo


      Formato JSON
      http://forpromotional.homelinux.com:9090/WsEstrategia/inventario
       
      Formato TXT
      http://forpromotional.homelinux.com:9090/WsEstrategia/inventarioTxt
      ";
    $WS3->save();
    redirect(@route('admin.dashboard'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\WSConfiguration  $wSConfiguration
  * @return \Illuminate\Http\Response
  */
  public function indexWS1()
  {
    $ws1 = WSConfiguration::findOrFail(1);
    return view('backoffice.ws_configuration.index-promo-opcion', ['ws'=>$ws1,
    'page_title'=>$this->title.' - Web Services Settings', 'message' => 'Aquí podrás actualizar la información de los servicios web'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\WSConfiguration  $wSConfiguration
  * @return \Illuminate\Http\Response
  */
  public function indexWS2()
  {
    $ws2 = WSConfiguration::findOrFail(2);
    return view('backoffice.ws_configuration.index-doble-vela', ['ws'=>$ws2,
    'page_title'=>$this->title.' - Web Services Settings', 'message' => 'Aquí podrás actualizar la información de los servicios web'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());

  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\WSConfiguration  $wSConfiguration
  * @return \Illuminate\Http\Response
  */
  public function indexWS3()
  {
    // 'debug' => env('APP_DEBUG', false)
    print(env('BANXICO_TOKEN'));
    $ws3 = WSConfiguration::findOrFail(3);
    return view('backoffice.ws_configuration.index-4-promotional', ['ws'=>$ws3,
    'page_title'=>$this->title.' - Web Services Settings', 'message' => 'Aquí podrás actualizar la información de los servicios web'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());

  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\WSConfiguration  $wSConfiguration
  * @return \Illuminate\Http\Response
  */
  public function updateWS1(Request $request)
  {
    $validatedData = $request->validate([
      'server_url' => 'required', ['server_url' => 'El :attribute es requerido', ['server_url' => 'El :attribute debe ser una url válida']],
    ]);
    if (!$validatedData) {
      return redirect(route('ws-settings.promo-opcion'))->withErrors($validatedData, 'update')->withInput($request->input());
    }
    $wsConf = WSConfiguration::findOrFail(1);
    $wsConf->server_url = $request->server_url;
    $wsConf->server_url_alternating = $request->server_url_alternating;
    $wsConf->server_alias = $request->server_alias;
    $wsConf->username = $request->username;
    $wsConf->password = $request->password;
    $wsConf->documentation = $request->documentation;
    $wsConf->save();

    // dd($wsConf);
    return redirect(route('ws-settings.promo-opcion'))->with('success', 'Configuración actualizada');

  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\WSConfiguration  $wSConfiguration
  * @return \Illuminate\Http\Response
  */
  public function updateWS2(Request $request, WSConfiguration $wSConfiguration)
  {
    $validatedData = $request->validate([
      'server_url' => 'required', ['server_url' => 'El :attribute es requerido', ['server_url' => 'El :attribute debe ser una url válida']],
    ]);
    if (!$validatedData) {
      return redirect(route('ws-settings.promo-opcion'))->withErrors($validatedData, 'update')->withInput($request->input());
    }
    $wsConf = WSConfiguration::findOrFail(2);
    $wsConf->server_url = $request->server_url;
    $wsConf->server_url_alternating = $request->server_url_alternating;
    $wsConf->server_alias = $request->server_alias;
    $wsConf->username = $request->username;
    $wsConf->password = $request->password;
    $wsConf->documentation = $request->documentation;
    $wsConf->save();

    // dd($wsConf);
    return redirect(route('ws-settings.doble-vela'))->with('success', 'Configuración actualizada');
  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\WSConfiguration  $wSConfiguration
  * @return \Illuminate\Http\Response
  */
  public function updateWS3(Request $request, WSConfiguration $wSConfiguration)
  {
    $validatedData = $request->validate([
      'server_url' => 'required', ['server_url' => 'El :attribute es requerido', ['server_url' => 'El :attribute debe ser una url válida']],
    ]);
    if (!$validatedData) {
      return redirect(route('ws-settings.promo-opcion'))->withErrors($validatedData, 'update')->withInput($request->input());
    }
    $wsConf = WSConfiguration::findOrFail(3);
    $wsConf->server_url = $request->server_url;
    $wsConf->server_url_alternating = $request->server_url_alternating;
    $wsConf->server_alias = $request->server_alias;
    $wsConf->username = $request->username;
    $wsConf->password = $request->password;
    $wsConf->documentation = $request->documentation;
    $wsConf->save();
    return redirect(route('ws-settings.4-promotional'))->with('success', 'Configuración actualizada');
  }



  /**
  * Display the specified resource to confirm deletion
  *
  * @param  \App\Article  $article
  * @return \Illuminate\Http\Response
  */
  public function confirmDelete(Article $article)
  {
    // dd($category);
    $row = Article::findOrFail($article->id);
    return view('backoffice.article.delete', ['row'=>$row,
    'page_title'=>"{$this->title} - {$row->name} [delete]", 'message' => 'Eliminar Artículo'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * getCurrencyConversion
   *
   * Get Currency Conversion from Banxico API
   *
   * @param type var Description
   * @return return array
   */
  public function getCurrencyConversion()
  {
    // See token info at: https://www.banxico.org.mx/SieAPIRest/service/v1/token
    $token = config('app.banxico_token');
    // Get catalog series from https://www.banxico.org.mx/SieAPIRest/service/v1/doc/catalogoSeries#
    $catalogs = [
        'SF43787', // Tipo de cambio pesos por dólar E.U.A. Interbancario a 48 horas Apertura compra
        'SF43786', // Tipo de cambio Pesos por dólar E.U.A. Interbancario a 48 horas Cierre venta
    ];

    $series = implode($catalogs, ',');
    $query = 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/'.$series.'/datos/oportuno?token='.$token;
    $exchange = json_decode(file_get_contents($query), true);
    list($tipoCambioCompra, $tipoCambioVenta) = [$exchange['bmx']['series'][0]['datos'], $exchange['bmx']['series'][1]['datos']];
    $tipoCambioCompraFecha = $tipoCambioCompra[0]['fecha'];
    $tipoCambioCompraPrecio = $tipoCambioCompra[0]['dato'];
    // dd($tipoCambioCompraFecha);
    $tipoCambioVentaFecha = $tipoCambioVenta[0]['fecha'];
    $tipoCambioVentaPrecio = $tipoCambioVenta[0]['dato'];

    dd(['tipoCambioCompra'=>$tipoCambioCompraPrecio, 'tipoCambioVentaPrecio'=>$tipoCambioVentaPrecio]);
  }
}
