<?php

namespace App\Http\Controllers;

use App\Category;
use App\ProductsBrandMarket;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Route;
use Storage;
use Validator;

// use App\ProductsBrandMarket;
class ProductsBrandMarketController extends Controller
{
    public $current_route = '';
    public $current_user = '';
    public $title = 'Administration Panel Products Brand Market';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->search) {
            // dd($request->search);
            $rows = ProductsBrandMarket::where('description', 'like', "%{$request->search}%")
                ->orWhere('name', 'like', "%{$request->search}%")
                ->orWhere('model', 'like', "%{$request->search}%")
                ->paginate(100);
        } else {
            $rows = ProductsBrandMarket::paginate(100);
        }
        $columns = ['Modelo', 'Nombre', 'Descripción', 'Precio'];
        return view('backoffice.products.brand-market.index', [
            'rows' => $rows, 'columns' => $columns,
            'page_title' => $this->title . ' - Product', 'message' => 'Productos'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $optsCategory = Category::all();
        $optsSubcategory = Subcategory::all();
        return view('backoffice.products.brand-market.create', [
            'optsCategory' => $optsCategory, 'optsSubcategory' => $optsSubcategory,
            'page_title' => "{$this->title} - Agregar", 'message' => 'Producto'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // $validatedData = $request->validate(
        //   ['name' => 'required|unique:products|max:190',['name'=>'El :attribute es requerido'], ['name' => 'nombre']]
        // );
        // if (!$validatedData) {
        //   return redirect(route('category.create'))->withErrors($validatedData, 'create');
        // }
        $validatedData = \Validator::make($request->all(), [
            'name' => 'required|unique:products_brand_market|max:190', ['name' => 'El :attribute es requerido'], ['name' => 'nombre'],
            // 'cover_image' => 'required', ['cover_image'=>'El :attribute es requerido'], ['cover_image' => 'imagen de portada'],
        ]);
        // dd($validatedData->fails());
        if ($validatedData->fails()) {
            return redirect(route('slide-details.create', ['category' => $category]))->withInput()->withErrors($validatedData, 'create');
        }

        $row = new ProductsBrandMarket();
        $row->category_id = $request->category_id;
        $row->subcategory_id = $request->subcategory_id;
        $row->model = $request->model;
        $row->color = $request->color;
        $row->colors = $request->colors;
        $row->name = $request->name;
        $row->slug = Str::slug($request->name);
        $row->description = $request->description;
        $row->size = $request->size;
        $row->material = $request->material;
        $row->printing = $request->printing;
        $row->printing_area = $request->printing_area;
        $row->package_weight = $request->package_weight;
        $row->package_height = $request->package_height;
        $row->package_width = $request->package_width;
        $row->package_length = $request->package_length;
        $row->package_quantity = $request->package_quantity;
        if ($request->hasfile('image')) {
            if ($request->provider_name == 'doble-vela') { }
            $destinationPath = "";
            $filename = $request->file('image')->hashName();
            $request->file('image')->store('public/doble-vela/uploads');
            $row->image = "{$filename}";
        }
        $row->price = $request->price;
        $row->existence = $request->existence;
        $row->min_stock_alert = $request->min_stock_alert;
        $row->min_stock_message = $request->min_stock_message;
        $row->is_featured = $request->is_featured;
        $row->stock = $request->stock;
        $row->save();
        $redirectTo = [];
        $action = $request->action;
        switch ($action) {
            case 'saveAddOther':
                $redirectTo['route_name'] = 'product.create';
                $redirectTo['params'] = [];
                break;
            default:
                $redirectTo['route_name'] = 'product.show';
                $redirectTo['params'] = $row;
                break;
        }
        return redirect(route($redirectTo['route_name'], $redirectTo['params']))->with('success', 'Registro Agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductsBrandMarket  $product
     * @return \Illuminate\Http\Response
     */
    public function show(ProductsBrandMarket $product)
    {
        $row = ProductsBrandMarket::with('Category')->with('Subcategory')->find($product->id); // ->findOrFail($product->id);
        // dd($row->Category->name);
        // $row = ProductsBrandMarket::where('id', $product->id)->first();
        return view('backoffice.products.brand-market.show', [
            'row' => $row,
            'page_title' => "{$this->title} - {$row->name}", 'message' => 'Producto'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductsBrandMarket  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductsBrandMarket $product)
    {
        // dd($product->category_id);
        // $row = $product->with('Category')->first();
        // dd($row->Category('name'));
        // $optsCategory = Category::all();
        // $optsSubcategory = Subcategory::all();

        $row = ProductsBrandMarket::with('Category')->with('Subcategory')->find($product->id); // ->findOrFail($product->id);
        // dd($row->Subcategory->name);

        return view('backoffice.products.brand-market.edit', [
            'row' => $row,
            'page_title' => "{$this->title} - {$row->name}", 'message' => 'Producto'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductsBrandMarket $product)
    {
        // dd($request->description);
        if ($request->name != $product->name) {
            // $validatedData = $request->validate(['name' => 'required|unique:products_brand_market|max:190',['name'=>'El :attribute es requerido'], ['name' => 'nombre']]);
            // dd($validatedData);
            // if (!$validatedData) {
            //   return redirect(route('product.edit', $product))->withErrors($validatedData, 'update')->withInput($request->input());
            // }
            $validatedData = \Validator::make($request->all(), [
                'name' => 'required|unique:products_brand_market|max:190', ['name' => 'El :attribute es requerido'], ['name' => 'nombre'],
                // 'cover_image' => 'required', ['cover_image'=>'El :attribute es requerido'], ['cover_image' => 'imagen de portada'],
            ]);
            // dd($validatedData->fails());
            if ($validatedData->fails()) {
                return redirect(route('product.edit', $product))->withInput()->withErrors($validatedData, 'create');
            }
        }

        $row = $product;
        // $row->category_id = $request->category_id;
        // $row->subcategory_id = $request->subcategory_id;
        $row->model = $request->model;
        $row->color = $request->color;
        $row->colors = $request->colors;
        $row->name = $request->name;
        $row->slug = Str::slug($request->name);
        $row->description = $request->description;
        $row->size = $request->size;
        $row->material = $request->material;
        $row->printing = $request->printing;
        $row->printing_area = $request->printing_area;
        $row->package_weight = $request->package_weight;
        $row->package_height = $request->package_height;
        $row->package_width = $request->package_width;
        $row->package_length = $request->package_length;
        $row->package_quantity = $request->package_quantity;
        if ($request->hasfile('image')) {
            $destinationPath = "";
            $filename = $request->file('image')->hashName();
            $request->file('image')->store('public');
            $row->image = "{$filename}";
        }
        $row->price = $request->price;
        $row->existence = $request->existence;
        $row->min_stock_alert = $request->min_stock_alert;
        $row->min_stock_message = $request->min_stock_message;
        $row->is_featured = $request->is_featured;
        $row->stock = $request->stock;

        $row->save();

        return redirect(route('product.show', $product))->with('success', 'Porducto actualizado');
    }

    /**
     * Display the specified resource to confirm deletion
     *
     * @param  \App\ProductsBrandMarket  $product
     * @return \Illuminate\Http\Response
     */
    public function confirmDelete(ProductsBrandMarket $product)
    {
        // dd($product);
        $row = ProductsBrandMarket::findOrFail($product->id);
        return view('backoffice.products.brand-market.delete', [
            'row' => $row,
            'page_title' => "{$this->title} - {$row->name} [delete]", 'message' => 'Eliminar Producto'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductsBrandMarket $product)
    {
        $row = ProductsBrandMarket::findOrFail($product->id);
        if (Storage::disk('public')->exists($row->image)) {
            Storage::disk('public')->delete($row->image);
        }
        $row->delete();
        return redirect(route('product.index'))->with('success', 'Registro Eliminado');
    }
}
