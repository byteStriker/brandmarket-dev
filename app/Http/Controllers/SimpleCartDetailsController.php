<?php

namespace App\Http\Controllers;

use App\SimpleCartDetails;
use Illuminate\Http\Request;

class SimpleCartDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SimpleCartDetails  $simpleCartDetails
     * @return \Illuminate\Http\Response
     */
    public function show(SimpleCartDetails $simpleCartDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SimpleCartDetails  $simpleCartDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(SimpleCartDetails $simpleCartDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SimpleCartDetails  $simpleCartDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SimpleCartDetails $simpleCartDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SimpleCartDetails  $simpleCartDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(SimpleCartDetails $simpleCartDetails)
    {
        //
    }
}
