<?php

namespace App\Http\Controllers;

use App\ProductsBrandMarket;
use App\ProductsDobleVela;
use App\WSConfiguration;
use Artisaninweb\SoapWrapper\Client as SoapWrapperClient;
use DB;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Http\Request;
use Route;
use Str;


use App\Http\Traits\DobleVelaTrait;
// use DiscountsTrait;


class DobleVelaController extends Controller
{
    use DobleVelaTrait;
    //  public $current_route = '';
    public $current_user = '';
    public $title = 'Administration Panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('hola mundo');
        // $rows = ProductsFourPromotional::paginate(10, ['item', 'father', 'family', 'name', 'description']);
        $rows = ProductsDobleVela::paginate(1000, ['element_id', 'model', 'short_name', 'color', 'price_distribution', 'price_public']);
        // dd($rows);
        $columns = ['element_id', 'model', 'short_name', 'color', 'price_distribution', 'price_public'];
        return view('backoffice.products.doble-vela.index', [
            'rows' => $rows, 'columns' => $columns,
            'page_title' => $this->title . ' - Doble Vela Products', 'message' => 'Aquí podrás consultar, editar y borrar las categorías de los productos de la plataforma'
        ])
            ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wsConfig = WSConfiguration::where('server_alias', 'DobleVela')->firstOrFail();
        // dd($wsConfig->password);

        $wsWrapperClient = new SoapWrapperClient(
            $wsConfig->server_url,
            [
                'Password' => '5Dp/qvDrxtWqg8powRrOzA==',
            ]
        );
        // dd($wsWrapperClient->getFunctions());
        dd($wsWrapperClient->GetExistenciaAll(['Key' => 'GL3210']));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        try {
            $wsConfig = WSConfiguration::where('server_alias', '=', 'DobleVela')->firstOrFail();
            // $client = new \GuzzleHttp\Client();
            // $client = new \GuzzleHttp\Client(["base_uri" => "http://srv-datos.dyndns.info/doblevela/service.asmx/GetrProdImagenes"]);
            $client = new \GuzzleHttp\Client(["base_uri" => "http://srv-datos.dyndns.info/doblevela/service.asmx/GetExistencia"]);
            $options = [
                'form_params' => [
                    // "Codigo" => "A2541", // << para GetrProdImagenes
                    "codigo" => "A2541", // <<< GetExistencia
                    // "Key" => "5Dp/qvDrxtWqg8powRrOzA==",
                    "Key" => "5Dp/qvDrxtWqg8powRrOzA==",
                ],
            ];
            $response = $client->post("http://srv-datos.dyndns.info/doblevela/service.asmx/GetrProdImagenes", $options);
            // $response = $client->post("http://srv-datos.dyndns.info/doblevela/service.asmx/GetExistencia", $options);
            echo $response->getBody();
        } catch (Exception $e) {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
        }
    }

    /**
     * complementWebServiceData
     *
     * Complement Web Service Data with First Excel Data Source
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function complementWebServiceData(Request $request)
    {

        print("<pre>Start Migration Process</pre><br/>\n");
        // $wsRows = DB::table('import_complement_ws_doble_velas')
        $wsRows = DB::table('import_doble_vela_products')
            ->where('short_name', '<>', '');
        // ->groupBy('family', 'subfamily', 'color', 'article_id', 'id');
        print("<pre>total: {$wsRows->count()} SQL {$wsRows->toSql()}</pre><br/>\n");

        if ($rows = $wsRows->get()) {
            foreach ($rows as $row) {
                // print("<pre></pre>");
                $parent = $this->_saveProduct($row);
            }
        }
    }

    /**
     * saveParentProduct
     *
     * Save Unique Product AS Parent ID for co-related table
     *
     * @param object $row
     * @return object
     **/
    private function _saveProduct($row)
    {
        $hasDuplicates = ProductsBrandMarket::where('model', '=', $row->model)
            ->where('category_doble_vela', $row->category)
            ->where('subcategory_doble_vela', $row->subcategory)
            ->where('color', $row->color);
        // dd($hasDuplicates->get()->count());
        if ($hasDuplicates->get()->count()) {
            print("is Product Duplicated: {$hasDuplicates->toSql()}");
            print("<pre style=\"color:red;\">[{$row->id}] / {$row->model} / {$row->category} / {$row->subcategory}</pre>");
            return false;
        }
        $ProductDobleVela = new ProductsBrandMarket();
        // dd([$row, $categoryId, $subCategoryId]);
        // print("<pre>{$row->category}, {$row->subcategory}</pre>");
        list($categoryId, $subCategoryId) = $this->setCategorySubcategoryBrandMarket(strtoupper($row->category), strtoupper($row->subcategory));
        print("<pre>{$categoryId}, {$subCategoryId}</pre>");
        if (!$categoryId && !$subCategoryId) {
            print("<pre>*** ERROR ***<span style=\"background-color:#ff0000\">{$categoryId}, {$subCategoryId}</span></pre>");
        }
        $ProductDobleVela->category_id = $categoryId;
        $ProductDobleVela->subcategory_id = $subCategoryId;
        $ProductDobleVela->provider_name = 'Doble-Vela';
        $ProductDobleVela->model = $row->model; // BM (inicialArticulo) / (codigo-color)  / (inicial-material) /  num_proveedor / (inicial Nombre)
        // $ProductDobleVela->model = bin2hex($row->model); // BM (inicialArticulo) / (codigo-color)  / (inicial-material) /  num_proveedor / (inicial Nombre)
        $ProductDobleVela->color = $row->color;
        $ProductDobleVela->colors = '';
        if (!empty(trim($row->short_name))) {
            $ProductDobleVela->name = trim($row->short_name);
            $ProductDobleVela->slug = Str::slug($ProductDobleVela->name);
        } else {
            $ProductDobleVela->name = substr($row->description, 0, 100);
            $ProductDobleVela->slug = Str::slug($ProductDobleVela->name);
        }
        $ProductDobleVela->description = $row->description;
        $ProductDobleVela->size = $row->product_size; /////////// find this info at other db $specificModel->product_size
        $ProductDobleVela->material = $row->material; /////////// find this info at other db  $specificModel->material
        $ProductDobleVela->printing = $row->printing_type;
        // $ProductDobleVela->printing_area = $row->print_area;
        $ProductDobleVela->package_weight = (float) $row->weight; /////////// find this info at other db $specificModel->box_net_weight
        $ProductDobleVela->package_height = (float) $row->height;
        $ProductDobleVela->package_width = (float) $row->width;
        $ProductDobleVela->package_length = (float) $row->depth; /////////// find this info at other db $specificModel->depth
        $ProductDobleVela->package_quantity = (float) $row->units_per_package; /////////// find this info at other db  $specificModel->
        $ProductDobleVela->image = ''; // $complement->slide_image; /////////// find this info at other db  $specificModel->slide_image
        if ($ProductDobleVela->provider_name == 'Doble-Vela') {
            // print(storage_path("public/{$row->family}/300X240/{$row->subfamily}/{$row->model}/{$row->model}.jpg"));
            $imagePath = "app/public/doble-vela/" . trim(strtoupper(Str::slug($row->category))) . "/300X240/" . strtoupper($row->model) . "/" . strtoupper($row->model) . ".jpg";

            $hasLocalImage = storage_path($imagePath);
            // print("<pre>{$row->model} hasLocalImage: {$hasLocalImage}</pre>");
            if (file_exists($hasLocalImage)) {
                // print("<pre>{$row->model} hasLocalImage: {$hasLocalImage}</pre>");
                $ProductDobleVela->image = "storage/doble-vela/" . trim(strtoupper(Str::slug($row->category))) . "/300X240/" . strtoupper($row->model) . "/" . strtoupper($row->model) . ".jpg";
                print("<pre>{$row->model} hasLocalImage: {$hasLocalImage}</pre>");
            }
        }

        $ProductDobleVela->original_price = (float) $row->price_distribution;
        $ProductDobleVela->price = 0;
        $ProductDobleVela->pieces = intval(str_replace(",", "", $row->units_per_package));
        $ProductDobleVela->existence = 'True';
        // $ProductDobleVela->min_stock_alert = ''; /////////// find this info at other db
        // $ProductDobleVela->min_stock_message = ''; /////////// find this info at other db
        // $ProductDobleVela->is_featured = ''; /////////// find this info at other db
        $ProductDobleVela->article_id = $row->article_id;
        $ProductDobleVela->created_at = Date('Y-m-d h:i:s');
        $ProductDobleVela->stock = 0; /////////// find this info at other db $specificModel->slide_image
        $ProductDobleVela->orig_id = $row->id;
        $ProductDobleVela->category_doble_vela = $row->category;
        $ProductDobleVela->subcategory_doble_vela = $row->subcategory;
        $ProductDobleVela->save();
        print("*");
        print("{$row->id} - Saved product: {$ProductDobleVela->name} <br/>");
        // $rowFromWebService->delete();
        //   return $ProductDobleVela->id;
    }


    /**
     * complementWebServiceImages
     *
     * Complements the missing images when empty in database.
     *
     * @return void
     **/
    public function complementWebServiceImages()
    {

        $rowsWithOutImages = DB::table('products_brand_market as p');
        $rowsWithOutImages->selectRaw('model, article_id, name, category_doble_vela, subcategory_doble_vela');
        $rowsWithOutImages->whereRaw("image = ''");
        $rowsWithOutImages->groupBy('model');
        $rowsWithOutImages->orderBy('model');
        $rowsWithOutImages->chunk(25, function ($emptyImage) {
            // dd($emptyImage);
            $missingImages = [];

            foreach ($emptyImage as $image) {
                // print("<pre>image {$image->article_id}</pre>");
                array_push($missingImages, $image->article_id);
            }
            $key = '5Dp/qvDrxtWqg8powRrOzA==';

            print("Missing images count ? " . count($missingImages) . " <br/>");
            $url = 'http://srv-datos.dyndns.info/doblevela/service.asmx/GetrProdImagenes';
            $url .= "?Codigo={'CLAVES':[\"" . implode('","', $missingImages) . "\"]}";
            $url .= "&Key={$key}";
            print("<code>{$url}</code>");
            // dd($missingImages);
            $httpClient = new GuzzleClient();
            $response = $httpClient->request('GET', $url);
            $xml = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_PARSEHUGE);
            $return = json_decode($xml[0]);
            if ($return->intCodigo == 0) {
                if (count($return->Resultado->MODELOS) >= 1) {

                    $findImages = collect($return->Resultado->MODELOS);
                    if ($found = $findImages->where('encodedImage')) {
                        print("<p>encodedImage index where found in response:<br/></p>");
                        // $encodedImages = $found->encodedImage;
                        foreach ($found as $response) {
                            if (isset($response->MODELO)) {
                                $model = $response->MODELO;
                                // print("<pre>\$model: {$model}</pre>");
                                if (count($response->encodedImage)) {
                                    foreach ($response->encodedImage as $encImg) {
                                        // print("<pre>response: " . print_r($response->encodedImage, 1) . "</pre>");
                                        if ($ProductMissingImage = ProductsBrandMarket::where('model', '=', $model)->whereRaw("image = '' OR image IS NULL")->first()) {
                                            // print("<pre>\$ProductMissingImage: {$ProductMissingImage->model}</pre>");
                                            // print("<pre>".(string)$encImg."</pre>");
                                            if ((string) $encImg) {
                                                $ProductMissingImage->image = (string) $encImg;
                                                // $ProductMissingImage->image = base64_decode($encImg);
                                                print("<pre>to Save a product {$ProductMissingImage->model}</pre>");
                                                $ProductMissingImage->save();
                                            }
                                        };
                                    }
                                }
                            }
                        }
                    };
                }
            }
        });
    }


    /**
     * getExistenciasAll
     *
     * Get raw response with a json object.
     * That response will be parsed and used to update brand market products
     * This method is part of the "initial load" for the entire catalog
     *
     * @return type
     * @throws conditon
     **/
    public function getExistenciasAllWeb()
    {

        // $key = '5Dp/qvDrxtWqg8powRrOzA==';

        // \Log::info(__METHOD__ . "->" . __LINE__ . "-> fetching product price for every single row in db:\n");
        // $rows = \App\ProductsBrandMarket::where('provider_name', 'Doble-Vela');
        // if ($rows->count() > 0) {
        //     // dd($missingImages);
        //     $httpClient = new GuzzleClient();
        //     $updated = 1;
        //     $outdated = [];
        //     foreach ($rows->get() as $r) {
        //         $url = 'http://srv-datos.dyndns.info/doblevela/service.asmx/GetExistencia';

        //         $url .= "?Codigo=" . strtoupper($r->article_id);
        //         // $url .= "?Codigo={$row->article_id}";
        //         $url .= "&Key={$key}";
        //         print("<code>{$url}</code><br/>");
        //         // \Log::info("<code>{$url}</code>");

        //         $response = $httpClient->request('GET', $url);
        //         $xml = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_PARSEHUGE);
        //         $return = json_decode($xml[0]);
        //         if ($return->intCodigo == 0) {
        //             if (count($return->Resultado) >= 1) {
        //                 // \Log::info("price for:  [{$r->article_id}] / {$r->model}  " . print_r($return->Resultado, 1));
        //                 print("<pre>price for:  [{$r->article_id}] / {$r->model}  updated stock to: {$response->EXISTENCIAS}</pre>");
        //                 // if ($response = $return->Resultado[0]) {
        //                 //     // dd($response);
        //                 //     if ($ProductsDobleVela = \App\ProductsBrandMarket::where('article_id', '=', $response->MODELO)->get()) {
        //                 //         foreach ($ProductsDobleVela as $ProductDobleVela) {
        //                 //             $ProductDobleVela->stock = $response->EXISTENCIAS;
        //                 //             $ProductDobleVela->save();
        //                 //         }
        //                 //         $updated++;
        //                 //     }
        //                 // }
        //             } else {
        //                 array_push($outdated, $r->article_id);
        //             }
        //         }
        //     }
        //     print("<pre>
        //     \$updated = {$updated}; <br/>
        //     \$outdated = " . count($outdated) . ";
        //     </pre>");
        // }


        // $key = '5Dp/qvDrxtWqg8powRrOzA==';

        // print("fetching db json formatted from doble vela<br/>");
        // $url = 'http://srv-datos.dyndns.info/doblevela/service.asmx/GetExistenciaAll';
        // // $url .= "?Codigo={'CLAVES':[\"" . implode('","', $missingImages) . "\"]}";
        // $url .= "?Key={$key}";
        // print("<code>{$url}</code>");
        // // dd($missingImages);
        // $httpClient = new GuzzleClient();
        // $response = $httpClient->request('GET', $url);
        // $xml = simplexml_load_string($response->getBody()->getContents(), null, LIBXML_PARSEHUGE);
        // $return = json_decode($xml[0]);
        // if ($return->intCodigo == 0) {
        //     if (count($return->Resultado) >= 1) {
        //         foreach ($return->Resultado  as $model) {
        //             // if($product = \DB::table('products_brand_market')->where('model', 'like',"{$model->MODELO}%")){
        //             if ($product = ProductsBrandMarket::where('model', 'like', "{$model->MODELO}%")) {
        //                 if ($specificModel = $product->get()) {
        //                     foreach ($specificModel as $r) {
        //                         $r->stock = $model->EXISTENCIAS;
        //                         if ($r->stock > 1) {
        //                             $r->existence = 'true';
        //                         } else {
        //                             $r->existence = 'false';
        //                         }
        //                         // $r->original_price = $model->Price;
        //                         $r->save();
        //                         print("\$r->id {$r->id} / {$r->model} / {$r->stock} || ");
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
    }
}
