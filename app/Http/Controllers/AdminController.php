<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Route;

class AdminController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel';
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->current_user = auth()->user();
  }


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    // dd(auth()->user());
    $request->user()->authorizeRoles(['admin']);
    // dd();
    return view('backoffice.home', ['user'=>auth()->user(), 'title'=>$this->title, 'current_route'=>Route::currentRouteName(),
    'page_title'=>'Plataforma de Administración']);

  }
}
