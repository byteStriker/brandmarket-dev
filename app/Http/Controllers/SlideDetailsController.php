<?php

namespace App\Http\Controllers;

use App\SlideDetails;
use App\Slide;
use Illuminate\Http\Request;
use Route;
use Validator;
use Storage;

class SlideDetailsController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel Slide Details';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    // $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($slide)
  {
    // dd($slide);
    // $rows = SlideDetails::with('slide')->where('slide_id', $slide)->paginate(100, ['id', 'name']);
    // $rows = DB::table('slide_details')->with('slide')->where('slide_id', $slide)->paginate(100, ['id', 'name']);
    // $rows = SlideDetails::where('id', $slide)->slide()->paginate();
    $rows = Slide::find($slide)->SlideDetails()->paginate(5);
    // dd($rows);
    $columns = ['Nombre'];
    return view('backoffice.slide-details.index', [
      'rows' => $rows, 'columns' => $columns,
      'page_title' => "{$this->title} - Detalles de Galería ", 'message' => 'Listado de Fotos'
    ])
      ->with('slide_id', $slide)
      ->with('current_route', Route::currentRouteName());
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($slide)
  {

    $slide = Slide::findOrFail($slide);
    // dd($rows);

    // $slide_id = false;
    $simple_create = false;
    // if (isset($request->slide_id)) {
    //   $row = Slide::findOrFail($request->slide_id);
    //   $slide_id = $request->slide_id;
    // } else {
    //   $row = Slide::all();
    //   $simple_create = true;
    // }
    // // dd(count($row));

    return view('backoffice.slide-details.create', [
      'page_title' => "{$this->title} - Fotos - Agregar",
      'slide' => $slide, 'simple_create' => $simple_create,
      'message' => 'Fotos para el Slide'
    ])
      ->with('slide_id', $slide->id)->with('current_route', Route::currentRouteName());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, Slide $slide)
  {

    $validatedData = $request->validate([
      'name' => 'required|max:190', ['name' => 'El :attribute es requerido'], ['name' => 'nombre'],
      'cover_image' => 'required', ['cover_image' => 'El :attribute es requerido'], ['cover_image' => 'nombre']
    ]);
    if (!$validatedData) {
      return redirect(route('slide-details.create'))->withErrors($validatedData, 'create');
    }

    // dd($request->name);
    $row = new SlideDetails();
    $row->slide_id = $slide->id;
    $row->name = $request->input('name');
    $row->link_text = $request->input('link_text');
    $row->text_color = $request->input('text_color');
    $row->link_url = $request->input('link_url');
    $row->link_type = $request->input('link_type');
    $row->cover_type = $request->input('cover_type');
    $row->published_at = $request->input('published_at');
    $row->status = $request->input('status');

    if ($request->hasfile('cover_image')) {
      $filename = $request->file('cover_image')->hashName();
      $path = $request->file('cover_image');
      $handle = fopen($path, 'r');
      $content = fread($handle, filesize($path));
      fclose($handle);
      \Storage::disk('slides')->put($filename, $content);
      $row->cover_image = "storage/main-slide/{$filename}";
    }

    // dd([$row]);
    $row->save();
    $redirectTo = [];
    $action = $request->action;
    // dd($request->input('action'));
    switch ($action) {
      case 'saveAddOther':
        $redirectTo['route_name'] = 'slide-details.create';
        $redirectTo['params'] = ['slide' => $row->slide_id];
        break;
      default:
        $redirectTo['route_name'] = 'slide-details.show';
        $redirectTo['params'] = ['slide' => $slide, 'slide_details' => $row];
        break;
    }
    // dd(route($redirectTo['route_name'], $redirectTo['params']));
    return redirect(route($redirectTo['route_name'], $redirectTo['params']))->with('success', 'Registro Agregado');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\SlideDetails  $SlideDetails
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, Slide $slide, SlideDetails $slideDetails)
  {
    // print("<strong>Stuff!!</strong>" . print_r($slideDetails, 1));
    return view('backoffice.slide-details.show', [
      'slide' => $slide,
      'row' => $slideDetails,
      'page_title' => "{$this->title} {$slideDetails->name}",
      'message' => 'Detalles de las subcategorías'
    ])
      ->with('current_route', Route::currentRouteName());
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Slide  $SlideDetails
   * @return \Illuminate\Http\Response
   */
  public function edit(Slide $slide, SlideDetails $slideDetails)
  {
    // dd([$slide, $slideDetails]);
    // $row = SlideDetails::with('slide')->findOrFail($request->slide_detail);
    // $slide = Slide::all();
    return view(
      'backoffice.slide-details.edit',
      [
        'row' => $slideDetails, 'slide' => $slide, 'simple_create' => false,
        'page_title' => "{$this->title} - {$slideDetails->name}", 'message' => 'Subcategoría'
      ]
    )->with('current_route', Route::currentRouteName());
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Slide  $SlideDetails
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Slide $slide)
  {

    $validatedData = \Validator::make($request->all(), [
      'name' => 'required|max:190', ['name' => 'El :attribute es requerido'], ['name' => 'nombre'],
      // 'cover_image' => 'required', ['cover_image'=>'El :attribute es requerido'], ['cover_image' => 'imagen de portada'],
      'published_at' => 'date_format:Y-m-d', ['name' => 'El :attribute requiere un formato YYYY-mm-dd'],
    ]);
    // dd($validatedData->fails());
    if ($validatedData->fails()) {
      $slideDetails = SlideDetails::findOrFail($request->id);
      // dd($validatedData->errors());
      return redirect(route('slide-details.edit', ['slide' => $slide, 'slide_details' => $slideDetails]))->withInput()->withErrors($validatedData, 'create');
    }

    $row = SlideDetails::findOrFail($request->id);
    $row->slide_id = $request->slide_id;
    $row->name = $request->name;
    $row->link_text = $request->link_text;
    $row->text_color = $request->text_color;
    $row->link_url = $request->link_url;
    $row->link_type = $request->link_type;
    // $row->cover_image = $request->cover_image;
    if ($request->hasfile('cover_image')) {
      $filename = $request->file('cover_image')->hashName();
      if ($filename != $row->cover_image) {
        if (Storage::disk('slides')->exists($row->cover_image)) {
          Storage::disk('slides')->delete($row->cover_image);
        }
      }
      $filename = $request->file('cover_image')->hashName();
      $path = $request->file('cover_image');
      $handle = fopen($path, 'r');
      $content = fread($handle, filesize($path));
      fclose($handle);
      \Storage::disk('slides')->put($filename, $content);
      $row->cover_image = "storage/main-slide/{$filename}";
    }
    $row->cover_type = $request->cover_type;
    $row->published_at = $request->published_at;
    $row->status = $request->status;
    $row->save();

    return redirect(route('slide-details.show', ['slide' => $slide, 'slide_details' => $row]))->with('success', 'Registro Actualizado');
  }

  /**
   * Display the specified resource to confirm deletion
   *
   * @param  \App\SlideDetails  $slidedetails
   * @return \Illuminate\Http\Response
   */
  public function confirmDelete(Request $request, SlideDetails $slidedetails)
  {
    // dd($request->slide_details);
    $row = SlideDetails::where('id', $request->slide_details)->with('Slide')->first();
    // dd($row->Slide()->first());
    // dd($row);
    return view('backoffice.slide-details.delete', [
      'row' => $row,
      'slide' => $slidedetails->Slide()->first(),
      'page_title' => "{$this->title} - {$row->name} [delete]", 'message' => 'Eliminar Foto'
    ])
      ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Slide  $SlideDetails
   * @return \Illuminate\Http\Response
   */
  public function destroy(SlideDetails $slidedetails)
  {
    // dd([request()->slide_details]);
    $row = SlideDetails::with('Slide')->findOrFail(request()->slide_details);
    $row->delete();
    return redirect(route('slide-details.index', $row->Slide()->first()))->with('success', 'Row destroyed');
  }
}
