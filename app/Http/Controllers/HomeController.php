<?php

namespace App\Http\Controllers;

use App\Home;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Slide;
use Route;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;


class HomeController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel';
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    // $this->middleware('auth');
    $this->current_route = Route::currentRouteName();
    $this->current_user = auth()->user();
  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // dd(auth()->user());
    // $request->user()->authorizeRoles(['user']);
    $slides = Slide::with('SlideDetails')->where('id', 1)->orderBy('id', 'desc')->first();

    return view('frontend.welcome', [
      'slides' => $slides, 'title' => $this->title, 'current_route' => Route::currentRouteName(),
      'page_title' => ''
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function myProfile(Request $request)
  {
    $row = User::findOrFail(auth()->user()->id);
    if (Gate::allows('update-profile', $row)) {
      return view('frontend.account.profile', [
        'row' => $row,
        'page_title' => "{$this->title} - {$row->name}", 'message' => 'Usuario'
      ])
        ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
    }
    abort(403);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function updateMyProfile(Request $request)
  {
    $row = User::findOrFail(auth()->user()->id);
    if (Gate::allows('update-profile', $row)) {
      $row->name = $request->name;
      $row->email = $request->email;

      if ($row->password != $request->password) {
        $new_pass = Hash::make($request->password);
        if ($new_pass != $row->password) {
          $row->password = $new_pass;
        }
      }
      $row->update();
      $row->roles()->attach(Role::where('name', 'user')->first());
      return redirect(route('account.profile'))->with('success', 'Registro actualizado');
    }
    abort(403);
  }

  /**
   * showPrivacyNotice
   *
   * Displays a single page with common info for users
   *
   **/
  public function showPrivacyNotice()
  {
    if ($post = \DB::table('articles')->where('slug', 'privacy-notice')->first()) {
      return view('frontend.post-single', [
        'post' => $post, 'title' => $this->title, 'current_route' => Route::currentRouteName(),
        'page_title' => ''
      ]);
    } else {
      abort(404);
    }
  }

  /**
   * showTermsConditions
   *
   * Displays a single page with common info for users
   *
   **/
  public function showTermsConditions()
  {
    if ($post = \DB::table('articles')->where('slug', 'terms-and-conditions')->first()) {
      return view('frontend.post-single', [
        'post' => $post, 'title' => $this->title, 'current_route' => Route::currentRouteName(),
        'page_title' => ''
      ]);
    } else {
      abort(404);
    }
  }

  /**
   * showCookieNotice
   *
   * Displays a single page with common info for users
   *
   **/
  public function showCookieNotice()
  {
    if ($post = \DB::table('articles')->where('slug', 'cookie-notice')->first()) {
      return view('frontend.post-single', [
        'post' => $post, 'title' => $this->title, 'current_route' => Route::currentRouteName(),
        'page_title' => ''
      ]);
    } else {
      abort(404);
    }
  }
}
