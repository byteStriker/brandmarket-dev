<?php

namespace App\Http\Controllers;

use App\FinanceSettings;
use Illuminate\Http\Request;
use Route;

class FinanceSettingsController extends Controller
{
  public $current_route = '';
  public $current_user = '';
  public $title = 'Administration Panel';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {

  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $financeSettings = FinanceSettings::findOrFail(1);
    // dd(Route::currentRouteName());
    return view('backoffice.finance-settings.index', ['financeSettings'=>$financeSettings,
    'page_title'=>$this->title.' - Gain Percentaje Settings', 'message' => 'Aquí podrás actualizar la información de las ganancias'])
    ->with('user', auth()->user())->with('current_route', Route::currentRouteName());
  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\FinanceSettings  $financeSettings
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, FinanceSettings $financeSettings)
  {
    $validatedData = $request->validate([
      'general_gain_percentaje' => 'required', ['general_gain_percentaje' => 'El :attribute es requerido', ['general_gain_percentaje' => 'El :attribute debe ser una url válida']],
      'individual_gain_percentaje' => 'required', ['individual_gain_percentaje' => 'El :attribute es requerido', ['individual_gain_percentaje' => 'El :attribute debe ser una url válida']]
    ]);
    if (!$validatedData) {
      return redirect(route('finance-settings.index'))->withErrors($validatedData, 'update')->withInput($request->input());
    }
    $wsConf = FinanceSettings::findOrFail(1);
    $wsConf->general_gain_percentaje = $request->general_gain_percentaje;
    $wsConf->individual_gain_percentaje = $request->individual_gain_percentaje;
    $wsConf->save();

    // dd($wsConf);
    return redirect(route('finance-settings.index'))->with('success', 'Configuración actualizada');
  }

}
