<?php
$BrandMarketCategories = array();

$BrandMarketCategories['Category']['Bar & Gourmet'] = array();
$BrandMarketCategories['Category']['Bebidas'] = array();
array_push ($BrandMarketCategories['Category']['Bebidas'],"Cilindros plástico");
array_push ($BrandMarketCategories['Category']['Bebidas'],"Cilindro Metálicos");
array_push ($BrandMarketCategories['Category']['Bebidas'],"Tazas");
array_push ($BrandMarketCategories['Category']['Bebidas'],"Vasos");
array_push ($BrandMarketCategories['Category']['Bebidas'],"Termos");
//
$BrandMarketCategories['Category']['Bolígrafos'] = array();
array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Plástico");
array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Metálicos");
array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Ecologicos");
array_push ($BrandMarketCategories['Category']['Bolígrafos'], "Multifuncionales");
//
$BrandMarketCategories['Category']['Cuidado Personal'] = array();
array_push ($BrandMarketCategories['Category']['Cuidado Personal'], "Salud");
array_push ($BrandMarketCategories['Category']['Cuidado Personal'], "Belleza");
//
$BrandMarketCategories['Category']['Deportes y Entretenimiento'] = array();
$BrandMarketCategories['Category']['Herramientas'] = array();
$BrandMarketCategories['Category']['Hieleras y Loncheras'] = array();
$BrandMarketCategories['Category']['Mascotas'] = array();
$BrandMarketCategories['Category']['Niños'] = array();
//
$BrandMarketCategories['Category']['Oficina'] = array();
//
array_push ($BrandMarketCategories['Category']['Oficina'], "Agendas y carpetas");
array_push ($BrandMarketCategories['Category']['Oficina'], "Libretas");
array_push ($BrandMarketCategories['Category']['Oficina'], "Llaveros");
array_push ($BrandMarketCategories['Category']['Oficina'], "Generales de Oficina");
array_push ($BrandMarketCategories['Category']['Oficina'], "Portarretratos");
array_push ($BrandMarketCategories['Category']['Oficina'], "Relojes");
array_push ($BrandMarketCategories['Category']['Oficina'], "Anti-stress");
$BrandMarketCategories['Category']['Paraguas e impermeables'] = array();
$BrandMarketCategories['Category']['Textiles'] = [];
array_push ($BrandMarketCategories['Category']['Textiles'],"Bolsas y morrales");
array_push ($BrandMarketCategories['Category']['Textiles'],"Chamarras, chalecos y sudaderas");
array_push ($BrandMarketCategories['Category']['Textiles'],"Gorras");
array_push ($BrandMarketCategories['Category']['Textiles'],"Playeras y tipo Polo");
array_push ($BrandMarketCategories['Category']['Textiles'],"Maletas");
array_push ($BrandMarketCategories['Category']['Textiles'],"Mochilas");
array_push ($BrandMarketCategories['Category']['Textiles'],"Portafolios");
$BrandMarketCategories['Category']['Tecnología'] = array();
array_push($BrandMarketCategories['Category']['Tecnología'], "Accesorios de computo");
array_push($BrandMarketCategories['Category']['Tecnología'], "Accesorios Smartphone y tablets");
array_push($BrandMarketCategories['Category']['Tecnología'], "Audio");
array_push($BrandMarketCategories['Category']['Tecnología'], "Power Bank");
array_push($BrandMarketCategories['Category']['Tecnología'], "USB´s");
$BrandMarketCategories['Category']['Viaje'] = array();

// print_r($BrandMarketCategories);
// por cada uno de los elementos de Category: as ()
foreach($BrandMarketCategories['Category'] as $idx => $category){
  print("{$idx}  (".count($category).") elementos => \n");
  // print_r($idx);
  if (count($category)>=1) {
    foreach ($category as $idy => $subcategory) {
      print(" {$subcategory}, ");
    }
    print("\n====\n");
  }
  print("\n");
}
//
// foreach ($BrandMarketCategories['Category'] as $index) {
//   $category = new Category();
//   $category->name = $faker->word();
//   $category->save();
//   foreach ($category as $indexB){
//     $subcategory = new Subcategory();
//     $subcategory->category_id = $category->id;
//     $subcategory->name = $indexB;
//     $subcategory->save();
//   }
// }
?>
