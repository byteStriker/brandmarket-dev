import $ from 'jquery';
window.$ = window.jQuery = $;
// window.$ = window.jQuery = $ = require('jquery');

import 'jquery.cycle2';
import 'jquery.cycle2.carousel';
import 'jquery-ui';
import 'jquery-ui/ui/widgets/autocomplete.js';
import 'jquery-ui/ui/widgets/datepicker.js';
import Swal from 'sweetalert2';

const brandMarketHost = $('meta[name="the-url"]').attr('content');

// console.info("Hi! This is app.js in action!!");
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    'Authorization': 'Bearer ' + $('meta[name="user-token"]').attr('content'),
    'Access-Control-Allow-Origin': '*',
  },
});

$(function () {

  var slideshows = $('.cycle-slideshow').on('cycle-next cycle-prev', function (e, opts) {
    // advance the other slideshow
    slideshows.not(this).cycle('goto', opts.currSlide);
  });
  $('.goto-slide-url').click(function (event) {
    event.preventDefault();
    var me = $(this).data();

    window.location = me.url;
  });

  $('#product-thumbnails .cycle-slide').hover(function () {
    var index = $('#product-thumbnails').data('cycle.API').getSlideIndex(this);
    slideshows.cycle('goto', index);
  });



  $("input[type=radio]").on("change", function () {
    // console.info("Here we have the data! ", $(this).data());
    $('#providerStock').empty().html(0);
    $('input[type=radio]').removeAttr("checked");



    if (this.checked) {
      // console.log($(this).parent());
      var me = $(this).data();
      // console.info("radio.data: ", me);
      $('#providerStock').empty().html(me.productStock);
      $(this).attr('checked', true);
      $(this)
        .parent()
        .addClass("is-checked")
        .siblings()
        .removeClass("is-checked");
    } else {
      $(this)
        .parent()
        .removeClass("is-checked");
    }
  });

  //** AUTO COMPLETE STUFF PNM */
  $("#search").autocomplete({
    source: function (request, response) {
      $.ajax({
        url: `${brandMarketHost}/api/autocomplete`,
        type: 'POST',
        data: {
          term: request.term
        },
        dataType: "json",
        success: function (data) {
          var resp = $.map(data, function (obj) {
            // console.info("Result from search: ", obj);
            if (obj.is_category == 'no') {
              return {
                label: `${obj.model} - ${obj.name}`,
                data: { category: obj.category_slug, subcategory: obj.subcategory_slug, model: obj.model, slug: obj.slug, is_category: false, provider_name: obj.crypt_provider_name, provider_name_plain: obj.provider_name },
                value: `${obj.model} - ${obj.name}`
              };
            } else {
              return {
                label: `Categoría: ${obj.name}`,
                data: { category: obj.slug, slug: obj.slug, is_category: true },
                value: `${obj.name}`
              };
            }
          });
          response(resp);
        }
      });
    },
    minLength: 2
  }).on("autocompleteselect", function (event, ui) {
    console.info("data from product: ", ui.item.data.slug, " -> ", ui.item.data.provider_name, " -> ", ui.item.data.provider_name_plain);
    let url = "";
    if (ui.item.data.is_category == false) {
      var provOne = $('meta[name="cryptProvOne"]').attr('content'); // means doble vela
      var provTwo = $('meta[name="cryptProvTwo"]').attr('content'); // means 4 promotional
      var provThree = $('meta[name="cryptProvThree"]').attr('content'); // means promo opcion
      // console.info("is a category?: ", ui.item.data.is_category, " providerName: ", ui.item.data.provider_name.toLowerCase());
      if (ui.item.data.provider_name.toLowerCase() == '27691a11cb602cfee793680cf7c0795c') {
        console.info("providerOne ", provOne);
        if (ui.item.data.subcategory) {
          url = `${brandMarketHost}/store/product/${provOne}/${ui.item.data.category}/${ui.item.data.subcategory}/${ui.item.data.slug}/${ui.item.data.model}`;
        } else {
          url = `${brandMarketHost}/store/product/${provOne}/${ui.item.data.category}/${ui.item.data.category}/${ui.item.data.slug}/${ui.item.data.model}`;
        }
      }
      if (ui.item.data.provider_name.toLowerCase() == '5c6554c57eca3235593260c05db30f93') {
        console.info("providerTwo ", provTwo);
        if (ui.item.data.subcategory) {
          url = `${brandMarketHost}/store/product/${provTwo}/${ui.item.data.category}/${ui.item.data.subcategory}/${ui.item.data.slug}/${ui.item.data.slug}`;
        } else {
          url = `${brandMarketHost}/store/product/${provTwo}/${ui.item.data.category}/${ui.item.data.category}/${ui.item.data.slug}/${ui.item.data.slug}`;
        }
      }

      if (ui.item.data.provider_name.toLowerCase() == 'd89bee49b2e3453ce01e769a73a2bbfc') {
        console.info("providerThree ", provThree);
        if (ui.item.data.subcategory) {
          url = `${brandMarketHost}/store/product/${provThree}/${ui.item.data.category}/${ui.item.data.subcategory}/${ui.item.data.slug}/${ui.item.data.slug}`;
        } else {
          url = `${brandMarketHost}/store/product/${provThree}/${ui.item.data.category}/${ui.item.data.category}/${ui.item.data.slug}/${ui.item.data.slug}`;
        }
      }
    } else {
      url = `${brandMarketHost}/store/products/${ui.item.data.category}`;
    }
    console.info("url to find is: ", url);
    window.location.href = url;
  });;

  //  Buttons for add to wish list and add to cart
  $('#addToWishList').on('click', function (event) {
    event.preventDefault();
    // console.info("Displaying stuff of wishlist for user");
    $("#modal-add-wishlist.modal").addClass("is-active");
    // return false;
  });

  // add to wishlist
  $('#create-new-wishlist').click(function (event) {

    event.preventDefault();
    var selectedProduct = $('label.is-checked').data();
    // console.info("selectedProduct: ", selectedProduct);
    if (!selectedProduct) {
      Swal.fire(
        'Oops, ocurrió un error',
        'Olvidaste seleccionar el color del producto que deseas adquirir.',
        'error'
      );
      return false;
    }
    if (!$('#quantity').val()) {
      Swal.fire(
        'Oops, ocurrió un error',
        'Indica el número de piezas que deseas adquirir.',
        'error'
      )
      return false;
    }

    // $('#create-new-wishlist').addClass('is-loading');
    var data = {};
    data.quantity = $('#quantity').val();
    data.productId = selectedProduct.productId;
    data.productModel = selectedProduct.productModel;
    data.productParentCategory = selectedProduct.productParentCategory;
    data.productParentSubCategory = selectedProduct.productParentSubCategory;
    data.color = selectedProduct.colorName;
    data.unitPrice = $('#unitPrice').val();
    data.providerName = $('#providerName').val();
    data.originalPrice = $('#originalPrice').val();
    data.wishlist_name = $('#wishlist_name').val();

    $.ajax({
      type: "POST",
      url: `${brandMarketHost}/api/save-product-to-wishlist`,
      data: data,
      success: function (a, b) {
        let cart = a.cart;
        let cartDetail = a.cartDetails;
        // console.log("a is: ", a);
        // console.log("b is: ", b);
        $('#saveToWishList').removeClass('is-loading');
        $('#quantity').value = '';
        $('input[type=radio]').removeAttr("checked");
        $("#modal-add-product.modal").addClass("is-active");
        Swal.fire(
          'Listo!',
          'Tu producto ha sido añadido a la lista de deseos',
          'success'
        );
        $('.modal-close').click();
        window.location.reload();
      },
      error: function (a, b) {
        // $('#addToCart').removeClass('is-loading');
        Swal.fire(
          'Oops, ocurrió un error',
          'El servidor tuvo un problema al procesar tu solicitud ',
          'error'
        );
      },
      dataType: 'JSON'
    });
  });

  $('#saveToWishList').on('click', function (event) {
    var selectedWishList = $('#wishListId :selected');
    // console.info("selectedWishList: ", selectedWishList.val());
    event.preventDefault();
    var selectedProduct = $('label.is-checked').data();
    if (!selectedProduct) {
      Swal.fire(
        'Oops, ocurrió un error',
        'Olvidaste seleccionar el color del producto que deseas adquirir.',
        'error'
      );
      return false;
    }
    if (!$('#quantity').val()) {
      Swal.fire(
        'Oops, ocurrió un error',
        'Indica el número de piezas que deseas adquirir.',
        'error'
      )
      return false;
    }


    $('#saveToWishList').addClass('is-loading');
    var data = {};
    data.simpleCartId = selectedWishList.val();
    data.quantity = $('#quantity').val();
    data.productId = selectedProduct.productId;
    data.productModel = selectedProduct.productModel;
    data.color = selectedProduct.colorName;
    data.unitPrice = $('#unitPrice').val();
    data.providerName = $('#providerName').val();
    data.originalPrice = $('#originalPrice').val();
    // console.info("data for send to api: ", data);
    // return false;
    $.ajax({
      type: "POST",
      url: `${brandMarketHost}/api/save-product-to-wishlist`,
      data: data,
      success: function (a, b) {
        let cart = a.cart;
        let cartDetail = a.cartDetails;
        // console.log("a is: ", a);
        // console.log("b is: ", b);
        $('#saveToWishList').removeClass('is-loading');
        $('#quantity').value = '';
        $('input[type=radio]').removeAttr("checked");
        $("#modal-add-product.modal").addClass("is-active");
        Swal.fire(
          'Listo!',
          'Tu producto ha sido añadido a la lista de deseos',
          'success'
        );
        $('.modal-close').click();


      },
      error: function (a, b) {
        $('#addToCart').removeClass('is-loading');
        Swal.fire(
          'Oops, ocurrió un error',
          'El servidor tuvo un problema al procesar tu solicitud ',
          'error'
        );
      },
      dataType: 'JSON'
    });
  });

  $('#addToCart').on('click', function (event) {
    // console.info("there is a console info!");
    event.preventDefault();
    var selectedProduct = $('label.is-checked').data();
    // console.info("selectedProduct: ", selectedProduct);
    if (!selectedProduct) {
      Swal.fire(
        'Oops, ocurrió un error',
        'Olvidaste seleccionar el color del producto que deseas adquirir.',
        'error'
      );
      return false;
    }
    if (!$('#quantity').val()) {
      Swal.fire(
        'Oops, ocurrió un error',
        'Indica el número de piezas que deseas adquirir.',
        'error'
      )
      return false;
    }

    $('#addToCart').addClass('is-loading');
    var data = {};
    data.quantity = $('#quantity').val();
    data.productId = selectedProduct.productId;
    data.productModel = selectedProduct.productModel;
    data.productParentCategory = selectedProduct.productParentCategory;
    data.productParentSubCategory = selectedProduct.productParentSubCategory;
    data.color = selectedProduct.colorName;
    data.unitPrice = $('#unitPrice').val();
    data.providerName = $('#providerName').val();
    data.originalPrice = $('#originalPrice').val();
    // console.info("data has: ", data, "\nhttpLocation: ", httpLocation);
    $.ajax({
      type: "POST",
      url: `${brandMarketHost}/api/save-product`,
      data: data,
      success: function (a, b) {
        let cart = a.cart;
        let cartDetail = a.cartDetails;
        // console.log("a is: ", a);
        // console.log("b is: ", b);
        $('#addToCart').removeClass('is-loading');
        $('#quantity').value = '';
        $('input[type=radio]').removeAttr("checked");
        $("#modal-add-product.modal").addClass("is-active");
      },
      error: function (a, b) {
        $('#addToCart').removeClass('is-loading');
        Swal.fire(
          'Oops, ocurrió un error',
          'El servidor tuvo un problema al procesar tu solicitud: ',
          'error'
        );
      },
      dataType: 'JSON'
    });

    return false;
  });

  $("#showModal").click(function () {
    $(".modal").addClass("is-active");
  });

  $(".modal-close").click(function () {
    $(".modal").removeClass("is-active");
  });

  $("#close-modal.custom-modal-close").click(function () {
    $(".modal").removeClass("is-active");
  });

  $('.updateCartProductQuantity').click(function () {
    var me = $(this);
    // console.info("me.data ", me.data());
    var cartId, cartDetailId = undefined;
    cartId = me.data('simpleCartId');
    cartDetailId = me.data('simpleCartDetailsId');
    // console.info("cartId: ", cartId, " cartDetailId: ", cartDetailId);
    if ($('#quantity-' + cartDetailId).val() >= 1) {
      $('#simple_cart_id_update').val(cartId);
      $('#simple_cart_details_id_update').val(cartDetailId);
      $('#simple_cart_details_quantity').val($('#quantity-' + cartDetailId).val());
      $('#originalPrice').val(me.data('originalPrice'));
      window.frmUpdateProducts.submit();
    }
  });

  $('.removeProduct').click(function () {
    var me = $(this);
    var cartId, cartDetailId = undefined;
    cartId = me.data('simpleCartId');
    cartDetailId = me.data('simpleCartDetailsId');
    // console.info("cartId: ", cartId, " cartDetailId: ", cartDetailId);
    if ($('#quantity-' + cartDetailId).val() >= 1) {
      $('#simple_cart_id_delete').val(cartId);
      $('#simple_cart_details_id_delete').val(cartDetailId);
      window.frmDeleteProducts.submit();
    }
  });

  // update product existence
  $('#updateStockProvider').click(function (event) {
    event.preventDefault();
    // var selectedProduct = $('label.is-checked').data();
    // // console.info("selectedProduct: ", selectedProduct);
    // if (!selectedProduct) {
    //   Swal.fire(
    //     'Oops, ocurrió un error',
    //     'Olvidaste seleccionar el color del producto que deseas adquirir.',
    //     'error'
    //   );
    //   return false;
    // }

    $('#updateStockProvider').addClass('is-loading');

    var me = $(this).data();
    if (me.provider == 'cryptProvOne') {
      $.ajax({
        type: "GET",
        url: `${brandMarketHost}/api/update-prov-one/${me.productModel}`,
        dataType: 'JSON',
        success: function (a, b) {
          if ('success' === b) {
            $('#updateStockProvider').removeClass('is-loading');
            console.log("a is: ", a.length);
            if (a.length > 0) {
              var updated = a[0];
              $.each(updated, function (i, el) {
                console.info("**UPDATED**", i, " => ", el.color);
                var color = $('#color-' + el.id).data('productStock', el.stock);
              });
            }
          }
        },
        error: function (a, b) {
          Swal.fire(
            'Oops, ocurrió un error',
            'El servidor tuvo un problema al procesar tu solicitud ',
            'error'
          );
          $('#updateStockProvider').removeClass('is-loading');
        },
      });
    } else {

    }

  });


  // Onchange event's action for "All products"
  $('#all-products').on('change', function () {
    var selected = $(this).find(':selected').data();
    var slugCategory = selected.slugCategory;
    var slugSubCategory = selected.slugSubCategory;

    // console.log("ea ea ea a ea ea!!!");
    if (slugSubCategory != '' && slugCategory != '') {
      var url = `${brandMarketHost}/store/products/:category/:subcategory`;
      url = url;
      url = url.replace(/\:category/g, slugCategory);
      url = url.replace(/\:subcategory/g, slugSubCategory);
    }
    if (slugCategory != '' && slugSubCategory == '') {
      // console.log("***slugCategory: ", slugCategory);
      var url = `${brandMarketHost}/store/products/:category`;
      url = url;
      url = url.replace(':category', slugCategory);
    }
    // console.info("url location is ", url);
    document.location.href = url;
  });



  // BACKOFFICE FUNCTIONS AND STUFF
  var backofficeFormCreate = $('#createForm');
  $('#btnSaveAddOther').click(function (event) {
    event.preventDefault();
    var action = "<input type='hidden' value='saveAddOther' name='action'>";
    backofficeFormCreate.append(action);
    backofficeFormCreate.submit();
  });

  $('#btnSaveAddSubCategory').click(function (event) {
    event.preventDefault();
    var action = "<input type='hidden' value='saveAddSubCategory' name='action'>";
    backofficeFormCreate.append(action);
    backofficeFormCreate.submit();
  });

  // Slides stuff:
  $('#btnSaveAddSlideDetail').click(function (event) {
    console.info("Here is a damn it click!! ");
    event.preventDefault();
    var action = "<input type='hidden' value='saveAddSlideDetail' name='action'>";
    form.append(action);
    form.submit();
  });


});




