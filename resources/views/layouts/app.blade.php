<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		@if (isset($title))
			{{$title}}
		@else
			Login
		@endif
	</title>
	<link href="{{asset('/backoffice-assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backoffice-assets/css/londinium-theme.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backoffice-assets/css/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backoffice-assets/css/icons.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/backoffice-assets/css/wysihtml5/wysiwyg-color.css')}}" rel="stylesheet" type="text/css">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/charts/sparkline.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/inputmask.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/autosize.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/inputlimit.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/listbox.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/multiselect.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/tags.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/switch.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/uploader/plupload.full.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/uploader/plupload.queue.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/wysihtml5/wysihtml5.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/forms/wysihtml5/toolbar.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/fancybox.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/moment.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/datatables.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/colorpicker.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/fullcalendar.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/timepicker.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/interface/collapsible.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/plugins/tinymce/tinymce.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backoffice-assets/js/application_blank.js')}}"></script>
</head>
<body class="sidebar-narrow">
	<!-- Navbar -->
	<div class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="{{asset('/backoffice-assets/images/logo.png')}}" alt="Brand Market"></a>
			@auth
				<a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
			@endauth
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
				<span class="sr-only">Toggle navbar</span>
				<i class="icon-grid3"></i>
			</button>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar">
				<span class="sr-only">Toggle navigation</span>
				<i class="icon-paragraph-justify2"></i>
			</button>
		</div>
		<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
			@auth
				<li class="user dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="//placehold.it/300" alt="">
						<span>{{auth()->user()->name}}</span>
						<i class="caret"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right icons-right">
						<li><a href="{{route('user.profile')}}"><i class="icon-user"></i> Perfil</a></li>
						<li>
							<a href="#logout" onClick="window.frm_logout.submit();"><i class="icon-exit"></i> Salir</a>
							<form action="{{@route('logout')}}" method="POST" name="frm_logout" id="frm_logout" style="display:none;">
								@csrf
							</form>
						</li>
					</ul>
				</li>
			@endif
		</ul>
	</div>
	<!-- /navbar -->
	<!-- Page container -->
	<div class="page-container">
		@auth
			<!-- Sidebar -->
			<div class="sidebar collapse">
				<div class="sidebar-content">

					<!-- User dropdown -->
					<div class="user-menu dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							{{-- <img src="//placehold.it/300" alt=""> --}}
							<div class="user-info">
								{{auth()->user()->name}} <span>{{auth()->user()->role}}</span>
							</div>
						</a>
						<div class="popup dropdown-menu dropdown-menu-right">
							<div class="thumbnail">
								<div class="thumb">
									<img alt="" src="//placehold.it/300">
									<div class="thumb-options">
										<span>
											<a href="#" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a>
											<a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a>
										</span>
									</div>
								</div>

								<div class="caption text-center">
									<h6>{{auth()->user()->name}} <small>{{auth()->user()->email}}</small></h6>
								</div>
							</div>
							{{-- @if (auth()->user()->role == 'user')
								<ul class="list-group">
									<li class="list-group-item"><i class="icon-pencil3 text-muted"></i> Mis Cotizaciones <span class="label label-success">289</span></li>
								</ul>
							@endif --}}
						</div>
					</div>
					<!-- /user dropdown -->

					<!-- Main navigation -->
					<ul class="navigation">
						@if (auth()->user()->roles[0]->name == 'admin')
							<li @if ($current_route == 'admin.dashboard') class="active" @endif><a href="{{@route('admin.dashboard')}}"><span>Dashboard</span> <i class="icon icon-dashboard"></i></a></li>
							<li @if (in_array($current_route, [
							'product.index', 'product.create', 'product.edit', 'product.show', 'product.confirm-delete',
							])) class="active" @endif><a href="{{@route('product.index')}}"><span>Products</span> <i class="icon icon-grid"></i></a></li>

							<li @if (in_array($current_route, [
							'category.index', 'category.create', 'category.edit', 'category.show', 'category.confirm-delete',
							'subcategory.index', 'subcategory.create', 'subcategory.edit', 'subcategory.show', 'subcategory.confirm-delete'
							])) class="active" @endif>
								<a href="#" id="second-level-two"
								@if (in_array($current_route, [
								'category.index', 'category.create', 'category.edit', 'category.show', 'category.confirm-delete',
								'subcategory.index', 'subcategory.create', 'subcategory.edit', 'subcategory.show', 'subcategory.confirm-delete'
								])) class="expand" @endif
								>
									<span>Brand Market</span> <i class="icon icon-archive"></i>
								</a>
								<ul>
									<li @if (in_array($current_route, ['category.index', 'category.create', 'category.edit', 'category.show', 'category.confirm-delete'])) class="active" @endif><a href="{{@route('category.index')}}"><i class="icon-folder"></i> <span>Categorías</span> </a></li>
									<li @if (in_array($current_route, ['subcategory.index', 'subcategory.create', 'subcategory.edit', 'subcategory.show', 'subcategory.confirm-delete'])) class="active" @endif><a href="{{@route('subcategory.index')}}"><i class="icon-folder2"></i> <span>Subcategorías</span> </a></li>
								</ul>
							</li>

										<li @if (in_array($current_route, [
											'article.index', 'article.create', 'article.edit', 'article.show', 'article.confirm-delete',
											'slide.index', 'slide.create', 'slide.edit', 'slide.show', 'slide.confirm-delete',
											'slide-details.index', 'slide-details.create', 'slide-details.edit', 'slide-details.show', 'slide-details.confirm-delete'
											])) class="active" @endif>
											<a href="#" id="second-level-two"
											@if (in_array($current_route, [
												'article.index', 'article.create', 'article.edit', 'article.show', 'article.confirm-delete',
												'slide.index', 'slide.create', 'slide.edit', 'slide.show', 'slide.confirm-delete',
												'slide-details.index', 'slide-details.create', 'slide-details.edit', 'slide-details.show', 'slide-details.confirm-delete'

												])) class="expand" @endif
												><span>CMS</span> <i class="icon-newspaper"></i>
											</a>
											<ul>
												<li @if (in_array($current_route, ['article.index', 'article.create', 'article.edit', 'article.show', 'article.confirm-delete'])) class="active" @endif><a href="{{@route('article.index')}}"><i class="icon icon-table2"></i> <span>Artículos [Posts]</span> </a></li>
													<li @if (in_array($current_route, [
														'slide.index', 'slide.create', 'slide.edit', 'slide.show', 'slide.confirm-delete',
														'slide-details.index', 'slide-details.create', 'slide-details.edit', 'slide-details.show', 'slide-details.confirm-delete'
													])) class="active" @endif><a href="{{@route('slide.index')}}"><i class="icon icon-images"></i> <span>Slides [Galerías]</span> </a></li>
													</ul>
												</li>


												<li @if (in_array($current_route, ['promo-opcion.read-products','promo-opcion.upload-complement-view'])) class="active" @endif>
													<a href="#" id="second-level"
													@if (in_array($current_route, ['promo-opcion-read','ws.upload-db-view'])) class="expand" @endif
														><span>Provider Products</span> <i class="icon icon-tag"></i>
													</a>
													<ul>
														<li @if (in_array($current_route, ['promo-opcion.read-products'])) class="active" @endif><a href="{{@route('promo-opcion.read-products')}}"><i class="icon icon-table2"></i> <span>Productos > Promo Opcion</span> </a></li>
														<li @if (in_array($current_route, ['doble-vela.read-products'])) class="active" @endif><a href="{{@route('doble-vela.read-products')}}"><i class="icon icon-table2"></i> <span>Productos > Doble Vela</span> </a></li>
														<li @if (in_array($current_route, ['four-promotional.read-products'])) class="active" @endif><a href="{{@route('four-promotional.read-products')}}"><i class="icon icon-table2"></i> <span>Productos > 4 Promotional</span> </a></li>
														<li @if (in_array($current_route, ['.read-products'])) class="active" @endif><a href="{{@route('four-promotional.read-products')}}"><i class="icon icon-table2"></i> <span>Productos > 4 Promotional</span> </a></li>
														{{--
														<li @if (in_array($current_route, ['ws-settings.doble-vela'])) class="active" @endif><a href="{{@route('ws-settings.doble-vela')}}"><i class="icon icon-key2"></i> <span>Doble Vela</span> </a></li>
														<li @if (in_array($current_route, ['ws-settings.4-promotional'])) class="active" @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>4promotional</span> </a></li>
														<li @if (in_array($current_route, ['ws-settings.confirm-set-defaults'])) class="active"  @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>Restaurar Configuración</span> </a></li> --}}
													</ul>
												</li>

												<li @if (in_array($current_route, ['ws-settings.promo-opcion','ws-settings.doble-vela','ws-settings.4-promotional'])) class="active" @endif>
													<a href="#" id="second-level"
													@if (in_array($current_route, ['ws-settings.promo-opcion','ws-settings.doble-vela','ws-settings.4-promotional'])) class="expand" @endif
														><span>WebServices [Conf]</span> <i class="icon icon-wrench2"></i>
													</a>
													<ul>
														<li @if (in_array($current_route, ['ws-settings.promo-opcion'])) class="active" @endif><a href="{{@route('ws-settings.promo-opcion')}}"><i class="icon icon-key2"></i> <span>PromoOpcion</span> </a></li>
															<li @if (in_array($current_route, ['ws-settings.doble-vela'])) class="active" @endif><a href="{{@route('ws-settings.doble-vela')}}"><i class="icon icon-key2"></i> <span>Doble Vela</span> </a></li>
																<li @if (in_array($current_route, ['ws-settings.4-promotional'])) class="active" @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>4promotional</span> </a></li>
																	<li @if (in_array($current_route, ['ws-settings.confirm-set-defaults'])) class="active"  @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>Restaurar Configuración</span> </a></li>
																	</ul>
																</li>


																<li @if (in_array($current_route, ['finance-settings.index'])) class="active" @endif><a href="{{@route('finance-settings.index')}}"><span>{{@route('finance-settings.index')}}Ajustes de Ganancias</span> <i class="icon-star6"></i></a></li>
																	<li @if (in_array($current_route, ['user.index', 'user.create', 'user.edit', 'user.show', 'user.confirm-delete'])) class="active" @endif><a href="{{@route('user.index')}}"><i class="icon icon-user"></i> <span>Usuarios</span> </a></li>

																		{{-- <li><a href="#my-quotations"><span>Productos de Proveedores</span> <i class="icon-cabinet"></i></a></li>
																		<li><a href="#my-quotations"><span>Productos Brand Market</span> <i class="icon-table2"></i></a></li> --}}

																	@else
																		<li><a href="#my-quotations"><span>Mis Cotizaciones</span></a></li>
																	@endif

														</ul>
														<!-- /main navigation -->
													</div>
												</div>
												<!-- /sidebar -->
											@endif
											@auth
												<!-- Page content -->
												<div class="page-content">
													@if (isset($page_title) && isset($message))
														@component('backoffice.components.page_header')
															@slot('page_title')
																{{$page_title}}
															@endslot
															@slot('message')
																{{$message}}
															@endslot
														@endcomponent
													@endif
													<div class="container">
														<div class="row justify-content-center">
															@yield('content')
														</div>
													</div>
													<!-- Footer -->
													<div class="footer clearfix">
														<div class="pull-left">&copy; {{date('Y')}}.</div>
													</div>
													<!-- /footer -->
												</div>
												<!-- /page content -->
												{{-- here @ content --}}
											@else
												{{-- here @ login --}}
												@yield('login')
												@yield('reset-password')
												@yield('forgot-password')
												@yield('register-user')
											@endif



										</div>
										<!-- /content -->
										<script type="text/javascript" id="eaea">

										$(document).ready(function(){

										});
									</script>
								</body>
								</html>
