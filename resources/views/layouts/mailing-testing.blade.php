<!doctype html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Brand Market</title>

  <link href="{{@asset('storage/frontend-assets/app.css')}}" rel="stylesheet"></head>
</head>
  <body>
    <section>
      <div class="container">
        <!--Cada tabla es un bloque-->
        <table class="factura">
          <!-- Header -->
          <thead>
            <tr>
              <td>
                <table class="header">
                  <tr>
                    <td>
                      <img src="{{@asset('storage/frontend-assets/img/brandmarket-blue.png')}}" alt="">
                    </td>
                    <td class="has-text-centered">
                      <p class="title is-4">ORDEN DE COMPRA</p>
                      <p class="subtitle is-4">– Interno –</p>
                      <p>Levantamiento de pedido</p>
                    </td>
                    <td>
                      <p class="has-text-primary"><strong>Control Interno. {{Str::uuid()}}</strong></p>
                      <p>Cotización emitida el día  de  de  a las  horas (GMT-6)</p>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      <p><strong>Hola, {{auth()->user()->name}}</strong></p>
                      <p>Te informamos que se ha iniciado el proceso de preparación y esto completa tu pedido.</p>
                      <p>Tu pedido está en camino y ya no puede ser modificado.</p>
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
          </thead>
          <!-- Header -->

          <tbody>

            @if ($simpleCart)
                        @php
                          $simpleCartDetails = $simpleCart->Details->all();
                        @endphp
                        {{-- {{dd($simpleCartDetails)}} --}}
                        @foreach ($simpleCartDetails as $simpleCartDetail)
                          {{-- {{dd($simpleCartDetail)}} --}}
                        {{-- {{dd($simpleCart)}} --}}

                          <tr>
                            <td>
                              <table class="outer">
                                <tr>
                                  <td colspan="2">
                                    <!--Inicia tabla del producto-->
                                    <table class="inner">
                                      <tr>
                                        <td rowspan="2" class="has-image">
                                          {{-- <img src="{{@asset("storage{}")}}" alt=""> --}}
                                        </td>
                                        <th>Modelo</th>
                                        <th>Producto</th>
                                        <th>Color</th>
                                        <th>Unidades</th>
                                      </tr>
                                        <tr>
                                        <td>{{$simpleCartDetail->name}}</td>
                                        <td>{{$simpleCartDetail->price}}</td>
                                        <td>...</td>
                                        <td></td>
                                      </tr>
                                    </table>
                                    <!--Termina tabla del producto-->
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <!--Inicia tabla del detalle del importador-->
                                    <table class="inner">
                                      <tr>
                                        <th colspan="2">Detalles importador</th>
                                      </tr>
                                      <tr>
                                        <td>Precio de Lista</td>
                                        <td>{{$simpleCartDetail->price}}</td>
                                      </tr>
                                      {{-- <tr>
                                        <td>Precio de Lista</td>
                                        <td>$1.00</td>
                                      </tr> --}}
                                    </table>
                                    <!--Termina tabla del detalle del importador-->
                                  </td>
                                  <td>
                                    <!--Inicia tabla del detalle del cliente-->
                                    {{-- <table class="inner">
                                      <tr>
                                        <th colspan="2">Detalles cliente</th>
                                      </tr>
                                      <tr>
                                        <td>Precio de Lista</td>
                                        <td>$1.00</td>
                                      </tr>
                                      <tr>
                                        <td>Precio de Lista</td>
                                        <td>$1.00</td>
                                      </tr>
                                    </table> --}}
                                    <!--Termina tabla del detalle del cliente-->
                                  </td>
                                </tr>
                                <tr>
                                  <!--Cuando hay promo-->
                                  <td colspan="2" class="has-promo">
                                    Total a pagar PROMO OPCIÓN $45 MXN
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        @endforeach

                      @endif

          </tbody>
          <!-- Factura - Footer -->
          <tfoot>
            <tr>
              <td>
                <table class="outer">
                  <tr>
                    <td>
                      <table class="inner">
                        <tr>
                          <th colspan="3">Datos del cliente</th>
                        </tr>
                        <tr>
                          <td>{{strtoupper(auth()->user()->name)}} </td>
                          <td>RFC</td>
                          <td>{{strtolower(auth()->user()->email)}}</td>
                        </tr>
                        <tr>
                          <td colspan="3">
                            <strong>Total a facturar: US $</strong>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <p>Mercancía en constante movimiento.</p>
                <p>Los precios están sujetos a cambios sin previo aviso.</p>
                <p>El color de los artículos puede variar algunos tonos al color real por la calibración y resolución de tu monitor.</p>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </section>

  </body>
</html>
