<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Brand Market</title>
    <link rel="icon" href="{{@asset('storage/frontend-assets/assets/favicon.png')}}">
    <link href="{{@asset('storage/frontend-assets/app.css')}}" rel="stylesheet"></head>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript">
    var httpLocation = "{{request()->getHttpHost()}}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>
  </head>
  <body>
    @component('backoffice.components.header-nav') @endcomponent
    <section class="section">
      <div class="columns">
        <div class="column is-2">
          @component('backoffice.components.menu') @endcomponent
        </div>
        <div class="column is-10">
          @yield('content')
        </div>
      </div>
    </section>
    @component('backoffice.components.footer') @endcomponent
  </body>
</html>
{{-- <script type="text/javascript" src="{{asset('storage/frontend-assets/bundle.js')}}"></script> --}}
