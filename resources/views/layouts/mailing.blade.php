<!doctype html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="cryptProvOne" content="{{ md5('doble-vela') }}">
  <meta name="cryptProvTwo" content="{{ md5('4-promotional') }}">
  <meta name="cryptProvThree" content="{{ md5('promo-opcion') }}">
  <title>Brand Market - Mailing</title>
  <link href="{{URL::to("storage/frontend-assets/app.css")}}" rel="stylesheet">
</head>
  <body>
    @yield('content')
  </body>
</html>
