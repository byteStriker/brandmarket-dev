<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @auth
    <meta name="user-token" content="{{ auth()->user()->api_token }}">
    <meta name="the-url" content="{{ @url('/') }}">
    <meta name="cryptProvOne" content="{{ md5('doble-vela') }}">
    <meta name="cryptProvTwo" content="{{ md5('4-promotional') }}">
    <meta name="cryptProvThree" content="{{ md5('promo-opcion') }}">
  @endauth
  <title>Brand Market</title>
  <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="//cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css">
  <link rel="icon" href="{{@asset('storage/frontend-assets/assets/favicon.png')}}">
  <link href="{{@asset('storage/frontend-assets/app.css')}}" rel="stylesheet">
  <script src="http://malsup.github.io/jquery.cycle2.center.js"></script>
  <script type="text/javascript"> var httpLocation = "{{request()->getHttpHost()}}";</script>
  <script src="{{@asset('storage/frontend-assets/app.js')}}"></script>
  </head>
</head>
  <body>
    @yield('login')
    @auth
      @component('frontend.components.header-nav') @endcomponent
      @yield('account-profile')
      @yield('wishlist')
      @yield('cart')
      @yield('content')
      @component('frontend.components.featured-products') @endcomponent
      @component('frontend.components.footer') @endcomponent
    @endauth
  </body>
</html>
