@extends('layouts.app', ['title'=>'Brand Market - Registro de Usuario'])

@section('register-user')
  <div class="login-wrapper">
    <form method="POST" action="{{ route('register') }}">
      @csrf
      <div class="popup-header">
        <a href="#" class="pull-left"><i class="icon-user-plus"></i></a>
        <span class="text-semibold">Registro de Usuario</span>
        <div class="btn-group pull-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i></a>
          <ul class="dropdown-menu icons-right dropdown-menu-right">
            <li>
              @if (Route::has('password.request'))
                  <a class="btn btn-link" href="{{ route('password.request') }}">
                      {{ __('Forgot Your Password?') }}
                  </a>
              @endif
            </li>
            <li>
              <a class="btn btn-link" href="{{ route('login') }}">
                  {{ __('Sign In') }}
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="well">
        <div class="form-group has-feedback">
          <label>{{ __('Name') }}</label>
          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            <i class="icon-users form-control-feedback"></i>
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>



        <div class="form-group has-feedback">
          <label>{{ __('E-Mail Address') }}</label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            <i class="icon-envelop form-control-feedback"></i>
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror

          </div>

          <div class="form-group has-feedback">
            <label>{{ __('Password') }}</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
              <i class="icon-lock form-control-feedback"></i>
              @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror

            </div>

            <div class="form-group has-feedback">
              <label>{{ __('Confirm Password') }}</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
              <i class="icon-lock form-control-feedback"></i>
              @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror

              </div>

            <div class="row form-actions">
              <div class="col-xs-6">
                <div class="checkbox checkbox-success">
                  <label>


                  </label>
                </div>
              </div>

              <div class="col-xs-6">
                <button type="submit" class="btn btn-warning pull-right"><i class="icon-menu2"></i>
                  {{ __('Register') }}
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>



    @endsection
