@extends('layouts.app', ['title'=> 'Brand Market - Olvidé la contraseña!'])

@section('reset-password')
  <div class="login-wrapper">
    <form method="POST" action="{{ route('password.email') }}">
      @csrf
      <div class="popup-header">
        <a href="#" class="pull-left"><i class="icon-user-plus"></i></a>
        <span class="text-semibold">Recuperar Contraseña</span>
        <div class="btn-group pull-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i></a>

        </div>
        @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
        @endif
      </div>
      <div class="well">
        <div class="form-group has-feedback">
          <label>
            {{ __('E-Mail Address') }}
          </label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            <i class="icon-users form-control-feedback"></i>
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                {{ __('Send Password Reset Link') }}
              </button>
            </div>
          </div>

        </div>
      </form>
    </div>



  @endsection
