@extends('layouts.app', ['title'=> 'Brand Market - Restablecer Contraseña'])

@section('reset-password')
  <div class="login-wrapper">
    <form method="POST" action="{{ route('password.update') }}" role="form">
      @csrf
      <input type="hidden" name="token" value="{{ $token }}">

      <div class="popup-header">
        <a href="#" class="pull-left"><i class="icon-user-plus"></i></a>
        <span class="text-semibold">Recuperar Contraseña</span>
        <div class="btn-group pull-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i></a>
          <ul class="dropdown-menu icons-right dropdown-menu-right">
            <li>
              @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                  {{ __('Forgot Your Password?') }}
                </a>
              @endif
              {{-- <a href="#"><i class="icon-info"></i> Recuperar Contraseña?</a> --}}
            </li>
          </ul>
        </div>
      </div>
      <div class="well">
        <div class="form-group has-feedback">
          <label>
            {{ __('E-Mail Address') }}
          </label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
            <i class="icon-users form-control-feedback"></i>
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group has-feedback">
            <label>
              {{ __('Password') }}
            </label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
              <i class="icon-lock form-control-feedback"></i>
              @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror

            </div>

            <div class="form-group has-feedback">
              <label>{{ __('Confirm Password') }}</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                <i class="icon-lock form-control-feedback"></i>
                {{-- @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror --}}

              </div>

              <div class="form-group row mb-0">
                  <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">
                          {{ __('Reset Password') }}
                      </button>
                  </div>
              </div>

            </div>
          </form>
        </div>



      @endsection
