@extends('layouts.frontend', ['title' => 'Brand Market - Acceder'])
@section('login')

  <section class="login">
    <div class="container">
      <div class="columns is-centered">
        <div class="column is-9">
          <div class="columns">
            <div class="column">
              <div class="cycle-slideshow" data-cycle-fx="fade" data-cycle-timeout="4200" data-cycle-slides="> div">
                <div class="image is-square"><img src="{{ asset('storage/frontend-assets/img/login-a.jpg') }}" alt=""></div>
                <div class="image is-square"><img src="{{ asset('storage/frontend-assets/img/login-b.jpg') }}" alt=""></div>
                <div class="image is-square"><img src="{{ asset('storage/frontend-assets/img/login-c.jpg') }}" alt=""></div>
                <div class="image is-square"><img src="{{ asset('storage/frontend-assets/img/login-e.jpg') }}" alt=""></div>
                <div class="image is-square"><img src="{{ asset('storage/frontend-assets/img/login-e.jpg') }}" alt=""></div>
                <div class="image is-square"><img src="{{ asset('storage/frontend-assets/img/login-f.jpg') }}" alt=""></div>
              </div>
            </div>
            <div class="column is-5 is-offset-1">
              <div class="card has-text-centered">
                <figure>
                  @component('frontend.components.logo-blue') @endcomponent
                </figure>
                <form method="POST" action="{{ route('login') }}" name="frmLogin" id="frmLogin">
                  @csrf
                  <div class="field">
                    <div class="control">
                      <input id="email" type="email" placeholder="Correo Electrónico" class="input" name="email" value="{{ old('email') }}">
                      @error('email')
                        {{$message}}
                      @enderror
                    </div>
                  </div>
                  <div class="field">
                    <div class="control">
                      <input class="input" type="password" name="password" id="password" placeholder="Contraseña">
                        @error('password')
                          {{$message}}
                        @enderror
                    </div>
                  </div>
                  <div class="field is-grouped is-grouped-centered">
                    <p class="control">
                      <button type="submit" class="button is-primary">
                        Iniciar sesión
                      </button>
                    </p>
                  </div>
                  <p>
                    @if (Route::has('password.request'))
                      <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Restablecer Contraseña') }}
                      </a>
                    @endif

                  </p>
                </form>
                <p>
                  <small>
                    ¿No eres cliente? envia un correo a <a href="mailto:acontacto@brandmarket.com.mx"> contacto@brandmarket.com.mx</a>
                  </small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
