<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Brand Market</title>
<link rel="icon" href="/assets/favicon.png"><link href="app.css" rel="stylesheet"></head>
<body>
  <section class="login">
    <div class="container">
      <div class="columns is-centered">
        <div class="column is-9">
          <div class="columns">
            <div class="column">
            </div>
            <div class="column is-5 is-offset-1">
              <div class="card has-text-centered">
                <figure>
                  @component('frontend.components.logo')
                  @endcomponent
                </figure>
                <form action="">
                  <div class="field">
                    <div class="control">
                      <input class="input" type="text" placeholder="Nombre de usuario / Correo electrónico">
                    </div>
                  </div>
                  <div class="field">
                    <div class="control">
                      <input class="input" type="password" placeholder="Contraseña">
                    </div>
                  </div>
                  <div class="field is-grouped is-grouped-centered">
                    <p class="control">
                      <a class="button is-primary">
                        Iniciar sesión
                      </a>
                    </p>
                  </div>
                  <p>
                    <a href="">
                      <small>¿Olvidaste tu contraseña?</small>
                    </a>
                  </p>
                </form>
                <p>
                  <small>
                    ¿No eres cliente? envia un correo <a href="mailto:acontacto@brandmarket.com.mx">acontacto@brandmarket.com.mx</a>
                  </small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</html>
<script type="text/javascript" src="bundle.js"></script>
