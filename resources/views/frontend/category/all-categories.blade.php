@extends('layouts.frontend')
@section('content')
<section class="section">
  <div class="container">
    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[0]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[0]->cover_image}")}});"></div>
          <span>{{$category[0]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <div class="tile is-vertical-title">
          <div class="tile is-child has-greenc-background">
            <a href="{{@route('products.list',[$category[1]->slug])}}">
              <h4>Bebidas</h4>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[0]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[0]->cover_image}")}});" class="has-blue-overlay"></div>
                <span>{{$category[1]->Subcategories[0]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[1]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[1]->cover_image}")}});" class="has-red-overlay"></div>
                <span>{{$category[1]->Subcategories[1]->name}}</span>
              </a>
            </div>
          </div>
          <div class="tile">
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[2]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[2]->cover_image}")}});" class="has-green-overlay"></div>
                <span>{{$category[1]->Subcategories[2]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[3]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[3]->cover_image}")}});" class="has-blue-overlay"></div>
                <span>{{$category[1]->Subcategories[3]->name}}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="tile is-parent">
        <div class="tile is-vertical">
          <div class="tile is-horizontal-title">
            <div class="tile is-child has-greeno-background">
              <a href="{{@route('products.list',[$category[2]->slug])}}">
                <span>{{$category[2]->name}}</span>
              </a>
            </div>
          </div>
          <div class="tile">
            <div class="tile">
              <a href="{{@route("products.list",[$category[2]->slug, $category[2]->Subcategories[0]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[2]->Subcategories[0]->cover_image}")}});"></div>
                <span>{{$category[2]->Subcategories[0]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[2]->slug, $category[2]->Subcategories[1]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[2]->Subcategories[1]->cover_image}")}});"></div>
                <span>{{$category[2]->Subcategories[1]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[2]->slug, $category[2]->Subcategories[2]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[2]->Subcategories[2]->cover_image}")}});"></div>
                <span>{{$category[2]->Subcategories[2]->name}}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <div class="tile is-vertical">
          <div class="tile is-horizontal-title">
            <div class="tile is-child has-orange-background">
              <a href="{{@route('products.list',[$category[3]->slug])}}" class="tile is-child">
                <span>{{$category[3]->name}}</span>
              </a>
            </div>
          </div>
          <div class="tile">
            <div class="tile">
              <a href="{{@route("products.list",[$category[3]->slug, $category[3]->Subcategories[0]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[3]->Subcategories[0]->cover_image}")}});"></div>
                <span>{{$category[3]->Subcategories[0]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[3]->slug, $category[3]->Subcategories[1]->slug])}}" class="tile is-child">
                <div style="background-image:url({{asset("storage/{$category[3]->Subcategories[1]->cover_image}")}});"></div>
                <span>{{$category[3]->Subcategories[1]->name}}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="tile is-parent">
        <a href="{{@route("products.list",[$category[4]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[4]->cover_image}")}});"></div>
          <span>{{$category[4]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <a href="{{@route("products.list",[$category[5]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[5]->cover_image}")}});"></div>
          <span>{{$category[5]->name}}</span>
        </a>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <a href="{{@route("products.list",[$category[6]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[6]->cover_image}")}});"></div>
          <span>{{$category[6]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
          <a href="{{@route("products.list",[$category[7]->slug])}}" class="tile is-child">
              <div style="background-image:url({{asset("storage/{$category[7]->cover_image}")}});"></div>
              <span>{{$category[7]->name}}</span>
          </a>
      </div>
      <div class="tile is-parent">
        <a href="{{@route("products.list",[$category[8]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[8]->cover_image}")}});"></div>
          <span>{{$category[8]->name}}</span>
        </a>
      </div>
    </div>
    <div class="tile is-ancestor is-parent">
      <div class="tile">
        <a href="{{@route('products.list',[$category[9]->slug])}}" class="tile is-child">
          <div class="has-red-overlay" style="background-image:url({{asset("storage/{$category[9]->cover_image}")}});"></div>
          <span>{{$category[9]->name}}</span>
        </a>
      </div>
      <div class="tile is-vertical">
        <div class="tile">
          <a href="{{@route("products.list",[$category[9]->slug, $category[9]->Subcategories[0]->slug])}}" class="tile is-child">
            <div class="has-blue-overlay" style="background-image:url({{asset("storage/{$category[9]->Subcategories[0]->cover_image}")}});"></div>
            <span>{{$category[9]->Subcategories[0]->name}}</span>
          </a>
        </div>
        <div class="tile">
          <a href="{{@route("products.list",[$category[9]->slug, $category[9]->Subcategories[1]->slug])}}" class="tile is-child">
            <div class="has-red-overlay" style="background-image:url({{asset("storage/{$category[9]->Subcategories[1]->cover_image}")}});"></div>
            <span>{{$category[9]->Subcategories[1]->name}}</span>
          </a>
        </div>
      </div>
      <div class="tile is-vertical">
        <div class="tile">
          <a href="{{@route("products.list",[$category[9]->slug, $category[9]->Subcategories[2]->slug])}}" class="tile is-child">
            <div class="has-greeno-overlay" style="background-image:url({{asset("storage/{$category[9]->Subcategories[2]->cover_image}")}});"></div>
            <span>{{$category[9]->Subcategories[2]->name}}</span>
          </a>
        </div>
        <div class="tile">
          <a href="{{@route("products.list",[$category[9]->slug, $category[9]->Subcategories[3]->slug])}}" class="tile is-child">
            <div class="has-hellow-overlay" style="background-image:url({{asset("storage/{$category[9]->Subcategories[3]->cover_image}")}});"></div>
            <span>{{$category[9]->Subcategories[3]->name}}</span>
          </a>
        </div>
      </div>
      <div class="tile is-vertical">
        <div class="tile">
          <a href="{{@route("products.list",[$category[9]->slug, $category[9]->Subcategories[4]->slug])}}" class="tile is-child">
            <div class="has-red-overlay" style="background-image:url({{asset("storage/{$category[9]->Subcategories[4]->cover_image}")}});"></div>
            <span>{{$category[9]->Subcategories[4]->name}}</span>
            </a>
        </div>
        <div class="tile">
          <a href="{{@route("products.list",[$category[9]->slug, $category[9]->Subcategories[5]->slug])}}" class="tile is-child">
            <div class="has-blue-overlay" style="background-image:url({{asset("storage/{$category[9]->Subcategories[5]->cover_image}")}});"></div>
            <span>{{$category[9]->Subcategories[5]->name}}</span>
          </a>
        </div>
      </div>
      <div class="tile">
        <a href="{{@route("products.list",[$category[9]->slug, $category[9]->Subcategories[6]->slug])}}" class="tile is-child">
          <div class="has-green-overlay" style="background-image:url({{asset("storage/{$category[9]->Subcategories[6]->cover_image}")}});"></div>
          <span>{{$category[9]->Subcategories[6]->name}}</span>
        </a>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent is-4">
        <a href="{{@route('products.list',[$category[10]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[10]->cover_image}")}});"></div>
          <span>{{$category[10]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <div class="tile is-vertical-title">
          <div class="tile is-child has-greenc-background">
            <h4>
              <a href="{{@route('products.list',[$category[11]->slug])}}">
                <span>{{$category[11]->name}}</span>
              </a>
            </h4>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[0]->slug])}}" class="tile is-child">
              <div class="has-greenc-overlay" style="background-image:url({{asset("storage/{$category[11]->Subcategories[0]->cover_image}")}});" class="has-green-overlay"></div>
              <span>{{$category[11]->Subcategories[0]->name}}</span>
            </a>
          </div>
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[1]->slug])}}" class="tile is-child">
              <div class="has-yellow-overlay" style="background-image:url({{asset("storage/{$category[11]->Subcategories[1]->cover_image}")}});" class="has-green-overlay"></div>
              <span>{{$category[11]->Subcategories[1]->name}}</span>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[2]->slug])}}" class="tile is-child">
              <div class="has-blue-overlay" style="background-image:url({{asset("storage/{$category[11]->Subcategories[2]->cover_image}")}});" class="has-green-overlay"></div>
              <span>{{$category[11]->Subcategories[2]->name}}</span>
            </a>
            </div>
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[3]->slug])}}" class="tile is-child">
              <div class="has-red-overlay" style="background-image:url({{asset("storage/{$category[11]->Subcategories[3]->cover_image}")}});" class="has-green-overlay"></div>
              <span>{{$category[11]->Subcategories[3]->name}}</span>
            </a>
            </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[4]->slug])}}" class="tile is-child">
              <div class="has-yellow-overlay" style="background-image:url({{asset("storage/{$category[11]->Subcategories[4]->cover_image}")}});" class="has-yellow-overlay"></div>
              <span>{{$category[11]->Subcategories[4]->name}}</span>
            </a>
          </div>
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[5]->slug])}}" class="tile is-child">
              <div class="has-green-overlay" style="background-image:url({{asset("storage/{$category[11]->Subcategories[5]->cover_image}")}});" class="has-green-overlay"></div>
              <span>{{$category[11]->Subcategories[5]->name}}</span>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[6]->slug])}}" class="tile is-child">
              <div class="has-blue-overlay" style="background-image:url({{asset("storage/{$category[11]->Subcategories[6]->cover_image}")}});" class="has-green-overlay"></div>
              <span>{{$category[11]->Subcategories[6]->name}}</span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <a class="tile is-child" href="{{@route('products.list',[$category[12]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[12]->cover_image}")}});"></div>
          <span>{{$category[12]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <a class="tile is-child" href="{{@route('products.list',[$category[13]->slug])}}" class="tile is-child">
          <div style="background-image:url({{asset("storage/{$category[13]->cover_image}")}});"></div>
          <span>{{$category[13]->name}}</span>
        </a>
      </div>
    </div>
  </div>
</section>
@endsection
