@extends('layouts.frontend')
@section('content')
  <section class="section">
    <div class="container">
      {{-- <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/">INICIO</a></li>
          <li><a href="{{@route('category.all')}}">{{strtoupper('CATEGORÍAS')}}</a></li>
           <li class="is-active"><a href="{{@route('category.list', ['category'=>request()->segment(3)] )}}"> {{strtoupper(request()->segment(3))}}</a> </li>
        </ul>
      </nav> --}}
      <div class="columns is-multiline">
        @if (isset($subcategory))
          @if (count($subcategory))
            @foreach ($subcategory as $row)
              <!-- Empieza producto -->
              <div class="column is-one-fifth">
                <a href="{{route('products.list', [$row->Category->slug, $row->slug])}}">
                  <figure class="image is-square">
                    <img src="{{@asset("storage/{$row->cover_image}")}}" alt="">
                  </figure>
                  <p class="title">
                    {{$row->name}}
                  </p>
                </a>
              </div>
              <!-- Termina producto -->
            @endforeach
          @endif
        @endif
      </div>
    </div>
  </section>
@endsection
