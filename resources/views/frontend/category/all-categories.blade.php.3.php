@extends('layouts.frontend')
@section('content')
<section class="section">
  <div class="container">
    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[0]->slug])}}" class="tile is-child" style="background-image:url({{asset("storage/{$category[0]->cover_image}")}});">
          <span>{{$category[0]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <div class="tile is-vertical-title">
          <div class="tile is-child has-green-background">
            <a href="{{@route('products.list',[$category[1]->slug])}}">
            {{-- <div style="background-image:url({{asset("storage/{$category[1]->cover_image}")}});"></div> --}}
            {{-- <h4>Bebidas</h4> --}}
              <h4>{{$category[1]->name}}</h4>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[0]->slug])}}" class="tile is-child">
                {{-- <span>Cilindros de plástico</span> --}}
              <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[0]->cover_image}")}});" class="has-blue-overlay"></div>
              <span>{{$category[1]->Subcategories[0]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[1]->slug])}}" class="tile is-child">
                {{-- <span>Cilindros metálicos</span> --}}
                <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[1]->cover_image}")}});" class="has-red-overlay"></div>
                <span>{{$category[1]->Subcategories[1]->name}}</span>
              </a>
            </div>
          </div>
          <div class="tile">
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[2]->slug])}}" class="tile is-child">
                {{-- <span>Tazas</span> --}}
                <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[2]->cover_image}")}});" class="has-green-overlay"></div>
                <span>{{$category[1]->Subcategories[2]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[1]->slug, $category[1]->Subcategories[3]->slug])}}" class="tile is-child">
                {{-- <span>Thermos</span> --}}
                <div style="background-image:url({{asset("storage/{$category[1]->Subcategories[3]->cover_image}")}});" class="has-yellow-overlay"></div>
                <span>{{$category[1]->Subcategories[3]->name}}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="tile is-parent">
        <div class="tile is-vertical">
          <div class="tile is-horizontal-title">
            <div class="tile is-child has-background-primary">
              {{-- Bolígrafos --}}
              <a href="{{@route('products.list',[$category[2]->slug])}}">
                {{-- <div style="background-image:url({{asset("storage/{$category[2]->cover_image}")}});"></div> --}}
                <span>{{$category[2]->name}}</span>
              </a>
            </div>
          </div>
          <div class="tile">
            <div class="tile">
              <a href="{{@route("products.list",[$category[2]->slug, $category[2]->Subcategories[0]->slug])}}" class="tile is-child ">
                  <div style="background-image:url({{asset("storage/{$category[2]->Subcategories[0]->cover_image}")}});"></div>
                  {{-- <span>Plásticos</span> --}}
                <span>{{$category[2]->Subcategories[0]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[2]->slug, $category[2]->Subcategories[1]->slug])}}" class="tile is-child ">
                  <div style="background-image:url({{asset("storage/{$category[2]->Subcategories[1]->cover_image}")}});"></div>
                  {{-- <span>Plásticos</span> --}}
                <span>{{$category[2]->Subcategories[1]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[2]->slug, $category[2]->Subcategories[3]->slug])}}" class="tile is-child ">
                  <div style="background-image:url({{asset("storage/{$category[2]->Subcategories[3]->cover_image}")}});"></div>
                  {{-- <span>Multi funcionales</span> --}}
                <span>{{$category[2]->Subcategories[3]->name}}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <div class="tile is-vertical">
          <div class="tile is-horizontal-title">
            <div class="tile is-child has-orange-background">
            {{-- Cuidado Personal --}}
              <a href="{{@route('products.list',[$category[3]->slug])}}">
                {{-- <div style="background-image:url({{asset("storage/{$category[3]->cover_image}")}});"></div> --}}
                <span>{{$category[3]->name}}</span>
              </a>
            </div>
          </div>
          <div class="tile">
            <div class="tile">
              <a  href="{{@route("products.list",[$category[3]->slug, $category[3]->Subcategories[0]->slug])}}" class="tile is-child ">
                {{-- <span>Salud</span> --}}
                <div style="background-image:url({{asset("storage/{$category[3]->Subcategories[0]->cover_image}")}});"></div>
                <span>{{$category[3]->Subcategories[0]->name}}</span>
              </a>
            </div>
            <div class="tile">
              <a href="{{@route("products.list",[$category[3]->slug, $category[3]->Subcategories[1]->slug])}}" class="tile is-child ">
                <div style="background-image:url({{asset("storage/{$category[3]->Subcategories[1]->cover_image}")}});"></div>
                {{-- <span>Belleza</span> --}}
                <span>{{$category[3]->Subcategories[1]->name}}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[4]->slug])}}" class="tile is-child" style="background-image:url({{asset("storage/{$category[4]->cover_image}")}});">
          {{-- <span>Deportes y entretenimiento</span> --}}
          <span>{{$category[4]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[5]->slug])}}" class="tile is-child" style="background-image:url({{asset("storage/{$category[5]->cover_image}")}});">
          {{-- <span>Herramientas</span> --}}
          <span>{{$category[5]->name}}</span>
        </a>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[6]->slug])}}" class="tile is-child"  style="background-image:url({{asset("storage/{$category[6]->cover_image}")}});">
          {{-- <span>Hieleras y loncheras</span> --}}
          <span>{{$category[6]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[7]->slug])}}" class="tile is-child"  style="background-image:url({{asset("storage/{$category[7]->cover_image}")}});">
          {{-- <span>Mascotas</span> --}}
          <span>{{$category[7]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[8]->slug])}}" class="tile is-child" style="background-image:url({{asset("storage/{$category[8]->cover_image}")}});">
          {{-- <span>Niños</span> --}}
          <span>{{$category[8]->name}}</span>
        </a>
      </div>
    </div>
    <div class="tile is-ancestor is-parent">
      <div class="tile">
        <a href="{{@route('products.list',[$category[9]->slug])}}" class="tile is-child ">
          {{-- <span>Oficina</span> --}}
          <div style="background-image:url({{asset("storage/{$category[9]->cover_image}")}});" class="has-yellow-overlay"></div>
          <span>{{$category[9]->name}}</span>
        </a>
      </div>
      <div class="tile is-vertical">
        <div class="tile">
          <a href="{{@route('products.list',[$category->[9]->slug, $category[9]->Subcategories[0]->slug])}}" class="tile is-child ">
            {{-- <span>Agendas y carpetas</span> --}}
            <div style="background-image:url({{asset("storage/{$category[9]->Subcategories[0]->cover_image}")}});" class="has-blue-overlay"></div>
            <span>{{$category[9]->Subcategories[0]->name}}</span>
          </a>
        </div>
        <div class="tile">
          <a href="{{@route('products.list',[$category[9]->slug, $category[9]->Subcategories[1]->slug])}}" class="tile is-child ">
            {{-- <span>Libretas</span> --}}
            <div style="background-image:url({{asset("storage/{$category[9]->Subcategories[1]->cover_image}")}});" class="has-red-overlay"></div>
            <span>{{$category[9]->Subcategories[1]->name}}</span>
          </a>
        </div>
      </div>
      <div class="tile is-vertical">
        <div class="tile">
          <a href="{{@route('products.list',[$category[9]->slug, $category[9]->Subcategories[2]->slug])}}" class="tile is-child ">
            {{-- <span>Llaveros</span> --}}
            <div style="background-image:url({{asset("storage/{$category[9]->Subcategories[2]->cover_image}")}});" class="has-green-overlay"></div>
            <span>{{$category[9]->Subcategories[2]->name}}</span>
          </a>
        </div>
        <div class="tile">
          <a href="{{@route('products.list',[$category[9]->slug, $category[9]->Subcategories[3]->slug])}}" class="tile is-child ">
            {{-- <span>Oficina</span> --}}
            <div style="background-image:url({{asset("storage/{$category[9]->Subcategories[3]->cover_image}")}});" class="has-yellow-overlay"></div>
            <span>{{$category[9]->Subcategories[3]->name}}</span>
          </a>
        </div>
      </div>
      <div class="tile is-vertical">
        <div class="tile">
          <a href="{{@route('products.list',[$category[9]->slug, $category[9]->Subcategories[4]->slug])}}" class="tile is-child ">
            {{-- <span>Portaretratos</span> --}}
            <div style="background-image:url({{asset("storage/{$category[9]->Subcategories[4]->cover_image}")}});" class="has-red-overlay"></div>
            <span>{{$category[9]->Subcategories[4]->name}}</span>
          </a>
        </div>
        <div class="tile">
          <a href="{{@route('products.list',[$category[9]->slug, $category[9]->Subcategories[5]->slug])}}" class="tile is-child ">
            <div style="background-image:url({{asset("storage/{$category[9]->Subcategories[5]->cover_image}")}});" class="has-blue-overlay"></div>
            <span>{{$category[9]->Subcategories[5]->name}}</span>
          </a>
        </div>
      </div>
      <div class="tile">
        <a href="{{@route('products.list',[$category[9]->slug, $category[9]->Subcategories[6]->slug])}}" class="tile is-child ">
          {{-- <span>Anti Stress</span> --}}
          <div style="background-image:url({{asset("storage/{$category[9]->Subcategories[6]->cover_image}")}});" class="has-green-overlay"></div>
          <span>{{$category[9]->Subcategories[6]->name}}</span>
        </a>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent is-4">
        <a href="{{@route('products.list',[$category[10]->slug])}}" class="tile is-child" style="background-image:url({{asset("storage/{$category[10]->cover_image}")}});">
          {{-- <span>Paraguas e Impermeables</span> --}}
          <span>{{$category[10]->name}}</span>
        </a>
      </div>
      <div class="tile is-parent">
        <div class="tile is-vertical-title">
          <div class="tile is-child has-background-primary">
            <a href="{{@route('products.list',[$category[11]->slug])}}" class="tile is-child has-background-danger">
              {{-- <div style="background-image:url({{asset("storage/{$category[11]->cover_image}")}});"></div> --}}
              {{-- <h4>Textiles</h4> --}}
              <h4>{{$category[11]->name}}</h4>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[0]->slug])}}" class="tile is-child has-background-danger">
              {{-- <span>Bolsas y Morrales</span> --}}
              <div style="background-image:url({{asset("storage/{$category[11]->Subcategories[0]->cover_image}")}});" class="has-green-overlay"></div>
              <span>{{$category[11]->Subcategories[0]->name}}</span>
            </a>
          </div>
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[1]->slug])}}" class="tile is-child has-background-danger">
              {{-- <span>Gorras</span> --}}
              <div style="background-image:url({{asset("storage/{$category[11]->Subcategories[1]->cover_image}")}});" class="has-yellow-overlay"></div>
              <span>{{$category[11]->Subcategories[1]->name}}</span>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[2]->slug])}}" class="tile is-child has-background-danger">
              {{-- <span>Gorras</span> --}}
              <div style="background-image:url({{asset("storage/{$category[11]->Subcategories[2]->cover_image}")}});" class="has-blue-overlay"></div>
              <span>{{$category[11]->Subcategories[2]->name}}</span>
            </a>
          </div>
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[3]->slug])}}" class="tile is-child has-background-danger">
              {{-- <span>Gorras</span> --}}
              <div style="background-image:url({{asset("storage/{$category[11]->Subcategories[3]->cover_image}")}});" class="has-red-overlay"></div>
              <span>{{$category[11]->Subcategories[3]->name}}</span>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[4]->slug])}}" class="tile is-child has-background-danger">
              {{-- <span>Maletas</span> --}}
              <div style="background-image:url({{asset("storage/{$category[11]->Subcategories[4]->cover_image}")}});" class="has--overlay"></div>
              <span>{{$category[11]->Subcategories[4]->name}}</span>
            </a>
          </div>
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[5]->slug])}}" class="tile is-child has-background-danger">
              {{-- <span>Chamarras</span> --}}
              <div style="background-image:url({{asset("storage/{$category[11]->Subcategories[5]->cover_image}")}});" class="has-yellow-overlay"></div>
              <span>{{$category[11]->Subcategories[5]->name}}</span>
            </a>
          </div>
        </div>
        <div class="tile is-vertical">
          <div class="tile">
            <a href="{{@route('products.list',[$category[11]->slug, $category[11]->Subcategories[6]->slug])}}" class="tile is-child has-background-danger">
              {{-- <span>Portafolios</span> --}}
              <div style="background-image:url({{asset("storage/{$category[11]->Subcategories[6]->cover_image}")}});" class="has-blue-overlay"></div>
              <span>{{$category[11]->Subcategories[6]->name}}</span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="tile is-ancestor">
      <div class="tile is-parent">
      <a href="{{@route('products.list',[$category[12]->slug])}}" class="tile is-child" style="background-image:url({{asset("storage/{$category[12]->cover_image}")}});"></div>
        {{-- <span>Tecnología</span> --}}
        <span>{{$category[12]->name}}</span>
      </a>
      </div>
      <div class="tile is-parent">
        <a href="{{@route('products.list',[$category[13]->slug])}}" class="tile is-child" style="background-image:url({{asset("storage/{$category[13]->cover_image}")}});">
          {{-- <span>Tecnología</span> --}}
          <span>{{$category[13]->name}}</span>
        </a>
      </div>
    </div>
  </div>
</section>
@endsection
