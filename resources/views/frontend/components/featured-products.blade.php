@if (isset($featuredProducts) && \Route::current()->getName() == 'account.home')
  <section>
    <div class="container">
      {{-- {{print_r($featuredProducts[0])}} --}}
      <div class="columns">
        @foreach ($featuredProducts as $product)
          <div class="column">
            <figure data-provider-name="{{strtolower($product->provider_name)}}">
              <a href="{{@route('products.product',[md5(strtolower($product->provider_name)), $product->cat_slug, $product->subcat_slug, $product->slug, $product->model])}}">
                <img src="{{@asset($product->image)}}" alt="">
              </a>
            </figure>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
