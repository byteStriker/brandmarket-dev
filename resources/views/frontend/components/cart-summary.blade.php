
<div class="column is-3 {{$class_offset ?? ''}}">
  <div class="{{$table_class ?? 'table'}}">
    @if (isset($error))

      @if ($error)
        <article class="message is-warning">
          <div class="message-header">
            <p>Cuidado!</p>
            {{-- <button class="delete" aria-label="delete"></button> --}}
          </div>
          <div class="message-body">
            {{$error ?? ''}}
          </div>
        </article>
        <script type="text/javascript">
        setTimeout(function(){
          $('.message').remove();
        }, 6000);
        </script>
      @endif
    @endif

    <h4 class="title is-5">Resumen de Cotización</h4>
    <sub>
    {{-- {{dd($myCart)}} --}}
    </sub>
    <table class="table">
      <tr>
        <td class="has-text-right">Subtotal</td>
        <td>USD
          @if (isset($myCart->subtotal))
            {{number_format($myCart->subtotal ?? 0, 2, '.', ',')}}
          @else
            {{number_format(0, 2, '.', ',')}}
          @endif
        </td>
      </tr>
      <tr>
        <td class="has-text-right">Logística</td>
        <td>USD
          @if (isset($myCart->subtotal))
            {{number_format($myCart->freight_amount / $exchangeRate ?? 0, 2, '.', ',')}}
          @else
            {{number_format(0, 2, '.', ',')}}
          @endif
        </td>
      </tr>
      <tr>
        <th class="has-text-right">Total</th>
        <th>USD
          @if (isset($myCart->total))
            @if  ($myCart->total > 2.70)
            {{number_format($myCart->total ?? 0, 2, '.', ',')}}
            @else
            {{number_format(0,2)}}
            @endif
          @else
            {{number_format(0, 2, '.', ',')}}
          @endif
        </th>
      </tr>
    </table>
    <div class="buttons is-centered">
      @if (isset($myCart))
        @if ($myCart->total > 0)
        <form action="{{@route('cart.checkout')}}" method="POST" name="frmCheckout" id="frmCheckout" >
          @csrf
          <input type="hidden" name="id" id="id" value="{{$myCart->id}}">
          <button type="submit" name="button" class="button is-primary"> Solicitar Cotización&nbsp;
              <i class="material-icons">send</i>
          </button>
        </form>
        @endif

      @else
        <a href="{{@route('category.all')}}" class="button is-primary">
          Agregar Productos&nbsp;
          <i class="material-icons">add_shopping_cart</i>
        </a>
      @endif
    </div>
  </div>
</div>
