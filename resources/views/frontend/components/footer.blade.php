<footer class="footer content">
  <figure>
      @component('frontend.components.logo')
      @endcomponent
  </figure>
  <p>
    <strong>
      Contacto:
      <a href="tel:5544341335">55 4434 1335</a> –
      <a href="mailto:atencionaclientes@brandmarket.com.mx">atencionaclientes@brandmarket.com.mx</a>
    </strong>
  </p>
  <p>
  <a href="{{@route('privacy-notice')}}"><small>Aviso de Privacidad</small></a>
  <a href="{{@route('terms-conditions')}}"><small>Términos y Condiciones</small></a>
  <a href="{{@route('cookie-notice')}}"><small>Uso de Cookies</small></a>
  </p>
</footer>
