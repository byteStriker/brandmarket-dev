@if (in_array(Route::currentRouteName(), [ 'category.list', 'subcategory.list', 'products.list', 'products.product']))
  <nav class="navbar" role="navigation" aria-label="category navigation">
    <div id="navbarBottom" class="navbar-menu">
      <div class="navbar-start">
        <div class="navbar-item">
          <p class="is-size-4 has-text-white">
            {{$nav_category->name}}
          </p>
          {{-- <p>(Todos los productos)</p> --}}
        </div>
      </div>
      <div class="navbar-start">
        {{-- {{print_r(request()->segment(5), 1)}} --}}
        @if (count($nav_subcategories))
          @forelse ($nav_subcategories as $nav_subcategory)
            <a href="{{@route("products.list",[$nav_category->slug, $nav_subcategory->slug])}}" class="navbar-item"
              @if (request()->segment(5) == $nav_subcategory->slug || request()->segment(4) == $nav_subcategory->slug)
                style="background-color: #fafafa;color: #1da042;"
              @endif
              >
              {{$nav_subcategory->name}}
            </a>
          @empty
          @endforelse
        @endif
      </div>
    </div>
  </nav>
@endif
