<div class="modal">
  <div class="modal-background"></div>
  <div class="modal-content">
    <div class="field">
      <form class="" action="{{@route('cart.save-as-wishlist', ['simple_carts'=>$myCartDetails[0]->id])}}" method="post">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" id="id" value="{{$myCartDetails[0]->id}}">
        @if (isset($wishlist2Cart))
          <input type="hidden" name="wishList2Cart_id" value="{{$wishlist2Cart->id}}">
        @endif
        <label class="label has-text-white-bis">Ingresa un Nombre para tu nuevo Wish List</label>
        <div class="field-body">
          <div class="field">
            <p class="control">
              <input name="wishlist_name" id="wishlist_name" class="input" type="text" value="" placeholder="Proyecto Asombroso" required>
            </p>
          </div>
        </div>
      </div>
      <div class="">
        {{-- <a href="#cart-2-whis-list" onClick="javascript:cart2WishList();" class="button is-primary"> Crear Wish List</a> --}}
        <button type="submit" name="submit" class="button is-primary">Crear Wish List</button>
        <button type="button" class="button custom-modal-close is-dark" id="close-modal">Cancelar</button>
      </div>
    </form>

  </div>
  <button class="modal-close is-large" aria-label="close"></button>
</div>

