<header>

  <div class="container">
    <nav class="navbar" role="navigation" aria-label="main navigation">
      <div class="navbar-brand">
        <a class="navbar-item" href="/">
          @component('frontend.components.logo',['width'=>"112", "height"=>"28"])
          @endcomponent

        </a>
        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div id="navbarTop" class="navbar-menu">
        <div class="navbar-start">
          <div class="field has-addons has-addons-centered">
            <p class="control">
              <span class="select">
                <select id="all-products" name="all-products">
                  <option>Todos los productos</option>
                  @if (count($allProductsOptions))
                    @foreach ($allProductsOptions as $category)
                      <option value="{{$category->id}}" data-slug-category="{{$category->slug}}" data-slug-sub-category
                          data-son-of-category label="{{$category->name}}" id="cat-father-of-{{$category->id}}">{{$category->name}}</option>
                      @foreach ($category->Subcategories as $subcategory)
                        <option value="{{$subcategory->id}}" data-slug-category="{{$category->slug}}" data-slug-sub-category="{{$subcategory->slug}}"
                          data-son-of-category="{{$category->id}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$subcategory->name}}</option>
                      @endforeach
                    @endforeach
                  @endif
                </select>
              </span>
            </p>
            </p>
            <p class="control">
              <input class="input" type="text" id="search" name="search" placeholder="Busca tus productos">
            </p>
            <p class="control">
              <a class="button is-primary">
                <i class="material-icons">search</i>
              </a>
            </p>
          </div>
        </div>
        <div class="navbar-end">
          <div class="navbar-item account">
            <p><strong>¡Hola, {{strtoupper(auth()->user()->name)}}!</strong></p>
            <div class="is-flex">
              <a href="{{@route('account.profile')}}">Cuenta</a>&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="{{@route('cart.wish-list')}}">Wish List</a>&nbsp;&nbsp;&nbsp;&nbsp;
              <form action="{{@route('logout')}}" method="POST" name="frm_logout" id="frm_logout" style="display:none;">
                @csrf
              </form>
              <a href="#logout" onClick="window.frm_logout.submit();"> Salir</a>
            </div>
          </div>
          <a href="{{@route('category.all')}}" class="navbar-item">
            <strong>
              Categorías
            </strong>
          </a>
          <a href="{{@route('cart.index')}}" class="navbar-item">
              @if ($cartCount)
                <span class="tag is-light">
                    {{$cartCount}}
                </span>
              @endif
              &nbsp;
              <span class="icon has-text-primary">
                  <i class="material-icons">add_shopping_cart</i>
                </span>
              {{-- <strong>
              Carrito
              </strong> --}}
          </a>
        </div>
      </div>
    </nav>
    @component('frontend.components.nav-sub-category')@endcomponent
  </div>
  {{-- {{@route("api.autoComplete")}} --}}

</header>
