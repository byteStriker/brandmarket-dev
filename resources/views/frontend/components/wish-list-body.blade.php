<div class="column is-9">
  <table class="table">
    <tr>
      <th></th>
      <th>Nombre del Proyecto</th>
      <th>Total</th>
      <th></th>
    </tr>
    @if ($myWishList)
      @foreach ($myWishList as $cart)
        <tr>
          <td></td>
          <td>
            <div class="columns">
              <div class="column">
                <h1>{{strtoupper($cart->wishlist_name)}}</h1>
              </div>
            </div>
          </td>
          <td>US $ {{number_format($cart->total, 2, ".", ",")}}</td>
          <td>
            <form action="{{@route('cart.wish2cart')}}" method="POST" name="frmOpenCart_{{$cart->id}}" id="frmOpenCart_{{$cart->id}}" style="display:none;">
              @csrf
              @method('PUT')
              <input type="hidden" name="simple_cart_id" id="simple_cart_id-{{$cart->id}}" value="{{$cart->id}}">
            </form>
            <a href="#openCart" onClick="window.frmOpenCart_{{$cart->id}}.submit();" class="button is-primary"> Editar</a>
          </td>
        </tr>
      @endforeach
    @endif
  </table>
</div>
