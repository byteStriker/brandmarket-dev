<section>
  <div class="cycle-slideshow hero-slideshow" data-cycle-fx="fade" data-cycle-timeout="4200" data-cycle-slides="> a">
    @if ($slides)
    @if($slides->count())

    @forelse ($slides->SlideDetails as $detail)
      <a href="#" style="background-image: url({{@url($detail->cover_image)}})">
        {{-- <img src="{{@asset($detail->cover_image)}}" alt="{{$detail->name}}"  id="no-slides-block-{{$detail->id}}"> --}}
        <div class="container hero-container">
          <div class="columns">
            <div class="column is-6 is-offset-6 slide-info">
              <div class="content">
                <h1 class="{{$detail->text_color ?? ''}}">{{$detail->name}}</h1>
                @php
                    $colorButton = 'is-primary';
                    switch ($detail->text_color){
                      case'has-text-white':
                        $colorButton = 'is-white';
                      break;
                      case'has-text-black':
                        $colorButton = 'is-black';
                      break;
                      case'has-text-light':
                        $colorButton = 'is-light';
                      break;
                      case'has-text-primary':
                        $colorButton = 'is-primary';
                      break;
                      case'has-text-info':
                        $colorButton = 'is-info';
                      break;
                      case'has-text-warning':
                        $colorButton = 'is-warning';
                      break;
                      case'has-text-danger':
                        $colorButton = 'is-danger';
                      break;
                    }

                @endphp
                <button data-url="{{@url($detail->link_url)}}" class="{{$colorButton}} my-changes button is-medium goto-slide-url">{{$detail->link_text}}</button>
              </div>
            </div>
          </div>
        </div>
      </a>
    @empty
    <a href="#">
      <img src="{{@asset('storage/main-slide/bar-y-gourmet.png')}}" id="no-slides-block-0-5">
    </a>
    @endforelse
    @else
    @php
    $arrCovers = ["bar-y-gourmet.png","bebidas-02.png","bebidas.png","boligrafos-02.png","mascotas.png"]
    @endphp

    {{-- <a href="{{route('products.list', [$row->Category->slug, $row->slug])}}"> --}}
      <a href="{{@route('products.list', ['bar-gourmet'])}}">
        <img src="{{@asset('storage/main-slide/bar-y-gourmet.png')}}" id="no-slides-block-1">
        <div class="container">
          <div class="columns">
            <div class="column is-6 is-offset-6">
              <div class="content">
                <p>**37 Quisque massa augue, sodales a nisi quis, malesuada scelerisque diam. In tristique lobortis fringilla.</p>
                <button class="button is-medium">Boton 2</button>
              </div>
            </div>
          </div>
        </div>
      </a>
      <a href="{{@route('products.list', ['bebidas'])}}"> <img src="{{@asset('storage/main-slide/bebidas-02.png')}}" id="no-slides-block-2"> </a>
      <a href="{{@route('products.list', ['bebidas'])}}"> <img src="{{@asset('storage/main-slide/bebidas.png')}}" id="no-slides-block-3"> </a>
      <a href="{{@route('products.list', ['boligrafos'])}}"> <img src="{{@asset('storage/main-slide/boligrafos-02.png')}}" id="no-slides-block-4"> </a>
      <a href="{{@route('products.list', ['mascotas'])}}"> <img src="{{@asset('storage/main-slide/mascotas.png')}}" id="no-slides-block-5"> </a>
      @endif
    @endif

      {{-- <div class="cycle-prev"><i class="mdi mdi-chevron-left"></i></div>
      <div class="cycle-next"><i class="mdi mdi-chevron-right"></i></div> --}}
      <div class="cycle-pager"></div>
    </div>
  </section>
