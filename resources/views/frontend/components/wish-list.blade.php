<div class="container">
  @if (isset($show_hello))
    @if ($show_hello)
      <h2 class="title is-5 has-text-primary">¡Hola, {{strtoupper(auth()->user()->name)}}!</h2>
    @endif
  @endif
  <h3 class="subtitle is-5">Mis wishlists</h3>
  <div class="columns">
    @component('frontend.components.cart-summary', ['error' => false]) @endcomponent
      @component('frontend.components.wish-list-body') @endcomponent
      </div>
    </div>
