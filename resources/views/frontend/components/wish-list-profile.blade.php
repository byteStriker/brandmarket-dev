{{-- {{dd($myCart)}} --}}
@if (isset($myWishList[0]))
  <section class="section">
    <div class="container">
      @if (isset($show_hello))
        @if ($show_hello)
          <h2 class="title is-5 has-text-primary">¡Hola, {{strtoupper(auth()->user()->name)}}!</h2>
        @endif
      @endif
      <h3 class="subtitle is-5">Lista de Deseos: {{strtoupper($myWishList[0]->sc_wishlist_name)}}</h3>
      <div class="columns">
      @component('frontend.components.wish-list-body') @endcomponent
      @component('frontend.components.cart-summary') @endcomponent
      </div>
    </div>
  </section>
@endif
