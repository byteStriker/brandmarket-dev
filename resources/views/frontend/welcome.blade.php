@extends('layouts.frontend', ['title'=>$page_title])
@section('content')
  @component('frontend.components.hero-slide', [
    'slides' => $slides
  ])
  @endcomponent
@endsection
