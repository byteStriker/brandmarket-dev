@extends('layouts.frontend', ['title'=>$page_title])
@section('content')
<div class="container">
  <div class="content is-medium">
    <p></p>
    <h1 class="has-text-primary	">{{$post->title}}</h1>
    <br/>
    <p>{!!$post->content!!}</p>
  </div>
</div>
@endsection
