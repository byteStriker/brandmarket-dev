@extends('layouts.frontend', ['title'=>$page_title])
@section('cart')
  {{-- {{dd($myCart)}} --}}
  <section class="section">
    <div class="container">
      <h2 class="title is-5 has-text-primary">¡Hola, {{strtoupper(auth()->user()->name)}}!</h2>
      <div class="card  ">
        <header class="card-header">
          <p class="card-header-title">
            <span class="is-size-3">Acción Requerida</span>
          </p>
          <a href="#" class="card-header-icon" aria-label="more options">
            <span class="icon">
              <i class="fas fa-angle-down" aria-hidden="true"></i>
            </span>
          </a>
        </header>
        <div class="card-content">
          <div class="content">
            <p class="is-size-6">
              Detectamos que hay un carrito de compras sin cotizar que fue creado el
              <time datetime="{{date('Y-m-d', strtotime($openCart->created_at))}}">
                {{ date('d', strtotime($openCart->created_at))}} de {{date('m', strtotime($openCart->created_at))}} del {{date('Y', strtotime($openCart->created_at)) }}
                a las {{ date('h:iA', strtotime($openCart->created_at))}}
              </time><br/><br/>
            </p>
            <p class="is-size-6">
              Te proponemos tomar una de las siguientes acciones
            </p>
          </div>
        </div>
        <footer class="card-footer">
          <form action="{{@route('cart.checkout')}}" method="POST" name="frmCheckout" id="frmCheckout" >
            @csrf
            <input type="hidden" name="id" id="id" value="{{$myCart->id}}">
          </form>
          <a href="{{@route('cart.index')}}" class="card-footer-item button is-dark is-outlined">Quiero ir a mi carrito de compras</a>
          <a href="#" onClick="window.frmCheckout.submit();" class="card-footer-item button is-primary is-outlined">Solicitar Cotización</a>
          <a href="#" id="showModal" class="card-footer-item button is-info is-outlined">Guardar como Wish List</a>
        </footer>
      </div>
      </div>
    </div>
  </section>

  @component('frontend.components.modal-save-wishlist', ['wishlist2Cart'=> $wishList2Cart]) @endcomponent

@endsection
