@extends('layouts.frontend', ['title'=>$page_title])
@section('cart')
{{-- {{dd($myCart)}} --}}
<section class="section">
  <div class="container">
    <h2 class="title is-5 has-text-primary">¡Hola, {{strtoupper(auth()->user()->name)}}!</h2>
    <h3 class="subtitle is-5">Cotización</h3>
    <div class="columns">
      @if ($myCartDetails)
      <div class="column is-9">
        <table class="table">
          <tr>
            <th width="45%"></th>
            <th width="20%">Precio Unitario</th>
            <th width="10%">Cantidad</th>
            <th width="16%">Total</th>
            <th width="4%"></th>
          </tr>
          @foreach ($myCartDetails as $cartDetail)
          <tr>
            <td>
              <div class="columns">
                <div class="column">
                  <figure>
                      @php
                      $imgSrc = '';// '//placeimg.com/200/200/arch/grayscale';
                      $prefixSrc = '';
                      @endphp

                      @if (strtolower($cartDetail->pbm_provider_name) == 'doble-vela')
                        @if  (strstr($cartDetail->pbm_image, 'storage/' ))
                          @php
                          $image = explode(',', $cartDetail->pbm_image);
                          printf('<img src="%s"  data-from="url" class="is-flex"/>', asset($image[0]));
                          @endphp
                        @else
                          @if ( base64_encode(base64_decode($cartDetail->pbm_image, true)) === $cartDetail->pbm_image)
                            @if ($cartDetail->pbm_image != '')
                              @php printf('<img src="data:image/png;base64,%s"  data-from="base64" class="is-flex"/>', $cartDetail->pbm_image); @endphp
                            @else
                              @php printf('<img src="%s"  data-from="default-logo" class="is-flex" style="padding-top:30px"/>', asset('storage/frontend-assets/brandmarket-blue.png')); @endphp
                            @endif
                          @else
                            @php
                            printf('<img src="%s"  data-from="default-logo" class="is-flex"/>', asset('storage/frontend-assets/brandmarket-blue.png'));
                            @endphp
                          @endif
                        @endif
                      @elseif ($cartDetail->pbm_provider_name == '4-promotional')
                        @php
                        $image = explode(',', $cartDetail->pbm_image);
                        printf('<img src="%s"  data-from="url" class="is-flex"/>', asset($image[0]));
                        @endphp
                      @endif
                    {{-- @if (strstr($cartDetail->pbm_image, 'storage'))
                      <img src="/{{$cartDetail->pbm_image}}" alt="{{$cartDetail->scd_product_name}}" style="width:60px; height:60px;">
                    @else

                    @endif --}}
                  </figure>
                </div>
                <div class="column">

                  <h1>{{strtoupper($cartDetail->scd_product_name)}}</h1>
                  <p>Color: {{strtoupper($cartDetail->scd_color)}}</p>
                  <p><small>{{$cartDetail->pbm_stock}} Disponibles</small></p>
                </div>
              </div>
            </td>
            <td>USD $ {{number_format($cartDetail->scd_price, 2, ".",",")}}</td>
            <td>
              <div class="columns">
                <div class="column is-half">
                  <input class="input" type="text" id="quantity-{{$cartDetail->scd_id}}" name="quantity-{{$cartDetail->scd_id}}" value="{{$cartDetail->scd_quantity}}">
                </div>
                <div class="column is-half">
                  {{-- <button type="button" name="button" class="button is-dark" onClick="updateProductQuantity({{$cartDetail->sc_id}}, {{$cartDetail->scd_id}})"> --}}
                    <button type="button" name="button" class="button is-primary updateCartProductQuantity"
                    data-simple-cart-id="{{$cartDetail->sc_id}}" data-simple-cart-details-id="{{$cartDetail->scd_id}}"
                    data-original-price="{{$cartDetail->original_price}}"
                    >
                      <i class="material-icons">update</i>
                    </button>
                  </div>
                </div>
              </td>
              <td>USD $ {{number_format($cartDetail->scd_subtotal, 2, ".", ",")}}</td>
              <td>

                  <button type="button" name="button" class="button is-link removeProduct" data-simple-cart-id="{{$cartDetail->sc_id}}" data-simple-cart-details-id="{{$cartDetail->scd_id}}">
                      <i class="material-icons">remove_shopping_cart</i>
                  </button>
              </td>
            </tr>
            @endforeach
          </table>

          <div class="buttons is-right">
            @if (!$error)
            <a href="{{@route('category.all')}}" class="button is-primary">Comprar Más &nbsp;
              <i class="material-icons">add_shopping_cart</i>
            </a>

            @else
            <div class="column is-12">
              <h1 class="is-size-1">
                Aún no haz elegido ningún producto.¡Hazlo ya!

            </h1>
              <a href="{{@route('category.all')}}" class="button is-primary">
                Agregar Productos
                <i class="material-icons">add_shopping_cart</i>

              </a>
            </div>

            @endif
            @if (!$error && isset($myCartDetails[0]))
            <a href="#cart-to-wish-list" id="showModal" class="button is-dark">
                Crear Wish List &nbsp;
                <i class="material-icons">format_list_bulleted</i>
              </a>
            {{-- <a class="button is-black">Actualizar piezas disponibles</a> --}}
            @endif
          </div>
        </div>
        @component('frontend.components.cart-summary', ['error' => $error]) @endcomponent
        @if (isset($myCartDetails[0]))
          @component('frontend.components.modal-save-wishlist') @endcomponent
        @endif
        {{-- <script type="text/javascript">
          function dropProduct(form){
            $('#'+form).submit();
          }
        </script> --}}
        @else
        <div class="column is-12">
          <h1 class="is-size-1">
            Aún no haz elegido ningún producto. ¡Hazlo ya!
          </h1>
          <a href="{{@route('category.all')}}" class="button is-primary">
            Agregar Productos
              <i class="material-icons">add_shopping_cart</i>

          </a>
        </div>
        @endif
      </div>
    </div>
  </section>
  <form class="" action="{{@route('cart.delete-product')}}" name="frmDeleteProducts" id="frmDeleteProducts" method="POST" style="display:none">
      @method('DELETE')
      @csrf
      <input type="text" name="simple_cart_id" id="simple_cart_id_delete" value="">
      <input type="text" name="simple_cart_details_id" id="simple_cart_details_id_delete" value="">
  </form>
  <form class="" action="{{@route('cart.update-product-quantity')}}" name="frmUpdateProducts" id="frmUpdateProducts" method="POST" style="display:none">
    @method('PUT')
    @csrf
    <input type="hidden" name="originalPrice" id="originalPrice" value="">
    <input type="hidden" name="simple_cart_id" id="simple_cart_id_update" value="">
    <input type="hidden" name="simple_cart_details_id" id="simple_cart_details_id_update" value="">
    <input type="hidden" name="simple_cart_details_quantity" id="simple_cart_details_quantity" value="">
  </form>
  @endsection
