@extends('layouts.frontend')
@section('content')
<section class="section">
  <div class="container">
    <div class="columns is-multiline">
      @if (isset($filteredProducts))
        @foreach ($filteredProducts as $product)
        <!-- Empieza producto -->
        <div class="column is-one-fifth has-product">
          {{-- <label>{{$product->provider_name}}</label> --}}
          @if (strtolower($product->provider_name) == 'doble-vela')
            @if ($subcat_slug)
              <a href="{{@route('products.product', [md5('doble-vela'), $cat_slug, $subcat_slug, $product->slug, $product->model])}}">
            @else
              <a href="{{@route('products.product', [md5('doble-vela'), $cat_slug, $cat_slug, $product->slug, $product->model])}}">
            @endif
          @elseif (strtolower($product->provider_name) == '4-promotional')
            @if ($subcat_slug)
              <a href="{{@route('products.product', [md5('4-promotional'), $cat_slug, $subcat_slug, $product->slug, \Str::slug($product->model)])}}">
            @else
              <a href="{{@route('products.product', [md5('4-promotional'), $cat_slug, $cat_slug, $product->slug, \Str::slug($product->model)])}}">
            @endif
          @endif
          <figure class="image">
            @php
            $imgSrc = '';// '//placeimg.com/200/200/arch/grayscale';
            $prefixSrc = '';
            @endphp
            @if (strtolower($product->provider_name) == 'doble-vela')
              {{-- @if  (strstr($product->image, 'storage/' ))
                @php
                $image = explode(',', $product->image);
                printf('<img src="%s"  data-from="url" class="is-flex"/>', asset($image[0]));
                @endphp
              @else
                @if ( base64_encode(base64_decode($product->image, true)) === $product->image)
                  @if ($product->image != '')
                    @php printf('<img src="data:image/png;base64,%s"  data-from="base64" class="is-flex"/>', $product->image); @endphp
                  @else
                    @php printf('<img src="%s"  data-from="default-logo" class="is-flex" style="padding-top:102px"/>', asset('storage/frontend-assets/brandmarket-blue.png')); @endphp
                  @endif
                @else
                  @php
                  printf('<img src="%s"  data-from="default-logo" class="is-flex"/>', asset('storage/frontend-assets/brandmarket-blue.png'));
                  @endphp
                @endif
              @endif --}}
              @php
              try {
                // print("<br/> public/doble-vela/".strtoupper(\Str::slug($product->category_doble_vela)).'/300X240/'.strtoupper($product->model));
                if(\Storage::disk('local')->has("public/doble-vela/".strtoupper(\Str::slug($product->category_doble_vela)).'/300X240/'.strtoupper($product->model))){
                  // print("file found");
                  // $images = File::allFiles(base_path()."/storage/app/public/doble-vela/".strtoupper(Str::slug($product->category_doble_vela))."/300X240/".strtoupper($product->model)."/");
                  // $path = explode('/', $image[0]);
                  // print("path: ". print_r($path, 1));
                  // $tmp = array_splice($path, -5);
                  // $imagePath = implode('/', $tmp);
                  printf('<img src="%s"  data-from="url" class="is-flex"/>', @asset("/storage/doble-vela/".strtoupper(Str::slug($product->category_doble_vela))."/300X240/".strtoupper($product->model)."/".strtoupper($product->model).".jpg"));
                }
                else if( base64_encode(base64_decode($product->image)) === $product->image) {
                  // print("data image");
                  $prefixSrc = 'data:image/jpg;base64,';
                  $images = explode(',', $product->image);
                  // printf('<img src="%s" data-from="default-logo" class="is-flex" style="padding-top:102px"/>', "{$prefixSrc}{$images[0]}");
                  printf('<img src="%s" data-from="default-logo" class="is-flex" style="padding-top:102px"/>', asset('storage/frontend-assets/brandmarket-blue.png'));

                } else {
                  printf('<img src="%s" data-from="default-logo" class="is-flex" style="padding-top:102px"/>', asset('storage/frontend-assets/brandmarket-blue.png'));

                }
              } catch (\Throwable $th) {
                printf('<img src="%s" data-from="default-logo" class="is-flex" style="padding-top:102px"/>', asset('storage/frontend-assets/brandmarket-blue.png'));
              }
              @endphp
            @elseif ($product->provider_name == '4-promotional')
              @php
              $image = explode(',', $product->image);
              printf('<img src="%s"  data-from="url" class="is-flex"/>', asset($image[0]));
              @endphp
            @endif
          </figure>
              <p class="has-background-light has-text-centered has-text-dark">
                {{number_format($product->stock)}} disponibles
              </p>
              <p class="is-size-6 has-text-weight-bold">
                {{$product->name}}
              </p>
              <div class="control color-selector is-small">
                @php
                $productColors = explode(',', strtolower($product->colors));
                // print_r($productColors);
                @endphp
                @if (count($productColors))
                  @forelse ($productColors as $productColor)
                    @php
                        list($color, $id, $currentStock) = explode('|',$productColor);
                    @endphp
                    @if (isset($colors[trim(rtrim($color))]))
                    <label class="radio  @if (trim(rtrim($color))== 'blanco' || trim(rtrim($color)) == 'transparente') light-color @endif" style="background-color:{{$colors[trim(rtrim($color))]}}">
                      <input type="radio" name="foobar" disabled>
                      <i class="mdi mdi-check-bold"></i>
                    </label>
                    @endif
                    @empty
                  @endforelse
                @endif
              </div>
              <p class="is-size-6">
                <strong class="has-text-primary">
                  @php
                  // print("<pre>\$rateExchangeAmount: ".floatval($rateExchangeAmount)."</pre>");
                  $rateUtility = (double)(1.2000);
                  $tax = (double)(1.1600);
                  $redeem = (double)(0.3500);
                  $shipperPrice = (double)($product->original_price);
                  $maxPrice = (double)($shipperPrice) * (double)($rateUtility);
                  $unitPriceCalculus = ((double)$maxPrice * (double)($tax)) / ((double)($rateExchangeAmount) - (double)($redeem)) * 1;
                  $unitPrice = number_format((float)$unitPriceCalculus, 2);
                  @endphp
                  USD {{$unitPrice}}
                </strong>
                <small>
                  Precio unitario
                </small>
              </p>
            </a>
          </div>
          <!-- Termina producto -->
          @endforeach
        {{-- <div class="column is-full">
          <div class="is-centered">
              {{$filteredProducts->links()}}

          </div>
        </div> --}}
      @endif

      </div>
    </div>
  </section>
  @endsection
