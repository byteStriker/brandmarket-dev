@extends('layouts.frontend')
@section('content')

<section class="section">
  <div class="container">
    <div class="columns">
      <div class="column">
        <div id="slideshow-1">
          <div id="cycle-1" class="cycle-slideshow"
          data-cycle-slides="> div"
          data-cycle-timeout="0"
          data-cycle-prev="#slideshow-1 .cycle-prev"
          data-cycle-next="#slideshow-1 .cycle-next"
          data-cycle-caption="#slideshow-1 .custom-caption"
          data-cycle-caption-template="Slide {!!'&#123;&#123;slideNum&#125;&#125;'!!} of {!!'&#123;&#123;slideCount&#125;&#125;'!!}"
          data-cycle-fx="tileBlind">
          <div><img src="http://malsup.github.io/images/beach1.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach2.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach3.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach4.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach5.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach6.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach7.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach8.jpg" width=500 height=500></div>
          <div><img src="http://malsup.github.io/images/beach9.jpg" width=500 height=500></div>
        </div>
      </div>

      <div id="slideshow-2">
        <div id="cycle-2" class="cycle-slideshow"
        data-cycle-slides="> div"
        data-cycle-timeout="0"
        data-cycle-prev="#slideshow-2 .cycle-prev"
        data-cycle-next="#slideshow-2 .cycle-next"
        data-cycle-caption="#slideshow-2 .custom-caption"
        data-cycle-caption-template="{!!'&#123;&#123;slideNum&#125;&#125;'!!} of {!!'&#123;&#123;slideCount&#125;&#125;'!!}"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="5"
        data-cycle-carousel-fluid=true
        data-allow-wrap="false"
        >
        <div><img src="http://malsup.github.io/images/beach1.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach2.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach3.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach4.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach5.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach6.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach7.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach8.jpg" width=100 height=100></div>
        <div><img src="http://malsup.github.io/images/beach9.jpg" width=100 height=100></div>
      </div>

      <!-- <p>
        <a href="#" class="cycle-prev">&laquo; prev</a> | <a href="#" class="cycle-next">next &raquo;</a>
        <span class="custom-caption"></span>
      </p> -->
    </div>

  </div>
  <div class="column">
    <div class="content">
      <h1 class="title is-4">Libreta negra de pasta dura</h1>
      <p>Mauris sit amet elit at ante pharetra vehicula. Suspendisse enim nunc, tempor dignissim dui mollis, consectetur mattis nulla. Vivamus quis nisi non ipsum lacinia blandit. Quisque sed justo leo.</p>
      <div class="columns is-vcentered">
        <div class="column">
          <h3 class="title is-2">$ 0.00</h3>
          <h6 class="subtitle is-5 has-text-primary">Precio unitario</h6>
        </div>
        <div class="column">
          <div class="field is-horizontal">
            <div class="field-label is-medium">
              <label class="label">Cantidad</label>
            </div>
            <div class="field-body">
              <div class="field">
                <p class="control">
                  <input class="input is-medium" type="text" value="">
                </p>
              </div>
            </div>
          </div>
          <div class="field is-grouped">
            <div class="control is-expanded">
              <button class="button is-fullwidth is-dark ">Agregar a wishlist</button>
            </div>
            <div class="control">
              <button class="button is-fullwidth is-primary">Agregar al carrito</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <table class="units">
      <tr>
        <th>1+</th>
        <th>A+</th>
        <th>B+</th>
        <th>C+</th>
        <th>D+</th>
      </tr>
      <tr>
        <td>US $0.00</td>
        <td>US $0.00</td>
        <td>US $0.00</td>
        <td>US $0.00</td>
        <td>US $0.00</td>
      </tr>
    </table>
    <p class="has-text-right">
      <small>
        Precio unitario con descuento por volumen
      </small>
    </p>
    <div class="title is-4">Color</div>
    <div class="field">
      <div class="control color-selector">
        <label class="radio" style="background-color:#000000">
          <input type="radio" name="foobar">
          <i class="mdi mdi-check-bold"></i>
        </label>
        <label class="radio" style="background-color:#3d4094">
          <input type="radio" name="foobar">
          <i class="mdi mdi-check-bold"></i>
        </label>
        <label class="radio" style="background-color:#be1f3a">
          <input type="radio" name="foobar">
          <i class="mdi mdi-check-bold"></i>
        </label>
        <label class="radio" style="background-color:#d66843">
          <input type="radio" name="foobar">
          <i class="mdi mdi-check-bold"></i>
        </label>
        <label class="radio" style="background-color:#bfdf63">
          <input type="radio" name="foobar">
          <i class="mdi mdi-check-bold"></i>
        </label>
        <label class="radio" style="background-color:#8775a7">
          <input type="radio" name="foobar">
          <i class="mdi mdi-check-bold"></i>
        </label>
        <label class="radio" style="background-color:#ff93ce">
          <input type="radio" name="foobar">
          <i class="mdi mdi-check-bold"></i>
        </label>
      </div>
    </div>

    <div class="title is-4">Especificaciones</div>
    <table class="table">
      <tr>
        <th>
          Material:
        </th>
        <td>
          Curpiel
        </td>
      </tr>
      <tr>
        <th>
          Tamaño:
        </th>
        <td>
          14.6 x 21.6 cm
        </td>
      </tr>
      <tr>
        <th>
          Modelo:
        </th>
        <td>
          OFPL01
        </td>
      </tr>
    </table>
  </div>
</div>
</div>
</section>
<!-- Destacados -->
<section>
  <div class="container">
    <div class="columns">
      <div class="column">
        <figure><img src="" alt=""></figure>
      </div>
      <div class="column">
        <figure><img src="" alt=""></figure>
      </div>
      <div class="column">
        <figure><img src="" alt=""></figure>
      </div>
      <div class="column">
        <figure><img src="" alt=""></figure>
      </div>
      <div class="column">
        <figure><img src="" alt=""></figure>
      </div>
    </div>
  </div>
</section>

</div>
</div>
</div>
</section>
<div class="modal">
    <div class="modal-background"></div>
    <div class="modal-content">
      <div class="box">
        <!-- Agregar al carrito -->
        <div class="content has-text-centered">
          <p class="title is-5">Tu producto ha sido agregado al carrito</p>
          <div class="buttons is-centered">
            <button class="button is-dark ">Seguir comprando</button>
            <button class="button is-primary">Ir al carrito</button>
          </div>
        </div>
        <!-- Agregar al carrito -->
        <div class="content has-text-centered">
          <p class="title is-5">Selecciona tu Wishlist</p>
          <div class="field has-addons has-addons-centered">
            <div class="control ">
              <div class="select">
                <select name="country">
                  <option>Campamento BBVA</option>
                  <option>Activación Gatorade</option>
                </select>
              </div>
            </div>
            <div class="control">
              <button type="submit" class="button is-primary">Choose</button>
            </div>
          </div>
          <div class="buttons is-centered">
            <button class="button is-dark ">Seguir comprando</button>
            <button class="button is-primary">Ir al carrito</button>
          </div>
        </div>
      </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
  </div>
</html>
@endsection
