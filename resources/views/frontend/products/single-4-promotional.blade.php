@extends('layouts.frontend')
@section('content')
<section class="section">
  <div class="container">
      <div class="columns">
          <div class="column">
            <div class="product-image">
              <div id="product-image" class="cycle-slideshow" data-cycle-slides="> div" data-cycle-timeout="0">
                @php
                @endphp
                {{-- {{dd($images)}} --}}
                @if (count($images))
                @forelse ($images as $image)
                <div><img src="{{$image}}" style="width:500px; height:500px;"></div>
                @empty
                <div>@component('frontend.components.logo', ['width'=>"500", "height"=>"500"])@endcomponent</div>
                @endforelse
                @endif
              </div>
              <div id="product-thumbnails" class="cycle-slideshow"
              data-cycle-slides="> div"
              data-cycle-timeout="0"
              data-cycle-prev="#product-thumbnails .cycle-prev"
              data-cycle-next="#product-thumbnails .cycle-next"
              data-cycle-fx="carousel"
              data-cycle-carousel-visible="5"
              data-cycle-carousel-fluid="true"
              data-allow-wrap="false"
              data-cycle-carousel-vertical="true">
              <a href="" class="cycle-prev"><</a>
              <a href="" class="cycle-next">></a>


              @if (count($images))
              @forelse ($images as $image)
                <div><img src="{{$image}}" style="width:100px; height:100px;"></div>
              @empty
                <div>@component('frontend.components.logo', ['width'=>"100", "height"=>"100"])@endcomponent</div>
              @endforelse
              @endif
            </div>
          </div>
    </div>
    <div class="column">
      {{-- <form class="form-horizontal validate" action="{{@route('cart.add-product')}}" method="post" id="addProductForm"> --}}
        <form class="form-horizontal validate" method="post" id="addProductForm">
          @csrf

          <div class="content">
            <h1 class="title is-4">{{$product->name}}</h1>
            <p>
              @php
              $tempProductDescriptionInput = explode(" ", $product->description);
              $tempSearchVal = ["/negro/i","/blanco/i","/amarillo/i","/azul/i","/rojo/i","/verde/i","/naranja/i","/violeta/i","/purpura/i","/morado/i",
              "/rosa/i","/gris/i","/humo/i","/marron/i","/cafe/i","/plateado/i","/plata/i","/dorado/i","/oro/i","/limon/i","/lima/i","/mostaza/i",
              "/lavanda/i","/lila/i","/salmon/i","/fucsia/i","/magenta/i","/escarlata/i","/borgona/i","/marfil/i","/champaña/i","/beige/i",
              "/ocre/i","/cobre/i","/ambar/i","/aguamarina/i","/turquesa/i","/cian/i", "/jade/i","/azul/i"];
              $replaceVal = [" - "];
              $productDescription  = preg_replace($tempSearchVal, ' ', $product->description);
              @endphp
              {{$productDescription}}

            </p>
            @if ($product->original_price > 0)

            <div class="columns is-vcentered">
              <div class="column">
                <h3 class="title is-2 is-unit-price">
                  @php
                  // dd($product->original_price);
                  $rateUtility = floatval(1.20);
                  $tax = floatval(1.16);
                  $redeem = floatval(0.35);
                  $shipperPrice = floatval($product->original_price);
                  $maxPrice = floatval($shipperPrice) * floatval($rateUtility);
                  $unitPriceCalculus = ((floatval($shipperPrice) * floatval($rateUtility)) * floatval($tax)) / (floatval($rateExchangeAmount) - floatval($redeem));
                  $unitPrice = number_format($unitPriceCalculus, 2);
                  @endphp
                  USD {{$unitPrice}}
                </h3>
                <input type="hidden" name="unitPrice" id="unitPrice" value="{{$unitPrice}}">
                <input type="hidden" name="originalPrice" id="originalPrice" value="{{$product->original_price}}">
                <input type="hidden" name="providerName" id="providerName" value="4-promotional">
                <h6 class="subtitle is-5 has-text-primary">Precio unitario</h6>
              </div>

            </div>

            @endif


          </div>
          @if ($product->original_price > 0)

          <table class="units">
            <tr>
              <th>1+</th>
              <th>
                @php
                $priceAPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(3000.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountA+ '.$quantity.' = (3000 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.05);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * 0.05</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceAPlusTemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceAPlusTemp: ".number_format($priceAPlusTemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceAPlus = floatval($priceAPlusTemp) / (floatval($quantity));
                // print("<pre>priceAPlus: ".number_format($priceAPlus, 2)." = ".number_format($priceAPlusTemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>
              <th>

                @php
                $priceBPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(6001.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountB+ '.$quantity.' = (6001.00 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.07);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * ".floatval( 0.07)."</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceBTemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceBTemp: ".number_format($priceBTemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceBPlus = floatval($priceBTemp) / (floatval($quantity));
                // print("<pre>priceB: ".number_format($priceB, 2)." = ".number_format($priceBTemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>
              <th>
                @php
                $priceCPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(10001.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountB+ '.$quantity.' = (10001.00 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.10);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * ".floatval(0.10)."</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceCTemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceCTemp: ".number_format($priceCTemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceCPlus = floatval($priceCTemp) / (floatval($quantity));
                // print("<pre>priceC: ".number_format($priceC, 2)." = ".number_format($priceCTemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>
              <th>
                @php
                $priceDPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(20001.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountB+ '.$quantity.' = (20001.00 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.15);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * ".floatval(0.15)."</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceDTemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceDTemp: ".number_format($priceDTemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceDPlus = floatval($priceDTemp) / (floatval($quantity));
                // print("<pre>priceD: ".number_format($priceD, 2)." = ".number_format($priceDTemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>
              <th>
                @php
                $priceEPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(35001.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountB+ '.$quantity.' = (35001.00 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.17);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * ".floatval(0.17)."</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceETemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceETemp: ".number_format($priceETemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceEPlus = floatval($priceETemp) / (floatval($quantity));
                // print("<pre>priceE: ".number_format($priceE, 2)." = ".number_format($priceETemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>
              <th>@php
                $priceFPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(45001.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountB+ '.$quantity.' = (45001.00 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.18);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * ".floatval(0.18)."</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceFTemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceETemp: ".number_format($priceFTemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceFPlus = floatval($priceFTemp) / (floatval($quantity));
                // print("<pre>priceE: ".number_format($priceF, 2)." = ".number_format($priceFTemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>
              <th>
                @php
                $priceGPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(55001.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountB+ '.$quantity.' = (55001.00 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.19);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * ".floatval( 0.19)."</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceGPlusTemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceETemp: ".number_format($priceGPlusTemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceGPlus = floatval($priceGPlusTemp) / (floatval($quantity));
                // print("<pre>priceE: ".number_format($priceGPlus, 2)." = ".number_format($priceGPlusTemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>

              <th>
                @php
                $priceHPlus = 0;
                // print("<pre>shipperPrice: {$shipperPrice}---------- {$rateExchangeAmount} </pre>");
                $quantity = ceil(floatval(70001.00) / floatval($shipperPrice));
                // print('<pre>pricesForDiscountB+ '.$quantity.' = (70001.00 / '.$shipperPrice.')</pre>');
                $shipperTotalPrice =  floatval($quantity) * floatval($shipperPrice);
                // print('<pre>shipperTotalPrice: '.$shipperTotalPrice.' =  '.$quantity.' * '.$shipperPrice.'</pre>');
                $shipperDiscount = floatval($shipperTotalPrice) * floatval(0.20);
                // print("<pre>shipperDiscount: {$shipperDiscount} = {$shipperTotalPrice} * ".floatval( 0.20)."</pre>");
                $tempPrice = floatval($shipperTotalPrice) - floatval($shipperDiscount);
                // print("<pre>tempPrice (shipTotal - shipDiscount): {$tempPrice} = {$shipperTotalPrice} - {$shipperDiscount}</pre>");
                $shipperPricePlusUtility = floatval($tempPrice) * floatval($rateUtility);
                // print("<pre>shipperPlusUtility: ".number_format($shipperPricePlusUtility, 2)." = ".number_format($tempPrice, 2)." * ".number_format($rateUtility, 2)."</pre>");
                $priceWithTaxAPlus = floatval($shipperPricePlusUtility) * floatval($tax);
                // print("<pre>priceWithTaxAPlus: ".number_format($priceWithTaxAPlus, 2)." = ".number_format($shipperPricePlusUtility, 2)." * ".number_format($tax, 2)."</pre>");
                $priceHPlusTemp = floatval($priceWithTaxAPlus) / (floatval($rateExchangeAmount) - floatval($redeem));
                // print("<pre>priceETemp: ".number_format($priceGPlusTemp, 2)." = ".number_format($priceWithTaxAPlus, 2)." / (".number_format(($rateExchangeAmount), 2)." - ".number_format($redeem, 2).")</pre>");
                $priceHPlus = floatval($priceHPlusTemp) / (floatval($quantity));
                // print("<pre>priceE: ".number_format($priceGPlus, 2)." = ".number_format($priceGPlusTemp, 2)." / ".number_format($rateExchangeAmount, 2)."</pre>");
                @endphp
                {{$quantity}}
              </th>
            </tr>
            <tr>
              <td>USD {{number_format($unitPrice, 2)}}</td>
              <td>USD {{number_format($priceAPlus, 2)}}</td>
              <td>USD {{number_format($priceBPlus, 2)}}</td>
              <td>USD {{number_format($priceCPlus, 2)}}</td>
              <td>USD {{number_format($priceDPlus, 2)}}</td>
              <td>USD {{number_format($priceEPlus, 2)}}</td>
              <td>USD {{number_format($priceFPlus, 2)}}</td>
              <td>USD {{number_format($priceGPlus, 2)}}</td>
              <td>USD {{number_format($priceHPlus, 2)}}</td>
            </tr>
          </table>
          <p class="has-text-right">
            <small>
              Precio unitario con descuento por volumen
            </small>
          </p>
          @endif

          <div class="columns ">
            <div class="column is-one-third is-pulled-right">
              <small>Piezas Disponibles</small>
            </div>
            <div class="column is-half is-pulled-right">
              <a href="#" id="updateStockProvider" data-product-model="{{strtoupper($product->model)}}" class="button is-info is-disabled">
                <strong id="providerStock">{{number_format($product->stock)}}</strong>
                <i class="material-icons" id="updateExistenceIcon">update</i>
              </a>
            </div>
          </div>
          {{-- <p>
            {{auth()->user()->api_token}}
          </p> --}}
          <div class="title is-4">Color</div>
          <div class="field">
            <div class="control color-selector">
              @php
              $productColors = explode(',', $product->colors);
              @endphp
              @if (count($productColors))
              @forelse ($productColors as $productColor)
              @php
              list($color, $id, $currentStock) = explode('|',$productColor);
              @endphp
              @if (isset($colors[trim(rtrim($color))]))
              <label class="radio @if (trim(rtrim($color))== 'blanco' || trim(rtrim($color))== 'transparente') light-color @endif" style="background-color:{{$colors[trim(rtrim($color))]}}"
              data-product-id="{{$id}}"
              data-product-parent-category="{{strtoupper(Str::slug($product->category_doble_vela))}}"
              data-product-parent-sub-category="{{strtoupper(Str::slug($product->subcategory_doble_vela))}}"
              data-product-name="{{$product->name}}"
              data-product-model="{{$product->model}}"
              data-product-stock="{{$currentStock}}"
              data-color-name="{{trim(rtrim($color))}}">
              <input type="radio" name="color"
              id="color-{{$id}}"
              data-color-name="{{trim(rtrim($color))}}"
              data-product-id="{{$id}}"
              data-product-parent-category="{{strtoupper(Str::slug($product->category_doble_vela))}}"
              data-product-parent-sub-category="{{strtoupper(Str::slug($product->subcategory_doble_vela))}}"
              data-product-name="{{$product->name}}"
              data-product-model="{{$product->model}}"
              data-product-stock="{{$currentStock}}"
              >
              <i class="mdi mdi-check-bold"></i>
            </label>
            @endif
            @empty
            @endforelse
            @endif
          </div>
        </div>
        @if ($product->original_price > 0 )

        <div class="columns">
          <div class="column">
            <div class="field is-horizontal">
              <div class="field-label is-medium">
                <label class="label">Cantidad</label>
              </div>
              <div class="field-body">
                <div class="field">
                  <p class="control">
                    <input class="input is-medium required" id="quantity" name="quantity" type="text" value="" required>
                  </p>
                </div>
              </div>
            </div>
            <div class="field is-grouped">
              <div class="control is-expanded">
                <button id="addToWishList" href="#addToWishList" class="button is-fullwidth is-dark">Agregar a wishlist</button>
              </div>
              <div class="control">
                <button id="addToCart" class="button is-fullwidth is-primary">Agregar al carrito</button>
              </div>
            </div>
          </div>
        </div>
        @endif

        <div class="title is-4">Especificaciones</div>
        <table class="table">
          <tr>
            <th>
              Material:
            </th>
            <td>
              {{$product->material}}
            </td>
          </tr>
          <tr>
            <th>
              Tamaño:
            </th>
            <td>
              {{$product->size}}
            </td>
          </tr>
          <tr>
            <th>
              Modelo:
            </th>
            <td>
              {{$product->model}}
            </td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>

<div id="related-products">
  <a href="" class="cycle-prev"><</a>
  <a href="" class="cycle-next">></a>
  <div class="slideshow"
  data-cycle-slides="> div"
  data-cycle-timeout="0"
  data-cycle-prev="#related-products .cycle-prev"
  data-cycle-next="#related-products .cycle-next"
  data-cycle-fx="carousel"
  data-cycle-carousel-visible="4"
  data-cycle-carousel-fluid="true"
  data-allow-wrap="false">
  <div>
    {{-- Aquí los productos relacionadoss --}}
    @if (count($relatedProducts))
    @foreach ($relatedProducts as $product)
    @if (strtolower($product->provider_name) == 'doble-vela')
    @if ($subcat_slug)
    <a href="{{@route('products.product', [md5('doble-vela'), $cat_slug, $subcat_slug, $product->slug, $product->model])}}">
      @else
      <a href="{{@route('products.product', [md5('doble-vela'), $cat_slug, $cat_slug, $product->slug, $product->model])}}">
        @endif
        @elseif (strtolower($product->provider_name) == '4-promotional')
        @if ($subcat_slug)
        <a href="{{@route('products.product', [md5('4-promotional'), $cat_slug, $subcat_slug, $product->slug, \Str::slug($product->model)])}}">
          @else
          <a href="{{@route('products.product', [md5('4-promotional'), $cat_slug, $cat_slug, $product->slug, \Str::slug($product->model)])}}">
            @endif
            @endif
            {{-- <figure class="thumbnail"> --}}

              @php
              $image = explode(',', $product->image);
              printf('<img src="%s"  data-from="url" style="width:110px; height:110px"/>', asset($image[0]));
              @endphp

              {{-- </figure> --}}
            </a>
            @endforeach
            @endif
          </div>
        </div>
      </div>



    </section>

    <div class="modal" style="z-index:9999" id="modal-add-product">
      <div class="modal-background"></div>
      <div class="modal-content">
        <div class="box">
          <!-- Agregar al carrito -->
          <div class="content has-text-centered">
            <p class="title is-5">Tu producto ha sido agregado al carrito</p>
            {{-- <p class="is-12" id="cart-mini-summary"></p> --}}
            <div class="buttons is-centered">
              <a class="button is-dark custom-modal-close" id="close-modal">Seguir comprando</a>
              <a href="{{@route('cart.index')}}" class="button is-primary" >Ir al carrito</a>
            </div>
          </div>
        </div>
      </div>
      <button class="modal-close is-large" aria-label="close"></button>
    </div>

    <div class="modal" style="z-index:9999" id="modal-add-wishlist">
      <div class="modal-background"></div>
      <div class="modal-content">
        <div class="box">
          <!-- Agregar al carrito -->
          <div class="content has-text-centered">
            @if (!count($myWishList))
            <p class="title is-5">Crea un Nuevo Wish List</p>
            <p><small>... y presiona el botón "Guardar" para generar tu nuevo wish list<br/>
              el producto seleccionado será añadido automáticamente.</small></p>
              @else
              <p class="title is-5">Selecciona tu Wishlist</p>
              @endif

              <div class="field has-addons has-addons-centered">
                @if (count($myWishList))
                <div class="control ">
                  <div class="select">
                    <select name="wishListId" id="wishListId">
                      <option value="-1">Seleccionar</option>
                      @foreach ($myWishList as $item)
                      <option value="{{$item->id}}">{{$item->wishlist_name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="control">
                  <a id="saveToWishList" class="button is-primary">Guardar Aquí</a>
                </div>
                @else
                <div class="control">
                  <input class="input" type="text" placeholder="Nombre del Wish List" name="wishlist_name" id="wishlist_name" value="">
                </div>
                @endif
              </div>
              {{-- <div class="content" id="wishlist-mini-summary"></div> --}}
              <div class="buttons is-centered">
                @if (!count($myWishList))
                <a class="button is-primary is-light" id="create-new-wishlist">Guardar Aquí</a>
                @endif
                <a class="button is-dark custom-modal-close" id="close-modal">Seguir comprando</a>
                <a href="{{@route('cart.index')}}" class="button is-primary" >Ir al carrito</a>
              </div>
            </div>
          </div>
        </div>
        <button class="modal-close is-large" aria-label="close"></button>
      </div>
      @endsection
