@extends('layouts.frontend')
@section('content')
  <section class="section">
    <div class="container">
      {{-- <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/">INICIO</a></li>
           <li><a href="{{@route('category.list', ['category'=>request()->segment(3)] )}}"> {{strtoupper(request()->segment(3))}}</a> </li>
           <li><a href="{{@route('subcategory.list', ['subcategory'=>request()->segment(4)] )}}"> {{strtoupper(request()->segment(4))}}</a> </li>
          <li class="is-active"><a href="#" aria-current="page">PRODUCTOS</a></li>
        </ul>
      </nav> --}}
      <div class="columns is-multiline">
        <!-- Empieza producto -->
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <!-- Termina producto -->
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
        <div class="column is-one-fifth">
          <a href="">
            <figure class="image is-square">
              <img src="" alt="">
            </figure>
            <p class="has-background-light">
              2,200 Disponibles
            </p>
            <p class="title">
              Libreta Alessa
            </p>
            <div class="control">
              <label class="radio">
                <input type="radio" name="color" value="negro" checked>
              </label>
              <label class="radio">
                <input type="radio" name="color" value="azul">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rojo">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="naranja">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="verde">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="morado">
              </label>
              <label class="radio">
                <input type="radio" name="color" value="rosa">
              </label>
            </div>
            <div class="is-flex">
              <p class="has-text-primary">
                <strong>
                  US $00.00
                </strong>
              </p>
              <p>
                <small>
                  Precio unitario
                </small>
              </p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
@endsection
