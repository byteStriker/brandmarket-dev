
@extends('layouts.frontend', ['title'=>$page_title])
@section('account-profile')
  <section class="section">
    <div class="container">
      <h2 class="title is-5 has-text-primary">¡Hola {{auth()->user()->name}}!</h2>
      <form method="POST" action="{{ route('account.update-profile') }}" role="form" class="form-horizontal validate">
        @csrf
        @method('PUT')
        <div class="columns">
          <div class="column">
            <div class="field">
              <label>{{ __('Name') }}</label>
              <div class="control">
                <input id="name" type="text" class="input @error('name') is-invalid @enderror" name="name" value="{{ $row->name }}" required autocomplete="name" autofocus>
                  @error('name')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label>{{ __('E-Mail Address') }}</label>
                <div class="control">
                  <input id="email" type="email" class="input @error('email') is-invalid @enderror" name="email" value="{{ $row->email }}" required autocomplete="email" autofocus>
                    @error('email')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="column">
                <div class="field">
                  <label>{{ __('Password') }}</label>
                  <div class="control">
                    <input id="password" type="password" class="input @error('password') is-invalid @enderror" name="password" value="{{$row->password}}" required autocomplete="current-password">

                      @error('password')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="column">
                  <div class="field">
                    <label>{{ __('Confirm Password') }}</label>
                    <div class="control">
                      <input id="password-confirm" type="password" class="input" name="password_confirmation" autocomplete="new-password">
                      @error('password')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <footer class="row text-center">
                <button type="submit" class="button is-primary"><i class="icon-menu2"></i>
                  {{ __('Actualizar Mis Datos') }}
                </button>
              </footer>
            </form>
            @component('frontend.components.wish-list-profile',['show_hello'=>false]) @endcomponent
          </div>
        </section>
      @endsection
