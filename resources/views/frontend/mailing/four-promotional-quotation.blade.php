@extends('layouts.mailing')
@section('content')
  <section>
    <div class="container">
      <!--Cada tabla es un bloque-->
      <table class="factura">
        <tr>
          <td>
            <p>Levantamiento de pedido</p>
            <p class="title is-4">{{ $provider_name ?? ''}}</p>
            Cliente {{$client_name}}. Numero de Cliente:{{$client_number}}
          </td>
          <td class="has-text-centered">
          </td>
          <td>
              {{-- <p class="has-text-primary"><strong>Control Interno. {{$internalCode}}</strong></p> --}}
            <p>Cotización emitida el dia {{$year}} de {{$month}} de {{$day}} a las {{$time}} horas ({{$timezone}})</p>
          </td>
        </tr>
        <tr>
          <td colspan="3">
            <p>Hola, favor de apoyar con el siguiente pedido:</p>
          </td>
        </tr>
      </table>
      <table class="table inner">
        <!-- Header -->
        <thead>
            <th></th>
            <th>Modelo</th>
            <th>Producto</th>
            <th>Color</th>
            <th>Unidades</th>
        </thead>
        <!-- Header -->
        <tbody>
            @if ($simpleCart)
              @foreach ($simpleCart as $simpleCartDetail)
                <tr>
                  <td>
                    <img src="{{$simpleCartDetail->image}}" alt="Brand Market" style="width:70px; height:70px">
                  </td>
                  <td>{{strtoupper($simpleCartDetail->model)}}</td>
                  <td>{{strtoupper($simpleCartDetail->name)}}</td>
                  <td>{{strtoupper($simpleCartDetail->color)}}</td>
                  <td>{{$simpleCartDetail->quantity}}</td>
                </tr>
              @endforeach
            @endif
          </tbody>
        <!-- Factura - Footer -->
      </table>
      Favor de enviar la relación de cajas a entregar y de que dimensiones.
    </div>
  </section>
@endsection
