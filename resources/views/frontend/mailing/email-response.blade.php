@extends('layouts.frontend')
@section('content')
  <section class="section">
    <div class="container">
      <h2 class="title is-5 has-text-primary">¡Hola!</h2>
      <h4 class="title is-5 has-text-primary">Recibimos tu cotización y ya estamos dándole seguimiento</h4>
      <div class="columns">
      <div class="column is-12">
        Haz recibido este mensaje electrónico desde nuestro servidor Brand Market Mailer.<br/>
        Adjunto a este mensaje encontrarás un archivo en formato PDF que contiene la información<br/>
        de la cotización solicitada
      </div>
      </div>
    </div>
  </section>
  @endsection
