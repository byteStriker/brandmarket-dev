@extends('layouts.mailing')
@section('content')
@php
  // $stuff = $simpleCart->first();
  // dd($stuff);
  $total = 0;
@endphp
<section>
  <div class="container">
    <!--Cada tabla es un bloque-->
    <table class="table">
      <thead ></thead>
      <tr>
        <td>
            <img src="{{$message->embed(storage_path('app/public/frontend-assets/img/brandmarket-blue.png'))}}" style="width:200px;height:96px" alt="Brand Market">
        </td>
        <td>
          <p><strong>Confirmación de pedido</strong></p>
        </td>
        <td>
          <p class="has-text-primary"><strong>Control Interno. {{$internalCode}}</strong></p>
          <p>Cotización emitida el día {{$year}} de {{$month}} de {{$day}} a las {{$time}} horas ({{$timezone}})</p>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <p><strong>Hola, {{auth()->user()->name}}</strong></p>
          {{-- <p><strong>Encargado:</strong> Gonzalo García.</p> --}}
        </td>
      </tr>
    </table>
    <table class="table inner">
      <thead>
        <tr>
          <th></th>
          <th>Modelo</th>
          <th>Producto</th>
          <th>Color</th>
          <th>Unidades</th>
          <th>Costo U</th>
          <th>Costo</th>
        </tr>
      </thead>
      <tbody>
        @if ($simpleCart)
        @foreach ($simpleCart as $simpleCartDetail)
        <tr>
          <td width="15%">
              @php
              // print("{$simpleCartDetail->provider_name}");
              if (strtolower($simpleCartDetail->provider_name) == 'doble-vela'){
                $style = 'style="width:70px; height:70px"';
                $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".png";
                if(!\File::exists(storage_path($image))){
                  $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".jpg";
                }
                if(!\File::exists(storage_path($image))){
                  $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".jpeg";
                }
                if(!\File::exists(storage_path($image))){
                  $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".gif";
                }
                if(!\File::exists(storage_path($image))){
                  $image = "app/public/frontend-assets/brandmarket-blue.png";
                  $style = 'style="width:150px; height:70px"';
                }
                print("<img src=\"".$message->embed(storage_path($image))."\" alt=\"Brand Market\" {$style} />");
              } else if (strtolower($simpleCartDetail->provider_name) == '4-promotional'){
                print("<img src=\"{$simpleCartDetail->image}\" alt=\"Brand Market\" {$style} />");
              }
            @endphp

          </td>
          <td>{{strtoupper($simpleCartDetail->model)}}</td>
          <td>{{strtoupper($simpleCartDetail->name)}}</td>
          <td>{{strtoupper($simpleCartDetail->color)}}</td>
          <td>{{$simpleCartDetail->quantity}}</td>
          <td>USD {{number_format($simpleCartDetail->d_price, 2)}}</td>
          <td>USD {{number_format($simpleCartDetail->d_subtotal, 2)}}</td>
        </tr>
        @php
          $total += $simpleCartDetail->d_subtotal;
        @endphp
        @endforeach
        @endif
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5"></td>
          <td>Total</td>
          <td>
            <strong>USD {{number_format($total, 2)}}</strong>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
</section>
@endsection
