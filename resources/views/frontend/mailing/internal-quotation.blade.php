@extends('layouts.mailing')
@section('content')
  <section>
    <div class="container">
      <!--Cada tabla es un bloque-->
      <table class="factura">
        <!-- Header -->
        <thead>
          <tr>
            <td>
              <table class="header">
                <tr>
                  <td>
                    <img src="{{$message->embed(storage_path('app/public/frontend-assets/img/brandmarket-blue.png'))}}" alt="Brand Market">
                  </td>
                  <td class="has-text-centered">
                    <p class="title is-4">ORDEN DE COMPRA</p>
                    <p class="subtitle is-4">– Interno –</p>
                    <p>Levantamiento de pedido</p>
                  </td>
                  <td>
                    <p class="has-text-primary"><strong>Control Interno. {{$internalCode}}</strong></p>
                    <p>Cotizacion emitida el dia {{$year}} de {{$month}} de {{$day}} a las {{$time}} horas ({{$timezone}})</p>
                  </td>
                </tr>
                {{-- <tr>
                  <td colspan="3">
                    <p><strong>Hola, {{auth()->user()->name}}</strong></p>
                    <p>Te informamos que se ha iniciado el proceso de preparación y esto completa tu pedido.</p>
                    <p>Tu pedido está en camino y ya no puede ser modificado.</p>
                  </td>
                </tr> --}}
              </table>

            </td>
          </tr>
        </thead>
        <!-- Header -->
        <tbody data-count-provider-products="{{count($simpleCart)}}">

          @if ($simpleCart)
            @php
              $sumSubTotalMXN = $sumSubTotalUSD = 0;
            @endphp
            @foreach ($simpleCart as $simpleCartDetail)
              <tr id="product-row-{{$loop->index}}">
                <td>
                  <table class="outer">
                    <tr>
                      <td colspan="2">
                        <!--Inicia tabla del producto-->
                        <table class="inner">
                          <tr>
                            <td rowspan="2" class="has-image">
                              @php
                                $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".png";
                                if(!\File::exists(storage_path($image))){
                                  $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".jpg";
                                }
                                if(!\File::exists(storage_path($image))){
                                  $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".jpeg";
                                }
                                if(!\File::exists(storage_path($image))){
                                  $image = "app/public/doble-vela/".strtoupper(Str::slug($simpleCartDetail->product_parent_category))."/300X240/".strtoupper($simpleCartDetail->model)."/".strtoupper($simpleCartDetail->model).".gif";
                                }
                                if(!\File::exists(storage_path($image))){
                                  $image = "app/public/frontend-assets/brandmarket-blue.png";
                                  $style = 'style="width:150px; height:70px"';
                                }

                              @endphp
                              <img src="{{$message->embed(storage_path($image) )}}" alt="Brand Market" style="width:70px; height:70px">
                            </td>
                            <th>Modelo</th>
                            <th>Producto</th>
                            <th>Color</th>
                            <th>Unidades</th>
                          </tr>
                          <tr>
                            <td>{{$simpleCartDetail->model}}</td>
                            <td>{{$simpleCartDetail->name}}</td>
                            <td>{{$simpleCartDetail->color}}</td>
                            <td>{{$simpleCartDetail->quantity}}</td>
                          </tr>
                        </table>
                        <!--Termina tabla del producto-->
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <!--Inicia tabla del detalle del importador-->
                        <table class="inner">
                          <tr>
                            <th colspan="2">Detalles importador</th>
                          </tr>
                          <tr>
                            <td>Precio de Lista</td>
                            <td>MXN {{number_format($simpleCartDetail->d_original_price, 2)}}</td>
                          </tr>
                          <tr>
                            <td>Subtotal</td>
                            <td>MXN {{number_format($simpleCartDetail->d_original_price * $simpleCartDetail->quantity, 2)}}</td>
                          </tr>
                        </table>
                        <!--Termina tabla del detalle del importador-->
                      </td>
                      <td>
                        <!--Inicia tabla del detalle del cliente-->
                        <table class="inner">
                          <tr>
                            <th colspan="2">Detalles cliente</th>
                          </tr>
                          <tr>
                            <td>Precio de Lista</td>
                          <td>USD {{number_format($simpleCartDetail->d_price, 2)}}</td>
                          </tr>
                          <tr>
                            <td>Precio de Lista</td>
                            <td>USD {{number_format($simpleCartDetail->d_subtotal, 2)}}</td>
                          </tr>
                        </table>
                        <!--Termina tabla del detalle del cliente-->
                      </td>
                    </tr>
                    @php
                      $sumSubTotalMXN += $simpleCartDetail->d_subtotal_mx;
                      $sumSubTotalUSD += $simpleCartDetail->d_subtotal;
                    @endphp
                    @if ($loop->index + 1 == count($simpleCart))
                    <tr>
                        <!--Cuando hay promo-->
                        <td colspan="2" class="has-promo">
                          Total a pagar {{strtoupper($simpleCartDetail->provider_name)}} MXN {{number_format($sumSubTotalMXN, 2)}}
                        </td>
                      </tr>

                    @endif
                  </table>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
        <!-- Factura - Footer -->
        <tfoot>
          <tr>
            <td>
              <table class="outer">
                <tr>
                  <td>
                    <table class="inner">
                      <tr>
                        <th colspan="3">Datos del cliente</th>
                      </tr>
                      <tr>
                        <td>{{strtoupper(auth()->user()->name)}}</td>
                        <td>RFC</td>
                        <td>{{strtolower(auth()->user()->email)}}</td>
                      </tr>
                      <tr>
                        <td colspan="3">
                          <strong>Total a facturar: USD {{$simpleCart[0]->total_usd}}</strong>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <p>Mercancía en constante movimiento.</p>
              <p>Los precios están sujetos a cambios sin previo aviso.</p>
              <p>El color de los artículos puede variar algunos tonos al color real por la calibración y resolución de tu monitor.</p>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </section>

@endsection
