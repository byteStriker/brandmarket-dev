@extends('layouts.mailing')

@section('content')
<section class="section">
  <div class="container">
    <h2 class="title is-5 has-text-primary">¡Hola!</h2>
    {{-- <h4 class="title is-5 has-text-primary">Recibimos tu cotización y ya estamos dándole seguimiento</h4> --}}
    <div class="columns">
      <div class="column is-12">
        <div class="panel">
          <div class="panel-heading">
            <div class="columns">
              <div class="column">
                <h1 class="panel-title has-text-white	">Gracias, {{strtoupper(auth()->user()->name)}}</h1>
              </div>

            </div>
          </div>
          <div class="panel-block">
            Tu cotización ya está siendo procesada.
            Revisa tu bandeja de correo electrónico, si no encuentras el mensaje recuerda buscar también en la carpeta de
            SPAM por favor.<br/>
            <h4 class="has-text-primary"><a href="{{URL::to('/')}}">Quiero ir al sitio.</a></h4>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection
