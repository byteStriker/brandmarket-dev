@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  @component('backoffice.components.table', ['columns'=> $columns, 'link_create'=>route('product.create'), 'search_route' => 'product.index'])

  @slot('rows')
  @forelse ($rows as $row)
  <tr>
    <th>{{$loop->iteration}}</th>
    <td>{{$row->model}}</td>
    <td>{{$row->name}}</td>
    <td>{{$row->description}}</td>
    <td>$ {{number_format($row->price, 2, '.', ',')}}</td>
    <td>
      @component('backoffice.components.action-buttons-table')
      @slot('linkDetails')
      @component('backoffice.components.link', ['linkText'=>'Detalles','class'=>'dropdown-item has-text-light', 'routeLink'=>@route('product.show',$row)])@endcomponent
      @endslot
      @slot('linkEdit')
      @component('backoffice.components.link', ['linkText'=>'Editar','class'=>'dropdown-item has-text-light', 'routeLink'=>@route('product.show',$row)])@endcomponent
      @endslot
      @slot('linkRemove')
      @component('backoffice.components.link', ['linkText'=>'Borrar','class'=>'dropdown-item has-text-light', 'routeLink'=>@route('product.confirm-delete',$row)])@endcomponent
      @endslot
      @endcomponent
    </td>
  </tr>
  @empty
  @endforelse
  @endslot
  @slot('pagination_links')
  {{$rows->appends(request()->input())->links()}}
  @endslot
  @endcomponent
  @endsection
