@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')

  @component('backoffice.components.delete',
    [
      'link_goback' => @route('product.index'),
      'link_edit' => @route('product.edit', $row),
      'link_delete' => @route('product.destroy',$row),
      'columnsMultiLineClass'=>'columns is-multiline'


    ])

    @slot('row_properties')


      @component('backoffice.components.input',['label' => 'category_id', 'name' => 'category_id', 'id' => 'category_id', 'value' => $row->Category->name, 'readonly' => 'readonly', 'columnRatio' => 'is-6'])@endcomponent
      @component('backoffice.components.input',['label' => 'subcategory_id', 'name' => 'subcategory_id', 'id' => 'subcategory_id', 'value' => $row->Subcategory->name, 'readonly' => 'readonly', 'columnRatio' => 'is-6'])@endcomponent

      @component('backoffice.components.input',['label' => 'name', 'name' => 'name', 'id' => 'name', 'value' => $row->name, 'readonly' => 'readonly', 'columnRatio' => 'is-6'])@endcomponent
      @component('backoffice.components.input',['label' => 'description', 'name' => 'description', 'id' => 'description', 'value' => $row->description, 'readonly' => 'readonly', 'columnRatio' => 'is-6'])@endcomponent

      @component('backoffice.components.input',['label' => 'model', 'name' => 'model', 'id' => 'model', 'value' => $row->model, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'size', 'name' => 'size', 'id' => 'size', 'value' => $row->size, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'color', 'name' => 'color', 'id' => 'color', 'value' => $row->color, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'colors', 'name' => 'colors', 'id' => 'colors', 'value' => $row->colors, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent

      @component('backoffice.components.input',['label' => 'material', 'name' => 'material', 'id' => 'material', 'value' => $row->material, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'printing', 'name' => 'printing', 'id' => 'printing', 'value' => $row->printing, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'printing_area', 'name' => 'printing_area', 'id' => 'printing_area', 'value' => $row->printing_area, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package weight', 'name' => 'Package weight', 'id' => 'package_weight', 'value' => $row->package_weight, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package height', 'name' => 'Package height', 'id' => 'package_height', 'value' => $row->package_height, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package width', 'name' => 'Packag _width', 'id' => 'package_width', 'value' => $row->package_width, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package length', 'name' => 'Package length', 'id' => 'package_length', 'value' => $row->package_length, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package quantity', 'name' => 'Package_q antity', 'id' => 'package_quantity', 'value' => $row->package_quantity, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Price', 'name' => 'Price', 'id' => 'price', 'value' => $row->price, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Existence', 'name' => 'Existence', 'id' => 'existence', 'value' => $row->existence, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Min stock alert', 'name' => 'Min stock alert', 'id' => 'min_stock_alert', 'value' => $row->min_stock_alert, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Min stock message', 'name' => 'Min stock message', 'id' => 'min_stock_message', 'value' => $row->min_stock_message, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'is_featured', 'name' => 'is_featured', 'id' => 'is_featured', 'value' => $row->is_featured, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Stock', 'name' => 'Stock', 'id' => 'stock', 'value' => $row->stock, 'readonly' => 'readonly', 'columnRatio' => 'is-3'])@endcomponent

    @endslot
  @endcomponent
@endsection
