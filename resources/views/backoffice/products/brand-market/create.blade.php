@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  @component('backoffice.components.create',
  [
    'link_goback' => @route('product.index'),
    'link_create' => @route('product.store'),
    // 'buttons' => $buttons,
    'columnsMultiLineClass'=>'columns is-multiline',
  ])
  @slot('create_script')
    {{-- <script type="text/javascript" src="{{asset('/backoffice-assets/js/webapp/forms/product.ui.controller.js')}}"></script> --}}
    <script type="text/javascript">
    $(document).ready(function(){
  var form = $('#createForm');
  $('#btnSaveAddOther').click(function(){
      var action = "<input type='hidden' value='saveAddOther' name='action'>";
      form.append(action);
      form.submit();
  });
  $('select[name=category_id]').change(function() {
    $('select[name=subcategory_id]').empty().html('...');
    var d = $(this).find(':selected').data();
    var url = `{{@route('api.getOptionsForCategory')}}/${d.slug}`;
    $.getJSON(url,function(data) {
      var items = ['<option value="-1">Selecciona una Subcategoría</option>'];
      $.each( data, function( key, val ) {
        items.push( "<option value='" + val.id + "' data-slugSubcategory='"+val.slug+"'>" + val.name + "</option>" );
      });
      $('select[name=subcategory_id]').empty().html(items.join("\n"));
    });
  });

})

    </script>
  @endslot
  @slot('row_properties')
      <div class="column is-6">
        <div class="field">
          <label class="label">Category ID</label>
          <div class="control is-expanded">
            <div class="select is-fullwidth">
              <select id="category_id" name="category_id">
                <option value="-1">Categorías Disponibles</option>
                @if ($optsCategory)
                  @forelse ($optsCategory as $category)
                    <option data-slug="{{$category->slug}}" value="{{$category->id}}"> {{$category->name}}</option>
                  @empty
                    <option value="-1">No hay categorías</option>
                  @endforelse
                @endif
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="column is-6">
        <div class="field">
          <label class="label">Subcategory ID</label>
          <div class="control is-expanded">
            <div class="select is-fullwidth">
              <select id="subcategory_id" name="subcategory_id">
                <option value="-1">Subcategorías Disponibles</option>
                {{-- @if ($optsSubcategory)
                  @forelse ($optsSubcategory as $subcategory)
                    <option value="{{$subcategory->id}}"> {{$subcategory->name}}</option>
                  @empty
                    <option value="-1">No hay categorías</option>
                  @endforelse
                @endif --}}
              </select>
            </div>
          </div>
        </div>
      </div>
      @component('backoffice.components.input',['label' => 'Name', 'placeholder' => 'Name', 'name' => 'name', 'id' => 'name', 'columnRatio' => 'is-6'])@endcomponent
      @component('backoffice.components.input',['label' => 'Description', 'placeholder' => 'Description', 'name' => 'description', 'id' => 'description', 'columnRatio' => 'is-6'])@endcomponent
      @component('backoffice.components.input',['label' => 'Model', 'placeholder' => 'Model', 'name' => 'model', 'id' => 'model', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Size', 'placeholder' => 'Size', 'name' => 'size', 'id' => 'size', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Color', 'placeholder' => 'Color', 'name' => 'color', 'id' => 'color', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Colors', 'placeholder' => 'Colors', 'name' => 'colors', 'id' => 'colors', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Material', 'placeholder' => 'Material', 'name' => 'material', 'id' => 'material', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Printing', 'placeholder' => 'Printing', 'name' => 'printing', 'id' => 'printing', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Printing area', 'placeholder' => 'Printing area', 'name' => 'printing_area', 'id' => 'printing_area', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package weight', 'placeholder' => 'Package weight', 'name' => 'package_weight', 'id' => 'package_weight', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package height', 'placeholder' => 'Package height', 'name' => 'package_height', 'id' => 'package_height', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package width', 'placeholder' => 'Package width', 'name' => 'package_width', 'id' => 'package_width', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package length', 'placeholder' => 'Package length', 'name' => 'package_length', 'id' => 'package_length', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Package quantity', 'placeholder' => 'Package quantity', 'name' => 'package_quantity', 'id' => 'package_quantity', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Image', 'type' => 'file', 'name' => 'image', 'id' => 'image', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Price', 'placeholder' => 'Price', 'name' => 'price', 'id' =>' price', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Existence', 'placeholder' => 'Existence', 'name' => 'existence', 'id' => 'existence', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Min stock alert', 'placeholder' => 'Min stock alert', 'name' => 'min_stock_alert', 'id' => 'min_stock_alert', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'Min stock message', 'placeholder' => 'Min stock message', 'name' => 'min_stock_message', 'id' => 'min_stock_message', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'is_featured', 'placeholder' => 'is_featured', 'name' => 'is_featured', 'id' => 'is_featured', 'columnRatio' => 'is-3'])@endcomponent
      @component('backoffice.components.input',['label' => 'stock', 'placeholder' => 'stock', 'name' => 'stock', 'id' => 'stock', 'columnRatio' => 'is-3'])@endcomponent
      <div class="column is-2">
          <div class="field">
            <label class="label">Proveedor</label>
            <div class="control is-expanded">
              <div class="select is-fullwidth">
                <select name="status" id="status">
                  <option value="-1">Seleccione</option>
                  <option value="doble-vela" @if (strtolower($row->provider_name) == 'doble-vela') selected="selected" @endif>Doble Vela</option>
                  <option value="4-promotional" @if (strtolower($row->provider_name) == '4-promotional') selected="selected" @endif>4 Promotional</option>
                  <option value="promo-opcion" @if (strtolower($row->provider_name) == 'promo-opcion') selected="selected" @endif>Promo Opción</option>
                </select>
              </div>
            </div>
          </div>
        </div>
    {{-- </div> --}}
  @endslot
@endcomponent

@endsection
