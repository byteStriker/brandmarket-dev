@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @component('backoffice.components.edit',
    [
      'link_goback' => @route('product.index'),
      'link_update' => @route('product.update', $row),
      'link_delete' => @route('product.confirm-delete', $row),
      'id' => $row->id,
      'columnsMultiLineClass'=>'columns is-multiline'
    ])
    @slot('row_properties')
      {{-- <input type="hidden" name="id" value="{{$row->id}}">
      <input type="hidden" name="category_id" value="{{$row->category_id}}">
      <input type="hidden" name="subcategory_id" value="{{$row->subcategory_id}}"> --}}
      {{-- {{ dd($row->name) }} --}}
        @component('backoffice.components.input',['label' => 'Categoría', 'placeholder' => 'category_id', 'name' => 'category_name', 'id' => 'category_form', 'value' => $row->Category->name, 'columnRatio' => 'is-6', 'readonly' => 'readonly'])@endcomponent
        @component('backoffice.components.input',['label' => 'Subcategoría', 'placeholder' => 'subcategory_id', 'name' => 'form', 'id' => 'form', 'value' => $row->Subcategory->name, 'columnRatio' => 'is-6', 'readonly' => 'readonly'])@endcomponent
        @component('backoffice.components.input',['label' => 'Name', 'placeholder' => 'Name', 'name' => 'name', 'id' => 'name', 'value' => $row->name, 'columnRatio' => 'is-6'])@endcomponent
        @component('backoffice.components.input',['label' => 'Description', 'placeholder' => 'Description', 'name' => 'description', 'id' => 'description', 'value' => $row->description, 'columnRatio' => 'is-6'])@endcomponent
        @component('backoffice.components.input',['label' => 'Model', 'placeholder' => 'Model', 'name' => 'model', 'id' => 'model', 'value' => $row->model, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Size', 'placeholder' => 'Size', 'name' => 'size', 'id' => 'size', 'value' => $row->size, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Color', 'placeholder' => 'Color', 'name' => 'color', 'id' => 'color', 'value' => $row->color, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Colors', 'placeholder' => 'Colors', 'name' => 'colors', 'id' => 'colors', 'value' => $row->colors, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Caterial', 'placeholder' => 'Caterial', 'name' => 'material', 'id' => 'material', 'value' => $row->material, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Printing', 'placeholder' => 'Printing', 'name' => 'printing', 'id' => 'printing', 'value' => $row->printing, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Printing area', 'placeholder' => 'Printing area', 'name' => 'printing_area', 'id' => 'printing_area', 'value' => $row->printing_area, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Package weight', 'placeholder' => 'Package weight', 'name' => 'package_weight', 'id' => 'package_weight', 'value' => $row->package_weight, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Package height', 'placeholder' => 'Package height', 'name' => 'package_height', 'id' => 'package_height', 'value' => $row->package_height, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Package width', 'placeholder' => 'Package width', 'name' => 'package_width', 'id' => 'package_width', 'value' => $row->package_width, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Package length', 'placeholder' => 'Package length', 'name' => 'package_length', 'id' => 'package_length', 'value' => $row->package_length, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Package quantity', 'placeholder' => 'Package quantity', 'name' => 'package_quantity', 'id' => 'package_quantity', 'value' => $row->package_quantity, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Image', 'type' => 'file', 'name' => 'image', 'id' => 'image', 'value' => $row->image, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Price', 'placeholder' => 'Price', 'name' => 'price', 'id' => 'price', 'value' => $row->price, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Existence', 'placeholder' => 'Existence', 'name' => 'existence', 'id' => 'existence', 'value' => $row->existence, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Min stock alert', 'placeholder' => 'Min stock alert', 'name' => 'min_stock_alert', 'id' => 'min_stock_alert', 'value' => $row->min_stock_alert, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'Min stock message', 'placeholder' => 'Min stock message', 'name' => 'min_stock_message', 'id' => 'min_stock_message', 'value' => $row->min_stock_message, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'is_featured', 'placeholder' => 'is_featured', 'name' => 'is_featured', 'id' => 'is_featured', 'value' => $row->is_featured, 'columnRatio' => 'is-3'])@endcomponent
        @component('backoffice.components.input',['label' => 'stock', 'placeholder' => 'stock', 'name' => 'stock', 'id' => 'stock', 'value' => $row->stock, 'columnRatio' => 'is-3'])@endcomponent
        <div class="column is-2">
          <div class="field">
            <label class="label">Proveedor</label>
            <div class="control is-expanded">
              <div class="select is-fullwidth">
                <select name="status" id="status">
                  <option value="-1">Seleccione</option>
                  <option value="doble-vela" @if (strtolower($row->provider_name) == 'doble-vela') selected="selected" @endif>Doble Vela</option>
                  <option value="4-promotional" @if (strtolower($row->provider_name) == '4-promotional') selected="selected" @endif>4 Promotional</option>
                  <option value="promo-opcion" @if (strtolower($row->provider_name) == 'promo-opcion') selected="selected" @endif>Promo Opción</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      @if ($row->image)
        <div class="row text-center"> {{asset("storage/{$row->image}")}}
          <div class="row text-center">
            <div class="col-sm-12">
              <img src="{{asset("storage/{$row->image}")}}" alt="{{$row->name}}">
            </div>
          </div>
        </div>
      @endif
    @endslot
  @endcomponent

@endsection
