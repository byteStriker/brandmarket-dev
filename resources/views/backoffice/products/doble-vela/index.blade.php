@extends('layouts.backoffice', ['title' => $page_title])
@section('content')

  <!-- Bordered datatable inside panel -->
  <div class="panel">
    <div class="panel-heading">
      <div class="columns">
        <div class="column">
          <h6 class="panel-title has-text-white	">Lista de Registros</h6>
        </div>
        <div class="column">
          <div class="is-pulled-right">
            <form action="#" role="form" method="POST" name="updateForm" id="updateForm" class="form-horizontal validate" enctype="multipart/form-data" style="display:none">
              @csrf
              @method('POST')
            </form>
            @component('backoffice.components.link', ['linkText'=>'Importar CSV', 'events'=>'onClick="window.updateForm.submit()"','class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>'#update'])@endcomponent
            </div>

          </div>
        </div>
      </div>
<div class="panel-block">

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            @foreach ($columns as $column)
              <th>{{$column}}</th>
            @endforeach
            {{-- <th>Acciones</th> --}}
          </tr>
        </thead>
        <tbody>
          @forelse ($rows as $row)
            <tr>
              <th>{{$loop->iteration}}</th>
              {{-- 'element_id', 'model', 'short_name', 'color', 'price_distribution', 'price_public' --}}
              <td>{{$row->element_id}}</td>
              <td>{{$row->model}}</td>
              <td>{{$row->short_name}}</td>
              <td>{{$row->color}}</td>
              <td>{{$row->price_distribution}}</td>
              <td>{{$row->price_public}}</td>
              {{-- <td>

            </td> --}}
          </tr>
        @empty
        @endforelse
      </tbody>
    </table>
  </div>

    <div class="row text-center">
      {{ $rows->links() }}
    </div>
  </div>
  <!-- /bordered datatable inside panel -->

@endsection
