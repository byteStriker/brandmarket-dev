@extends('layouts.backoffice', ['title' => $page_title])
@section('slogan')
  Categorías > Agregar
@endsection
@section('content')
  @php
  $buttons = [];

  @endphp
  @component('backoffice.components.create',
  [
    'link_goback' => @route('category.index'),
    'link_create' => @route('category.store'),
    'buttons' => $buttons,
  ])

  @slot('row_properties')
    <div class="form-group">
      <div class="col-sm-4">
        <label>Nombre de la Categoría</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="">
      </div>
      <div class="col-sm-6">
        <label>Imagen de Portada</label>
        <input type="file" class="form-control" id="cover_image" name="cover_image" placeholder="">
      </div>

    </div>
  @endslot
@endcomponent

@endsection
