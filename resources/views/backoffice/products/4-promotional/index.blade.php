@extends('layouts.backoffice', ['title' => $page_title])
@section('content')

  <!-- Bordered datatable inside panel -->
  <div class="panel">
    <div class="panel-heading">
      <div class="columns">
        <div class="column">
          <h6 class="panel-title has-text-white	">Lista de Registros que Provienen del Web Service</h6>
        </div>
        <div class="column">
          <div class="is-pulled-right">
            {{-- @component('backoffice.components.link', ['linkText'=>'Agregar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_create??'#'])@endcomponent --}}
            @component('backoffice.components.link', ['linkText'=>'Iniciar Procesamiento', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>@route('four-promotional.complement-web-service')])@endcomponent
          </div>

        </div>
      </div>
    </div>
<div class="panel-block">
{{-- {{dd($rows['rows'])}} --}}
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            @foreach ($columns as $column)
              <th>{{$column}}</th>
            @endforeach
            {{-- <th>Acciones</th> --}}
          </tr>
        </thead>
        <tbody>
          @php
              $r = $rows['rows'];
              // dd($r[0]->id_article_formatted);
          @endphp
          @foreach ($r as $row)
            <tr>
              {{-- <th>{{dd($row->id_article_formatted)}}</th> --}}
              <th>{{$loop->iteration}}</th>
              {{-- 'id_article_formatted', 'category', 'subcategory', 'description', 'color', 'article_name', 'price' --}}
              <td>{{$row->id_article_formatted}}</td>
              <td>{{$row->category}}</td>
              <td>{{$row->sub_category}}</td>
              <td>{{$row->description}}</td>
              <td>{{$row->color}}</td>
              <td>{{$row->name}}</td>
              <td>{{$row->model_color}}</td>
              {{-- <td></td> --}}
            </tr>
          {{-- @empty --}}
          @endforeach
        </tbody>
      </table>
    </div>

    {{-- <div class="row text-center">
      {!! $rows->appends(request()->all())->links() !!}
    </div> --}}
  </div>
  <!-- /bordered datatable inside panel -->
@endsection
