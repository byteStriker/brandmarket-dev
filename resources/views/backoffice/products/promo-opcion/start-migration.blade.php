@extends('layouts.backoffice', ['title' => $page_title])
@section('slogan')
  Agregar un Archivo de Base de Datos para Promo Opción
@endsection
@section('content')

  <form action="{{ @route('promo-opcion.import-migration') }}" role="form" method="POST" name="startMigrationForm" id="startMigrationForm" class="form-horizontal validate">
      @csrf
      @method('POST')
      <div class="form-group">
        {{-- <fieldset>
          <ul>
            <li>Bebidas
              <ul>
                <li>Cilindros plástico (cilindros plásticos)</li>
                <li>Cilindros Metálicos (Cilindros metálicos)</li>
                <li>Tazas (tazas)</li>
                <li>Termos (termo de plástico, termo de metal)</li>
              </ul>
            </li>
            <li>
              Bolígrafos
              <ul>
                <li>Plástico (Bolígrafos plástico)</li>
                <li>Metal (Bolígrafos metálicos)</li>
                <li>Multifuncionales (Bolígrafos funciones)</li>
              </ul>
            </li>

            <li>Oficina
              <ul>
                <li>Agendas y carpetas (carpetas)</li>
                <li>Libretas (Libretas)</li>
                <li>Llaveros (Llaveros funcionales, llaveros metálicos)</li>
                <li>Oficina (artículos de oficina)</li>
                <li>Portarretratos (portarretratos)</li>
                <li>Relojes (relojes)</li>
                <li>Anti-stress (anti-stress)</li>
              </ul>
            </li>

            <li>Textiles
              <ul>
                <li>Bolsas y morrales (Bolsas)</li>
                <li>Chamarras, chalecos y sudaderas (chamarras y chalecos)</li>
                <li>Gorras (Gorras y sombreros) </li>
                <li>Playeras y tipo Polo (playeras) </li>
                <li>Maletas (maletas)</li>
                <li>Mochilas (mochilas)</li>
                <li>Portafolios (portafolios)</li>
              </ul>
            </li>
            <li>Tecnología
              <ul>
                <li>Accesorios de computo (Acc computo)</li>
                <li>Accesorios Smartphone y tablets (acc smartph y tablet)</li>
                <li>Audio (Audio)</li>
                <li>USB´s  (usb)</li>
              </ul>
            </li>
          </ul>

        </fieldset> --}}


        <div class="col-sm-4">
          <label for="bebidas"></label>
          <select class="form-control input-lg" id="bebidas" name="bebidas">          {{-- <option value="-1">Bebidas</option> --}}
            <option value="-1">Bebidas</option>
            @if ($categoryBebidas)
              @forelse ($categoryBebidas as $category)
                <optgroup value="{{$category->id}}" data-slug-category="{{$category->slug}}" label="{{$category->name}}">
                  @forelse ($category->Subcategories as $key)
                    <option value="{{$key->id}}" data-slug-category="{{$category->slug}}"  data-slug-sub-category="{{$key->slug}}" label="{{$key->name}}">{{$key->name}}</option>
                  @empty

                  @endforelse
              @empty
                <option value="-1">No hay categorías</option>
              @endforelse
            @endif
          </select>
        </div>

        <div class="col-sm-4">
          <label for="boligrafos"></label>

          <select class="form-control input-lg" id="boligrafos" name="boligrafos">          {{-- <option value="-1">Bebidas</option> --}}
            <option value="-1">Bolígrafos</option>
            @if ($categoryBoligrafos)
              @forelse ($categoryBoligrafos as $category)
                <optgroup value="{{$category->id}}" data-slug-category="{{$category->slug}}" label="{{$category->name}}">
                  @forelse ($category->Subcategories as $key)
                    <option value="{{$key->id}}" data-slug-category="{{$category->slug}}" data-slug-sub-category="{{$key->slug}}" label="{{$key->name}}">{{$key->name}}</option>
                  @empty

                  @endforelse
              @empty
                <option value="-1">No hay categorías</option>
              @endforelse
            @endif
          </select>
        </div>
        <div class="col-sm-4">
          <label for="oficina"></label>
          <select class="form-control input-lg" id="oficina" name="oficina">
            <option value="-1">Oficina</option>
            @if ($categoryOficina)
              @forelse ($categoryOficina as $category)
                <optgroup value="{{$category->id}}" data-slug-category="{{$category->slug}}" label="{{$category->name}}">
                  @forelse ($category->Subcategories as $key)
                    <option value="{{$key->id}}" data-slug-category="{{$category->slug}}" data-slug-sub-category="{{$key->slug}}" label="{{$key->name}}">{{$key->name}}</option>
                  @empty

                  @endforelse
              @empty
                <option value="-1">No hay categorías</option>
              @endforelse
            @endif
          </select>
        </div>

        <div class="col-sm-4">
          <label for="textiles"></label>
          <select class="form-control input-lg" id="textiles" name="textiles">
            <option value="-1">Textiles</option>
            @if ($categoryTextiles)
              @forelse ($categoryTextiles as $category)
                <optgroup value="{{$category->id}}" data-slug-category="{{$category->slug}}" label="{{$category->name}}">
                  @forelse ($category->Subcategories as $key)
                    <option value="{{$key->id}}" data-slug-category="{{$category->slug}}" data-slug-sub-category="{{$key->slug}}" label="{{$key->name}}">{{$key->name}}</option>
                  @empty

                  @endforelse
              @empty
                <option value="-1">No hay categorías</option>
              @endforelse
            @endif
          </select>
        </div>

        <div class="col-sm-4">
          <label for="tecnologia"></label>
          <select class="form-control input-lg" id="tecnologia" name="tecnologia">
            <option value="-1">Tecnología</option>
            @if ($categoryTecnologia)
              @forelse ($categoryTecnologia as $category)
                <optgroup value="{{$category->id}}" data-slug-category="{{$category->slug}}" label="{{$category->name}}">
                  @forelse ($category->Subcategories as $key)
                    <option value="{{$key->id}}" data-slug-category="{{$category->slug}}" data-slug-sub-category="{{$key->slug}}" label="{{$key->name}}">{{$key->name}}</option>
                  @empty

                  @endforelse
              @empty
                <option value="-1">No hay categorías</option>
              @endforelse
            @endif
          </select>
        </div>
      </div>

      <footer class="row text-center">
          @if (isset($buttons))
            @if (is_array($buttons))
              <div class="btn-group dropup">
                  <button class="btn btn-lg btn-info dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Guardar y... <span class="icon caret"></span></button>
                  <ul class="dropdown-menu icons-right">
                    <li>
                      <a href="#" onClick="window.createForm.submit()" class="btn-lg btn-link">Finalizar <span class="icon save"></span></a>
                    </li>
                    @forelse ($buttons as $button)
                      <li>
                        <a href="#{{$button->name}}"  id="{{$button->id}}" class="btn-lg btn-link">{{$button->value}} <span class="icon disk"></span></a>
                      </li>
                    @empty
                      {{-- <button type="submit" name="button" class="btn link"><i class="icon-disk"></i> Guardar</button> --}}
                    @endforelse
                  </ul>
                </div>
              @else
                <a href="{{$extra_link->href}}" class="btn-lg btn-link">{!!$extra_link->i_class!!} {{$extra_link->value}}</a>
            @endif
          @endif

          <input type="hidden" name="categorySlug" value="" id="categorySlug" >
          <input type="hidden" name="subCategorySlug" value="" id="subCategorySlug" >
      </footer>
  </form>
  <script type="text/javascript">
    $(document).ready(function(){
      $('select').on('change', function(){
        console.log($(this).find(':selected').data());
          $('#categorySlug').attr('value', $(this).find(':selected').data('slugCategory'))
          $('#subCategorySlug').attr('value', $(this).find(':selected').data('slugSubCategory'))

        $('form#startMigrationForm').submit();
      });
    });
  </script>
@endsection
