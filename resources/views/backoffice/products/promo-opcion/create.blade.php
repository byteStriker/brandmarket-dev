@extends('layouts.backoffice', ['title' => $page_title])

@section('slogan')
  Agregar un Archivo de Base de Datos para Promo Opción
@endsection

@section('content')
  @php
  $buttons = [];
  @endphp
  
  @component('backoffice.components.create',
  [
    'link_goback' => @route('promo-opcion.read-products'),
    'link_create' => @route('promo-opcion.store-excel-file'),
    'buttons' => $buttons,
  ])

  @slot('row_properties')
  <div class="columns">
    @component('backoffice.components.input',['label' => 'Archivo de Base de Datos', 'type' => 'file', 'name' => 'dbFile', 'id'=>'dbFile', 'columnRatio' => 'is-12'])@endcomponent
  </div>
  @endslot

@endcomponent

@endsection