@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  <form action="{{ @route('promo-opcion.prepare-imports') }}" role="form" method="POST" name="selectedRowsImportsForm" id="selectedRowsImportsForm" class="form-horizontal validate">
    @csrf
    @method('POST')
    <input type="text" name="categorySlug" id="categorySlug" value="{{request()->input('categorySlug')}}">
    <input type="text" name="subCategorySlug" id="subCategorySlug" value="{{request()->input('subCategorySlug')}}">
  <!-- Bordered datatable inside panel -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h6 class="panel-title"><i class="icon-file"></i> Lista de Registros</h6>
      <div class="pull-right">
        {{-- <a href="{{@route('promo-opcion.upload-complement-view')}}" class="btn btn-sm btn-success"><i class="icon-plus"></i> Importar CSV</a> --}}
        {{-- <a href="{{@route('promo-opcion.start-migration')}}" class="btn btn-sm btn-success"><i class="icon-plus"></i> Importar Registros</a> --}}
        <button type="submit" name="button" class="btn btn-sm btn-success">Complementar Registros</button>
      </div>
    </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            @foreach ($columns as $column)
              <th>{{$column}}</th>
            @endforeach
            <th>Importar</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($rows as $row)
            <tr>
              <th>{{$loop->iteration}}</th>
              {{-- 'Elementos', 'Padre', 'Familia', 'Nombre', 'Descripcion' --}}
              <td>{{$row->page}}</td>
              <td>{{$row->code}}</td>
              <td>{{$row->description}}</td>
              <td>{{$row->price}}</td>
              <td>
                {{-- <input type="hidden" name="id[]" id="id-{{$row->id}}" value="{{$row->id}}"> --}}
                <input type="checkbox" name="selectedRows[]"  id="selected-rows-promo-opcion-{{$row->id}}" value="{{$row->id}}" checked="checked">
              </td>
            </tr>
          @empty
          @endforelse
        </tbody>
      </table>

  </div>
  <div class="row text-center">
    {{ $rows->links() }}
  </div>
  <!-- /bordered datatable inside panel -->
</form>

@endsection
