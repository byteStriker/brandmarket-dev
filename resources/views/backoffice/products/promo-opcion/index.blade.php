@extends('layouts.backoffice', ['title' => $page_title])
@section('content')

  <!-- Bordered datatable inside panel -->
  <div class="panel">
    <div class="panel-heading">
      <div class="columns">
        <div class="column">
          <h6 class="panel-title has-text-white	">Lista de Registros</h6>
        </div>
        <div class="column">
          <div class="is-pulled-right">
            @component('backoffice.components.link', ['linkText'=>'Importar CSV', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>@route('promo-opcion.upload-complement-view')])@endcomponent
            {{-- @component('backoffice.components.link', ['linkText'=>'Iniciar Migración', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>@route('promo-opcion.start-migration')])@endcomponent --}}
          </div>
        </div>
      </div>
    </div>
    <div class="panel-block">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            @foreach ($columns as $column)
              <th>{{$column}}</th>
            @endforeach
          </tr>
        </thead>
        <tbody>
          @forelse ($rows as $row)
            <tr>
              <th>{{$loop->iteration}}</th>
              <td>{{$row->page}}</td>
              <td>{{$row->code}}</td>
              <td>{{$row->description}}</td>
              <td>{{$row->price}}</td>
            </tr>
          @empty
          @endforelse
        </tbody>
      </table>
    </div>
    <div class="row text-center">
      {{ $rows->links() }}
    </div>
  </div>
  <!-- /bordered datatable inside panel -->
@endsection
