@extends('layouts.backoffice', ['title' => $page_title])

@section('content')
  @component('backoffice.components.create',
  [
    'link_goback' => @route('user.index'),
    'link_create' => @route('user.store'),
    'saveText' => 'Guardar y ...',
    'columnsMultiLineClass'=>'columns is-multiline',

  ])
  @slot('linkSaveOther')
    @component('backoffice.components.link',
    ['linkText'=>'Agregar Otro', 'id'=>'btnSaveAddOther',
    'routeLink'=>'#',
    'class'=>'dropdown-item has-text-light',
    ])
    @endcomponent
  @endslot
  @slot('create_script')
    <script type="text/javascript" src="{{asset('/backoffice-assets/js/webapp/forms/user.ui.controller.js')}}"></script>
  @endslot
  @slot('row_properties')
    @component('backoffice.components.input',['label' => __('Name'), 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name'])
    @endcomponent
    @component('backoffice.components.input',['label' => __('E-Mail Address'), 'type'=>'email', 'placeholder' => 'Email', 'name' => 'email', 'id'=>'email'])
    @endcomponent
    @component('backoffice.components.input',['label' => __('Password'), 'type'=>'password', 'placeholder' => 'Email', 'name' => 'password', 'id'=>'password'])
    @endcomponent
    @component('backoffice.components.input',['label' => __('Confirm Password'), 'type'=>'password', 'placeholder' => 'password', 'name' => 'password_confirmation', 'id'=>'password_confirmation'])
    @endcomponent
  @endslot
@endcomponent

@endsection
