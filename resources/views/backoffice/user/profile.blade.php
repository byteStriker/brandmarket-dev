
@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')

  <div class="container">
    <h2 class="title is-5 has-text-primary">¡Hola {{strtoupper(auth()->user()->name)}}!</h2>
    <form method="POST" action="{{ route('user.update-profile') }}" role="form" class="form-horizontal validate">
      @csrf
      @method('PUT')
      <div class="columns">
        <div class="column">
          <div class="field">
            <label>{{ __('Name') }}</label>
            <div class="control">

              <input id="name" type="text" class="input @error('name') is-invalid @enderror" name="name" value="{{ $row->name }}" required autocomplete="name" autofocus>
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
                {{-- <input id="name" type="text" class="input " name="name" value="admin prueba actualizar 1" required="" autocomplete="name" autofocus=""> --}}
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
              <label>{{ __('E-Mail Address') }}</label>

              <div class="control">
                <input id="email" type="email" class="input " name="email" value="admin@example.com" required="" autocomplete="email" autofocus="">
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
              <label>Contraseña</label>
              <div class="control">
                <input id="password" type="password" class="input @error('password') is-invalid @enderror" name="password" value="{{$row->password}}" required autocomplete="current-password">
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label>{{ __('Confirm Password') }}</label>

                <div class="control">
                  <input id="password-confirm" type="password" class="input" name="password_confirmation" autocomplete="new-password">

                  @error('password')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
            </div>
          </div>
          <footer class="row text-center">
          <button type="submit" class="button is-primary"><i class="icon-menu2"></i>
          {{ __('Update') }}
          </button>
          </footer>

          {{--
          <div class="popup-header">
          <a href="#" class="pull-left"><i class="icon-user-plus"></i></a>
          <span class="text-semibold">Perfil del Usuario</span>
        </div>
        <div class="well">
        <div class="form-group">
        <div class="col-sm-6">
        <label>{{ __('Name') }}</label>
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $row->name }}" required autocomplete="name" autofocus>
        @error('name')
        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>
  <div class="col-sm-6">
  <label>{{ __('E-Mail Address') }}</label>
  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $row->email }}" required autocomplete="email" autofocus>
  @error('email')
  <span class="invalid-feedback" role="alert">
  <strong>{{ $message }}</strong>
</span>
@enderror
</div>

</div>

<div class="form-group">
<div class="col-sm-6">
<label>{{ __('Password') }}</label>
<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{$row->password}}" required autocomplete="current-password">

@error('password')
<span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
@enderror
</div>

<div class="col-sm-6">
<label>{{ __('Confirm Password') }}</label>
<input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">

@error('password')
<span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
@enderror

</div>
</div>

<footer class="row text-center">
<button type="submit" class="btn btn-success"><i class="icon-menu2"></i>
{{ __('Update') }}
</button>
</footer>

</div>
</form>--}}
</div>
</section>

@endsection
