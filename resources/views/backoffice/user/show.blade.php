@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @component('backoffice.components.show',
    [
      'link_goback' => @route('user.index'),
      'link_edit' => @route('user.edit', $row),
      'link_delete' => @route('user.confirm-delete', $row),
      'columnsMultiLineClass'=>'columns is-multiline',

    ])
    @slot('row_properties')
      @component('backoffice.components.input',['columnRatio'=>'is-half','label' => __('Name'), 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name, 'readonly'=>'readonly'])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half','label' => __('E-Mail Address'), 'type'=>'email', 'placeholder' => 'Email', 'name' => 'email', 'id'=>'email', 'value'=>$row->email, 'readonly'=>'readonly'])
      @endcomponent
    @endslot
  @endcomponent
@endsection
