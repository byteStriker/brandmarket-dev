@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @component('backoffice.components.edit',
    [
      'link_goback' => @route('user.index'),
      'link_update' => @route('user.update', $row),
      'link_delete' => @route('user.confirm-delete', $row),
      'id' => $row->id,
      'columnsMultiLineClass'=>'columns is-multiline',
    ])
    @slot('row_properties')
      @component('backoffice.components.input',['displayElement'=>'none', 'label'=> __('Name'), 'placeholder' => 'Nombre', 'name' => 'id', 'id'=>'id', 'value'=>$row->id])
      @endcomponent
      @component('backoffice.components.input',['label' => __('Name'), 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name])
      @endcomponent
      @component('backoffice.components.input',['label' => __('E-Mail Address'), 'type'=>'email', 'placeholder' => 'Email', 'name' => 'email', 'id'=>'email', 'value'=>$row->email])
      @endcomponent
      @component('backoffice.components.input',['label' => __('Password'), 'type'=>'password', 'placeholder' => 'Email', 'name' => 'password', 'id'=>'password', 'value'=>$row->password])
      @endcomponent
      @component('backoffice.components.input',['label' => __('Confirm Password'), 'type'=>'password', 'placeholder' => 'password', 'name' => 'password_confirmation', 'id'=>'password_confirmation'])
      @endcomponent
    @endslot
  @endcomponent
@endsection
