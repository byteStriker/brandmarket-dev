@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @component('backoffice.components.delete',
    [
      'link_goback' => @route('user.index'),
      'link_edit' => @route('user.edit', $row),
      'link_delete' => @route('user.destroy',$row),
      'columnsMultiLineClass'=>'columns is-multiline',
    ])
    @slot('row_properties')
      @component('backoffice.components.input',['columnRatio'=>'is-half','label' => __('Name'), 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name])
      @endcomponent
    @endslot
  @endcomponent
@endsection
