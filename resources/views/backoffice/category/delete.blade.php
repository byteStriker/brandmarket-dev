@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')

  @component('backoffice.components.delete',
    [
      'link_goback' => @route('category.index'),
      'link_edit' => @route('category.edit', [$row]),
      'link_delete' => @route('category.destroy',[$row])

    ])

    @slot('row_properties')
      @component('backoffice.components.input',['label' => 'Nombre', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name, 'readonly'=>'readonly'])@endcomponent
        @if ($row->cover_image)
          <div class="column">
                <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
          </div>
        @endif

    @endslot
  @endcomponent
@endsection
