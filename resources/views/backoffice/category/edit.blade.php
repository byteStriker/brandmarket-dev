@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')

  @component('backoffice.components.edit',
    [
      'link_goback' => @route('category.index'),
      'link_update' => @route('category.update', $row),
      'link_delete' => @route('category.confirm-delete', $row),
      'id' => $row->id
    ])
    @slot('row_properties')
      <input type="hidden" name="id" value="{{$row->id}}">
      @component('backoffice.components.input',['label' => 'Nombre', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name])@endcomponent
      @component('backoffice.components.input',['label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image', 'value'=>$row->cover_image])@endcomponent
      @if ($row->cover_image)
        <div class="column">
              <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
        </div>
      @endif
    @endslot
  @endcomponent

@endsection
