@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
{{-- @route('category.confirm-delete', $row) --}}
@php
  $link_extra = new stdclass();
  $link_extra->href = @route('subcategory.index', [$row]);
  $link_extra->icon = 'table2';
  $link_extra->text = 'Subcategorías';
@endphp
  @component('backoffice.components.show',
    [
      'link_goback' => @route('category.index'),
      'link_edit' => @route('category.edit', $row),
      'link_delete' => @route('category.confirm-delete', $row),
      'link_extra' => $link_extra
    ])
    @slot('row_properties')

      @component('backoffice.components.input',['label' => 'Nombre', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name, 'readonly'=>'readonly'])@endcomponent
      {{-- @component('backoffice.components.input',['label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image', 'value'=>$row->cover_image, 'readonly'=>'readonly'])@endcomponent --}}
      @if ($row->cover_image)
        <div class="column">
              <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
        </div>
      @endif
    @endslot
  @endcomponent
@endsection
