@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  @component('backoffice.components.table', ['columns'=> $columns, 'link_create'=>route('category.create')])
    @slot('rows')
      @forelse ($rows as $row)
        <tr>
          <th>{{$loop->iteration}}</th>
          <td>{{$row->name}} ({{$row->subcategories->count()}})</td>
          <td>
            @component('backoffice.components.action-buttons-table')
              @slot('linkDetails')
                @component('backoffice.components.link', ['linkText'=>'Detalles','class'=>'dropdown-item has-text-light', 'routeLink'=>@route('category.show',[$row])])@endcomponent
              @endslot
              @slot('linkEdit')
                @component('backoffice.components.link', ['linkText'=>'Editar', 'class'=>'dropdown-item has-text-light','routeLink'=>@route('category.edit',[$row])])@endcomponent
              @endslot
              @slot('linkRemove')
                @component('backoffice.components.link', ['linkText'=>'Borrar', 'class'=>'dropdown-item has-text-light','routeLink'=>@route('category.confirm-delete',[$row])])@endcomponent
              @endslot
              @slot('linkExtra')
                @component('backoffice.components.link', ['linkText'=>'Subcategorías', 'class'=>'dropdown-item has-text-light','routeLink'=>@route('subcategory.index',[$row]) ])@endcomponent
              @endslot
            @endcomponent
          </td>
        </tr>
      @empty
      @endforelse
    @endslot
    @slot('pagination_links')
      {{$rows->links()}}
    @endslot
  @endcomponent
@endsection
