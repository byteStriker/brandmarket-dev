@extends('layouts.backoffice', ['title' => $page_title])

@section('content')

  @component('backoffice.components.create',
  [
    'link_goback' => @route('category.index'),
    'link_create' => @route('category.store'),
    // 'buttons' => $buttons,
    'saveText' => 'Guardar y ...',
    'columnsMultiLineClass'=>'columns is-multiline',
  ])
  @slot('linkSaveOther')
    @component('backoffice.components.link',
    ['linkText'=>'Agregar Otro', 'id'=>'btnSaveAddOther',
    'routeLink'=>'#',
    'class'=>'dropdown-item has-text-light',
    ])
    @endcomponent
  @endslot
  @slot('linkSaveAddSubcategory')
    @component('backoffice.components.link',
    ['linkText'=>'Agregar una Subcategoría', 'id'=>'btnSaveAddSubCategory',
     'routeLink'=>'#',
    'class'=>'dropdown-item has-text-light',
    ])
    @endcomponent
  @endslot
  @slot('create_script')
    <script type="text/javascript" src="{{asset('/backoffice-assets/js/webapp/forms/category.ui.controller.js')}}"></script>
  @endslot
  @slot('row_properties')
    @component('backoffice.components.input',['label' => 'Nombre', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name'])@endcomponent
    @component('backoffice.components.input',['label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image'])@endcomponent
  @endslot
@endcomponent
@endsection
