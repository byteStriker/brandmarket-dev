@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  <!-- Datepicker -->
  <div class="panel panel-default">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      <br />
    @endif
    <div class="panel-heading">
      <div class="columns">
        <div class="column">
          <h6 class="panel-title has-text-white	">Lista de Registros</h6>
        </div>
        <div class="column">
          <div class="is-pulled-right">
            @component('backoffice.components.link',
            ['linkText'=>'Agregar', 'class'=>'button is-primary',
            'linkTextClass'=>'has-text-white', 'routeLink'=>$link_create??'#'])
            @endcomponent
          </div>
        </div>
      </div>
    </div>

    <div class="panel-block">
      <div class="container">
        <form action="{{@route('finance-settings.update')}}" role="form" method="POST" name="financeSettingsForm" id="financeSettingsForm" class="form-horizontal validate">
          @csrf
          @method('PUT')
          <div class="columns is-multline">
            @component('backoffice.components.input',['name' => 'id', 'id'=>'id', 'displayElement'=>'none', 'value'=>strtoupper($financeSettings->id)])
            @endcomponent
            @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Porcentaje de Ganancia General', 'placeholder' => '0.00', 'name' => 'general_gain_percentaje', 'id'=>'general_gain_percentaje', 'value'=>$financeSettings->general_gain_percentaje])
            @endcomponent
            @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Porcentaje de Ganancia Individual', 'placeholder' => '0.00', 'name' => 'individual_gain_percentaje', 'id'=>'individual_gain_percentaje', 'value'=>$financeSettings->individual_gain_percentaje])
            @endcomponent

          </div>
          {{-- <input type="hidden" name="id" value="{{$financeSettings->id}}" id="id">
          <div class="form-group">
            <div class="col-sm-5">
              <label>Porcentaje de Ganancia General</label>
              <input type="text" class="form-control" name="general_gain_percentaje" id="general_gain_percentaje" placeholder="" value="{{$financeSettings->general_gain_percentaje}}">
            </div>
            <div class="col-sm-5">
              <label>Porcentaje de Ganancia Individual</label>
              <input type="text" class="form-control" name="individual_gain_percentaje" id="individual_gain_percentaje" placeholder="" value="{{$financeSettings->individual_gain_percentaje}}">
            </div>
          </div>
          <footer class="row text-center">
            <div class="btn-group dropup">
            </div>
          </footer> --}}

          <footer class="has-text-centered">
            <div class="field is-grouped is-grouped-right">
              <div class="container">
                <div class="field">
                  <a href="#" onClick="window.financeSettingsForm.submit()" class="button is-primary">Actualizar <span class="icon save"></span></a>
                </div>
              </div>
            </div>
          </footer>
        </form>
      </div>

    </div>
  </div>
@endsection
