@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @component('backoffice.components.delete',
    [
      'link_goback' => @route('slide-details.index',[$row->Slide()->first()]),
      'link_edit' => @route('slide-details.edit', [$row->Slide()->first(), $row]),
      'link_delete' => @route('slide-details.destroy',$row),
      'columnsMultiLineClass'=>'columns is-multiline'
    ])
    @slot('row_properties')
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre de la Galería', 'placeholder' => 'Nombre', 'name' => 'name_', 'id'=>'name_', 'value'=>$row->slide->name])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre para la Fotografía', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name,'readonly'=>'readonly'])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Texto de la Liga', 'placeholder' => 'Texto de la liga', 'name' => 'link_text', 'id'=>'link_text', 'value'=>$row->link_text,'readonly'=>'readonly'])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Tipo de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_type', 'id'=>'link_type', 'value'=>$row->link_type,'readonly'=>'readonly'])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'URL de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_url', 'id'=>'link_url', 'value'=>$row->link_url,'readonly'=>'readonly'])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Tipo de Carátula', 'type'=>'text', 'name' => 'cover_type', 'id'=>'cover_type', 'value'=>$row->cover_type,'readonly'=>'readonly'])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half','label' => 'Fecha de Publicación', 'placeholder' => Date('Y-m-d'), 'name' => 'published_at', 'id'=>'published_at', 'readonly'=>'readonly', 'value'=>$row->published_at])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half','label' => 'Estatus', 'placeholder' => Date('Y-m-d'), 'name' => 'status', 'id'=>'status', 'readonly'=>'readonly', 'value'=>$row->status])
      @endcomponent
      @if ($row->cover_image)
        <div class="column">
              <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
        </div>
      @endif
    @endslot
  @endcomponent
@endsection
