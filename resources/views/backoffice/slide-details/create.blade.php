@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
@component('backoffice.components.create',
[
'link_goback' => @route('slide-details.index', ['slide' => $slide_id]),
'link_create' => @route('slide-details.store',['slide' => $slide_id]),
'saveText' => 'Guardar y ...',
'columnsMultiLineClass'=>'columns is-multiline',
])
@slot('linkSaveOther')
@component('backoffice.components.link',
['linkText'=>'Agregar Otro', 'id'=>'btnSaveAddSlideDetail',
'routeLink'=>'#',
'class'=>'dropdown-item has-text-light',
])
@endcomponent
@endslot
{{-- @slot('create_script')
<script type="text/javascript" src="{{@asset('/backoffice-assets/js/webapp/forms/slide-details.ui.controller.js')}}"></script>
@endslot --}}
@slot('row_properties')
@component('backoffice.components.input',['name' => 'slide_id', 'id'=>'slide_id', 'displayElement'=>'none', 'value'=>strtoupper($slide->id)])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Galería', 'placeholder' => 'Nombre', 'name' => 'gallery_name', 'id'=>'gallery_name', 'readonly'=>'readonly', 'value'=>strtoupper($slide->name)])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre para la Foto', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>request()->old('name')])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Texto de la Liga', 'placeholder' => 'Texto de la liga', 'name' => 'link_text', 'id'=>'link_text', 'value'=>request()->old('link_text')])
@endcomponent

<div class="column is-half">
  <div class="card">
      <label class="label">Color del Texto</label>
      <div class="control">
      <label class="radio" style="background-color: black">
        <input type="radio" name="text_color" value="has-text-white">
        <label class="has-text-white">Blanco</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-black">
        <label class="has-text-black">Negro</label>
      </label>
      <label class="radio" style="background-color: #ddd">
        <input type="radio" name="text_color" value="has-text-light">
        <label class="has-text-light">Ligero</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-primary">
        <label class="has-text-primary">Primario</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-info">
        <label class="has-text-info">Azul</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-warning">
        <label class="has-text-warning">Amarillo</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-danger">
        <label class="has-text-danger">Rojo</label>
      </label>
    </div>
  </div>
</div>
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Tipo de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_type', 'id'=>'link_type', 'value'=>request()->old('link_type')])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'URL de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_url', 'id'=>'link_url', 'value'=>request()->old('link_url')])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image', 'value'=>request()->old('cover_image')])
@endcomponent
<div class="column is-half">
  <div class="select">
    <label class="label">Tipo de Carátula</label>
    <select id="cover_type" name="cover_type">
      <option value="-1">Seleccione</option>
      <option value="Desktop">Escritorio</option>
      <option value="Mobile">Movil</option>
    </select>
  </div>
</div>
@component('backoffice.components.input',['label' => 'Fecha de Publicación', 'placeholder' => Date('Y-m-d'), 'name' => 'published_at', 'id'=>'published_at', 'value'=>request()->old('published_at')])
@endcomponent
<div class="column is-half">
  <div class="select">
    <label class="label">Estatus</label>
    <select id="status" name="status">
      <option value="Published" selected="selected">Publicado</option>
      <option value="Future">Future</option>
      <option value="Draft">Borrador</option>
    </select>
  </div>
</div>

@endslot
@endcomponent

@endsection
