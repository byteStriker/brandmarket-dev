@extends('layouts.backoffice', ['title' => $page_title])
@section('content')

  @component('backoffice.components.table', ['columns'=> $columns, 'link_create'=>@route('slide-details.create',['slide'=>$slide_id, ])])
    {{-- {{dd($rows[0]->id)}} --}}
    @slot('rows')
      @forelse ($rows->all() as $row)
        <tr>
          <th>{{$loop->iteration}}</th>
          <td>{{$row->name}}</td>
          <td>
            {{-- {{dd($row)}} --}}
            {{-- {{dd($row->slide)}} --}}
            @component('backoffice.components.action-buttons-table')
              @slot('linkDetails')
                @component('backoffice.components.link', ['linkText'=>'Detalles', 'class'=>'dropdown-item has-text-light','routeLink'=>@route('slide-details.show',[$slide_id, $row])])@endcomponent
              @endslot
              @slot('linkEdit')
                @component('backoffice.components.link', ['linkText'=>'Editar', 'class'=>'dropdown-item has-text-light','routeLink'=>@route('slide-details.edit',[$slide_id, $row])])@endcomponent
              @endslot
              @slot('linkRemove')
                @component('backoffice.components.link', ['linkText'=>'Borrar', 'class'=>'dropdown-item has-text-light','routeLink'=>@route('slide-details.confirm-delete',$row)])@endcomponent
              @endslot
            @endcomponent

          </td>
        </tr>
      @empty
      @endforelse
    @endslot
    @slot('pagination_links')
      {{$rows->links()}}
    @endslot
  @endcomponent
@endsection
