@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
@component('backoffice.components.edit',
[
'link_goback' => @route('slide-details.index',[$slide]),
'link_update' => @route('slide-details.update', [$slide]),
'link_delete' => @route('slide-details.confirm-delete', $row),
'id' => $row->id,
'columnsMultiLineClass'=>'columns is-multiline'
])
@slot('row_properties')

{{-- <input type="hidden" name="id" id="id" value="{{$row->id}}"> --}}
{{-- <input type="hidden" name="slide_id" id="slide_id" value="{{$row->slide_id}}"> --}}
@component('backoffice.components.input',['name' => 'id', 'id'=>'id', 'displayElement'=>'none', 'value'=>strtoupper($row->id)])
@endcomponent
@component('backoffice.components.input',['name' => 'slide_id', 'id'=>'slide_id', 'displayElement'=>'none', 'value'=>strtoupper($slide->id)])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre para la Galería', 'placeholder' => 'Nombre', 'name' => 'name_', 'id'=>'name_', 'value'=>$slide->name, 'readonly'=>'readonly'])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre para la Foto', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Texto de la Liga', 'placeholder' => 'Texto de la liga', 'name' => 'link_text', 'id'=>'link_text', 'value'=>$row->link_text])
@endcomponent
<div class="column is-half">
  <div class="card">
    <label class="label">Color del Texto</label>
    <div class="control">
        <label class="radio" style="background-color: black">
        <input type="radio" name="text_color" value="has-text-white" @if ($row->text_color == 'has-text-white') checked @endif>
        <label class="has-text-white">Blanco</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-black" @if ($row->text_color == 'has-text-black') checked @endif>
        <label class="has-text-black">Negro</label>
      </label>
      <label class="radio" style="background-color: #ddd">
        <input type="radio" name="text_color" value="has-text-light" @if ($row->text_color == 'has-text-light') checked @endif>
        <label class="has-text-light">Ligero</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-primary" @if ($row->text_color == 'has-text-primary') checked @endif>
        <label class="has-text-primary" checked="checked">Primario</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-info" @if ($row->text_color == 'has-text-info') checked @endif>
        <label class="has-text-info">Azul</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-warning" @if ($row->text_color == 'has-text-warning') checked @endif>
        <label class="has-text-warning">Amarillo</label>
      </label>
      <label class="radio">
        <input type="radio" name="text_color" value="has-text-danger"  @if ($row->text_color == 'has-text-danger') checked @endif>
        <label class="has-text-danger">Rojo</label>
      </label>
    </div>
  </div>
</div>
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Tipo de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_type', 'id'=>'link_type', 'value'=>$row->link_type])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'URL de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_url', 'id'=>'link_url', 'value'=>$row->link_url])
@endcomponent
@component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image', 'value'=>$row->cover_image])
@endcomponent
<div class="column is-half">
  <div class="select">
    <label class="label">Tipo de Carátula</label>
    <select id="cover_type" name="cover_type">
      <option value="-1">Seleccione</option>
      <option value="Desktop" @if (strtolower($row->cover_type)=='desktop') selected="selected" @endif>Escritorio</option>
      <option value="Mobile" @if (strtolower($row->cover_type)=='mobile') selected="selected" @endif>Movil</option>
    </select>
  </div>
</div>
@component('backoffice.components.input',['label' => 'Fecha de Publicación', 'placeholder' => Date('Y-m-d'), 'name' => 'published_at', 'id'=>'published_at', 'value'=>$row->published_at])
@endcomponent
<div class="column is-half">
  <div class="select">
    <label class="label">Estatus</label>
    <select id="status" name="status">
      <option value="-1">Seleccione</option>
      <option value="Published" @if ($row->status == 'Published') selected="selected" @endif>Publicado</option>
      <option value="Future" @if ($row->status == 'Future') selected="selected" @endif>Future</option>
      <option value="Draft" @if ($row->status == 'Draft') selected="selected" @endif>Borrador</option>
    </select>
  </div>
</div>
@if ($row->cover_image)
<div class="columns">
  <div class="column is-12">
    <div class="column is-12">
      <div class="column is-12">
        <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
      </div>
    </div>
  </div>
</div>
@endif
@endslot
@endcomponent
@endsection
