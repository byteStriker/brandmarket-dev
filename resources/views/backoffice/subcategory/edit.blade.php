@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @component('backoffice.components.edit',
    [
      'link_goback' => @route('subcategory.index',$category),
      'link_update' => @route('subcategory.update', $category),
      'link_delete' => @route('subcategory.confirm-delete', $row),
      'id' => $row->id,
      'columnsMultiLineClass'=>'columns is-multiline',

    ])
    @slot('row_properties')
      @component('backoffice.components.input',['name' => 'id', 'id'=>'id', 'displayElement'=>'none', 'value'=>strtoupper($row->id)])
      @endcomponent

      @component('backoffice.components.input',['name' => 'category_id', 'id'=>'category_id', 'displayElement'=>'none', 'value'=>strtoupper($category->id)])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Categoría', 'placeholder' => 'Categoría Padre', 'name' => 'category_name', 'id'=>'category_name', 'readonly'=>'readonly', 'value'=>strtoupper($category->name)])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre de la Sub Categoría', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name])@endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image', 'value'=>$row->cover_image])@endcomponent

        @if ($row->cover_image)
          <div class="columns">
            <div class="column is-12">
            <div class="column is-12">
              <div class="column is-12">
                <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
              </div>
            </div>
            </div>
          </div>

        @endif
    @endslot
  @endcomponent
@endsection
