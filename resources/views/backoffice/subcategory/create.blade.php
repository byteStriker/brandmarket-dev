@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  @component('backoffice.components.create',
  [
    'link_goback' => @route('subcategory.index', $category),
    'link_create' => @route('subcategory.store', $category),
    'saveText' => 'Guardar y ...',
    'columnsMultiLineClass'=>'columns is-multiline',
  ])
  @slot('linkSaveOther')
    @component('backoffice.components.link',
    ['linkText'=>'Agregar Otro', 'id'=>'btnSaveAddOther',
    'routeLink'=>'#',
    'class'=>'dropdown-item has-text-light',
    ])
    @endcomponent
  @endslot
  @slot('create_script')
    <script type="text/javascript" src="{{@asset('/backoffice-assets/js/webapp/forms/subcategory.ui.controller.js')}}"></script>
  @endslot
  @slot('row_properties')
    @component('backoffice.components.input',['name' => 'category_id', 'id'=>'category_id', 'displayElement'=>'none', 'value'=>strtoupper($category->id)])
    @endcomponent
    @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Categoría', 'placeholder' => 'Categoría Padre', 'name' => 'category_name', 'id'=>'category_name', 'readonly'=>'readonly', 'value'=>strtoupper($category->name)])
    @endcomponent

    @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre de la Sub Categoría', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name'])@endcomponent
    @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image'])@endcomponent
  @endslot
@endcomponent

@endsection
