@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @component('backoffice.components.delete', [
    'link_goback' => @route('subcategory.index',$category),
    'link_edit' => @route('subcategory.edit', [$category, $row]),
    'link_delete' => @route('subcategory.destroy', $row),
    'columnsMultiLineClass'=>'columns is-multiline' ])
    @slot('row_properties')
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Categoría', 'placeholder' => 'Categoría Padre', 'name' => 'category_name', 'id'=>'category_name', 'readonly'=>'readonly', 'value'=>strtoupper($category->name)])
      @endcomponent
      @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre de la Sub Categoría', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name','readonly'=>'readonly', 'value'=>$row->name])
      @endcomponent
      @if ($row->cover_image)
        <div class="columns">
          <div class="column is-12">
            <div class="column is-12">
              <div class="column is-12">
                <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
              </div>
            </div>
          </div>
        </div>
      @endif
    @endslot
  @endcomponent
@endsection
