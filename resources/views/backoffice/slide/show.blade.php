@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  @php
  $link_extra = new stdclass();
  $link_extra->href = @route('slide-details.index', [$row]);
  $link_extra->icon = 'table2';
  $link_extra->text = 'Detalles del Slide';
@endphp
@component('backoffice.components.show',
[
  'link_goback' => @route('slide.index'),
  'link_edit' => @route('slide.edit', $row),
  'link_delete' => @route('slide.confirm-delete', $row),
  'link_extra' => $link_extra,
  'columnsMultiLineClass'=>'columns is-multiline'
])
@slot('row_properties')
  @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Nombre para la Galería', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name', 'value'=>$row->name,'readonly'=>'readonly'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Texto de la Liga', 'placeholder' => 'Texto de la liga', 'name' => 'link_text', 'id'=>'link_text', 'value'=>$row->link_text,'readonly'=>'readonly'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Tipo de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_type', 'id'=>'link_type', 'value'=>$row->link_type,'readonly'=>'readonly'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'URL de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_url', 'id'=>'link_url', 'value'=>$row->link_url,'readonly'=>'readonly'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Tipo de Carátula', 'type'=>'text', 'name' => 'cover_type', 'id'=>'cover_type', 'value'=>$row->cover_type,'readonly'=>'readonly'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Fecha de Publicación', 'placeholder' => Date('Y-m-d'), 'name' => 'published_at', 'id'=>'published_at', 'readonly'=>'readonly', 'value'=>$row->published_at])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half', 'label' => 'Estatus', 'placeholder' => Date('Y-m-d'), 'name' => 'status', 'id'=>'status', 'readonly'=>'readonly', 'value'=>$row->status])
  @endcomponent
  @if ($row->cover_image)
    <div class="columns">
      <div class="column is-12">
      <div class="column is-12">
        <div class="column is-12">
          <img src="{{asset("storage/{$row->cover_image}")}}" alt="{{$row->name}}">
        </div>
      </div>
      </div>
    </div>
  @endif
@endslot
@endcomponent
@endsection
