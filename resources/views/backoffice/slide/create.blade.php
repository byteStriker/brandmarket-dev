@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  @component('backoffice.components.create',
  [
    'link_goback' => @route('slide.index'),
    'link_create' => @route('slide.store'),
    'saveText' => 'Guardar y...',
    'columnsMultiLineClass'=>'columns is-multiline'
  ])
  @slot('linkSaveOther')
    @component('backoffice.components.link',
    ['linkText'=>'Agregar Otro', 'id'=>'btnSaveAddOther',
    'routeLink'=>'#',
    'class'=>'dropdown-item has-text-light',
  ])
@endcomponent
@endslot
@slot('linkSaveAddSubcategory')
  @component('backoffice.components.link',
  ['linkText'=>'Agregar Fotografías', 'id'=>'btnSaveAddSlideDetail',
  'routeLink'=>'#',
  'class'=>'dropdown-item has-text-light',
])
@endcomponent
@endslot
{{--
  @slot('create_script')
  <script type="text/javascript" src="{{@url('storage/backoffice-assets/forms/article.ui.controller.js')}}"></script>
@endslot
--}}
@slot('row_properties')
  @component('backoffice.components.input',['columnRatio'=>'is-half','label' => 'Nombre para la Galería', 'placeholder' => 'Nombre', 'name' => 'name', 'id'=>'name'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half','label' => 'Texto de la Liga', 'placeholder' => 'Texto de la liga', 'name' => 'link_text', 'id'=>'link_text'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half','label' => 'Tipo de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_type', 'id'=>'link_type'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half','label' => 'URL de la Liga', 'placeholder' => 'Tipo de la liga', 'name' => 'link_url', 'id'=>'link_url'])
  @endcomponent
  @component('backoffice.components.input',['columnRatio'=>'is-half','label' => 'Imagen de Portada', 'type'=>'file', 'name' => 'cover_image', 'id'=>'cover_image'])
  @endcomponent


  <div class="column is-half">
    <div class="select">
      <label class="label">Tipo de Carátula</label>
      <select id="cover_type" name="cover_type">
        <option value="-1">Seleccione</option>
        <option value="Desktop">Escritorio</option>
        <option value="Mobile">Movil</option>
      </select>
    </div>
  </div>

  @component('backoffice.components.input',['columnRatio'=>'is-4','label' => 'Fecha de Publicación', 'placeholder' => Date('Y-m-d'), 'name' => 'published_at', 'id'=>'published_at'])
  @endcomponent

  <div class="column is-half">
    <div class="select">
      <label class="label">Estatus</label>
      <select id="status" name="status">
        <option value="Published">Publicado</option>
        <option value="Future">Future</option>
        <option value="Draft">Borrador</option>
      </select>
    </div>
  </div>

@endslot
@endcomponent

@endsection
