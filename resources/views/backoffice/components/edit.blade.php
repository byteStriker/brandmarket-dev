<!-- EditForm -->
<div class="panel panel-default">
  @component('backoffice.components.notification', ['errors'=>$errors])@endcomponent
  <div class="panel-heading">
    <div class="columns">
      <div class="column">
        <h6 class="panel-title has-text-white	">Propiedades del Registro</h6>
      </div>
      <div class="column">
        <div class="is-pulled-right">
          @if (isset($link_goback))
            @component('backoffice.components.link', ['linkText'=>'Regresar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_goback??'#'])
            @endcomponent
          @endif
          @if (isset($link_delete))
            @component('backoffice.components.link', ['linkText'=>'Eliminar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_delete??'#'])
            @endcomponent
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="panel-block">
    <div class="container">
      <form action="{{$link_update}}" role="form" method="POST" enctype="multipart/form-data">
        @csrf
        {{ method_field('PUT') }}
        <div class="{{$columnsMultiLineClass??''}}">
          {{$row_properties??'default form'}}
        </div>
        <footer class="has-text-centered">
          <div class="field is-grouped is-grouped-right">
            <div class="container">
              <div class="field">
                <button type="submit" name="button" class="button is-primary"><i class="icon icon-disk"></i> Guardar</button>
              </div>
            </div>
          </div>
        </footer>
      </form>
    </div>
  </div>
</div>
<!-- /EditForm -->
