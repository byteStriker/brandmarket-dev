
  <form action="{{@route($search_route)}}"
      <div class="field has-addons has-addons-fullwidth">
        <div class="control">
          <input class="input" type="text" placeholder="Buscar un producto" name="search" id="search">
        </div>
        <div class="control">
            <button type="submit" class="button is-primary">
              Buscar Producto
            </button>
          </div>
        </div>
    </form>
