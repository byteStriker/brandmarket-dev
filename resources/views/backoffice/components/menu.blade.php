<ul class="vertical-menu">
  @if (auth()->user()->roles[0]->name == 'admin')
    <li @if (Route::currentRouteName() == 'admin.dashboard') class="active" @endif><a href="{{@route('admin.dashboard')}}"><span>Dashboard</span> <i class="icon icon-dashboard"></i></a></li>
      <li @if (in_array(Route::currentRouteName(), [
        'product.index', 'product.create', 'product.edit', 'product.show', 'product.confirm-delete',
        ])) class="active" @endif><a href="{{@route('product.index')}}"><span>Products</span> <i class="icon icon-grid"></i></a></li>
        <li @if (in_array(Route::currentRouteName(), ['category.index', 'category.create', 'category.edit', 'category.show', 'category.confirm-delete'])) class="active" @endif><a href="{{@route('category.index')}}"><i class="icon-folder"></i> <span>Categorías</span> </a></li>

            <li @if (in_array(Route::currentRouteName(), [
              'article.index', 'article.create', 'article.edit', 'article.show', 'article.confirm-delete',
              'slide.index', 'slide.create', 'slide.edit', 'slide.show', 'slide.confirm-delete',
              'slide-details.index', 'slide-details.create', 'slide-details.edit', 'slide-details.show', 'slide-details.confirm-delete'
              ])) class="active" @endif>
              <a href="#" id="second-level-two"
              @if (in_array(Route::currentRouteName(), [
                'article.index', 'article.create', 'article.edit', 'article.show', 'article.confirm-delete',
                'slide.index', 'slide.create', 'slide.edit', 'slide.show', 'slide.confirm-delete',
                'slide-details.index', 'slide-details.create', 'slide-details.edit', 'slide-details.show', 'slide-details.confirm-delete'

                ])) class="expand" @endif
                ><span>CMS</span> <i class="icon-newspaper"></i>
              </a>
              <ul>
                <li @if (in_array(Route::currentRouteName(), ['article.index', 'article.create', 'article.edit', 'article.show', 'article.confirm-delete'])) class="active" @endif><a href="{{@route('article.index')}}"><i class="icon icon-table2"></i> <span>Artículos [Posts]</span> </a></li>
                  <li @if (in_array(Route::currentRouteName(), [
                    'slide.index', 'slide.create', 'slide.edit', 'slide.show', 'slide.confirm-delete',
                    'slide-details.index', 'slide-details.create', 'slide-details.edit', 'slide-details.show', 'slide-details.confirm-delete'
                    ])) class="active" @endif><a href="{{@route('slide.index')}}"><i class="icon icon-images"></i> <span>Slides [Galerías]</span> </a></li>
                  </ul>
                </li>


                <li @if (in_array(Route::currentRouteName(), ['promo-opcion.read-products','promo-opcion.upload-complement-view'])) class="active" @endif>
                  <a href="#" id="second-level"
                  @if (in_array(Route::currentRouteName(), ['promo-opcion-read','ws.upload-db-view'])) class="expand" @endif
                    ><span>Provider Products</span> <i class="icon icon-tag"></i>
                  </a>
                  <ul>
                    <li @if (in_array(Route::currentRouteName(), ['promo-opcion.read-products'])) class="active" @endif><a href="{{@route('promo-opcion.read-products')}}"><i class="icon icon-table2"></i> <span>Productos > Promo Opcion</span> </a></li>
                    <li @if (in_array(Route::currentRouteName(), ['doble-vela.read-products'])) class="active" @endif><a href="{{@route('doble-vela.read-products')}}"><i class="icon icon-table2"></i> <span>Productos > Doble Vela</span> </a></li>
                    <li @if (in_array(Route::currentRouteName(), ['four-promotional.read-products'])) class="active" @endif><a href="{{@route('four-promotional.read-products')}}"><i class="icon icon-table2"></i> <span>Productos > 4 Promotional</span> </a></li>                        </li>

                            {{--
                            <li @if (in_array(Route::currentRouteName(), ['ws-settings.doble-vela'])) class="active" @endif><a href="{{@route('ws-settings.doble-vela')}}"><i class="icon icon-key2"></i> <span>Doble Vela</span> </a></li>
                            <li @if (in_array(Route::currentRouteName(), ['ws-settings.4-promotional'])) class="active" @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>4promotional</span> </a></li>
                            <li @if (in_array(Route::currentRouteName(), ['ws-settings.confirm-set-defaults'])) class="active"  @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>Restaurar Configuración</span> </a></li> --}}
                  </ul>

                        <li @if (in_array(Route::currentRouteName(), ['ws-settings.promo-opcion','ws-settings.doble-vela','ws-settings.4-promotional'])) class="active" @endif>
                          <a href="#" id="second-level"
                          @if (in_array(Route::currentRouteName(), ['ws-settings.promo-opcion','ws-settings.doble-vela','ws-settings.4-promotional'])) class="expand" @endif
                            ><span>WebServices [Conf]</span> <i class="icon icon-wrench2"></i>
                          </a>
                          <ul>
                            <li @if (in_array(Route::currentRouteName(), ['ws-settings.promo-opcion'])) class="active" @endif><a href="{{@route('ws-settings.promo-opcion')}}"><i class="icon icon-key2"></i> <span>PromoOpcion</span> </a></li>
                              <li @if (in_array(Route::currentRouteName(), ['ws-settings.doble-vela'])) class="active" @endif><a href="{{@route('ws-settings.doble-vela')}}"><i class="icon icon-key2"></i> <span>Doble Vela</span> </a></li>
                                <li @if (in_array(Route::currentRouteName(), ['ws-settings.4-promotional'])) class="active" @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>4promotional</span> </a></li>
                                  <li @if (in_array(Route::currentRouteName(), ['ws-settings.confirm-set-defaults'])) class="active"  @endif><a href="{{@route('ws-settings.4-promotional')}}"><i class="icon icon-key2"></i> <span>Restaurar Configuración</span> </a></li>
                                  </ul>
                                </li>


                                <li @if (in_array(Route::currentRouteName(), ['finance-settings.index'])) class="active" @endif><a href="{{@route('finance-settings.index')}}"><span>Ajustes de Ganancias</span> <i class="icon-star6"></i></a></li>
                                  <li @if (in_array(Route::currentRouteName(), ['user.index', 'user.create', 'user.edit', 'user.show', 'user.confirm-delete'])) class="active" @endif><a href="{{@route('user.index')}}"><i class="icon icon-user"></i> <span>Usuarios</span> </a></li>

                                    {{-- <li><a href="#my-quotations"><span>Productos de Proveedores</span> <i class="icon-cabinet"></i></a></li>
                                    <li><a href="#my-quotations"><span>Productos Brand Market</span> <i class="icon-table2"></i></a></li> --}}

                                  @else
                                    <li><a href="#my-quotations"><span>Mis Cotizaciones</span></a></li>
                                  @endif

                                </ul>
