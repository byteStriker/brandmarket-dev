
<div class="dropdown is-hoverable">
  <div class="dropdown-trigger">
    <button class="button has-text-primary" aria-haspopup="true" aria-controls="dropdown-menu4">
      <span>...</span>
      <span class="icon is-small">
        <i class="fas fa-angle-down" aria-hidden="true"></i>
      </span>
    </button>
  </div>
  <div class="dropdown-menu" role="menu">
    <div class="dropdown-content">
        {{ $linkDetails ?? '' }}
        {{ $linkEdit ?? '' }}
        {{ $linkRemove ?? '' }}
        {{ $linkExtra ?? '' }}
        {{ $linkExtra1 ?? '' }}
        {{ $linkExtra2 ?? '' }}
    </div>
  </div>
</div>
