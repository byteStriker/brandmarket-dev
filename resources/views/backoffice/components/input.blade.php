<div class="column {{$columnRatio??''}}" style="display:{{$displayElement??'block'}}">
  <div class="field">
    <label class="label">{{$label??'Default Label'}}</label>
    <div class="control">
      <input class="input" type="{{$type??'text'}}" placeholder="{{$placeholder??''}}" name="{{$name??''}}" id="{{$id??''}}" value="{{$value??''}}"
      {{$events??''}}
      {{$readonly??''}}>
    </div>
  </div>
</div>
