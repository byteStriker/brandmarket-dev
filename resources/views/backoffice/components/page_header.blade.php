<!-- Page header -->
<div class="page-header">
  <div class="page-title">
      <h3>{{$page_title}} <small>{{$message}}</small></h3>
  </div>
</div>
<!-- /page header -->
