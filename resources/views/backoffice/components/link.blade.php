<a href="{{$routeLink??'#'}}" class="{{$class??'button is-primary'}}" {{$events??''}} id="{{$id??''}}"><span class="{{$linkTextClass??'has-text-primary'}}">{{$linkText??'default'}}</span></a>
