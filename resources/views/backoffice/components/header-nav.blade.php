<header>
  <div class="container">
    <nav class="navbar" role="navigation" aria-label="main navigation">
      <div class="navbar-brand">
        <a class="navbar-item" href="/admin/">
          @component('frontend.components.logo',['width'=>"112", "height"=>"28"])
          @endcomponent
        </a>
        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div id="navbarTop" class="navbar-menu">
        <div class="navbar-start">
        </div>
        <div class="navbar-end">
          <div class="navbar-item account">
            <p><strong>¡Hola {{auth()->user()->name}}!</strong></p>
            <div class="is-flex">
              <a href="{{@route('user.profile')}}"><i class="icon-user"></i> Perfil!</a>
            </div>
          </div>
          <a href="#logout" onClick="window.frm_logout.submit();" class="navbar-item"> <h1>Salir</h1></a>
          <form action="{{@route('logout')}}" method="POST" name="frm_logout" id="frm_logout" style="display:none;">
            @csrf
          </form>

        </div>
      </div>
    </nav>
  </div>
</header>
