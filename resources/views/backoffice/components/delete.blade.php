<!-- Datepicker -->
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="columns">
      <div class="column">
        <h6 class="panel-title has-text-white	">Propiedades del Registro</h6>
      </div>
      <div class="column">
        <div class="is-pulled-right">
          @if (isset($link_goback))
            @component('backoffice.components.link', ['linkText'=>'Regresar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_goback??'#'])@endcomponent
          @endif
          @if (isset($link_edit))
            @component('backoffice.components.link', ['linkText'=>'Editar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_edit??'#'])@endcomponent
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="panel-block">
    <div class="container">
      <form action="{{$link_delete}}" method="POST" name="frm_delete" id="frm_delete" role="form">
        @method('DELETE')
        @csrf
        <div class="{{$columnsMultiLineClass??''}}">
          {{$row_properties}}
        </div>
        <footer class="has-text-centered">
          <div class="field is-grouped is-grouped-right">
            <div class="container">
              <div class="field">
                <button type="submit" name="button" class="button is-primary"><i class="icon icon-disk"></i> Sí, quiero eliminar este registro.</button>
              </div>
            </div>
          </div>
        </footer>
      </form>
    </div>
  </div>
</div>
<!-- /datepicker -->
