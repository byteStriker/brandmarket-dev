<!-- backoffice/components/show -->
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="columns">
      <div class="column">
        <h6 class="panel-title has-text-white	">Propiedades del Registro</h6>
      </div>
      <div class="column">
        <div class="is-pulled-right">
          @if (isset($link_goback))
            @component('backoffice.components.link', ['linkText'=>'Regresar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_goback??'#'])
            @endcomponent
          @endif
          @if (isset($link_edit))
            @component('backoffice.components.link', ['linkText'=>'Editar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_edit??'#'])
            @endcomponent
          @endif

          @if (isset($link_delete))
            @component('backoffice.components.link', ['linkText'=>'Eliminar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_delete??'#'])
            @endcomponent
          @endif

          @if (isset($link_extra))
            @component('backoffice.components.link', ['linkText'=>$link_extra->text, 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_extra->href??'#'])
            @endcomponent
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="panel-block">
    <form action="#" role="form" class="container">
      <div class="{{$columnsMultiLineClass??''}}">
        {{$row_properties??'default form'}}
      </div>
    </form>
  </div>
</div>