<!-- Bordered datatable inside panel -->
<div class="panel">
  <div class="panel-heading">
    <div class="columns">
      <div class="column">
        <h6 class="panel-title has-text-white	">Lista de Registros</h6>
      </div>
      <div class="column">
        <div class="is-pulled-right">
          @component('backoffice.components.link',
          ['linkText'=>'Agregar', 'class'=>'button is-primary',
          'linkTextClass'=>'has-text-white', 'routeLink'=>$link_create??'#'])
          @endcomponent
        </div>
      </div>
    </div>
  </div>
  {{ $header ?? '' }}
  <div class="panel-block">
  <table class="table table-bordered">
    <thead>
      @if (isset($search_route))
        <tr>
          <th colspan="{{count($columns) + 2}}" class=" has-text-centered">
            @component('backoffice.components.search-form',['search_route'=> $search_route])@endcomponent
          </th>
        </tr>
      @endif

      <tr>
        <th>#</th>
        @foreach ($columns as $column)
          <th>{{$column}}</th>
        @endforeach
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      {{$rows}}
    </tbody>
  </table>
</div>
  <div class="panel-block-pagination is-centered">
    {{ $pagination_links ?? '' }}
  </div>
</div>
<!-- /bordered datatable inside panel -->
