
  <footer class="footer content">
    <figure>
      @component('frontend.components.logo')
      @endcomponent
    </figure>
    <p>
      <strong>
        Contacto:
        <a href="tel:5544341335">55 4434 1335</a> –
        <a href="mailto:atencionaclientes@brandmarket.com.mx">atencionaclientes@brandmarket.com.mx</a>
      </strong>
    </p>
  </footer>
