<!-- CreateForm -->
<div class="panel panel-default">
  @component('backoffice.components.notification', ['errors'=>$errors])@endcomponent
    <div class="panel-heading">
      <div class="columns">
        <div class="column">
          <h6 class="panel-title has-text-white	">Propiedades del Registro</h6>
        </div>
        <div class="column">
          <div class="is-pulled-right">
            @if (isset($link_goback))
              @component('backoffice.components.link', ['linkText'=>'Regresar', 'class'=>'button is-primary', 'linkTextClass'=>'has-text-white', 'routeLink'=>$link_goback??'#'])
              @endcomponent
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="panel-block">
      <div class="container">
        <form action="{{$link_create??'#'}}" role="form" method="POST"
        name="createForm" id="createForm" class="validate" enctype="multipart/form-data">
          @csrf
          <div class="{{$columnsMultiLineClass??''}}">
            {{$row_properties??'default form'}}
          </div>
          <footer class="has-text-centered">
            <div class="container">
              <div class="field">&nbsp;
                <div class="dropdown is-hoverable">
                  <div class="dropdown-trigger">
                    <button class="button has-text-primary" aria-haspopup="true" aria-controls="dropdown-menu4">
                      <span>{{$saveText ?? '...'}}</span>
                      <span class="icon is-small">
                        <i class="fas fa-angle-down" aria-hidden="true"></i>
                      </span>
                    </button>
                  </div>
                  <div class="dropdown-menu" role="menu">
                    <div class="dropdown-content">
                      {{ $linkSaveOther ?? '' }}
                      {{ $linkSaveAddSubcategory ?? '' }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </form>
      </div>

    </div>
  </div>
  @if (isset($create_script))
    {{$create_script}}
  @endif
  <!-- /CreateForm -->
