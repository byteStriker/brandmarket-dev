<div class="column">
  <div class="select">
    <label class="label">{{$label??'Default Label'}}</label>
    <select id="{{$id??''}}" name="{{$name??''}}" {{$events??''}} >
    <option value="-1">Seleccione</option>
    @if ($rows)
      @if (count($rows) > 0)
        @forelse ($rows as $row)
          <option value="{{$row->id}}"
            @if (isset($selected_id))
              @if ($row->id == $selected_id) selected="selected" @endif
            @endif
            > {{$row->name}}</option>
        @empty
      @endif
        <option value="-1">No hay categorías</option>
      @endforelse
    @endif
  </select>
  </div>
</div>
