@extends('layouts.backoffice', ['title' => $page_title])
@section('content')
  <!-- Panel -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="columns">
        <div class="column">
          <h6 class="panel-title has-text-white	">Lista de Registros</h6>
        </div>
        <div class="column">
          <div class="is-pulled-right">
          </div>
        </div>
      </div>
    </div>
    <div class="panel-block">

      <form action="{{@route('ws-settings.update-4-promotional')}}" role="form" method="POST" name="wsForm" id="wsForm" class="form-horizontal validate">
        @csrf
        @method('PUT')
        <div class="columns is-multiline">
          @component('backoffice.components.input',['columnRatio'=>'is-3','id' => 'name', 'id'=>'id', 'displayElement'=>'none', 'value'=>$ws->id])
          @endcomponent
          @component('backoffice.components.input',['columnRatio'=>'is-3','label' => 'Server Alias', 'type'=>'text', 'name' => 'server_alias', 'id'=>'server_alias', 'value'=>$ws->server_alias])
          @endcomponent
          @component('backoffice.components.input',['columnRatio'=>'is-3','label' => 'Server URL', 'type'=>'text', 'name' => 'server_url', 'id'=>'server_url', 'value'=>$ws->server_url])
          @endcomponent
          @component('backoffice.components.input',['columnRatio'=>'is-3','label' => 'Server URL Alterna', 'type'=>'text', 'name' => 'server_url_alternating', 'id'=>'server_url_alternating', 'value'=>$ws->server_url_alternating])
          @endcomponent
          @component('backoffice.components.input',['columnRatio'=>'is-3','label' => 'Username', 'type'=>'text', 'name' => 'username', 'id'=>'username', 'value'=>$ws->username])
          @endcomponent
          @component('backoffice.components.input',['columnRatio'=>'is-3','label' => 'Password', 'type'=>'text', 'name' => 'password', 'id'=>'password', 'value'=>$ws->password])
          @endcomponent
        </div>

        <footer class="row text-center">
          <div class="btn-group dropup">
            <a href="#" onClick="window.wsForm.submit()" class="btn btn-lg btn-default">Actualizar <span class="icon save"></span></a>
          </div>
        </footer>
      </form>
    </div>
  </div>
@endsection
