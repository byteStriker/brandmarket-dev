@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')

  @component('backoffice.components.edit',
    [
      'link_goback' => @route('category.index'),
      'link_update' => @route('category.update', $row),
      'link_delete' => @route('category.confirm-delete', $row),
      'id' => $row->id
    ])

    @slot('row_properties')
      <input type="hidden" name="id" value="{{$row->id}}">
      <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <label>Nombre</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{$row->name}}">
          </div>
        </div>
      </div>
    @endslot
  @endcomponent

@endsection
