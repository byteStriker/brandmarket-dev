@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h6 class="panel-title"><i class="icon-bubble4"></i>Propiedades del Registro</h6>
      <div class="btn-group pull-right">
        <a href="{{@route('admin.index')}}" class="btn btn-lg btn-default"><i class="icon-arrow-left11"></i> Regresar</a>
      </div>
    </div>
    <div class="panel-body">
      <form action="{{@route('ws/set-defaults')}}" method="POST" name="frmDefaults" id="frmDefaults" role="form">
        @csrf
        <div class="form-group">
          <div class="row">
            <div class="col-sm-12">
              <label>¿Deseas re establecer todos los ajustes de todos los web services?</label>
            </div>
          </div>
        </div>
        <footer>
          <div class="col-sm-12 text-center">
            <a href="#" onClick="window.frmDefaults.submit()" class="btn btn-lg btn-warning"><i class="icon-check"></i> Sí, proceder con esta acción</a>
          </div>
        </footer>
      </form>
    </div>
  </div>
@endsection
