@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
{{-- @route('category.confirm-delete', $row) --}}
@php
  $link_extra = new stdclass();
  $link_extra->href = @route('subcategory.index', ['category_id'=>$row->id]);
  $link_extra->icon = 'table2';
  $link_extra->text = 'Subcategorías';
@endphp
  @component('backoffice.components.show',
    [
      'link_goback' => @route('category.index'),
      'link_edit' => @route('category.edit', $row),
      'link_delete' => @route('category.confirm-delete', $row),
      'link_extra' => $link_extra
    ])
    @slot('row_properties')

      <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <label>Nombre</label>
            <input type="text" class="form-control" placeholder="Elige un nombre innovador y atractivo para los usuarios" value="{{$row->name}}" readonly>
          </div>
        </div>
      </div>
    @endslot
  @endcomponent
@endsection
