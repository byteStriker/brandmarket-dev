@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')
{{-- @route('article.confirm-delete', $row) --}}
  @component('backoffice.components.show',
    [
      'link_goback' => @route('article.index'),
      'link_edit' => @route('article.edit', $row),
      'link_delete' => @route('article.confirm-delete', $row),
      'columnsMultiLineClass' => 'columns is-multiline',
    ])

    @slot('row_properties')
    {{-- <div class="columns is-multiline"> --}}
      @component('backoffice.components.input',['label' => 'Título', 'value' => $row->title, 'columnRatio' => 'is-12'])@endcomponent
      @component('backoffice.components.input',['label' => 'Orden', 'value' => $row->priority, 'columnRatio' => 'is-2'])@endcomponent
      @component('backoffice.components.input',['label' => 'Estatus', 'value' => $row->status, 'columnRatio' => 'is-2'])@endcomponent
      {{-- @component('backoffice.components.input',['label' => 'Prioridad (Orden para Mostrar)', 'value' => $row->priority, 'columnRatio' => 'is-2'])@endcomponent --}}
      @component('backoffice.components.input',['label' => 'SEO Keywords', 'value' => $row->seo_keywords, 'columnRatio' => 'is-4'])@endcomponent
      @component('backoffice.components.input',['label' => 'SEO Description', 'value' => $row->seo_description, 'columnRatio' => 'is-4'])@endcomponent
      <div class="column is-12">
        <div class="field">
          <label class="label">Contenido</label>
          <textarea class="textarea" name="content" id="content">{{$row->content}}</textarea>
        </div>
      </div>
    {{-- </div> --}}
    @if ($row->hero_image)
      <div class="row text-center"> {{asset("storage/{$row->hero_image}")}}
        <div class="row text-center">
          <div class="col-sm-12">
            <img src="{{asset("storage/{$row->hero_image}")}}" alt="{{$row->name}}">
          </div>
        </div>
      </div>
    @endif
  @endslot

  @endcomponent

@endsection
