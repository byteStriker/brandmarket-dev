@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')

  @component('backoffice.components.delete',
    [
      'link_goback' => @route('article.index'),
      'link_edit' => @route('article.edit', $row),
      'link_delete' => @route('article.destroy', $row)

    ])

    @slot('row_properties')
      <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <label>Título</label>
            <input type="text" class="form-control" value="{{$row->title}}" readonly>
          </div>
        </div>
      </div>
    @endslot
  @endcomponent
@endsection
