@extends('layouts.backoffice', ['title' => $page_title])
@section('content')

  {{-- @php
  $buttons = [];
  $btnSaveAddOther = new stdclass;
  $btnSaveAddOther->type = 'button';
  $btnSaveAddOther->name = 'btnSaveAddOther';
  $btnSaveAddOther->id = 'btnSaveAddOther';
  $btnSaveAddOther->i_class = '<i class="material-icons md-48">save</i></span>';
  $btnSaveAddOther->value = 'Agregar Otro';
  array_push($buttons, $btnSaveAddOther);
  @endphp
  @component('backoffice.components.create',
  [
    'link_goback' => @route('article.index'),
    'link_create' => @route('article.store'),
    'buttons' => $buttons,
    'columnsMultiLineClass'=>'columns is-multiline',

  ]) --}}
  @component('backoffice.components.create',
  [
    'link_goback' => @route('article.index'),
    'link_create' => @route('article.store'),
    'saveText' => 'Guardar y...',
    'columnsMultiLineClass'=>'columns is-multiline'
  ])
  @slot('linkSaveOther')
    @component('backoffice.components.link',
    ['linkText'=>'Agregar Otro', 'id'=>'btnSaveAddOther',
    'routeLink'=>'#',
    'class'=>'dropdown-item has-text-light',
  ])
@endcomponent
@endslot
  {{-- @slot('create_script')
    <script type="text/javascript" src="{{@url('storage/backoffice-assets/forms/article.ui.controller.js')}}"></script>
  @endslot --}}

  @slot('row_properties')
    @component('backoffice.components.input',['label' => 'Título', 'placeholder' => 'Título', 'name' => 'title', 'id'=>'title', 'columnRatio' => 'is-6'])@endcomponent
    @component('backoffice.components.input',['label' => 'Fecha de Publicación', 'placeholder' => 'Fecha de Publicación', 'name' => 'published_at', 'id'=>'published_at', 'columnRatio' => 'is-3'])@endcomponent
    <div class="column is-3">
      <label class="label">Type</label>
      <div class="control is-expanded">
        <div class="select is-fullwidth">
          <select name="edit" id="edit">
            <option value="-1">Seleccione</option>
            <option value="Post">Publicacion</option>
            <option value="Content">Content</option>
          </select>
        </div>
      </div>
    </div>
    @component('backoffice.components.input',['label' => 'Orden', 'placeholder' => 'Prioridad (Orden para Mostrar)', 'name' => 'priority', 'id'=>'priority', 'columnRatio' => 'is-2'])@endcomponent
    <div class="column is-2">
      <div class="field">
        <label class="label">Estatus</label>
        <div class="control is-expanded">
          <div class="select is-fullwidth">
            <select name="status" id="status">
              <option value="-1">Seleccione</option>
              <option value="Published">Publicado</option>
              <option value="Future">Future</option>
              <option value="Draft">Borrador</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    @component('backoffice.components.input',['label' => 'SEO Keywords', 'placeholder' => 'SEO Keywords', 'name' => 'seo_keywords', 'id'=>'seo_keywords', 'columnRatio' => 'is-4'])@endcomponent
    @component('backoffice.components.input',['label' => 'SEO Description', 'placeholder' => 'SEO Description', 'name' => 'seo_description', 'id'=>'seo_description', 'columnRatio' => 'is-4'])@endcomponent
    <div class="column is-6">
      <div class="field">
        <label class="label">Contenido</label>
        <textarea class="textarea" name="content" id="content"></textarea>
      </div>
    </div>
    @component('backoffice.components.input',['label' => 'Imagen Carátula', 'type'=>'file', 'name' => 'hero_image', 'id'=>'hero_image', 'columnRatio' => 'is-6'])@endcomponent
  @endslot

  @endcomponent
@endsection
