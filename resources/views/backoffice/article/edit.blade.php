@extends('layouts.backoffice', ['title'=>$page_title])
@section('content')

  @component('backoffice.components.edit',
    [
      'link_goback' => @route('article.index'),
      'link_update' => @route('article.update', $row),
      'link_delete' => @route('article.confirm-delete', $row),
      'id' => $row->id,
      'columnsMultiLineClass'=>'columns is-multiline',
    ])

  <input type="hidden" name="id" id="id" value="{{$row->id}}">
  @slot('row_properties')
    {{-- <div class="columns is-multiline"> --}}
    @component('backoffice.components.input',['label' => 'Título', 'placeholder' => 'Título', 'name' => 'title', 'id' => 'title', 'value' => $row->title, 'columnRatio' => 'is-6'])@endcomponent
    @component('backoffice.components.input',['label' => 'Fecha de Publicación', 'placeholder' => 'Fecha de Publicación', 'name' => 'published_at', 'id' => 'published_at', 'value' => $row->published_at, 'columnRatio' => 'is-3'])@endcomponent
    <div class="column is-3">
      <label class="label">Type</label>
      <div class="control is-expanded">
        <div class="select is-fullwidth">
          <select name="type" id="type">
            <option value="-1">Seleccione</option>
            <option value="Post" @if ($row->type)  @endif>Publicacion</option>
            <option value="Content" @if ($row->type)  @endif>Content</option>
          </select>
        </div>
      </div>
    </div>
    @component('backoffice.components.input',['label' => 'Orden', 'placeholder' => 'Prioridad (Orden para Mostrar)', 'name' => 'priority', 'id' => 'priority', 'value' => $row->priority, 'columnRatio' => 'is-2'])@endcomponent
    <div class="column is-2">
      <label class="label">Estatus</label>
      <div class="control is-expanded">
        <div class="select is-fullwidth">
          <select name="status" id="status">
            <option value="-1">Seleccione</option>
            <option value="Published" @if ($row->status == 'Published') @endif>Publicado</option>
            <option value="Future" @if ($row->status == 'Future') @endif>Future</option>
            <option value="Draft" @if ($row->status == 'Draft') @endif>Borrador</option>
          </select>
        </div>
      </div>
    </div>
    @component('backoffice.components.input',['label' => 'SEO Keywords', 'placeholder' => 'SEO Keywords', 'name' => 'seo_keywords', 'id' => 'seo_keywords', 'value' => $row->seo_keywords, 'columnRatio' => 'is-4'])@endcomponent
    @component('backoffice.components.input',['label' => 'SEO Description', 'placeholder' => 'SEO Description', 'name' => 'seo_description', 'id' => 'seo_description', 'value' => $row->seo_description, 'columnRatio' => 'is-4'])@endcomponent
    <div class="column is-6">
      <div class="field">
        <label class="label">Contenido</label>
        <textarea name="content" id="content" class="textarea">{{$row->content}}</textarea>
      </div>
    </div>
    @component('backoffice.components.input',['label' => 'Imagen Carátula', 'type' => 'file', 'name' => 'hero_image', 'id' => 'hero_image', 'value' => $row->hero_image, 'columnRatio' => 'is-6'])@endcomponent
    @if ($row->hero_image)
    <div class="column">
      <div class="column is-12">
        {{asset("storage/{$row->hero_image}")}}
        <img src="{{asset("storage/{$row->hero_image}")}}" alt="{{$row->name}}">
      </div>
    </div>
    @endif
  @endslot

  @endcomponent

@endsection