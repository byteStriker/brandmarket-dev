@if ($paginator->hasPages())
<nav class="pagination is-centered" role="navigation" aria-label="pagination">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <a href="{{ $paginator->previousPageUrl() }}" class="disabled pagination-previous" aria-disabled="true" aria-label="@lang('pagination.previous')">&lsaquo;</a>
    @else
        <a href="{{ $paginator->previousPageUrl() }}" class="pagination-previous" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
    @endif
    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" class="pagination-next" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
    @else
        <a href="{{ $paginator->nextPageUrl() }}" class="disabled pagination-next" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
    @endif
    <ul class="pagination-list">
        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <a class="disabled pagination-ea" aria-disabled="true"><span>{{ $element }}</span></a>
            @endif
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pagination-link is-current" aria-current="page"><span>{{ $page }}</span></li>
                    @else
                        <li><a class="pagination-link" href="{{ $url }}" aria-label="Ir a la página {{$page}}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
    </ul>
  </nav>
@endif
