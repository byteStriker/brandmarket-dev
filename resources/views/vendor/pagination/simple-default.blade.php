@if ($paginator->hasPages())
<nav class="pagination is-small is-centered " role="navigation" aria-label="pagination">
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled pagination-previous" aria-disabled="true"><span>@lang('pagination.previous')</span></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="pagination-previous">@lang('pagination.previous')</a></li>
        @endif
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next" class="pagination-next">@lang('pagination.next')</a></li>
        @else
            <li class="disabled pagination-next" aria-disabled="true"><span>@lang('pagination.next')</span></li>
        @endif
    </ul>
</nav>
@endif
