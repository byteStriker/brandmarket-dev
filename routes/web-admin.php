<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'Auth\LoginController@showLoginForm')->name('landing-home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('sign-in');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::prefix('admin')->middleware(['auth', 'admin'])->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');

    // Route::get('category/get/options', 'CategoryController@getOptions')->name('category.getOptions');

    Route::get('category/confirm-delete/{category}', 'CategoryController@confirmDelete')->name('category.confirm-delete');
    Route::resource('category', 'CategoryController');

    Route::get('subcategory/confirm-delete/{subcategory}', 'SubcategoryController@confirmDelete')->name('subcategory.confirm-delete');
    // Route::resource('subcategory', 'SubcategoryController');
    Route::get('subcategory/confirm-delete/{subcategory}', 'SubcategoryController@confirmDelete')->name('subcategory.confirm-delete');
    Route::get('subcategory/{category?}', 'SubcategoryController@index')->name('subcategory.index');
    Route::get('subcategory/{category?}/create', 'SubcategoryController@create')->name('subcategory.create');
    Route::post('subcategory/{category?}/store', 'SubcategoryController@store')->name('subcategory.store');
    Route::get('subcategory/{category?}/{subcategory}/show', 'SubcategoryController@show')->name('subcategory.show');
    Route::delete('subcategory/{subcategory}', 'SubcategoryController@destroy')->name('subcategory.destroy');
    Route::get('subcategory/{category?}/{subcategory}/edit', 'SubcategoryController@edit')->name('subcategory.edit');
    Route::put('subcategory/{category?}/update', 'SubcategoryController@update')->name('subcategory.update');

    Route::get('article/confirm-delete/{article}', 'ArticleController@confirmDelete')->name('article.confirm-delete');
    Route::resource('article', 'ArticleController');

    Route::get('slide-details/{slide_details}/confirm-delete', 'SlideDetailsController@confirmDelete')->name('slide-details.confirm-delete');
    Route::get('slide-details/{slide?}', 'SlideDetailsController@index')->name('slide-details.index');
    Route::get('slide-details/{slide?}/create', 'SlideDetailsController@create')->name('slide-details.create');
    Route::post('slide-details/{slide?}/store', 'SlideDetailsController@store')->name('slide-details.store');
    Route::get('slide-details/{slide?}/{slide_details}/show', 'SlideDetailsController@show')->name('slide-details.show');
    Route::delete('slide-details/{slide_details}', 'SlideDetailsController@destroy')->name('slide-details.destroy');
    Route::get('slide-details/{slide?}/{slide_details}/edit', 'SlideDetailsController@edit')->name('slide-details.edit');
    Route::put('slide-details/{slide?}/update', 'SlideDetailsController@update')->name('slide-details.update');

    // Route::resource('slidedetails/', 'SlideDetailsController');

    Route::get('slide/confirm-delete/{slide}', 'SlideController@confirmDelete')->name('slide.confirm-delete');
    Route::resource('slide', 'SlideController');

    Route::get('ws-settings/promo-opcion', 'WSConfigurationController@indexWS1')->name('ws-settings.promo-opcion');
    Route::put('ws-settings/promo-opcion', 'WSConfigurationController@updateWS1')->name('ws-settings.update-promo-opcion');
    Route::get('ws-settings/doble-vela', 'WSConfigurationController@indexWS2')->name('ws-settings.doble-vela');
    Route::put('ws/update/doble-vela', 'WSConfigurationController@updateWS2')->name('ws-settings.update-doble-vela');
    Route::get('ws-settings/4-promotional', 'WSConfigurationController@indexWS3')->name('ws-settings.4-promotional');
    Route::put('ws/update/4-promotional', 'WSConfigurationController@updateWS3')->name('ws-settings.update-4-promotional');
    Route::get('ws/set-defaults', 'WSConfigurationController@confirmSetDefaults')->name('ws-settings.confirm-set-defaults');

    Route::get('ws/currency-conversion', 'WSConfigurationController@getCurrencyConversion')->name('ws-settings.currency-conversion');

    Route::post('ws/set-defaults', 'WSConfigurationController@setDefaults')->name('ws-settings.defaults');
    Route::get('finance-settings/', 'FinanceSettingsController@index')->name('finance-settings.index');
    Route::put('ws/update/promo-opcion', 'FinanceSettingsController@update')->name('finance-settings.update');

    Route::get('user/profile', 'UserController@myProfile')->name('user.profile');
    Route::put('user/profile', 'UserController@updateMyProfile')->name('user.update-profile');

    Route::resource('user', 'UserController');
    Route::get('user/confirm-delete/{user}', 'UserController@confirmDelete')->name('user.confirm-delete');
    Route::resource('product', 'ProductsBrandMarketController');
    Route::get('product/{product}/confirm-delete', 'ProductsBrandMarketController@confirmDelete')->name('product.confirm-delete');

    // input selected by user for import to the brand market's products table.
    Route::get('promo-opcion/product-list', 'PromoOpcionController@index')->name('promo-opcion.read-products');
    Route::get('promo-opcion/upload-complement', 'PromoOpcionController@create')->name('promo-opcion.upload-complement-view');
    Route::post('promo-opcion/upload-complement', 'PromoOpcionController@store')->name('promo-opcion.store-excel-file');

    Route::get('promo-opcion/complement-with-web-service', 'PromoOpcionController@complementWebServiceData')->name('promo-opcion.complement-web-service');
    Route::get('promo-opcion/complement-with-web-service/getFullDb', 'PromoOpcionController@getFullDB')->name('promo-opcion.complement-web-service-full-db');
    Route::get('promo-opcion/complement-with-web-service/complement', 'FourPromotionalController@getComplement')->name('promo-opcion.complement-web-service-complement');

    // Step 5: to show a screen with recently imported records.

    Route::get('four-promotional/product-list', 'FourPromotionalController@index')->name('four-promotional.read-products');
    Route::get('four-promotional/upload-complement', 'FourPromotionalController@create')->name('four-promotional.upload-complement-view');
    Route::get('four-promotional/complement-with-web-service', 'FourPromotionalController@complementWebServiceData')->name('four-promotional.complement-web-service');
    Route::get('four-promotional/complement-with-web-service/getFullDb', 'FourPromotionalController@getFullDB')->name('four-promotional.complement-web-service-full-db');
    Route::get('four-promotional/complement-with-web-service/price', 'FourPromotionalController@getPrice')->name('four-promotional.complement-web-service-price');

    Route::get('doble-vela/product-list', 'DobleVelaController@index')->name('doble-vela.read-products');
    Route::get('doble-vela/upload-complement', 'DobleVelaController@create')->name('doble-vela.upload-complement-view');
    Route::get('doble-vela/complement-with-web-service', 'DobleVelaController@complementWebServiceData')->name('doble-vela.complement-web-service');
    Route::get('doble-vela/complement-with-web-service/fill-images', 'DobleVelaController@complementWebServiceImages')->name('doble-vela.complement-web-service-fill-images');
    Route::get('doble-vela/complement-with-web-service/stock', 'DobleVelaController@getExistenciasAllWeb')->name('doble-vela.complement-web-service-stock');

    // Route::post('doble-vela/complement-with-web-service', 'DobleVelaController@update')->name('doble-vela.complement-web-service');
    //
});

// Route::get('/', function () {
    //     return view('layouts.frontend');
    // });
