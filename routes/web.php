<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
                                  | App\Http\Controllers\Auth\LoginController@login                        | web,guest                     |
 login                            | App\Http\Controllers\Auth\LoginController@showLoginForm                | web,guest                     |
 logout                           | App\Http\Controllers\Auth\LoginController@logout                       | web                           |
 password.email                   | App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail  | web,guest                     |
 password.update                  | App\Http\Controllers\Auth\ResetPasswordController@reset                | web,guest                     |
 password.request                 | App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm | web,guest                     |
 password.reset                   | App\Http\Controllers\Auth\ResetPasswordController@showResetForm        | web,guest                     |
                                  | App\Http\Controllers\Auth\RegisterController@register                  | web,guest                     |
 register                         | App\Http\Controllers\Auth\RegisterController@showRegistrationForm      | web,guest
*/

Route::get('', 'Auth\LoginController@showLoginForm')->name('landing-home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('sign-in');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'Auth\LoginController@showLoginForm');
// Route::fallback(function () {
//   abort(404);
// });

// Route::prefix('admin')->middleware(['auth'])->group(function () {
//   Route::get('/', 'AdminController@index')->name('admin.index');
//   Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');
//   Route::resource('category', 'CategoryController');
//   Route::get('category/confirm-delete/{category}', 'CategoryController@confirmDelete')->name('category.confirm-delete');
//   Route::resource('subcategory', 'SubcategoryController');
//   Route::get('subcategory/confirm-delete/{subcategory}', 'SubcategoryController@confirmDelete')->name('subcategory.confirm-delete');
//   Route::resource('article', 'ArticleController');
//   Route::get('article/confirm-delete/{article}', 'ArticleController@confirmDelete')->name('article.confirm-delete');
//   Route::resource('slide', 'SlideController');
//   Route::get('slide/confirm-delete/{slide}', 'SlideController@confirmDelete')->name('slide.confirm-delete');
//   Route::resource('slide-details', 'SlideDetailsController');
//   Route::get('slide-details/confirm-delete/{slide_detail}', 'SlideDetailsController@confirmDelete')->name('slide-details.confirm-delete');
//   Route::get('ws-settings/promo-opcion', 'WSConfigurationController@indexWS1')->name('ws-settings.promo-opcion');
//   Route::put('ws-settings/promo-opcion', 'WSConfigurationController@updateWS1')->name('ws-settings.update-promo-opcion');
//   Route::get('ws-settings/doble-vela', 'WSConfigurationController@indexWS2')->name('ws-settings.doble-vela');
//   Route::put('ws/update/doble-vela', 'WSConfigurationController@updateWS2')->name('ws-settings.update-doble-vela');
//   Route::get('ws-settings/4-promotional', 'WSConfigurationController@indexWS3')->name('ws-settings.4-promotional');
//   Route::put('ws/update/4-promotional', 'WSConfigurationController@updateWS3')->name('ws-settings.update-4-promotional');
//   Route::get('ws/set-defaults', 'WSConfigurationController@confirmSetDefaults')->name('ws-settings.confirm-set-defaults');
//   Route::post('ws/set-defaults', 'WSConfigurationController@setDefaults')->name('ws-settings.defaults');
//   Route::get('finance-settings/', 'FinanceSettingsController@index')->name('finance-settings.index');
//   Route::put('ws/update/promo-opcion', 'FinanceSettingsController@update')->name('finance-settings.update');
//   Route::resource('user', 'UserController');
//   Route::get('user/confirm-delete/{user}', 'UserController@confirmDelete')->name('user.confirm-delete');
//   Route::get('user/profile', 'UserController@myProfile')->name('user.profile');
//   Route::put('user/profile', 'UserController@updateMyProfile')->name('user.update-profile');
//   // lectura de ws de los proveedores:
//   Route::get('promo-opcion/product-list', 'PromoOpcionController@index')->name('promo-opcion.read-products');
//   Route::get('promo-opcion/read-web-service', 'PromoOpcionController@read')->name('promo-opcion.read-products-web-service');
//   Route::get('promo-opcion/upload-complement', 'PromoOpcionController@uploadDB')->name('promo-opcion.upload-complement-view');
//   Route::get('four-promotional/product-list', 'FourPromotionalController@index')->name('four-promotional.read-products');
//   Route::get('four-promotional/upload-complement', 'FourPromotionalController@create')->name('four-promotional.upload-complement-view');
//   Route::get('doble-vela/product-list', 'DobleVelaController@index')->name('doble-vela.read-products');
//   Route::get('doble-vela/upload-complement', 'DobleVelaController@create')->name('doble-vela.upload-complement-view');
//
// });
//
// Route::prefix('account')->middleware(['auth'])->group(function(){
//   Route::get('home', 'HomeController@index')->name('account.home');
//   Route::get('settings', 'HomeController@myProfile')->name('account.profile');
//   Route::put('settings', 'HomeController@updateMyProfile')->name('account.update-profile');
//   Route::get('wish-list', 'HomeController@wishList')->name('account.wish-list');
//
//   Route::get('cart', 'SimpleCartController@index')->name('account.cart');
//   // Route::resource('cart', 'SimpleCartController')->name('account-cart');
//
// });

// Route::get('/', function () {
//     return view('layouts.frontend');
// });
