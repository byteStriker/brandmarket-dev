<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::prefix('account')->middleware(['auth', 'user-account'])->group(function () {
    Route::get('home', 'HomeController@index')->name('account.home');
    Route::get('settings', 'HomeController@myProfile')->name('account.profile');
    Route::put('settings', 'HomeController@updateMyProfile')->name('account.update-profile');
    Route::get('mailing/client', 'SimpleCartController@toSendEmailClient')->name('account.test-email-client');
    Route::get('mailing/internal/{simpleCart?}', 'SimpleCartController@toSendEmailInternal')->name('account.test-email-internal');
    Route::get('exchange-rate', 'ProductController@getExchangeRate')->name('exchange-rate');
    Route::get('privacy-notice', 'HomeController@showPrivacyNotice')->name('privacy-notice');
    Route::get('terms-conditions', 'HomeController@showTermsConditions')->name('terms-conditions');
    Route::get('cookie-notice', 'HomeController@showCookieNotice')->name('cookie-notice');
});
//
Route::prefix('cart')->middleware(['auth', 'user-account'])->group(function () {
    Route::get('current', 'SimpleCartController@index')->name('cart.index');
    Route::post('current', 'SimpleCartController@store')->name('cart.add-product');
    Route::delete('current', 'SimpleCartController@destroy')->name('cart.delete-product');
    Route::put('current', 'SimpleCartController@update')->name('cart.update-product');
    Route::post('current/checkout', 'SimpleCartController@checkout')->name('cart.checkout');
    Route::put('current/update-product-quantity', 'SimpleCartController@updateProductQuantity')->name('cart.update-product-quantity');
    Route::put('save-wishlist/{simple_carts}', 'SimpleCartController@saveAsWishlist')->name('cart.save-as-wishlist');
    Route::get('wish-list', 'SimpleCartController@wishList')->name('cart.wish-list');
    Route::put('wish-list/open', 'SimpleCartController@openWishList')->name('cart.wish2cart');
});
//
Route::prefix('store')->middleware(['auth', 'user-account'])->group(function () {
    Route::get('category/all', 'CategoryBrandMarketController@allCategories')->name('category.all');
    Route::get('category/{category?}', 'CategoryBrandMarketController@list')->name('category.list');
    // Route::get('subcategory', 'CategoryBrandMarketController@list')->name('subcategory.list');
    Route::get('products/{category?}/{subcategory?}', 'ProductController@list')->name('products.list');
    Route::get('product/{provider?}/{category?}/{subcategory?}/{slug?}/{id?}', 'ProductController@product')->name('products.product');

    // Route::get('test-calculus', 'ProductController@testCalculus')->name('test');

});
