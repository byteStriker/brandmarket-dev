<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::middleware('auth:api')->get('/category/options', 'CategoryController@getOptions')->name('api.getOptionsForCategory');
Route::get('/category/options/{categorySlug?}', 'API\PublicController@getCategoryOptions')->name('api.getOptionsForCategory');
Route::post('/autocomplete', 'API\PublicController@searchProducts')->name('api.autoComplete');

Route::middleware('auth:api')->post('/save-product', 'API\PublicController@addProductSimpleCart')->name('api.add-product');
Route::middleware('auth:api')->post('/save-product-to-wishlist', 'API\PublicController@addProductWishList')->name('api.add-product-to-wishlist');

Route::middleware('auth:api')->get('/update-prov-one/{model?}', 'API\PublicController@getStock')->name('api.update-doble-vela-stock');
