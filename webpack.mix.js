const mix = require('laravel-mix');
// const purgeCss = require('purgecss-webpack-plugin');
// const glob = require('glob-all');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//
// mix.autoload({ jquery: ['$', 'window.jQuery', 'jQuery'] })
//   .js('resources/js/app.js', 'storage/frontend-assets/app.js')
//   .sass('resources/scss/frontend/app.scss', 'storage/frontend-assets/app.css');

// if (mix.inProduction()) {
//   mix.webpackConfig({
//     plugins: [
//       new purgeCss({
//         paths: glob.sync([
//           path.join(__dirname, 'resources/views/**/*.blade.php')
//         ]),
//         extractors: [
//           {
//             extractor: class {
//               static extract(content) {
//                 return content.match(/[A-z0-9-:\/]+/g)
//               }
//             },
//             extensions: ['html', 'js', 'php']
//           }
//         ]
//       })
//     ]
//   })
// }
// if (mix.inProduction) {
mix.autoload({ jquery: ['$', 'window.jQuery', 'jQuery'] })
  .js('resources/js/app.js', 'storage/frontend-assets/app.js')
  .sass('resources/scss/frontend/app.scss', 'storage/frontend-assets/app.css');
  // mix.sass('resources/scss/frontend/mailing.scss', 'storage/frontend-assets/mailing.css');
    // .browserSync({
    //   proxy: 'http://brandmarket/',
    //   port: 8080
    // });
// }
